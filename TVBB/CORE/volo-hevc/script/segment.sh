SEG_BIN=/opt/volo/bin/segmentor
POST_SEG_SCRIPT=/opt/volo/script/playlist_ver1.sh

SRC=$1
OUT_PATH=$2
INTERVAL=$3

if [ -d $OUT_PATH ]
then
	rm -f $OUT_PATH/*.*

else
	mkdir -p $OUT_PATH

fi

if [ -z "$INTERVAL" ]
then
	INTERVAL=10

fi

$SEG_BIN -i $SRC -vcodec copy -acodec copy -f segment -segment_list $OUT_PATH/index.m3u8 -segment_time $INTERVAL $OUT_PATH/%04d.ts

$POST_SEG_SCRIPT $OUT_PATH/index.m3u8 $OUT_PATH/index.legacy.m3u8
