#!/bin/bash
#${LABEL} ${HOST} ${TARGETPATH} ${TARGETM3U8} ${ACCESSID} ${ACCESSKEY}
LABEL=$1
HOST=$2
TARGETPATH=$3
TARGETM3U8=$4
ACCESSID=$5
ACCESSKEY=$6
ACL=public-read
VOLO_HOME=/opt/volo
source $VOLO_HOME/etc/system.cfg
M3U8_PATH="${web_server_path}/hls/${LABEL}"

cd ${M3U8_PATH}

while true
do
	echo "${VOLO_HOME}/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --acl=$ACL --put=index.m3u8 -- http://${HOST}/${TARGETPATH}/${TARGETM3U8}"
	${VOLO_HOME}/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --acl=$ACL --put=index.m3u8 -- http://${HOST}/${TARGETPATH}/${TARGETM3U8}

	sleep 5
done
