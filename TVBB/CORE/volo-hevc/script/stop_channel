#!/bin/bash

LABEL=`echo $* | sed 's/[ \t\r\n\v\f]/_/g'`

if [ -z $LABEL ]
then
	echo "Error: Please provide a channel name or use 'ALL'."
	exit

fi

if [  $LABEL == "ALL" ]
then
	CLOSEALL=Y
	LABEL=""

fi

clearAll=N
jobFound=N
ringbuffer=""

while [ $clearAll == "N" ]; do
	# Find out any volo encoding job
	jobStr=`ps -Alf | grep "bin/volo " | grep "${LABEL}" | grep -v "\-recording Y" | grep -v bmdcapture | grep -v Octoshape | grep -v "stop_channel" |  grep -v "grep " | grep -v "tail " | grep -v "vi " | grep -v "nts " | grep -v "live_monitor " | grep -v "while true" | head -1`

	if [ ! -z "$jobStr" ]
	then
		jobFound=Y

		# Get the ringbuffer folder if any
		ringbuffer=`echo ${jobStr} | sed 's/^.*ring\:\/\/\([^ \t\r\n\v\f]*\) .*$/\1/'`

		echo "Ringbuffer = [ $ringbuffer ]"

		if [ ${#ringbuffer} -gt 50 ]
		then
			ringbuffer=""

		fi

		jobs=( $jobStr )
		if [ ${#jobs[@]} -gt 0 ]
		then
			echo "Job found, closing channel..."

			# Stop the parent job which is a while loop script
			if [ ${jobs[4]} != "1" ]
			then
				kill -9 ${jobs[4]}
			
			fi

			kill -9 ${jobs[3]}
			sleep 1

			if [ -f /dev/shm/nofno/${LABEL}/ ]
			then
				rm -Rf /dev/shm/nofno/${LABEL}/*

			fi

			# Stop the other related jobs such as monitor script and ts upload script
			mClearAll=N
                        while [ $mClearAll == "N" ]; do
                                mJobStr=`ps -Alf | grep "/volo/script/" | grep "${LABEL}" | grep -v "\-recording Y" | grep -v phantom | grep -v bmdcapture | grep -v Octoshape | grep -v "stop_channel" |  grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`

                                if [ ! -z "$mJobStr" ]
                                then
                                        mJobs=( $mJobStr )
                                        if [ ${#mJobs[@]} -gt 0 ]
                                        then
                                                if [ ${mJobs[4]} != "1" ]
                                                then
                                                        kill -9 ${mJobs[4]}

                                                fi

                                                kill -9 ${mJobs[3]}
                                                sleep 1

                                        fi

                                else
                                        mClearAll=Y

                                fi

                        done

			if [ ! -z "$ringbuffer" ]
			then
				# Find out any volo jobs that is using the ringbuffer
				jobStr=`ps -Alf | grep "bin/volo " | grep ring://${ringbuffer} | grep -v "\-ringbuffer" | grep -v bmdcapture | grep -v "tail " | grep -v "vi " | grep -v "grep " | head -1`
				if [ ! -z "$jobStr" ]
				then
					echo "There are other jobs that depend on the ringbuffer."

				else
					echo "No other job depends on the ringbuffer, closing ringbuffer..."

					clearAll=N

					while [ $clearAll == "N" ]; do
						# Stop the ringbuffer job as no other job depends on the ringbuffer
						jobs=( `ps -Alf | grep volo | grep "${ringbuffer}" | grep -v "tail " | grep -v "vi " | grep -v "grep " | head -1` )

						if [ ${#jobs[@]} -gt 0 ]
						then
							kill -9 ${jobs[3]}
							sleep 1
							
							rm -Rf "${ringbuffer}"

						else
							clearAll=Y
							echo "RingBuffer is closed."

						fi
					done

				fi

			fi
		fi

        else
                clearAll=Y
		if [ $jobFound == "Y" ]
		then
			echo "Channel is closed."

		else
			echo "No job found for this channel."

		fi
        fi
done

mClearAll=N
while [ $mClearAll == "N" ]; do
	mJobStr=`ps -Alf | grep "/volo/script/" | grep "${LABEL}" | grep -v bmdcapture | grep -v Octoshape | grep -v "stop_channel" |  grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`

	if [ ! -z "$mJobStr" ]
	then
		mJobs=( $mJobStr )
		if [ ${#mJobs[@]} -gt 0 ]
		then
			if [ ${mJobs[4]} != "1" ]
			then
				kill -9 ${mJobs[4]}

			fi

			kill -9 ${mJobs[3]}
			sleep 1

		fi

	else
		mClearAll=Y

	fi

done

wClearAll=N
while [ $wClearAll == "N" ]; do
	wJobStr=`ps -Alf | grep "while true" | grep "${LABEL}" | grep -v bmdcapture | grep -v Octoshape | grep -v "stop_channel" |  grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`

	if [ ! -z "$wJobStr" ]
	then
		wJobs=( $wJobStr )
		if [ ${#wJobs[@]} -gt 0 ]
		then
			if [ ${wJobs[4]} != "1" ]
			then
				kill -9 ${wJobs[4]}

			fi

			kill -9 ${wJobs[3]}
			sleep 1

		fi

	else
		wClearAll=Y

	fi

done

# If no encoding jobs, stop all capture jobs and delete all ringbuffer
jobStr=`ps -Alf | grep "bin/volo \|phantom" | grep "LIVE\|phantom" | grep -v bmdcapture | grep -v Octoshape | grep -v "stop_channel" | grep -v "grep " | grep -v "tail " | grep -v "vi " | grep -v "nts " | grep -v "live_monitor " | head -1`
if [ -z "$jobStr" ]
then
        clearAll=N

        while [ $clearAll == "N" ]; do
                jobs=( `ps -Alf | grep bmdcapture | grep -v "tail " | grep -v "vi " | grep -v "grep " | head -1` )

                if [ ${#jobs[@]} -gt 0 ]
                then
                        kill -9 ${jobs[3]}
                        sleep 1

                        rm -Rf /dev/shm/in/*
                        rm -Rf /dev/shm/nofno/*

                else
                        clearAll=Y

                fi
        done
fi

if [ "$CLOSEALL" == "Y" ]
then
	rm -f /opt/volo/etc/liveJobList.cfg
	sqlite3 /var/www/html/config/VOLOGUI.ENCTX.DB "UPDATE LIVE_ENCODE_DETAIL SET encode_status = 'S' where encode_status != 'P'"

fi
