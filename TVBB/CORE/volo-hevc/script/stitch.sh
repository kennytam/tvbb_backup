#!/bin/bash

FILES=(${1//,/ })
OUT=`readlink -f "$2"`

echo ${#FILES[@]}
for i in "${FILES[@]}"; do echo $i; done

MERGE_BIN=/opt/volo/bin/merger 
${MERGE_BIN} -y -f concat -i <( for i in "${FILES[@]}"; do echo "file '$i'"; done ) -c copy ${OUT}
