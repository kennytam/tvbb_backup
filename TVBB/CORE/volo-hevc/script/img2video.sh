#!/bin/bash

IMG=$1
VIDEO=$2
DURATION=$3

/opt/volo/bin/converter -y -loop 1 -i $IMG -vcodec volo -t $DURATION -pix_fmt yuv420p $VIDEO
