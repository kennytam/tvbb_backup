#!/bin/bash

FILES=(${1//,/ })
OUT=`readlink -f "$2"`
LOG=/opt/volo/logs/$(cat /proc/sys/kernel/random/uuid).log

echo ${#FILES[@]}
for i in "${FILES[@]}"; do echo $i; done

MERGE_BIN=/opt/volo/bin/volo-1.4

conStr=""

for i in "${FILES[@]}"; do
	f=$(cat /proc/sys/kernel/random/uuid)
	#echo "${MERGE_BIN} -i "$i" -c copy -bsf:v h264_mp4toannexb -f mpegts /tmp/$f.ts"
	echo "Merging $i..."
	${MERGE_BIN} -i "$i" -c copy -bsf:v h264_mp4toannexb -f mpegts /tmp/$f.ts 2>> $LOG
	if [ "$conStr" == "" ]
	then
		conStr="concat:/tmp/$f.ts"
	else
		conStr="${conStr}|/tmp/$f.ts"
	fi
done

${MERGE_BIN} -y -i "$conStr" -c copy -absf aac_adtstoasc ${OUT} 2>> $LOG
