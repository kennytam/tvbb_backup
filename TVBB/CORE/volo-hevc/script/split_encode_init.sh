export INSTALL_PATH=/opt/volo

export CPATH=/dev/shm/volo

export LICENSE=${CPATH}/license

export IPATH=${CPATH}/IN
export JPATH=${CPATH}/job
export PPATH=${CPATH}/proc
export LPATH=${CPATH}/log
export EPATH=${CPATH}/event

export CBDIR=${CPATH}/callback

TMP_LIVE_JOB_LOADER=${INSTALL_PATH}/script/S_job_template
TMP_FILE_JOB_LOADER=${INSTALL_PATH}/script/F_job_template

source ${INSTALL_PATH}/script/init.sh

if [ ! -d ${JPATH} ]
then
	mkdir -p ${JPATH} ${PPATH} ${LPATH} ${EPATH} ${CBDIR}

fi
