#!/bin/bash
LOG_FILE=$1
DURATION=$2
P_NAME=$3
#PROGRESS_EV=$3
#ERROR_LOG=$4

cal_progress() {
currentInSeconds=$(time2seconds $1)
echo $(round $(multiply $(divide $currentInSeconds $2) 100))
}

time2seconds() {
OLD_IFS=$IFS
IFS=":"
times=( $1 )
IFS=$OLD_IFS
echo $(add $(add $(multiply ${times[0]} 3600) $(multiply ${times[1]} 60)) ${times[2]})
};

round()
{
echo "$1" | awk '{printf("%d\n",$1 + 0.5)}'
};

divide()
{
echo "$1" "$2" | awk '{printf("%f\n",$1/$2)}'
};

multiply()
{
echo "$1" "$2" | awk '{printf("%f\n",$1*$2)}'
};

add()
{
echo "$1" "$2" | awk '{printf("%f\n",$1+$2)}'
}

ERROR_LOG=${LPATH}/${P_NAME}.log

#tail -f "/dev/shm/volo/log/$1" | grep " time=" | sed 's/^.* time=\([0-9.:]*\) bitrate=.*$/\1/g' >> /tmp/progress.log
last_progress=0
last_progress_ev="last_progress_ev_dummy.ev"
retry=10000
error_idx=3

while true; do
	if ls ${EPATH}/${P_NAME}_T*_END_R*.ev 1> /dev/null 2>&1
	then
		exit 0

	fi

	TIMESTAMP=$(date "+%s")

	if [[ -f "${ERROR_LOG}" ]]
	then
		rowcount=`tail -n +$error_idx ${ERROR_LOG} | wc -l`

		if [ $rowcount -gt 0 ]
		then
			ERROR_EV="${EPATH}/${P_NAME}_T${TIMESTAMP}_ERROR.ev"
			tail -n +$error_idx ${ERROR_LOG} >>  ${ERROR_EV}
			error_idx=$((error_idx+rowcount))

		fi
		#mv ${ERROR_LOG} ${EPATH}/PROGRESS_EV_ERROR.ev
		#mv ${ERROR_LOG} ${ERROR_EV}

	fi

	current=`tail -1 "${LOG_FILE}" | grep " time=" | sed 's/^.* time=\([0-9.:]*\) bitrate=.*$/\1/g'`
	echo "Current = $current"
	progress=$(cal_progress $current $DURATION)

	re='^[0-9]+$'
	if ! [[ $progress =~ $re ]]
	then
		sleep 1
		continue

	fi

	echo "Current = $current, Total = $DURATION, Progress = $progress%" >> /tmp/progress.log

	if [ "$progress" -gt "$last_progress" ]
	then
		echo "Progress Updated: $last_progress% -> $progress%" >> /tmp/progress.log

		#eval "EVENT_PROGRESS_FILE=`echo $PROGRESS_EV | sed 's/<progress>/$progress/'`"
		PROGRESS_EV="${EPATH}/${P_NAME}_T${TIMESTAMP}_PROGRESS_P${progress}.ev"
		#echo "Progress File = $PROGRESS_EV"
		touch $PROGRESS_EV

		if [ -f "$last_progress_ev" ]
		then
			#eval "LAST_EVENT_PROGRESS_FILE=`echo $PROGRESS_EV | sed 's/<progress>/$last_progress/'`"
			#LAST_PROGRESS_EV="${EPATH}/${P_NAME}_T${TIMESTAMP}_PROGRESS_P${last_progress}.ev"
			rm -f $last_progress_ev

		fi

		if [ $progress -eq 100 ]
		then
			break

		fi

		last_progress=$progress
		last_progress_ev=$PROGRESS_EV

	fi

	sleep 1;
	retry=$((retry-1))

	if [ $retry -eq 0 ]
	then
		break

	fi
done
