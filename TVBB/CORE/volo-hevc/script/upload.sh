#!/bin/bash
#${LABEL} ${HOST} ${STREAMID} ${EVENTNAME} ${TARGETM3U8} ${BITRATE} ${TARGETTS} {$count}
LABEL=$1
HOST=$2
STREAMID=$3
EVENTNAME=$4
TARGETM3U8=$5
BITRATE=$6
TS=$7
TARGETTS=$8
ROLLOVER=$9
ISMULTI=${10}
UPLOADM3U8=${11}

echo "curl -T $7 http://${HOST}/${STREAMID}/${EVENTNAME}/${BITRATE}/${ROLLOVER}/${TARGETTS}"
#curl -T $7 http://${HOST}/${STREAMID}/${EVENTNAME}/${BITRATE}/${ROLLOVER}/${TARGETTS}

if [ "$UPLOADM3U8" = "Y" ]
then
	if [ ${ISMULTI} == "Y" ]
	then
		echo "curl -T _live.m3u8 http://${HOST}/${STREAMID}/${EVENTNAME}/${BITRATE}/index.m3u8"
		#curl -T _live.m3u8 http://${HOST}/${STREAMID}/${EVENTNAME}/${BITRATE}/index.m3u8

	else
		echo "curl -T _live.m3u8 http://${HOST}/${STREAMID}/${EVENTNAME}/${TARGETM3U8}"
		#curl -T _live.m3u8 http://${HOST}/${STREAMID}/${EVENTNAME}/${TARGETM3U8}

	fi

fi

rm -f $7
#mv -f $7 /home/Volo-QA/record/
