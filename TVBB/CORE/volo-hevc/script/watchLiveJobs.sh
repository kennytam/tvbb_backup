#!/bin/bash
CFG=/opt/volo/etc/liveJobList.cfg

OLD_IFS=$IFS

while true; do
	hasRestart=N

	cat $CFG | while read line ; do
		echo "Line = $line ***"
		IFS="|"

		j=( $line )
		IFS="$OLD_IFS"
		echo "Label = ${j[0]}" 
		echo "Command = ${j[1]}" 
		echo "Monitor = ${j[2]}" 
		label=${j[0]}
		cmd=${j[1]}
		mon=${j[2]}

		cJob=`ps -Alf | grep "\-label $label" | grep -v "grep "`
		echo "Current Job = $cJob"

		if [ -z "$cJob" ]
		then
			echo "Warning: Job is not running..." >> /tmp/restart.log
			echo "Warning: Job is not running..."

			su - apache -s /bin/bash -c "$cmd"

			mJob=`ps -Alf | grep "$label" | grep "live_monitor.php"`
			echo "Monitor Job = $mJob"

			if [ -z "$mJob" ]
			then
				echo "Warning: Monitor Job is not running..." >> /tmp/restart.log
				echo "Warning: Monitor Job is not running..."

				su - apache -s /bin/bash -c "$mon"

			fi

			hasRestart=Y

		fi

	done

	if [ $hasRestart == 'N' ]
	then
		sleep 0.01

	else
		sleep 15 

	fi

done
