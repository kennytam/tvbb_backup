#!/bin/bash

round()
{
echo "$1" | awk '{printf("%d\n",$1 + 0.5)}'
};

divide()
{
echo "$1" "$2" | awk '{printf("%f\n",$1/$2)}'
};

multiply()
{
echo "$1" "$2" | awk '{printf("%f\n",$1*$2)}'
};

if [ "$#" -lt 3 ]
then
	exit 1
fi

SCRIPT_PATH="`dirname \"$0\"`"
source $SCRIPT_PATH/split_encode_init.sh

in_gop=1
in_logo=""
offset_X=-1
offset_Y=-1

while [ $# -ne 0 ]
do
        if [[ $1 != -* ]]
        then
                echo "No option"
                break


        elif [ $1 == "-gop" ]
        then
		shift
		in_gop=$1
		shift

        elif [ $1 == "-logo" ]
        then
                shift
                in_logo="$1"
                shift

        elif [ $1 == "-logo_offsets" ]
        then
                shift
                OLD_IFS=$IFS
                IFS=,
                offsets=( $1 )
                offset_X=${offsets[0]}
                offset_Y=${offsets[1]}
                echo "X = $offset_X, Y = $offset_Y"
		IFS=$OLD_IFS
                shift

        else
                echo "Error"
                exit 1

        fi

done

IN=`readlink -f "$1"`
OUT=`readlink -f "$2"`
PROFILE=$3

echo "GOP = $in_gop, Logo = $in_logo, Offset = ($offset_X, $offset_Y), IN = $IN"

echo $PROFILE

PROFILES=(${PROFILE//,/ })
noOfProfiles="${#PROFILES[@]}"
echo "No. of Profiles = $noOfProfiles"
PROFILE_SHARED=${PROFILES[0]}

VOLO_BIN=/opt/volo/bin/volo-1.4
SPLIT_SCRIPT=$(dirname $0)/encode_1.4.sh
AUDIO_BIN=/opt/volo/bin/volo-1.4
MERGE_BIN=/opt/volo/bin/volo-1.4
PATH_CFG='/opt/volo/etc/'
PATH_LOG='/opt/volo/logs/'
PATH_TEMP='/opt/volo/tmp/'
OUT_FILE=`basename ${OUT}`
OUT_NAME=`echo ${OUT_FILE%.*}`
OUT_FULL_NAME=`echo ${OUT%.*}`
OUT_EXT=`echo ${OUT_FILE##*.}`
#echo "EXT = $OUT_EXT"
SPLIT_LOG="${PATH_LOG}${OUT_NAME}-P${PROFILE_SHARED}-split.log"
AUDIOS=("24" "24" "48" "48" "48" "64" "64" "96" "96" "192")
SAMPLING=44100

echo "Start - `date`" > ${SPLIT_LOG}

#duration=`/opt/volo/bin/vprobe -show_streams $1 2>/dev/null | grep duration | grep -v grep | head -1 | sed -e 's/^duration=//' -e 's/\..*$//'`
duration=`/opt/volo/bin/vprobe-1.3.2-sf -show_streams $1 2>/dev/null | grep duration | grep -v grep | head -1 | sed 's/^duration=//'`
fps=`mediainfo $1 | grep "Frame rate" | grep "fps" | sed 's/^.*:[ ]*\([0-9.]*\).*$/\1/'`
echo "FPS = $fps"
echo "Total Duration = $duration"
#rDuration=$(round $duration)
#echo "Rounded Duration = $rDuration"
#d3=$((rDuration/3))
splitNo=3
#d3=`awk 'BEGIN { rounded = sprintf("%.0f", $duration/3); print rounded }'`
d3=$(round $(divide $duration $splitNo))
echo "Split Duration = $d3"

if [ $in_gop -gt 0 ]
then
	gop_in_sec=$(round $(multiply $in_gop $fps))

fi

echo "Key min = $gop_in_sec"

for (( p=0; p<$noOfProfiles; p++ ))
do
	profile=${PROFILES[$p]}
	CONCAT_LIST="${PATH_TEMP}${OUT_NAME}-P${profile}-list.txt"
	echo -n "" > ${CONCAT_LIST}

done

SPLIT_JOB=""
for((s=0; s<$splitNo; s++))
do
	if [ $s -eq 0 ]
	then
		j=-

	else
		j=$((d3*s))

	fi

	if [ $((s+1)) -eq $splitNo ]
	then
		d=-

	else
		d=$d3

	fi

	echo "#$((s+1)) Job: Jump = $j, Duration = $d"

	for (( p=0; p<$noOfProfiles; p++ ))
	do
		profile=${PROFILES[$p]}

		CONCAT_LIST="${PATH_TEMP}${OUT_NAME}-P${profile}-list.txt"

		SPLIT_OUT=${PATH_TEMP}${OUT_NAME}-P${profile}-$s.ts
		echo "file '${SPLIT_OUT}'" >> ${CONCAT_LIST}
	done

	echo "${SPLIT_SCRIPT} $IN ${OUT_NAME} $PROFILE $s $j $d \"\" \"${gop_in_sec}\" \"${in_logo}\" \"${offset_X}\" \"${offset_Y}\" & "
	SPLIT_JOB="${SPLIT_JOB}${SPLIT_SCRIPT} $IN ${OUT_NAME} $PROFILE $s $j $d \"\" \"${gop_in_sec}\" \"${in_logo}\" \"${offset_X}\" \"${offset_Y}\" & "
done

SPLIT_JOB="${SPLIT_JOB}wait"

TIMESTAMP=$(date "+%s")
export P_NAME=F_$(echo ${IN}| sed -e "s/_//g" -e "s/.*\///g" -e "s/\.[a-zA-Z]*//g")P${PROFILE}
P_EV="${EPATH}/${P_NAME}_T${TIMESTAMP}"
START_EV="${P_EV}_START.ev"
echo "Name = ${P_NAME}"
echo "Start file = ${START_EV}"
touch $START_EV

rm -f ${EPATH}/${P_NAME}_T*_END_R*.ev

#echo "Split Jobs = ${SPLIT_JOB}"
echo "Start encoding jobs..."
eval "time ( ${SPLIT_JOB} )"
RET=$?
echo "RET = $RET"


MERGE_JOBS=""
for (( p=0; p<$noOfProfiles; p++ ))
do
	profile=${PROFILES[$p]}

	MERGE_NAME="${PATH_TEMP}${OUT_NAME}-P${profile}-merge.ts"
	AUDIO_NAME="${PATH_TEMP}${OUT_NAME}-P${profile}-audio.mp4"
	AUDIO=${AUDIOS[$((profile-1))]}

	#AUDIO_JOB="${AUDIO_BIN} -i $IN -y -vn -acodec libfdk_aac -ac 2 -b:a ${AUDIO}k -ar ${SAMPLING} ${AUDIO_NAME} 2>> ${SPLIT_LOG}"
	#echo "Audio Job = ${AUDIO_JOB}"

	#echo "${MERGE_BIN} -y -f concat -i ${CONCAT_LIST} -c copy ${MERGE_NAME}"
	#${MERGE_BIN} -y -f concat -i ${CONCAT_LIST} -c copy ${MERGE_NAME} 2>> ${SPLIT_LOG}
	#echo "${MERGE_BIN} -y -f concat -i ${CONCAT_LIST} -c copy -absf aac_adtstoasc ${OUT}"
	CONCAT_LIST="${PATH_TEMP}${OUT_NAME}-P${profile}-list.txt"
	PROFILE_OUT="${OUT_FULL_NAME}-P${profile}.${OUT_EXT}"
	MERGE_JOBS="${MERGE_JOBS}${MERGE_BIN} -y -f concat -i ${CONCAT_LIST} -c copy -absf aac_adtstoasc ${PROFILE_OUT} 2>> ${SPLIT_LOG} & "

	#export DAOLAB_CONFIG_PATH=${PATH_CFG}mux.cfg
	#echo "${VOLO_BIN} -i ${MERGE_NAME} -i ${AUDIO_NAME} -y -map 0:v -map 1:a -vcodec copy -acodec copy ${OUT}"
	#${VOLO_BIN} -i ${MERGE_NAME} -i ${AUDIO_NAME} -y -map 0:v -map 1:a -vcodec copy -acodec copy ${OUT} 2>> ${SPLIT_LOG}

done

MERGE_JOBS="${MERGE_JOBS}wait"
#echo "${MERGE_JOBS}"
eval "${MERGE_JOBS}"

echo "End - `date`" >> ${SPLIT_LOG}

rm -f {CONCAT_LIST}
rm -f ${PATH_TEMP}${OUT_NAME%.*}-P${profile}-*.ts

TIMESTAMP=$(date "+%s")
P_EV="${EPATH}/${P_NAME}_T${TIMESTAMP}"

if [ "$RET" -eq 0 ]
then
	END_EV="${P_EV}_END_R${RET}.ev"
elif [ "$RET" -eq 255 ]
then
	END_EV="${P_EV}_CANCEL_R${RET}.ev"
else
	END_EV="${P_EV}_FAIL_R${RET}.ev"
fi
echo "END file = ${END_EV}"

touch $END_EV

echo "...Done"
