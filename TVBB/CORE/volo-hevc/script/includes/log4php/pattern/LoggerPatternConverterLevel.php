<?php
/*   __________________________________________________
    |                                                  |
    |              Obfuscated by Daolab Team           |
    |           Website: http://www.dao-lab.com        |
    |__________________________________________________|
*/
 class LoggerPatternConverterLevel extends LoggerPatternConverter { public function convert(LoggerLoggingEvent $event) { return $event->getLevel()->toString(); } }
