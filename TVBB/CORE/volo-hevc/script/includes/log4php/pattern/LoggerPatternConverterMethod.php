<?php
/*   __________________________________________________
    |                                                  |
    |              Obfuscated by Daolab Team           |
    |           Website: http://www.dao-lab.com        |
    |__________________________________________________|
*/
 class LoggerPatternConverterMethod extends LoggerPatternConverter { public function convert(LoggerLoggingEvent $event) { return $event->getLocationInformation()->getMethodName(); } }
