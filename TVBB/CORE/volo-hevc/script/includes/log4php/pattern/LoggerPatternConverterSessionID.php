<?php
/*   __________________________________________________
    |                                                  |
    |              Obfuscated by Daolab Team           |
    |           Website: http://www.dao-lab.com        |
    |__________________________________________________|
*/
 class LoggerPatternConverterSessionID extends LoggerPatternConverter { public function convert(LoggerLoggingEvent $event) { return session_id(); } }
