<?php
/*   __________________________________________________
    |                                                  |
    |              Obfuscated by Daolab Team           |
    |           Website: http://www.dao-lab.com        |
    |__________________________________________________|
*/
 class LoggerPatternConverterLocation extends LoggerPatternConverter { public function convert(LoggerLoggingEvent $event) { return $event->getLocationInformation()->getClassName() . "\56" . $event->getLocationInformation()->getMethodName() . "\x28" . $event->getLocationInformation()->getFileName() . "\72" . $event->getLocationInformation()->getLineNumber() . "\51"; } }
