<?php
/*   __________________________________________________
    |                                                  |
    |              Obfuscated by Daolab Team           |
    |           Website: http://www.dao-lab.com        |
    |__________________________________________________|
*/
 interface LoggerConfigurator { public function configure(LoggerHierarchy $hierarchy, $input = null); }
