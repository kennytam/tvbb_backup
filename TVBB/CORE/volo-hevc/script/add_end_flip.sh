#!/bin/bash

IN=`readlink -f "$1"`
FLIP=`readlink -f "$2"`
OUT=`readlink -f "$3"`

MERGE_BIN=/opt/volo/bin/merger 
${MERGE_BIN} -y -f concat -i <( echo "file '$IN'"; echo "file '$FLIP'" ) -map 0:v -map 0:a -c copy -bsf:a aac_adtstoasc -y ${OUT}
