#!/bin/bash
SRC=$1
OUT=$2
END_FLIP_ORIG=/opt/volo/extras/EF_8mono.mp4
VOLO_BIN=/opt/volo/bin/volo-1.4.1

if [ "$SRC" == "" ] || [ "$OUT" == "" ]
then
	echo "Usage: make_end_flip_source.sh <movie> <end_flip_src>"
	exit 1

fi

$VOLO_BIN -i $END_FLIP_ORIG  -i "$SRC" -map 0:v -vcodec copy -map 1:a -shortest -acodec pcm_s16le -af volume=0 "$OUT"
