#!/bin/bash

round()
{
echo "$1" | awk '{printf("%d\n",$1 + 0.5)}'
};

divide()
{
echo "$1" "$2" | awk '{printf("%f\n",$1/$2)}'
};

# Parameters
FILE_INPUT=$1
OUT_NAME=$2
PROFILE=$3
SPLIT_IDX=$4
JUMP=$5
DURATION=$6
WORKPATH=$7
GOP_IN_SEC=$8
LOGO=$9
OFFSET_X=${10}
OFFSET_Y=${11}

PROFILES=(${PROFILE//,/ })
noOfProfiles="${#PROFILES[@]}"
PROFILE_SHARED=${PROFILES[0]}

JDIFF=3

JUMP1=""
JUMP2=""
if [ $JUMP != "-" ]
then
	JUMP1="-ss $((JUMP-JDIFF))"
	JUMP2="-ss $JDIFF"

fi

TIME=""
if [ $DURATION != "-" ]
then
	TIME="-t $DURATION"

fi

VOLO='/opt/volo/bin/volo'
CONVERTER='/opt/volo/bin/converter'
PATH_CFG='/opt/volo/etc/'
PATH_LOGO='/opt/volo/extras/logos/'
PATH_TEMP='/opt/volo/tmp/'
PATH_LOG='/opt/volo/logs/'
RESOLUTIONS=("320:180" "426:240" "480:270" "640:360" "854:480" "1280:720" "1280:720" "1280:720" "1280:720" "")

BASEDIR=$(dirname $0)

(
cd ${BASEDIR}

# Local Variables
PATH_BASE=`pwd`
FILE_NAME=`basename ${FILE_INPUT}`

# Load Config
#source etc/config

# Generate Temp file, dir
if [ "$WORKPATH" == "" ]
then
	PATH_WORK=$(mktemp -d ${PATH_TEMP}volo.XXXXXXXXX) || { echo "Failed to create temp dir"; exit 1; }
else
	PATH_WORK=$WORKPATH
fi
#echo "Work Path = $PATH_WORK"

FILE_TEMP="${PATH_WORK}/temp.ts"

cd ${PATH_WORK}

FIRST="yes"
#EXTRA="-t 10"
EXTRA=""

export DAOLAB_CONFIG_PATH=${PATH_CFG}tvb.cfg
LOG_P1=${PATH_LOG}${OUT_NAME}-P${PROFILE_SHARED}-${SPLIT_IDX}-Pass1.log
for (( p=0; p<$noOfProfiles; p++ ))
do
        profile=${PROFILES[$p]}
	pass=$((p+2))
	eval "LOG_P$pass=${PATH_LOG}${OUT_NAME}-P${profile}-${SPLIT_IDX}-Pass$pass.log
	eval "FILE_LOGO$pass="${PATH_LOGO}P${profile}.tga"

done

FILE_LOGO="${PATH_LOGO}P${PROFILE_SHARED}.tga"
RES=${RESOLUTIONS[$((PROFILE_SHARED-1))]}
OVERLAY="0:0"

if [[ "$LOGO" != "" ]]
then
	FILE_LOGO=$LOGO

	if [[ "$OFFSET_X" != "-1" ]]
	then
		OVERLAY="${OFFSET_X}:${OFFSET_Y}"

	fi
fi

if [ ! -z "$RES" ]; then
	ENCODE_P1_VF="-vf \"format=pix_fmts=yuv420p,scale=${RES}\""
	ENCODE_VF="-vf \"movie=${FILE_LOGO}[logo];[in]format=pix_fmts=yuv420p,yadif=0,scale=${RES}[scale]; [scale][logo]overlay=${OVERLAY}\""
else
	ENCODE_P1_VF="-vf \"format=pix_fmts=yuv420p\""
	ENCODE_VF="-vf \"movie=${FILE_LOGO}[logo];[in]format=pix_fmts=yuv420p,yadif=0[scale]; [scale][logo]overlay=${OVERLAY}\""
fi

if [ "$GOP_IN_SEC" != "" ]
then
	GOP="-overwritex264 min-keyint:${GOP_IN_SEC},keyint:${GOP_IN_SEC},no-scenecut:1 -overwritevolo sc_threshold:0"

fi

#START=$(date +%s)

ERROR_LOG=${LPATH}/${P_NAME}.log
export FFREPORT=file=${ERROR_LOG}:level=${LOG_LEVEL}

if [ $DURATION == "-" ]
then
	total_duration=`/opt/volo/bin/vprobe-1.3.2-sf -show_streams ${FILE_INPUT} 2>/dev/null | grep duration | grep -v grep | head -1 | sed 's/^duration=//'`
	split_count=$((SPLIT_IDX+1))
	DURATION=$(round $(divide $total_duration $split_count))

fi

/opt/volo/script/progress.sh $LOG_P1 $DURATION $P_NAME >> /tmp/progress.log 2>&1 & 

# Encode Pass #1 if needed
#echo "${CONVERTER} -threads 2 ${JUMP1} -i ${FILE_INPUT} ${ENCODE_P1_VF} ${JUMP2} ${TIME} -vcodec rawvideo -an -f matroska - 2>/dev/null | ${VOLO} -in - - - -profile ${PROFILE_SHARED} ${GOP} -pass 1 -out ${FILE_TEMP} mpegts - 2> $LOG_P1"
eval "${CONVERTER} -threads 2 ${JUMP1} -i ${FILE_INPUT} ${ENCODE_P1_VF} ${JUMP2} ${TIME} -vcodec rawvideo -an -f matroska - 2>/dev/null | ${VOLO} -in - - - -profile ${PROFILE_SHARED} ${GOP} -pass 1 -out ${FILE_TEMP} mpegts - 2> $LOG_P1"

# Encode Pass #2
SPLIT_JOBS="${CONVERTER} -threads 2 ${JUMP1} -i ${FILE_INPUT} ${ENCODE_VF} ${JUMP2} ${TIME} -vcodec rawvideo -acodec pcm_s16le -f matroska - 2>/dev/null"
for (( p=0; p<$noOfProfiles; p++ ))
do
	profile=${PROFILES[$p]}
	pass=$((p+2))

	FILE_OUTPUT="${PATH_TEMP}${OUT_NAME}-P${profile}-${SPLIT_IDX}.ts"

	#echo "${CONVERTER} -threads 2 ${JUMP1} -i ${FILE_INPUT} ${ENCODE_VF} ${JUMP2} ${TIME} -vcodec rawvideo -acodec pcm_s16le -f matroska - 2>/dev/null | ${VOLO} -in - - - -profile ${profile} -pass 2 -out ${FILE_OUTPUT} mpegts - -audio 0 2> $LOG_P2" 

	if [ $p -eq $((noOfProfiles-1)) ]
	then
		SPLIT_JOBS="${SPLIT_JOBS} | ${VOLO} -in - - - -profile ${profile} ${GOP} -pass 2 -out ${FILE_OUTPUT} mpegts - -audio 0 2> \$LOG_P$pass"
	else
		SPLIT_JOBS="${SPLIT_JOBS} | tee >( ${VOLO} -in - - - -profile ${profile} ${GOP} -pass 2 -out ${FILE_OUTPUT} mpegts - -audio 0 2> \$LOG_P$pass )"
	fi

done

for (( p=0; p<$noOfProfiles; p++ ))
do
	profile=${PROFILES[$p]}
	pass=$((p+2))

	if [ $DURATION == "-" ]
	then
		total_duration=`/opt/volo/bin/vprobe-1.3.2-sf -show_streams ${FILE_INPUT} 2>/dev/null | grep duration | grep -v grep | head -1 | sed 's/^duration=//'`
		split_count=$((split_idx+1))
		DURATION=$(round $(divide $total_duration $split_cound))

	fi

	eval "SPLIT_LOG=\$LOG_P$pass"
	/opt/volo/script/progress.sh $SPLIT_LOG $DURATION $P_NAME >> /tmp/progress.log 2>&1 &

done

#eval "${CONVERTER} -threads 2 ${JUMP1} -i ${FILE_INPUT} ${ENCODE_VF} ${JUMP2} ${TIME} -vcodec rawvideo -acodec pcm_s16le -f matroska - 2>/dev/null | ${VOLO} -in - - - -profile ${PROFILE} -pass 2 -out ${FILE_OUTPUT} mpegts - -audio 0 2> $LOG_P2" 
#echo "${SPLIT_JOBS}"
eval "${SPLIT_JOBS}"

#END=$(date +%s)
#DIFF=$(( $END - $START ))
#echo "$PROFILE - Time: $DIFF seconds" >> ${PATH_BASE}/time.log

ENCODE_P1=

# Remove Temp
rm -f ${FILE_TEMP}
rm -rf ${PATH_WORK}

) # cd basedir


