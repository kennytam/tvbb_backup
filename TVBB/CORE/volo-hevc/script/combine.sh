#!/bin/bash
INPUTS=()
OUTPUT=""
COUNT=0

function usage {
	echo "Usage: combine.sh -i hls1.zip -i hls2.zip [ hls3.zip ... ] -o output.zip"
	exit 1
}

while [ $1 ]; do
	if [ $1 == "-i" ]
	then
		shift

		INPUTS[$COUNT]=$1
		let "COUNT=$COUNT+1"	

	elif [ $1 == "-o" ]
	then
		shift

		OUTPUT=$1

	else
		usage

	fi

	shift

done

if [ $COUNT -lt 2 ] || [ "$OUTPUT" == "" ]
then
	usage

fi

tmpFolder=".`date +%s`"
mkdir $tmpFolder

echo -n "#EXTM3U" > $tmpFolder/index.m3u8

for (( i=0; i<${#INPUTS[@]}; i++ ))
do
	echo "Processing Input = ${INPUTS[$i]}..."
	unzip -q ${INPUTS[$i]} -d $tmpFolder

	mv -f $tmpFolder/*.hls/*.m3u8 $tmpFolder/_index.m3u8
	mv -f $tmpFolder/*.hls/* $tmpFolder/
	rmdir $tmpFolder/*.hls
	echo "" >> $tmpFolder/index.m3u8
	tail -n +2 $tmpFolder/_index.m3u8 >> $tmpFolder/index.m3u8
	rm -f $tmpFolder/_index.m3u8

done

echo "Combining folders to file $OUTPUT..."

cd $tmpFolder
zip -q -r tmp.zip *
cd ..
mv $tmpFolder/tmp.zip $OUTPUT

rm -Rf $tmpFolder
