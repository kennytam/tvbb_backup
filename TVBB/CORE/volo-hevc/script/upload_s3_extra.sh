#!/bin/bash
#${LABEL} ${HOST} ${TARGETPATH} ${TARGETM3U8} ${BITRATE} ${TS} ${TARGETTS} {$ROLLOVER} {$ISMULTI} {$ACCESSID} {$ACCESSKEY}
LABEL=$1
HOST=$2
TARGETPATH=$3
TARGETM3U8=$4
BITRATE=$5
TS=$6
TARGETTS=$7
ROLLOVER=$8
ISMULTI=$9
ACCESSID=${10}
ACCESSKEY=${11}
ACL=public-read
VOLO_HOME=/opt/volo

COPYSRC=`echo $HOST | sed 's/\.s3\.amazonaws\.com//g'`/${TARGETPATH}/${BITRATE}/${ROLLOVER}/${TARGETTS}
echo $COPYSRC

CHANNEL=`echo $TARGETPATH | cut -d'/' -f 1`
echo $CHANNEL

VODFOLDER=tvod

CURRENTTIME=`date +"%Y%m%d%H"`

if [ $BITRATE -ge 500 ]
then
	BITRATETYPE=highbitrate

else
	BITRATETYPE=lowbitrate

fi

#${VOLO_HOME}/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --acl=$ACL --copysrc=cdn2.bestv-intl.com/JSBC_International/live/500/1/live_6.ts -- http://cdn2.bestv-intl.com.s3.amazonaws.com/JSBC_International/tvod/highbitrate/2013091623/live_6.ts"

echo "${VOLO_HOME}/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --acl=$ACL --copysrc=$COPYSRC -- http://${HOST}/${CHANNEL}/${VODFOLDER}/${BITRATETYPE}/${CURRENTTIME}/${TARGETTS}"
${VOLO_HOME}/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --acl=$ACL --copysrc=$COPYSRC -- http://${HOST}/${CHANNEL}/${VODFOLDER}/${BITRATETYPE}/${CURRENTTIME}/${TARGETTS}
