#!/bin/bash

export INSTALL_PATH=/opt/volo

export CPATH=/dev/shm/volo

export LICENSE=${CPATH}/license

export IPATH=${CPATH}/IN
export JPATH=${CPATH}/job
export PPATH=${CPATH}/proc
export LPATH=${CPATH}/log
export EPATH=${CPATH}/event

export CBDIR=${CPATH}/callback

TMP_LIVE_JOB_LOADER=${INSTALL_PATH}/script/S_job_template
TMP_FILE_JOB_LOADER=${INSTALL_PATH}/script/F_job_template

function clean() {
    for f in $(ls ${PPATH}); do
      KILLID=$( echo $f | sed "s/^.*_//g" )
      pkill -TERM -P ${KILLID}
      while kill -0 ${KILLID} 2>/dev/null; do
        sleep 0.1
      done
    done

    #change this!!!
    rm -f ${LPATH}/*.log ${EPATH}/*.ev ${LICENSE}

    source ${INSTALL_PATH}/script/uninit.sh
    killall lighttpd

    echo "Watchman [$$] stopped watching. "
}

trap "clean; exit;" SIGHUP SIGINT SIGTERM 

#if ! killall -0 lighttpd; then lighttpd -f ${INSTALL_PATH}/script/Samples/lighttpd.conf; fi
source ${INSTALL_PATH}/script/init.sh

mkdir -p ${JPATH} ${PPATH} ${LPATH} ${EPATH} ${CBDIR}
touch ${LICENSE}

echo "Watchman [$$] started watching on ($JPATH). "

while [ -f ${LICENSE} ]; do
    #Stop job if job file is removed.
    for f in $(ls ${PPATH}); do
    #  if [[ $f == S_* ]]; then
    #    if [ ! -f ${JPATH}/$(echo ${f} | sed "s/_[0-9]*$//g") ]; then
    #      KILLID=$( echo $f | sed "s/^.*_//g" )
    #      pkill -TERM -P ${KILLID}
    #      while kill -0 ${KILLID} 2>/dev/null; do
    #        sleep 0.1
    #      done
    #    fi
    #  fi

      if [[ $f == F_* ]]; then
	file=$(echo ${f} | sed "s/_[0-9]*$//g")_running
        if [ ! -f ${JPATH}/${file} ]; then
          KILLID=$( echo $f | sed "s/^.*_//g" )
          pkill -TERM -P ${KILLID}
          while kill -0 ${KILLID} 2>/dev/null; do
            sleep 0.1
          done
        fi
      fi
    done

    #Start job if profile file is not there.
    for f in $(ls ${JPATH}); do
      #if [[ $f == S_* ]]; then
      #  if [ $(ls ${PPATH}/${f}_* 2>/dev/null | wc -l) -eq 0 ]; then
      #    bash ${TMP_LIVE_JOB_LOADER} ${JPATH}/${f} &
      #  fi
      #fi
      if [[ $f == F_* && $f != F_*_running ]]; then
        bash ${TMP_FILE_JOB_LOADER} ${JPATH}/${f} &
	sleep 1
	#mv ${JPATH}/${f} ${JPATH}/${f}_running
        #rm ${JPATH}/${f}
      fi

    done

    #Process callback
    for f in $(ls ${CBDIR}); do
        bash ${CBDIR}/${f} &
    done

    #remove old log files and event files.
    [ "$(du -sk ${LPATH} | cut -f 1 )" -gt 1024 ] && echo "" > $(echo $(ls -S ${LPATH}/*.log | head -1))
    #remove too much event files.
    [ $(ls ${EPATH} | wc -l) -gt 6 ] && rm $(echo $(ls -rt ${EPATH}/* | head -1))
    [ $(ls ${LPATH} | wc -l) -gt 6 ] && rm $(echo $(ls -rt ${LPATH}/* | head -1))
  
    sleep 0.5
done
