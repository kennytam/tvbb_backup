round()
{
echo "$1" | awk '{printf("%d\n",$1 + 0.5)}'
};

INPUT=$1
OUTPUT=$2

echo "#EXTM3U" > $OUTPUT

head -5 $INPUT | tail -3 >> $OUTPUT

odd=0
tail -n +6 $INPUT | head -n -1 | while read line ; do

	if [ $odd -eq 0 ]
	then
		t=`echo $line | sed 's/^.*INF\:\([^\,]*\)\,$/\1/g'`
		echo "Time = $t"
		r=$(round $t)
		echo "Rounded Time = $r"

		echo "#EXTINF:$r," >> $OUTPUT

	else

		echo $line >> $OUTPUT

	fi

	let "odd=1-odd"
done

echo "#EXT-X-ENDLIST" >> $OUTPUT
