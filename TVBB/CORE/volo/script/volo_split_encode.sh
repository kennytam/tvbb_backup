#!/bin/bash

round()
{
echo "$1" | awk '{printf("%d\n",$1 + 0.5)}'
};

divide()
{
echo "$1" "$2" | awk '{printf("%f\n",$1/$2)}'
};

pushd `dirname $0` > /dev/null
SCRIPT_PATH=`pwd`
popd > /dev/null
source $SCRIPT_PATH/volo_split_init.sh

# Parameters
FILE_INPUT=$1
OUT_NAME=$2
PROFILE=$3
SPLIT_IDX=$4
JUMP=$5
DURATION=$6
WORKPATH=$7
GOP_IN_SEC=$8
LOGO=$9
OFFSET_X=${10}
OFFSET_Y=${11}

START_STR=""
if [ $JUMP != "-" ]
then
	START_STR="-start $JUMP"

fi

DURATION_STR=""
if [ $DURATION != "-" ]
then
	DURATION_STR="-duration $DURATION"

fi

#VOLO='/opt/volo/bin/volo-1.4'
VOLO=${SCRIPT_PATH}/volo
#CONVERTER='/opt/volo/bin/converter'
CONVERTER='/opt/volo/bin/volo-1.4'
PATH_CFG='/opt/volo/etc/'
PATH_LOGO='/opt/volo/extras/logos/'
#PATH_TEMP='/opt/volo/tmp/'
PATH_TEMP='/media/volo_spool/tmp/'
PATH_LOG='/opt/volo/logs/'
SIZES=(180 240 270 360 480 720 720 720 720 1080)
RESOLUTIONS=("320:180" "426:240" "480:270" "640:360" "854:480" "1280:720" "1280:720" "1280:720" "1280:720" "")
VBITRATES=(76 176 352 452 752 936 1136 1404 2404 3808)
VBUFSIZES=(46 106 212 272 452 562 682 843 1443 2285)
ABITRATES=(24 24 48 48 48 64 64 96 96 192)
LEVELS=(3.0 3.0 3.0 3.1 3.1 4.1 4.1 4.1 4.1 4.1)
#VOLO_PROFILES=(1080p_Q1_baseline 720p_Q1_baseline 720p_Q1_baseline 720p_Q1_main 720p_Q1_main 576p_Q1_high 576p_Q1_high 360p_Q1_high 360p_Q1_high 360p_Q1_high)
VOLO_PROFILES=(360p_Q1_baseline 360p_Q1_baseline 360p_Q1_baseline 576p_Q1_main 576p_Q1_main 720p_Q1_high 720p_Q1_high 720p_Q1_high 720p_Q1_high 1080p_Q1_high)
LOG_LEVEL_FATAL=8
LOG_LEVEL_ERROR=16
LOG_LEVEL_WARNING=24
LOG_LEVEL_DEBUG=48
LOG_LEVEL=$LOG_LEVEL_FATAL

BASEDIR=$(dirname $0)

(
cd ${BASEDIR}

# Local Variables
PATH_BASE=`pwd`
FILE_NAME=`basename ${FILE_INPUT}`

# Load Config
#source etc/config

# Generate Temp file, dir
if [ "$WORKPATH" == "" ]
then
	PATH_WORK=$(mktemp -d ${PATH_TEMP}volo.XXXXXXXXX) || { echo "Failed to create temp dir"; exit 1; }
else
	if [ ! -d $WORKPATH ]
	then
		mkdir -p $WORKPATH

	fi

	PATH_WORK=$WORKPATH
fi
#echo "Work Path = $PATH_WORK"

#FILE_TEMP="${PATH_WORK}/temp.ts"
FILE_TEMP="/dev/null"

cd ${PATH_WORK}

FIRST="yes"
#EXTRA="-t 10"
EXTRA=""

export DAOLAB_CONFIG_PATH=${PATH_CFG}tvb-d.cfg
export VOLO_MAGIC_NUMBER=ichi2san4
LOG=${PATH_LOG}${OUT_NAME}-${PROFILE}-${SPLIT_IDX}.log

FILE_LOGO="${PATH_LOGO}P${PROFILE_SHARED}.tga"
PIDX=$((PROFILE_SHARED-1))
OVERLAY="0:0"

if [ $GOP_IN_SEC != "" ]
then
	#GOP="-overwritex264 min-keyint:${GOP_IN_SEC},keyint:${GOP_IN_SEC},no-scenecut:1"
	GOP=":min-keyint=${GOP_IN_SEC}:keyint=${GOP_IN_SEC}:no-scenecut=1"

fi

#START=$(date +%s)

P_NAME=F_$(echo ${IN}| sed -e "s/_//g" -e "s/.*\///g" -e "s/\.[a-zA-Z]*//g")P${PROFILE}
ERROR_LOG=${LPATH}/${P_NAME}.log
rm -f $ERROR_LOG
export FFREPORT=file=${ERROR_LOG}:level=${LOG_LEVEL}

if [ $DURATION == "-" ]
then
	total_duration=`/opt/volo/bin/vprobe-1.3.2-sf -show_streams ${FILE_INPUT} 2>/dev/null | grep duration | grep -v grep | head -1 | sed 's/^duration=//'`
	split_count=$((SPLIT_IDX+1))
	DURATION=$(round $(divide $total_duration $split_count))

fi

SIZE=${SIZES[$pidx]}

FILE_OUTPUT="${PATH_TEMP}${OUT_NAME}-${PROFILE}-${SPLIT_IDX}.ts"
SPLIT_JOBS="${VOLO} -f ${FILE_INPUT} -o ${FILE_OUTPUT} -do ${PROFILE} ${START_STR} ${DURATION_STR} -container ts > $LOG"

echo "${SPLIT_JOBS}"
eval "${SPLIT_JOBS}"

#END=$(date +%s)
#DIFF=$(( $END - $START ))
#echo "$PROFILE - Time: $DIFF seconds" >> ${PATH_BASE}/time.log

ENCODE_P1=

# Remove Temp
#rm -f ${FILE_TEMP}
#rm -rf ${PATH_WORK}

) # cd basedir


