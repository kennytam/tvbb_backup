#!/bin/bash

/sbin/service volowatchd stop

crontab -l | grep -v "startPollScripts.sh" | crontab
crontab -l | grep -v "housekeeping.sh" | crontab

liveThumbJob=`ps -Alf | grep "GUI.liveThumbnails" | grep -v "grep " | grep -v "vi " | head -1`
if [ ! -z "$liveThumbJob" ]
then
	job=( $liveThumbJob )
	if [ ${#job[@]} -gt 0 ]
	then
		kill -9 ${job[3]}

	fi

fi

pollVodJob=`ps -Alf | grep "GUI.pollVodJobs" | grep -v "grep " | grep -v "vi " | head -1`
if [ ! -z "$pollVodJob" ]
then
	job=( $pollVodJob )
	if [ ${#job[@]} -gt 0 ]
	then
		kill -9 ${job[3]}

	fi


fi

pollLiveJob=`ps -Alf | grep "GUI.pollLiveJobs" | grep -v "grep " | grep -v "vi " | head -1`
if [ ! -z "$pollLiveJob" ]
then
	job=( $pollLiveJob )
	if [ ${#job[@]} -gt 0 ]
	then
		kill -9 ${job[3]}

	fi


fi

pollSystem=`ps -Alf | grep "GUI.pollSystem" | grep -v "grep " | grep -v "vi " | head -1`
if [ ! -z "$pollSystem" ]
then
	job=( $pollSystem )
	if [ ${#job[@]} -gt 0 ]
	then
		kill -9 ${job[3]}

	fi


fi

pollMountJob=`ps -Alf | grep "GUI.pollMount" | grep -v "grep " | grep -v "vi " | head -1`
if [ ! -z "$pollMountJob" ]
then
	job=( $pollMountJob )
	if [ ${#job[@]} -gt 0 ]
	then
		kill -9 ${job[3]}

	fi


fi

pollNasCopy=`ps -Alf | grep "GUI.pollNasCopy" | grep -v "grep " | grep -v "vi " | head -1`
if [ ! -z "$pollNasCopy" ]
then
	job=( $pollNasCopy )
	if [ ${#job[@]} -gt 0 ]
	then
		kill -9 ${job[3]}

	fi


fi

cpuUsage=`ps -Alf | grep "cpuUsage.sh" | grep -v "grep " | grep -v "vi " | head -1`
if [ ! -z "$cpuUsage" ]
then
	job=( $cpuUsage )
	if [ ${#job[@]} -gt 0 ]
	then
		kill -9 ${job[3]}

	fi


fi

cpuOverallUsage=`ps -Alf | grep "cpuOverallUsage.sh" | grep -v "grep " | grep -v "vi " | head -1`
if [ ! -z "$cpuOverallUsage" ]
then
	job=( $cpuOverallUsage )
	if [ ${#job[@]} -gt 0 ]
	then
		kill -9 ${job[3]}

	fi


fi
