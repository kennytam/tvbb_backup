#!/bin/bash
#export DAOLAB_CONFIG_PATH=/opt/volo/etc/daolab.cfg
export DAOLAB_CONFIG_PATH=/opt/volo/etc/debug.cfg
#export DAOLAB_CONFIG_PATH=/opt/volo/etc/test.cfg
#export DAOLAB_CONFIG_PATH=/opt/volo/etc/unlimited.cfg
srcFrame=$1
echo $srcFrame
echo $srcFrame >> /tmp/cb.log

fileName=$(basename $srcFrame)
tsFile=${fileName%.*}.ts
label=`echo $srcFrame | sed 's/^.*\/\([^\s]*\)\/.*$/\1/g'`
cfgFile=/opt/volo/tmp/${label}_multiBitrates.cfg
#cfgFile=/opt/volo/tmp/2pass_multiBitrates.cfg
tsListFile=/opt/volo/tmp/${label}.list
seqFile=/opt/volo/tmp/${label}.seq
tsNum=3
duration=10

IFS=$'\n'
#cmds=( `cat /tmp/multi.cfg` )

cmds=( `cat ${cfgFile}` )

if [ -f $seqFile ]
then
	seq=`cat $seqFile`

else
	seq=1
	echo -n 1 > $seqFile
fi

if [ -f $tsListFile ]
then
	tsList=( `cat $tsListFile` )	
	if [ ${#tsList[@]} -ge $tsNum ]
	then
		tsList=("${tsList[@]:1}")

	fi
fi

tsList+=($tsFile)

let totalDuration=${#tsList[@]}*$duration

m3u8Str="#EXTM3U\n\
#EXT-X-TARGETDURATION:${duration}\n\
#EXT-X-MEDIA-SEQUENCE:$seq\n\
#EXT-X-ALLOW-CACHE:NO\n"
echo -n "" > $tsListFile
for (( i=0; i<${#tsList[@]}; i++ ))
do
	echo ${tsList[$i]} >> $tsListFile
	m3u8Str="$m3u8Str#EXTINF:${duration}\n${tsList[$i]}\n"

done

tf=/opt/volo/tmp/$label_$tsFile
mkdir $tf
cd $tf

for (( c=0; c<${#cmds[@]}; c++ ))
do
	eval "echo ${cmds[$c]} >> /opt/volo/logs/${label}_${c}_c.log"
	eval "${cmds[$c]} 2> /opt/volo/logs/${label}_${c}_e.log"

	outputFile=`echo ${cmds[$c]} | sed 's/^.*-out \([^[:space:]]*\).*$/\1/g'`
	m3u8File=${outputFile%/*}/index.m3u8
	echo -en $m3u8Str > $m3u8File
done

cd -
rm -Rf $tf

rm -f $srcFrame

seq=$(($seq + 1))
echo -n $seq > $seqFile
