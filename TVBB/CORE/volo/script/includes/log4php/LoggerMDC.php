<?php
/*   __________________________________________________
    |                                                  |
    |              Obfuscated by Daolab Team           |
    |           Website: http://www.dao-lab.com        |
    |__________________________________________________|
*/
 class LoggerMDC { private static $map = array(); public static function put($key, $value) { self::$map[$key] = $value; } public static function get($key) { return isset(self::$map[$key]) ? self::$map[$key] : ''; } public static function getMap() { return self::$map; } public static function remove($key) { unset(self::$map[$key]); } public static function clear() { self::$map = array(); } }
