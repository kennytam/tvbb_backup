<?php
/*   __________________________________________________
    |                                                  |
    |              Obfuscated by Daolab Team           |
    |           Website: http://www.dao-lab.com        |
    |__________________________________________________|
*/
 class LoggerPatternConverterRelative extends LoggerPatternConverter { public function convert(LoggerLoggingEvent $event) { $ts = $event->getRelativeTime(); return number_format($ts, 4); } }
