<?php
/*   __________________________________________________
    |                                                  |
    |              Obfuscated by Daolab Team           |
    |           Website: http://www.dao-lab.com        |
    |__________________________________________________|
*/
 class LoggerPatternConverterLiteral extends LoggerPatternConverter { private $literalValue; public function __construct($literalValue) { $this->literalValue = $literalValue; } public function convert(LoggerLoggingEvent $event) { return $this->literalValue; } }
