<?php
/*   __________________________________________________
    |                                                  |
    |              Obfuscated by Daolab Team           |
    |           Website: http://www.dao-lab.com        |
    |__________________________________________________|
*/
 class LoggerPatternConverterLocation extends LoggerPatternConverter { public function convert(LoggerLoggingEvent $event) { return $event->getLocationInformation()->getClassName() . "\x2e" . $event->getLocationInformation()->getMethodName() . "\x28" . $event->getLocationInformation()->getFileName() . "\x3a" . $event->getLocationInformation()->getLineNumber() . "\51"; } }
