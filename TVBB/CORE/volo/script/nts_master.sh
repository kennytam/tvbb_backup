#!/bin/bash
#${LABEL} ${HOST} ${STREAMID} ${EVENTNAME} ${TARGETM3U8} 
LABEL=$1
HOST=$2
STREAMID=$3
EVENTNAME=$4
TARGETM3U8=$5
VOLO_HOME=/opt/volo
source $VOLO_HOME/etc/system.cfg
M3U8_PATH="${web_server_path}/hls/${LABEL}"

cd ${M3U8_PATH}

while true
do
	echo "curl -T index.m3u8 http://${HOST}/${STREAMID}/${EVENTNAME}/${TARGETM3U8}"
	curl -T index.m3u8 http://${HOST}/${STREAMID}/${EVENTNAME}/${TARGETM3U8}

	sleep 5
done
