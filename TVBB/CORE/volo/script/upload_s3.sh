#!/bin/bash
#${LABEL} ${HOST} ${TARGETPATH} ${TARGETM3U8} ${BITRATE} ${TS} ${TARGETTS} {$ROLLOVER} {$ISMULTI} {$ACCESSID} {$ACCESSKEY}
LABEL=$1
HOST=$2
TARGETPATH=$3
TARGETM3U8=$4
BITRATE=$5
TS=$6
TARGETTS=$7
ROLLOVER=$8
ISMULTI=$9
ACCESSID=${10}
ACCESSKEY=${11}
#ACCESSID=AKIAIQ5SMIPCNR4NF22A
#ACCESSKEY=pVjt/O3QJd5ZjPerq2XeD30gkMMDcit66RNTEeId
ACL=public-read
VOLO_HOME=/opt/volo

# e.g. /opt/volo/script/s3curl.pl --id=AKIAIQ5SMIPCNR4NF22A --key=pVjt/O3QJd5ZjPerq2XeD30gkMMDcit66RNTEeId --put=/var/www/html/MEDIA/IN/thumbnail/live/default.jpg -- http://cdn2.bestv-intl.com.s3.amazonaws.com/Hunan_TV_World/live/300/3.jpg

echo "${VOLO_HOME}/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --acl=$ACL --put=$TS -- http://${HOST}/${TARGETPATH}/${BITRATE}/${ROLLOVER}/${TARGETTS}"
${VOLO_HOME}/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --acl=$ACL --put=$TS -- http://${HOST}/${TARGETPATH}/${BITRATE}/${ROLLOVER}/${TARGETTS}

if [ ${ISMULTI} == "Y" ]
then
	echo "${VOLO_HOME}/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --acl=$ACL --put=_live.m3u8 -- http://${HOST}/${TARGETPATH}/${BITRATE}/index.m3u8"
	${VOLO_HOME}/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --acl=$ACL --put=_live.m3u8 -- http://${HOST}/${TARGETPATH}/${BITRATE}/index.m3u8

else
	echo "${VOLO_HOME}/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --acl=$ACL --put=_live.m3u8 -- http://${HOST}/${TARGETPATH}/${TARGETM3U8}"
	${VOLO_HOME}/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --acl=$ACL --put=_live.m3u8 -- http://${HOST}/${TARGETPATH}/${TARGETM3U8}

fi

rm -f $TS

#bash upload_s3_extra.sh ${LABEL} ${HOST} ${TARGETPATH} ${TARGETM3U8} ${BITRATE} ${TS} ${TARGETTS} {$ROLLOVER} {$ISMULTI} {$ACCESSID} {$ACCESSKEY}
