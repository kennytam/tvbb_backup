#!/bin/bash
#${LABEL} ${HOST} ${TARGETPATH} ${TARGETM3U8} ${BITRATE} ${TS} ${TARGETTS} {$ROLLOVER} {$ISMULTI} {$ACCESSID} {$ACCESSKEY}
LISTFILE=$1
ACCESSID=$2
ACCESSKEY=$3
ACL=public-read
VOLO_HOME=/opt/volo

for TARGET_FILE in `cat $LISTFILE`
do
	echo "/opt/volo/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --delete -- ${TARGET_FILE}"
	/opt/volo/script/s3curl.pl --id=$ACCESSID --key=$ACCESSKEY --delete -- ${TARGET_FILE}

done
