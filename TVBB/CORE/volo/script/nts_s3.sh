#!/bin/bash
HLSPATH=$1
HOST=$2
TARGETPATH=$3
TARGETM3U8=$4
BITRATE=$5
ISMULTI=$6
ACCESSID=$7
ACCESSKEY=$8
LABEL=`echo $HLSPATH | sed 's/\\(.*\\)\\//\\1_/'`
echo $LABEL > /tmp/ntsX.log
VOLO_HOME=/opt/volo
source $VOLO_HOME/etc/system.cfg
TS_PATH="${web_server_path}/hls/${HLSPATH}"
TS_INDEX="index.m3u8"
LOG_FILE=${VOLO_HOME}/logs/${LABEL}.log
COUNT_FILE=${VOLO_HOME}/tmp/count_${LABEL}.txt
ROLLOVERINTERVAL=10000

if [ ! -e ${COUNT_FILE} ]
then
	echo 0 > ${COUNT_FILE}

else
	count=`cat ${COUNT_FILE}`
	count=$(($count + 1))
	echo $count > ${COUNT_FILE}

fi

cd ${TS_PATH}
lastTS=""

countdown_rollover=$ROLLOVERINTERVAL
tsList=()
tsID=1

while true; do
  count=`cat ${COUNT_FILE}`
  newTS=`tail -1 ${TS_INDEX}` 

  if [[ ! $newTS == $lastTS ]]
  then
	if [ $countdown_rollover -eq 0 ]
	then
		count=$(($count + 1))
		echo $count > ${COUNT_FILE}
		countdown_rollover=$ROLLOVERINTERVAL
		tsID=1

	fi

	if [ ${#tsList[@]} -ge 3 ]
	then
		tsList=("${tsList[@]:1}")

	fi

	TARGETTS=live_${tsID}.ts
	
	if [ ${ISMULTI} == "Y" ]
	then
		tsList+=(${count}/${TARGETTS})
	
	else
		tsList+=(${BITRATE}/${count}/${TARGETTS})

	fi

	#eval "cat ${TS_PATH}/${TS_INDEX} | sed 's/\([0-9]*\).ts/${BITRATE}\/${count}\/live_&/' > ${TS_PATH}/_live.m3u8"

	m3u8Str="#EXTM3U\n\
	#EXT-X-TARGETDURATION:15\n\
	#EXT-X-MEDIA-SEQUENCE:$seq\n\
	#EXT-X-ALLOW-CACHE:NO\n"
	intervalStr=`tail -2 ${TS_INDEX} | head -1`
	m3u8Str=""
	for (( i=0; i<${#tsList[@]}; i++ ))
	do
		m3u8Str="$m3u8Str${intervalStr}\n${tsList[$i]}\n"

	done

	head -4 ${TS_INDEX} > ${TS_PATH}/_live.m3u8
	echo -en $m3u8Str >> ${TS_PATH}/_live.m3u8

	echo "New m3u8 - ${LABEL}"
	echo $newTS

	if [[ -f $newTS ]]
	then
		${VOLO_HOME}/script/upload_s3.sh ${LABEL} ${HOST} ${TARGETPATH} ${TARGETM3U8} ${BITRATE} ${newTS} ${TARGETTS} ${count} ${ISMULTI} ${ACCESSID} ${ACCESSKEY} &

	fi

	tsID=$((tsID + 1))

	countdown_rollover=$(($countdown_rollover - 1))

  fi

  lastTS=$newTS
  sleep 5
done

cd -
