#!/bin/bash

VolumeName="VoloDrive"
SizeInMB=256
NumSectors=$((15*1024*SizeInMB*2))
#export DeviceName=$(hdid -nomount ram://${NumSectors})
#diskutil eraseVolume HFS+ RAMDisk ${DeviceName}

export RAM_PATH=/dev/shm/volo

if [ ! -d $RAM_PATH ]
then
	mkdir -p /dev/shm/volo
	chmod -R 777 ${RAM_PATH}

fi

#diskutil disableJournal ${RAM_PATH}
