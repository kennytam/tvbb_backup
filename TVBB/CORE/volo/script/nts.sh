#!/bin/bash
VOLO_HOME=/opt/volo
source $VOLO_HOME/etc/system.cfg

HLSPATH=$1
HOST=$2
STREAMID=$3
EVENTNAME=$4
TARGETM3U8=$5
BITRATE=$6
ISMULTI=$7

LABEL=`echo $HLSPATH | sed 's/\\(.*\\)\\//\\1_/'`
TS_PATH="${web_server_path}/hls/${HLSPATH}"
TS_INDEX="index.m3u8"
LOG_FILE=${VOLO_HOME}/logs/${LABEL}.log
COUNT_FILE=${VOLO_HOME}/tmp/count_${LABEL}.txt
ROLLOVERINTERVAL=10000

if [ ! -e ${COUNT_FILE} ]
then
	echo 0 > ${COUNT_FILE}

else
	count=`cat ${COUNT_FILE}`
	count=$(($count + 1))
	echo $count > ${COUNT_FILE}

fi

cd ${TS_PATH}
lastTS=""

countdown_rollover=$ROLLOVERINTERVAL
tsList=()
tsID=1
seqID=1
intervalStr=""
m3u8header="#EXTM3U\n#EXT-X-TARGETDURATION:10\n#EXT-X-MEDIA-SEQUENCE:SEQID\n#EXT-X-ALLOW-CACHE:NO"
LAST_TS_INDEX_CONTENT=""

echo "" > /tmp/newts.log

while true; do
  count=`cat ${COUNT_FILE}`
  TS_INDEX_CONTENT=`cat ${TS_INDEX}`
  newTSList=( `diff --unchanged-line-format= --old-line-format= --new-line-format='%L' <(echo -e "$LAST_TS_INDEX_CONTENT") <(echo -e "$TS_INDEX_CONTENT") | grep '\.ts' | tail -3` )

  echo "****" >> /tmp/newts.log
  noOfNewTS=${#newTSList[@]}

  if [ $noOfNewTS -gt 0 ]
  then
	if [ $countdown_rollover -eq 0 ]
	then
		count=$(($count + 1))
		echo $count > ${COUNT_FILE}
		countdown_rollover=$ROLLOVERINTERVAL
		tsID=1

	fi

	if [ ${#tsList[@]} -ge 3 ]
	then
		tsList=("${tsList[@]:$noOfNewTS}")

	fi
	
	if [ ${ISMULTI} == "Y" ]
	then
		for (( i=0; i<$noOfNewTS; i++ ))
		do
			echo "NEW TS: ${newTSList[$i]}" >> /tmp/newts.log

			TARGETTS=live_$((tsID+i)).ts
			tsList+=(${count}/${TARGETTS})

		done
	
	else
		for (( i=0; i<$noOfNewTS; i++ ))
		do
			echo "NEW TS: ${newTSList[$i]}" >> /tmp/newts.log

			TARGETTS=live_$((tsID+i)).ts
			tsList+=(${BITRATE}/${count}/${TARGETTS})

		done

	fi

	if [ -z "$intervalStr" ]
	then
		intervalStr=`echo -e "${TS_INDEX_CONTENT}" | grep "#EXTINF:" | head -1`
		
	fi

	m3u8Str=""
	for (( i=0; i<${#tsList[@]}; i++ ))
	do
		m3u8Str="${m3u8Str}${intervalStr}\n${tsList[$i]}\n"

	done

	#echo -e "${TS_INDEX_CONTENT}" | head -4 > ${TS_PATH}/_live.m3u8
	#echo -en $m3u8Str >> ${TS_PATH}/_live.m3u8
	cat <(echo -e $m3u8header | sed "s/SEQID/${seqID}/") <(echo -en $m3u8Str) > ${TS_PATH}/_live.m3u8

	uploadM3U8=N

	for (( i=0; i<$noOfNewTS; i++ ))
	do
		if [ $i -eq $((noOfNewTS-1)) ]
		then
			uploadM3U8=Y

		fi

		newTS=${newTSList[$i]}
		TARGETTS=live_$((tsID+i)).ts

		if [[ -f $newTS ]]
		then
			${VOLO_HOME}/script/upload.sh ${LABEL} ${HOST} ${STREAMID} ${EVENTNAME} ${TARGETM3U8} ${BITRATE} ${newTS} ${TARGETTS} ${count} ${ISMULTI} ${uploadM3U8} &

		fi

	done

	tsID=$((tsID + noOfNewTS))
	seqID=$((seqID + 1))

	LAST_TS_INDEX_CONTENT=$TS_INDEX_CONTENT

  fi

  sleep 5
done

cd -
