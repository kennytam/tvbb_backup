#!/bin/bash
#export DAOLAB_CONFIG_PATH=/opt/volo/etc/daolab.cfg
export DAOLAB_CONFIG_PATH=/opt/volo/etc/debug.cfg
#export DAOLAB_CONFIG_PATH=/opt/volo/etc/test.cfg
#export DAOLAB_CONFIG_PATH=/opt/volo/etc/unlimited.cfg
srcFrame=$1
echo $srcFrame
echo $srcFrame >> /tmp/cb.log

fileName=$(basename $srcFrame)
tsFile=${fileName%.*}.ts
label=`echo $srcFrame | sed 's/^.*\/\([^\s]*\)\/.*$/\1/g'`
cfgFile=/opt/volo/tmp/${label}_multiBitrates.cfg
#cfgFile=/opt/volo/tmp/2pass_multiBitrates.cfg
seqFile=/opt/volo/tmp/${label}.seq
duration=10

IFS=$'\n'
#cmds=( `cat /tmp/multi.cfg` )

cmds=( `cat ${cfgFile}` )

if [ -f $seqFile ]
then
	seq=`cat $seqFile`

else
	seq=1
	echo -n 1 > $seqFile
fi

m3u8Str="#EXTM3U\n\
#EXT-X-TARGETDURATION:${duration}\n\
#EXT-X-MEDIA-SEQUENCE:$seq\n\
#EXT-X-ALLOW-CACHE:NO\n"

tf=/opt/volo/tmp/$label_$tsFile
mkdir $tf
cd $tf

for (( c=0; c<${#cmds[@]}; c++ ))
do
	eval "echo ${cmds[$c]} >> /opt/volo/logs/${label}_${c}_c.log"
	eval "${cmds[$c]} 2> /opt/volo/logs/${label}_${c}_e.log"

	outputFile=`echo ${cmds[$c]} | sed 's/^.*-out \([^[:space:]]*\).*$/\1/g'`
	m3u8File=${outputFile%/*}/index.m3u8

	if [ -f $m3u8File ]
	then
		head -n -1 $m3u8File > $m3u8File.bak
		mv $m3u8File.bak $m3u8File

	else
		echo -en $m3u8Str > $m3u8File

	fi

	echo -en "#EXTINF:${duration}\n${tsFile}\n#EXT-X-ENDLIST" >> $m3u8File
	
done

cd -
rm -Rf $tf

rm -f $srcFrame

seq=$(($seq + 1))
echo -n $seq > $seqFile
