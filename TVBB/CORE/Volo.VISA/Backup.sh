#!/bin/sh

cd /opt
 
# VISA
ARCHIVE=./VISA.SP.R1.4.9.`date +"%Y%m%d"`.tgz
tar -zcvf ${ARCHIVE} ./Volo.VISA/Backup.sh ./Volo.VISA/scheduler/ ./Volo.VISA/API.Tool/ ./Volo.VISA/ui/*.php ./Volo.VISA/ui/*.sh ./Volo.VISA/ui/data/ ./Volo.VISA/ui/cssmenu/ ./Volo.VISA/ui/template/ ./Volo.VISA/ui/js/ ./Volo.VISA/QA.Tool/ ./Volo.VISA/control/ ./Volo.VISA/client/ ./Volo.VISA/Notify/ ./Volo.VISA/*layout.*
 
# VOLO H264
#ARCHIVE=/home/volo/Volo.Backup/VOLO.H264.R1.4.9.`date +"%Y%m%d"`.tgz
#tar -zcvf ${ARCHIVE} ./volo/*
 
# VOLO H265
#ARCHIVE=/home/volo/Volo.Backup/VOLO.H265.R1.4.9.`date +"%Y%m%d"`.tgz
#tar -zcvf ${ARCHIVE} ./volo-hevc/*
