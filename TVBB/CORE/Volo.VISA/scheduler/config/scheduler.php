<?php
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Scheduler Control Parameter

return array(
// SYSTEM PARAMETER
  'volo_node'		=> 'TVB - XDCAM encode Platform', // VISA Node Name
  'volo_pass'		=> 'volo', // VISA Admin UI Password
  'volo_mode'		=> 'S',       // VISA Encode Mode (S-Split Encode, B-Batch Encode, N-Normal Encode)
  'min_slot'		=> 8,         // VISA Batch Mode Required Workers
  'volo_encode_unit'	=> 100,       // VOLO Encoder Maximum Encode Unit
  'time_zone'		=> 'HKT',     // VISA Time Zone
  'schd_queue_size'	=> 1600,      // VISA Queue Size, Allow No of Output Jobs
  'schd_debug_mode'     => true,     // VISA XML Debug Mode
  'schd_debug_output'   => '/opt/Volo.VISA/ui/xml_calls.html',     // VISA UI API URL
  'schd_api'            => 'http://172.30.40.111/scheduler/api.php', // SCHEDULER API URI
  'support_email'       => 'frankie.luk@dao-lab.com', // JOB SUPPORT EMAIL
  'control_path'        => '/opt/Volo.VISA/control', // SERVICE CONTROL SCRIPT PATH
  'logs_path'           => '/opt/Volo.VISA/logs', // SERVICE CONTROL SCRIPT PATH
  'status_path'         => '/opt/Volo.VISA/VISA_Control', // SERVICE STATUS FILE PATH
  'source_path'         => '', // DEFAULT SOURCE FILE NAS PATH
  'destination_path'    => '', // DEFAULT DESTINATION FILE NAS PATH
// ENCODE PATH PARAMETER
  'volo_job_path'	=> '/opt/Volo.VISA/jobs', // NORMAL ENCODE JOB PATH
  'volo_job_done'	=> '/opt/Volo.VISA/results',     // NORMAL ENCODE COMPLETE JOB PATH
  'volo_thumb_path'	=> '/opt/Volo.VISA/ui/thumbnail/', // UI THUMBNAIL PATH
  'volo_thumb_cmd'	=> '/opt/Volo.VISA/control/scripts/VISA.SCHD.GenThumb.sh', // UI THUMBNAIL GEN COMMAND
  'volo_errsrc_path'	=> '/VISA_Control', // SPLIT ENCODE ERROR BACKUP PATH
  'encode_err_pname'	=> 'Encode_ERROR', // SPLIT ENCODE ERROR BACKUP PATH NAME
  'encode_control_file' => 'ENCODE', // ENCODE MODE CONTROL FILE
  'audio_map_file'      => 'AUDIO_MAP.INFO', // ENCODE AUDIO MAPPING INDEX
  'source_list'         => 'mxf,mov,mpg,dat,avi,wmv,mp4,ts', // SOURCE MEDIA SUPPORT LIST
  'source_exclude_path' => 'Encode_ERROR', // SOURCE MEDIA EXCLUDE PATH LIST
  'volo_timeout'	=> 1800,      // ENCODE TIME OUT in Seconds
// SPLIT ENCODE PARAMETER
  'volo_split_node'	=> 1,  // DEFAULT SPLIT REQUIRED WORKER
  'volo_split_interval'	=> 60, // DEFAULT SPLIT INTERVAL 60SECS
  'volo_split_priority'	=> 40, // DEFAULT SPLIT JOB PRIORITY
  'volo_noencode_job_unit' => 105, //DEFAULT NO ENCODE JOB UNIT, KENNY
  'volo_split_minimum'	=> 0,  // MINIMUM SPLIT TRUNK SET FOR SPLIT PROCESS
  'volo_splitjob_done'	=> '/opt/Volo.VISA/splitjobs',     // SPLIT ENCODE COMPLETE JOB PATH
  'volo_splitjob_path'	=> '/dev/shm/Volo.VISA/splitjobs', // SPLIT ENCODE JOB PATH
  'volo_splitjob_work'	=> '/dev/shm/Volo.VISA/splitjobs', // SPLIT ENCODE WORKING PATH
  'volo_split_path_high'     => '/home/Video.Spool/Split_Encode/IN_Trunk', // SPLIT ENCODE HIGH PRIORITY SOURCE TRUNK PATH
  'volo_split_workpath_high' => '/home/Video.Spool/Split_Encode/JOB_Trunk',// SPLIT ENCODE HIGH PRIORITY WORK TRUNK PATH
  'volo_split_outpath_high'  => '/home/Video.Spool/Split_Encode/OUT_Trunk',// SPLIT ENCODE HIGH PRIORITY OUTPUT TRUNK PATH
  'volo_split_path_low'      => '/home/Video.Spool/Split_Encode/IN_Trunk', // SPLIT ENCODE LOW PRIORITY SOURCE TRUNK PATH
  'volo_split_workpath_low'  => '/home/Video.Spool/Split_Encode/JOB_Trunk',// SPLIT ENCODE LOW PRIORITY WORK TRUNK PATH
  'volo_split_outpath_low'   => '/home/Video.Spool/Split_Encode/OUT_Trunk',// SPLIT ENCODE LOW PRIORITY OUTPUT TRUNK PATH
  'volo_split_cmd'	=> '/opt/Volo.VISA/control/split_encode/encode_split.sh', // SPLIT SOURCE COMMAND
  'volo_merge_cmd'	=> '/opt/Volo.VISA/control/split_encode/encode_merge.sh', // MERGE TRUNK COMMAND
  'volo_muxaudio_cmd'	=> '/opt/Volo.VISA/control/split_encode/encode_audio.sh', // MUX AUDIO COMMAND
  'volo_genaudio_cmd'	=> '/opt/Volo.VISA/client/helper/splitencode_audio.sh',   // MANUAL REMAP AUDIO COMMAND
  //kenny
  'volo_encode_profile_path' => '/home/Video.Profile/',//XDCAM encoding profile,kenny
  'volo_audio_mapping_cmd'   => '/opt/Volo.VISA/client/helper/audio_mapping.php',//audio mapping cmd,kenny
);
?>
