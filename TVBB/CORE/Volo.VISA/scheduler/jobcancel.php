<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)                ##
## -- CALL FOR JOB CANCEL --                                                                           ##
#########################################################################################################
$para = include('config/scheduler.php');
if (isset($_POST["JID"])){
	$JID = htmlspecialchars($_POST["JID"]);
} else {
	echo "VISA Reponse ::<br />ERROR REQUEST";
	exit();
}

$xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<VISA>\n<Request Command=\"JobControl\">\n";
$xml .= "<JobID>" . $JID . "</JobID>";
$xml .= "<Operation>Abort</Operation></Request></VISA>";

$url = 'http://192.168.3.143/scheduler/api.php';

$post_data = array(
    "xml" => $xml,
);

$stream_options = array(
    'http' => array(
       'header'  => "Content-type: application/xml; charset=utf-8\r\n",
       'method'  => 'POST',
       'content' => $xml,
    ),
);

$context  = stream_context_create($stream_options);
$response = file_get_contents($para['schd_api'], null, $context);

echo "VISA Reponse ::<br />";
echo '<pre>', htmlentities($response), '</pre>';
?>
