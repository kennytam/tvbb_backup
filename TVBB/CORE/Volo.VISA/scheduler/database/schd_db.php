<?php
//
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
//
class VisaDB {
  protected $db;
	
  //! Destructor - Automatically clean up database.
  function __destruct() {
      if ( isset( $this->db) ) {
        // mysqli_close($this->db);
        pg_close($this->db);
        unset ( $this->db);
      }
  }

  ///////////////////////////////////////////////////
  // Helper
  ///////////////////////////////////////////////////
  private function connect() {
      if ( isset( $this->db ) ) return true;
      $config = include('config/database.php');
      // $this->db = new mysqli($config['db.server'], $config['db.user'], $config['db.pass'], $config['db.name']);
      // if ($this->db->connect_errno) return -1;
      $connstr = "host=" . $config['db_server'] .
                             " user=". $config['db_user'] .
                             " password=". $config['db_pass'] .
                             " dbname=".$config['db_name'];
      $this->db = pg_connect($connstr)
                  or die('Could not connect: ' . pg_last_error());
      return true;
  } 

  ///////////////////////////////////////////////////
  // Job Functions
  ///////////////////////////////////////////////////
  public function job_create($source,$destination,$source_info,$source_duration) {
      $this->connect();
      $query = "SELECT SP_job_create('" .
                $source . "','" .
                $destination . "','" .
                pg_escape_string($source_info) . "'," .
                $source_duration . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      $job_id = $row[0];
      pg_free_result($result);

      // Insert Job Handler for Thumbnail Creation
      $query = "INSERT INTO job_handler (job_id, c_insert, source_file, source_duration) VALUES (" . $job_id . ", true, '" . $source . "', " . $source_duration . ");";
      $result = pg_exec($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      pg_free_result($result);

      return $job_id;
  }
  public function job_not_cancel($job_id) {
      $this->connect();
      $query = "SELECT SP_job_not_cancel(" . $job_id . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      $count = $row[0];
      pg_free_result($result);
      return $count > 0;
  }
  public function job_cancel($job_id) {
      $this->connect();
      $query = "SELECT SP_job_cancel(" . $job_id . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      pg_free_result($result);
      $query = "SELECT SP_splitjob_cancel(" . $job_id . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      pg_free_result($result);
      return true;
  }
  public function job_retry($job_id, $mode) {
      $this->connect();
      if ($mode == "RetryJob"){
	$query = "INSERT INTO job_handler (job_id,output_id,c_retry) VALUES (" . $job_id . ",0,true);";
	$result = pg_exec($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
	pg_free_result($result);
	return true;
      }
      $query = "SELECT job_id, id FROM output WHERE job_id=" . $job_id . " AND stage NOT IN (0,2);";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      while ($row = pg_fetch_row($result)) {
	$oquery = "INSERT INTO job_handler (job_id,output_id,c_retry) VALUES (" . $row[0] . "," . $row[1] . ",true);";
	$updsql = pg_exec($this->db, $oquery)
                or die('Query failed: ' . $oquery . "<br>\nError: " . pg_last_error());
	pg_free_result($updsql);
      }
      pg_free_result($result);
      return true;
  }
  public function job_meta($job_id) {
      $meta = array();
      $this->connect();
      $query = "SELECT * FROM SP_job_meta(" . $job_id . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      $meta['source'] = $row[0];
      $meta['destination'] = $row[1];
      $meta['source_duration'] = $row[2];
      $meta['source_info'] = $row[3];
      $meta['ctime'] = substr($row[4], 0, 19);
      pg_free_result($result);
      return $meta;
  }
  public function job_query($job_id) {
      $list = array();
      if ( $job_id == 0 ) return $list;

      $this->connect();
      $query = "SELECT * FROM SP_job_query(" .
                $job_id . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      while ($row = pg_fetch_row($result)) {
	if (trim($row[12]) == "") $row[12] = '0';
        array_push($list, array(
            'id'       => intval($row[0]),
            'o_num'    => intval($row[1]),
            'filename' => $row[2],
            'container' => $row[3],
            'profile'  => $row[4],
            'stage'    => intval($row[5]),
            'progress' => intval($row[6]),
            'progress_fps' => intval($row[7]),
            'progress_mode' => intval($row[8]),
            'err'      => intval($row[9]),
            'stime'    => substr($row[10], 0, 19),
            'ltime'    => substr($row[11], 0, 19),
            'length'   => $row[12],
            'message'  => $row[13]
            )
        );
      }
      pg_free_result($result);
      return $list;
  }
  public function job_addoutput($job_id,$o_num,$file,$container,$profile,$vdbit,$adbit,$resolu,$deint,$pmode,$segmode,$length,$encode_mode,$priority,$split_encode) {
      $this->connect();
      // Set Custom Parameters
      if ( !isset($container) ) $container = "";
      if ( !isset($resolu) ) $resolu = "";
      if ( !isset($deint) ) $deint = "";
      if ( !isset($pmode ) ) $pmode = "";
      if ( !isset($segmode) ) $segmode = "";
      if ( !isset($vdbit) || trim($vdbit) == "" ) $vdbit='0';
      if ( !isset($adbit) || trim($adbit) == "" ) $adbit='0';
      if ( !isset($length) || trim($length) == "" ) $length='0';
      if ( !isset($encode_mode) || trim($encode_mode) == "" ) $encode_mode='0';
      if ( !isset($priority) || trim($priority) == "" ) $priority='0';
      if ( !isset($split_encode) || trim($split_encode) == "" ) $split_encode='false';
      // Calcutate Encode Unit
      $query = "SELECT unit FROM profile_unit WHERE profile='" . $profile . "';";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      $encode_unit = intval($row[0]);
      pg_free_result($result);
      if ($encode_unit < 1) $encode_unit = 15;

      $query = "SELECT SP_job_addoutput(" .
                $job_id . "," .
                $o_num  . ",'" .
                $file . "','" .
                $container . "','" .
                $profile . "','" .
                $vdbit . "','" .
                $adbit . "','" .
                $resolu . "','" .
                $deint . "','" .
                $pmode . "','" .
                $segmode . "','" .
                $length . "','" .
                $encode_mode . "','" .
                $priority . "'," .
                $split_encode . ",'" .
                $encode_unit . "');";
      ## SQL DEBUG
      ## file_put_contents('/tmp/pgsql-query.log', "(job_addoutput) SQL :: " . $query . "\n", FILE_APPEND);
      ## END SQL DEBUG
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      $output_id = intval($row[0]);
      pg_free_result($result);
      return $output_id;
  }

  ///////////////////////////////////////////////////
  // Output Functions
  ///////////////////////////////////////////////////
  public function output_queue_len() {
      $this->connect();
      $query = "SELECT SP_output_queue_len();";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      $qlen = intval( $row[0] );
      pg_free_result($result);
      return $qlen;
  }
  public function output_get_jobid($o_id) {
      $this->connect();
      $query = "SELECT * FROM SP_output_jobid(" . $o_id . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      if ( $row ) {
	      $job_id = intval( $row[0] );
      } else {
      	$job_id = 0;
      }
      pg_free_result($result);
      return $job_id;
  }
  public function output_pick($worker_id, $mode, $minslot) {
	// Current Date/Time
      if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
		@date_default_timezone_set(@date_default_timezone_get());
      $curTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
      $PARA = include('config/scheduler.php');
      $list = array();
      $workerpos = 0;
      $workunit = -1;
      $this->connect();
      # file_put_contents('/tmp/worker_pick.log', "IN WORKER    :: ID[" . $worker_id . "]\n", FILE_APPEND);
      // Check Worker Priority Queue and Loading Unit
      $query = "SELECT id, unit FROM worker WHERE mode='A' AND ready=true AND dead=false ORDER BY unit DESC, id ASC;";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      while ($row = pg_fetch_row($result)) {
      ## DEBUG ##
      # file_put_contents('/tmp/worker_pick.log', "WORKER TABLE :: ID[" . $row[0] . "] UNIT[" . $row[1] . "]\n", FILE_APPEND);
      ## DEBUG ##
	$workerpos++;
	$row[0] = intval($row[0]);
	$row[1] = intval($row[1]);
	if ($row[0] == $worker_id){
	    if ($row[1] < 1){
		$workerpos=99;
		break;
	    } else if ($workunit <= $row[1]){
		$workerpos=1;
	    }
	    $workunit = $row[1];
	    break;
	}
	$workunit = $row[1];
      }
      pg_free_result($result);
      // Skip Job Pick if low priority worker
      if ($workerpos > 1){
      ## DEBUG ##
      # file_put_contents('/tmp/worker_pick.log', "REJECT REQ ID[" . $worker_id . "] UNIT[" . $workunit . "|" . $row[1] . "] POS [" . $workerpos . "|" . $row[0] . "] MODE[" . $mode . "]\n", FILE_APPEND);
      ## DEBUG ##
	return $list;
      }

      // Skip Less than Minimum Unit
//      if ($workunit < 30) return $list;
      if ($workunit < 10) return $list;//kenny

      ## DEBUG ##
      # file_put_contents('/tmp/worker_pick.log', "ACCEPT REQ ID[" . $worker_id . "] UNIT[" . $workunit . "|" . $row[1] . "] POS [" . $workerpos . "|" . $row[0] . "] MODE[" . $mode . "]\n", FILE_APPEND);
      ## DEBUG ##

      // Check Any Split Encode Pending Job
      if ($mode == "S"){
	// High Prioriy Normal Encode Job First
	$query = "SELECT count(*) FROM output WHERE stage=0 AND split_encode=false AND c_cancel=false AND priority>0;";
	$result = pg_query($this->db, $query)
		or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
	$row = pg_fetch_row($result);
	pg_free_result($result);
	if (intval($row[0]) > 0){
		$mode="N";
	} else {
		// No Split Encode Job Running
		$query = "SELECT count(*) FROM output WHERE stage=1 AND split_encode=true AND c_cancel=false;";
		$result = pg_query($this->db, $query)
			or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
		$row = pg_fetch_row($result);
		if (intval($row[0]) < 1) $mode="N";
		pg_free_result($result);
	}
	
	// Get Top Priority JobID in Split Encode
	if ($mode == "S"){
		$pick_jobid=0;

		// Pick High Priority Running Split Encode Job - WorkerID > -1 :: 0-Split Trunk Ready >0-Running
		$query = "SELECT DISTINCT job_id, priority FROM split_output WHERE worker_id=0 AND stage=0 AND job_id IN (SELECT DISTINCT job_id FROM split_output WHERE worker_id>0 AND stage=1 AND c_cancel=false) AND c_cancel=false AND priority >=" . $PARA["volo_split_priority"] . " ORDER BY priority DESC, job_id ASC LIMIT 1;";
		$result = pg_query($this->db, $query)
			or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
		$row = pg_fetch_row($result);
		pg_free_result($result);

		if (intval($row[0]) > 0){
		   $pick_jobid=$row[0];
		   file_put_contents('/tmp/split_pick-run.log', "[" . $curTime . "] Pick Run HIGH :: WORKER -> " . $worker_id . "/" . $workunit . " JOBID -> " . $row[0] . " JOB PRIOR -> " . $row[1] . "\n", FILE_APPEND);
		} else {
		   // Pick Low Priority Running Split Encode Job - WorkerID > -1
		   $query = "SELECT DISTINCT job_id, priority FROM split_output WHERE worker_id=0 AND stage=0 AND job_id IN (SELECT DISTINCT job_id FROM split_output WHERE worker_id>0 AND stage=1 AND c_cancel=false) AND c_cancel=false AND priority <" . $PARA["volo_split_priority"] . " ORDER BY priority DESC, job_id ASC LIMIT 1;";
		   $result = pg_query($this->db, $query)
			or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
		   $row = pg_fetch_row($result);
		   pg_free_result($result);
		   if (intval($row[0]) > 0){
			 $pick_jobid=$row[0];
		   	 file_put_contents('/tmp/split_pick-run.log', "[" . $curTime . "] Pick Run LOW  :: WORKER -> " . $worker_id . "/" . $workunit . " JOBID -> " . $row[0] . " JOB PRIOR -> " . $row[1] . "\n", FILE_APPEND);
		   }
		}

		if ($pick_jobid < 1){
		   // Pick New Highest Priority Split Output Job
		   $query = "SELECT DISTINCT job_id, priority FROM output WHERE stage=1 AND split_encode=true AND c_cancel=false ORDER BY priority DESC, job_id ASC;";
		   $result = pg_query($this->db, $query)
				or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
		   while ($row = pg_fetch_row($result)) {
				// Any Waiting Split Job - Trunk Ready
				$jquery = "SELECT count(*) FROM split_output WHERE worker_id=0 AND job_id=" . $row[0] . " AND stage=0 AND c_cancel=false;";
				$jresult = pg_query($this->db, $jquery)
					or die('Query failed: ' . $jquery . "<br>\nError: " . pg_last_error());
				$jrow = pg_fetch_row($jresult);
				pg_free_result($jresult);
				if (intval($jrow[0]) > 0){
		   	 		file_put_contents('/tmp/split_pick-run.log', "[" . $curTime . "] Pick New TOP  :: WORKER -> " . $worker_id . "/" . $workunit . " JOBID -> " . $row[0] . " PRIOR -> " . $row[1] . "\n", FILE_APPEND);
					$pick_jobid=$row[0];
					break;
				}
		   }
		   pg_free_result($result);
		   if ($pick_jobid < 1){
				# file_put_contents('/tmp/split_pick-new.log', "NO NEW JOB" . "\n", FILE_APPEND);
				return $list; // Return No Split Job
		   }
		} // End Pick JobID < 1
	} // End Mode='S' check Running Job
      } // End Mode='S'

      // Split Encoding Job Pick
      if ($mode == "S"){
        $query = "SELECT * FROM SP_split_pick(" . $worker_id . "," . $pick_jobid . "," . $workunit . ");";
      } else if ($mode == "B"){
	// Get Available Worker
        $query = "SELECT * FROM SP_worker_gettotal();";
        $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
        $row = pg_fetch_row($result);
        $jobslot = intval($row[0]);
        pg_free_result($result);
	if ($jobslot <= $minslot){
		// Batch Mode
		$query = "SELECT * FROM SP_batch_pick(" . $worker_id . ");";
	} else {
		// Random Mode (S)
		$query = "SELECT * FROM SP_output_pick(" . $worker_id . "," . $workunit . ");";
		$mode = "S";
	}
      } else {
	// Skip if No High Priority Job in Run Queue
	if ($workunit < 60){
	   // $stTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	   $query = "SELECT DISTINCT job_id, priority FROM output WHERE stage=1 AND split_encode=false AND c_cancel=false AND encode_unit>=50 ORDER BY priority DESC, job_id ASC;";
	   $result = pg_query($this->db, $query)
		or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
	   $skipProc = true;
	   while ($job = pg_fetch_row($result)) {
	   	$query = "SELECT count(*) FROM output WHERE stage=0 AND encode_unit<=" . $workunit . " AND job_id=" . $job[0] . ";";
	   	$count = pg_query($this->db, $query)
			or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
	   	$row = pg_fetch_row($count);
	   	pg_free_result($count);

		## DEBUG ##
		## $curTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
		## file_put_contents('/tmp/Volo.HLS/worker_pick.log', "[" . $stTime . " - " . $curTime . "] CHECK  REQ[" . $worker_id . "] UNIT[" . $workunit . "] JID[" . $job[0] . "] WAIT[" . $row[0] . "]", FILE_APPEND);
		## DEBUG ##

	   	if (intval($row[0]) > 0){
		    ## DEBUG ##
		    ## $curTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
		    ## file_put_contents('/tmp/Volo.HLS/worker_pick.log', " ACCEPT" . "\n", FILE_APPEND);
		    ## DEBUG ##

		    $skipProc = false;
		    break;
	   	}
		## file_put_contents('/tmp/Volo.HLS/worker_pick.log', " CONT.." . "\n", FILE_APPEND);
	   }
	   pg_free_result($result);
	   if ($skipProc) return $list;
	}

        // Random Mode
        $query = "SELECT * FROM SP_output_pick(" . $worker_id . "," . $workunit . ");";
      }

      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      while ($row = pg_fetch_row($result)) {
	if ($mode == "S"){
	 $sp_id      = intval($row[0]);
	 $sp_jobid   = intval($row[1]);
	 $sp_outid   = intval($row[2]);
	 $sp_tstype  = trim($row[3]);
	 $sp_tsttl   = intval($row[4]);
	 $sp_srcpath = trim($row[5]);
	 $sp_srcfile = trim($row[6]);
	 $sp_dstpath = trim($row[7]);
	 $sp_dstfile = trim($row[8]);

	 $jquery  = "SELECT o_num, profile, videobit, audiobit, resolu, deint, pmode, encode_mode, encode_unit ";
	 $jquery .= "FROM output WHERE id='" . $sp_outid . "';";
	 $jresult = pg_query($this->db, $jquery)
                or die('Query failed: ' . $jquery . "<br>\nError: " . pg_last_error());
	 $jrow = pg_fetch_row($jresult);
	 if (trim($jrow[2]) == "") $jrow[2] = 0;
	 if (trim($jrow[3]) == "") $jrow[3] = 0;
         $encode_unit = intval($jrow[8]);
         array_push($list, array(
            'sp_id'       => $sp_id,
            'job_id'      => $sp_jobid,
            'out_id'      => $sp_outid,
            'o_num'       => intval($jrow[0]),
            'sp_tstype'   => $sp_tstype,
            'sp_tsttl'    => $sp_tsttl,
            'sp_srcpath'  => $sp_srcpath,
            'sp_srcfile'  => $sp_srcfile,
            'sp_dstpath'  => $sp_dstpath,
            'sp_dstfile'  => $sp_dstfile,
            'profile'     => trim($jrow[1]),
            'videobit'    => $jrow[2],
            'audiobit'    => $jrow[3],
            'resolu'      => trim($jrow[4]),
            'deint'       => trim($jrow[5]),
            'pmode'       => trim($jrow[6]),
            'encode_mode' => intval($jrow[7]),
            'encode_unit' => intval($jrow[8]),
            'mode'        => $mode,
            )
         );
	 pg_free_result($jresult);
	} else {
	 if (trim($row[4]) == "") $row[4] = 0;
	 if (trim($row[10]) == "") $row[10] = 0;
	 if (trim($row[11]) == "") $row[11] = 0;
	 if (trim($row[16]) == "") $row[16] = 0;
         $encode_unit = intval($row[19]);
         array_push($list, array(
            'id'          => intval($row[0]),
            'job_id'      => intval($row[1]),
            'o_num'       => intval($row[2]),
            'source'      => trim($row[3]),
            'source_duration'      => $row[4],
            'source_info' => $row[5],
            'destination' => trim($row[6]),
            'filename'    => trim($row[7]),
            'container'   => trim($row[8]),
            'profile'     => trim($row[9]),
            'videobit'    => $row[10],
            'audiobit'    => $row[11],
            'resolu'      => trim($row[12]),
            'deint'       => trim($row[13]),
            'pmode'       => trim($row[14]),
            'segmode'     => trim($row[15]),
            'length'      => $row[16],
            'priority'    => $row[17],
            'encode_mode' => $row[18],
            'encode_unit' => $row[19],
            'mode'        => $mode,
            )
         );
	}
        // Update Worker Unit
	$wquery = "UPDATE worker SET unit=unit-" . $encode_unit . " WHERE id = '" . $worker_id . "';";
	$wresult = pg_exec($this->db, $wquery)
                or die('Query failed: ' . $wquery . "<br>\nError: " . pg_last_error());
	pg_free_result($wresult);
      } // End Loop Output Jobs
      pg_free_result($result);

      return $list;
  }
  public function output_update($o_id, $stage, $progress, $progress_fps, $progress_mode, $err, $message) {
      $this->connect();
      $query = "SELECT SP_output_update(" .
                $o_id . "," .
                $stage . "," .
                $progress . "," .
                $progress_fps . ",'" .
                $progress_mode . "'," .
                $err . ", '" .
                $message . "');";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      pg_free_result($result);
      return true;
  }
  public function spoutput_update($s_id, $trunk_type, $stage, $progress, $progress_fps, $progress_mode, $err, $message) {
      $this->connect();
      $query = "UPDATE split_output SET stage=" . $stage . ", " .
               "progress=" .  $progress . ", " .
               "progress_fps=" . $progress_fps . ", " .
               "progress_mode='" . $progress_mode . "', " .
               "err=" . $err . ", " .
               "message='" . $message . "', ltime=now() WHERE id=" . $s_id . ";";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      pg_free_result($result);
      return true;
  }
  public function encode_start($o_id) {
      $this->connect();
      $query = "SELECT SP_encode_start(" .
                $o_id . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      pg_free_result($result);
      return true;
  }

  ///////////////////////////////////////////////////
  // Notification  Functions
  ///////////////////////////////////////////////////
  public function noti_clear() {
      $this->connect();
      $query = "SELECT SP_noti_clear();";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      pg_free_result($result);
      return true;
  }
  public function noti_register($ev, $url) {
      $this->connect();
      $query = "SELECT SP_noti_register(" .
                $ev . ",'" .
                $url . "');";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      pg_free_result($result);
      return true;
  }
  public function noti_unregister($ev, $url) {
      $this->connect();
      $query = "SELECT SP_noti_unregister(" .
                $ev . ",'" .
                $url . "');";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      pg_free_result($result);
      return true;
  }
  public function noti_geturl($ev) {
      $list = array();
      $this->connect();
      $query = "SELECT SP_noti_geturl(" . $ev . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      while ($row = pg_fetch_row($result)) {
        array_push($list, $row[0]);
      }
      pg_free_result($result);
      return $list;
  }

  ///////////////////////////////////////////////////
  // Worker Functions
  ///////////////////////////////////////////////////
  public function worker_list() {
      $list = array();
      $this->connect();
      $query = "SELECT * FROM SP_worker_list();";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      while ($row = pg_fetch_row($result)) {
        array_push($list, array(
            'id'    => intval($row[0]),
            'power' => intval($row[1]),
            'ready' => ($row[2]=='t')
            )
        );
      }
      pg_free_result($result);
      return $list;
  }
  public function worker_ident($uuid) {
      $this->connect();
      $query = "SELECT id,name,power FROM SP_worker_ident('" . $uuid . "');";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      $ident = array(
            'id'    => intval($row[0]),
            'name'  => $row[1],
            'power' => intval($row[2])
          );
      pg_free_result($result);
      return $ident;
  }

  public function worker_busy($uuid) {
      $this->connect();
      $query = "SELECT SP_worker_busy('" . $uuid . "');";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      pg_free_result($result);
      return true;
  }

  public function worker_join($uuid, $name, $power, $ready) {
      $this->connect();
      $query = "SELECT SP_worker_join('" .
                $uuid . "','" .
                $name . "'," .
                $power . "," .
                $ready . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      $worker_id = intval( $row[0] );
      pg_free_result($result);
      return $worker_id;
  }
  
  public function worker_leave($uuid) {
      $this->connect();
      $query = "SELECT SP_worker_leave('" . $uuid . "');";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      pg_free_result($result);
      $worker_id = intval( $row[0] );
      return $worker_id;
  }
  
  public function worker_heartbeat($uuid, $power, $ready) {
      $this->connect();
      $query = "SELECT SP_worker_heartbeat('" .
                $uuid . "'," .
                $power . "," .
                $ready . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      $shutdown = ($row[0] == 't');
      pg_free_result($result);
      return $shutdown;
  }
  
  public function worker_getstate($uuid) {
      $this->connect();
      $query = "SELECT SP_worker_getstate('" .
                $uuid . "');";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      $state = $row[0];
      pg_free_result($result);
      return $state;
  }

  public function worker_gettotal() {
      $this->connect();
      $query = "SELECT SP_worker_gettotal();";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      $row = pg_fetch_row($result);
      $total = $row[0];
      pg_free_result($result);
      return $total;
  }

  public function worker_deadcheck() {
      $deadlist = array();
      $this->connect();
      $query = "SELECT id,uuid,name,dtime FROM SP_worker_deadcheck();";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      while ($row = pg_fetch_row($result)) {
        array_push($deadlist, array(
            'id'    => intval($row[0]),
            'uuid'  => $row[1],
            'name'  => $row[2],
            'dtime' => intval($row[3])
            )
        );
      }
      pg_free_result($result);
      return $deadlist;
  }

  public function worker_cleandead($uuid) {
      $list = array();
      $this->connect();
      $query = "SELECT id,job_id,o_num FROM SP_worker_cleandead('" . $uuid . "');";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      while ($row = pg_fetch_row($result)) {
        array_push($list, array(
            'id'     => intval($row[0]),
            'job_id' => intval($row[1]),
            'o_num'  => intval($row[2])
            )
        );
      }
      pg_free_result($result);
      return $list;
  }

  public function worker_getcancel($uuid) {
      $list = array();
      $this->connect();
      $query = "SELECT id,job_id,o_num,stage FROM SP_worker_getcancel('" . $uuid . "');";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      while ($row = pg_fetch_row($result)) {
        array_push($list, array(
            'id'     => intval($row[0]),
            'job_id' => intval($row[1]),
            'o_num'  => intval($row[2]),
            'stage'  => intval($row[3])
            )
        );
      }
      pg_free_result($result);
      return $list;
  }

  public function worker_spgetcancel($uuid) {
      $list = array();
      $this->connect();
      $query = "SELECT id,output_id,job_id,trunk_type,stage FROM SP_worker_spgetcancel('" . $uuid . "');";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      while ($row = pg_fetch_row($result)) {
        array_push($list, array(
            'id'         => intval($row[0]),
            'output_id'  => intval($row[1]),
            'job_id'     => intval($row[2]),
            'trunk_type' => trim($row[3]),
            'stage'      => intval($row[4])
            )
        );
      }
      pg_free_result($result);
      return $list;
  }

  public function log($type, $who, $what) {
      $this->connect();
      $query = "SELECT SP_log(" . $type . "," . $who . "," . $what . ");";
      $result = pg_query($this->db, $query)
                or die('Query failed: ' . $query . "<br>\nError: " . pg_last_error());
      pg_free_result($result);
      return true;
  }

}

?>
