<?php
//
// VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
// This is the entry-point for all VISA-related request
// This expects a well-form XML input from HTTP-POST
//

require_once("core/proto_xml.php");
require_once("core/schd_api.php");
$config = include('config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curTime  = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $callip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $callip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $callip = $_SERVER['REMOTE_ADDR'];
}

// Create output
$writer = new VisaProto_XMLWriter();

// Parse input XML from HTTP-POST
try {
  $in_xml = new SimpleXMLElement( file_get_contents("php://input") );
  if ($config['schd_debug_mode']){
	file_put_contents($config['schd_debug_output'], "###### " . $config['time_zone'] . ' ' . $curTime . " (REMOTE HOST : " . $callip . ") VISA POST REQUEST XML STRING ######<br />");
	file_put_contents($config['schd_debug_output'], '<pre>' . htmlentities(file_get_contents("php://input")) . '</pre>', FILE_APPEND);
  }
} catch (Exception $e) {
  $resText .= '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
  $resText .= '<VISA>' . "\n";
  $resText .= '  <Response>' . "\n";
  $resText .= '   <Status>Failed</Status>' . "\n";
  $resText .= '   <Reason>Invalid XML</Reason>' . "\n";
  $resText .= '   <Timestamp>' . $config['time_zone'] . ' ' . $curTime . '</Timestamp>' . "\n";
  $resText .= '  </Response>' . "\n";
  $resText .= '</VISA>' . "\n";
  if ($config['schd_debug_mode']){
	file_put_contents($config['schd_debug_output'], "<br />###### VISA RESPONSE XML STRING ######<br />", FILE_APPEND);
	file_put_contents($config['schd_debug_output'], '<pre>' . htmlentities($resText) . '</pre>', FILE_APPEND);
  }
  echo $resText;
  exit;
}

// Process request(s)
$visa = new VisaSchedulerAPI();
foreach($in_xml->children() as $req){
   $visa->process($writer,$req);
}
unset ($visa);
unset ($in_xml);

// Return response
if ($config['schd_debug_mode']){
	file_put_contents($config['schd_debug_output'], "<br />###### VISA RESPONSE XML STRING ######<br />", FILE_APPEND);
	file_put_contents($config['schd_debug_output'], '<pre>' . htmlentities($writer->toText()) . '</pre>', FILE_APPEND);
}
header('Content-type: application/xml; charset=utf-8');
echo $writer->toText();
?>
