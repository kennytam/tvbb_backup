<?php
//
// VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
// This is the entry-point for all VISA-related request
// This expects a well-form XML input from HTTP-POST
//

require_once("core/proto_xml.php");
require_once("core/schd_manager.php");

// Create output
$writer = new VisaProto_XMLWriter();

// Parse input XML from HTTP-POST
try {
  $in_xml = new SimpleXMLElement( file_get_contents("php://input") );
} catch (Exception $e) {
  $config = include('config/scheduler.php');
  if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
  $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
  echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
  echo '<VISA>' . "\n";
  echo '  <Response>' . "\n";
  echo '   <Status>Failed</Status>' . "\n";
  echo '   <Reason>Invalid XML</Reason>' . "\n";
  echo '   <Timestamp>' . $config['time_zone'] . ' ' . $curTime . '</Timestamp>' . "\n";
  echo '  </Response>' . "\n";
  echo '</VISA>' . "\n";
  exit;
}

// Process request(s)
$visa = new VisaSchedulerManager();
foreach($in_xml->children() as $req){
   $visa->process($writer,$req);
}
unset ($visa);
unset ($in_xml);

// Return response
header('Content-type: application/xml; charset=utf-8');
echo $writer->toText();
?>
