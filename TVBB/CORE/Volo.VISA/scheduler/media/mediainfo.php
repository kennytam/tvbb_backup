<?php

class MediaInfo {
  static public function get($filename) {
    $rez = array();
    $rez['other'] = array();
    $rez['general'] = array();
    $rez['video'] = array();
    $rez['audio'] = array();

    // Installation check
    $mediainfo  = trim(shell_exec('type -P mediainfo'));
    if (empty($mediainfo)){
      $rez['success'] = false;
      $rez['status' ] = "MediaFile is not installed.";
      return $rez;
    }
    if($filename == '') {
        $rez['success'] = false;
        $rez['status' ] = "File not specified.";
        return $rez;
    }
    $filename = trim($filename);
    // Check if is directory
    if ( is_dir($filename) ) {
        $rez['success'] = false;
        $rez['status' ] = "File specified is a directory.";
        return $rez;
    }
    // Check if file exist
    if(!file_exists($filename)) {
      $rez['success'] = false;
      $rez['status' ] = "File not found.";
      return $rez;
    }

    $filename = escapeshellarg($filename);
    $ret = shell_exec("mediainfo $filename");
    $info = explode(chr(10),$ret);
    $section = 'other';
    foreach($info as $key=>$val) {
      $kv = explode(': ',$val);
      $k = trim($kv[0]);
      if (array_key_exists(1, $kv)) {
        $v = trim($kv[1]);
        $rez[$section][$k] = $v;
      } else {
        if ( $k == 'General' )    $section = 'general';
        else if ( $k == 'Video' ) $section = 'video';
        else if ( substr($k,0,5) == 'Audio' ) $section = 'audio';
        else $section = 'other';
      }
    }

//    if($info['general']['Format']=='') {
//      $rez['success'] = false;
//      $rez['status' ] = "Invalid media.";
//      return $rez;
//    }

    $rez['success'] = true;
    return $rez;
  }
};

?>
