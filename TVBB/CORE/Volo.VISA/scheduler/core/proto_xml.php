<?php
//
// VISA v1.3.4 Build Fri May 01 23:51:12 HKT 2015 (DaoLab)
//
require_once("error.php");


class VisaProto_XMLWriter {
  protected $dom;
  protected $STAGE_NAME;
  protected $TIMEZONE;
  protected $TIMESTAMP;

  ///////////////////////////////////////////////////
  // Constructor & Destructor
  ///////////////////////////////////////////////////
  function __construct() {
    $this->dom = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><VISA/>');
    $this->STAGE_NAME = array ( "Failed", "Scheduled", "Progress", "Completed", "Aborted" );
    $config = include('config/scheduler.php');
    if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
	@date_default_timezone_set(@date_default_timezone_get());
    $this->TIMEZONE = $config['time_zone'];
    $this->TIMESTAMP = $config['time_zone'] . " " . date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
  }
  function __destruct() {
      if ( isset( $this->STAGE_NAME) ) unset ( $this->STAGE_NAME);
      if ( isset( $this->TIMEZONE) ) unset ( $this->TIMEZONE);
      if ( isset( $this->TIMESTAMP) ) unset ( $this->TIMESTAMP);
      if ( isset( $this->dom) ) unset ( $this->dom);
  }

  ///////////////////////////////////////////////////
  // Convert to text
  ///////////////////////////////////////////////////
  public function toText() {
      // Debug
      $d = new DOMDocument('1.0');
      $d->preserveWhiteSpace = false;
      $d->formatOutput = true;
      $d->loadXML($this->dom->asXML());
      return $d->saveXml();
      // Release
      // return $this->dom->asXML();
  }

  ///////////////////////////////////////////////////
  // Visa API To Portal
  ///////////////////////////////////////////////////
  public function response_ok($command) {
      $e = new VisaError();
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_response->addChild("Status", $e->get_desc(VisaError::ERR_OK));
      $x_timestamp = $x_response->addChild("Timestamp", $this->TIMESTAMP);
      return true;
  }
  public function response_err($command, $err) {
      $e = new VisaError();
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_status = $x_response->addChild("Status", "Failed");
      $x_error = $x_response->addChild("Reason", $e->get_desc($err));
      $x_timestamp = $x_response->addChild("Timestamp", $this->TIMESTAMP);
      return true;
  }
  public function response_capacity_check($command, $result, $unit, $capacity) {
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_response->addChild("Status", $result);

      if (isset($unit)) {
	    $this->write_capacity($x_response, $unit, $capacity);
      }
      $x_timestamp = $x_response->addChild("Timestamp", $this->TIMESTAMP);
      return true;
  }
  public function response_job_submit($command, $result, $unit, $capacity, $job_id, $est_time) {
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_response->addChild("Status", $result);

      if (isset($job_id)) {
        $this->write_job_info($x_response, $job_id, $est_time);
      }

      if (isset($unit)) {
	    $this->write_capacity($x_response, $unit, $capacity);
      }
      $x_timestamp = $x_response->addChild("Timestamp", $this->TIMESTAMP);
      return true;
  }
  public function response_job_query($command, $result, $job_id, $meta, $outputs) {
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_response->addChild("Status", $result);
      $x_response->addChild("SourceUri", $meta['source']);
      $x_response->addChild("OutputPath", $meta['destination']);
      $x_response->addChild("SourceDuration", $meta['source_duration']);
      $x_response->addChild("JobSubmitTime", $this->TIMEZONE . " " . $meta['ctime']);

      if (isset($job_id)) {
        $this->write_job_info($x_response, $job_id, 0);
        $x_output = $x_response->addChild("Output");
        foreach ($outputs as $o) {
          $this->write_job_output($x_output,
                                  $o['o_num'],
                                  $o['filename'],
                                  $o['container'],
                                  $o['profile'],
                                  $o['stage'],
                                  $o['progress'],
                                  $o['progress_fps'],
                                  $o['progress_mode'],
                                  $o['err'],
                                  $this->TIMEZONE . " " . $o['stime'],
                                  $this->TIMEZONE . " " . $o['ltime'],
                                  $o['length'],
                                  $o['message']);
        }
      }
      $x_timestamp = $x_response->addChild("Timestamp", $this->TIMESTAMP);
      return true;
  }
  public function response_media_info($command, $result, $info) {
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_response->addChild("Status", $result);

      if ( isset($info) ) {
        $x_general = $x_response->addChild("General");
        $x_general->addChild("Size",     $info['general']['File size']);
        $x_general->addChild("Format",   $info['general']['Format']);
        $x_general->addChild("Duration", $info['general']['Duration']);
        $x_general->addChild("Bitrate",  $info['general']['Overall bit rate']);

        $x_video = $x_response->addChild("Video");
        $x_video->addChild("Format",   $info['video']['Format']);
        $x_video->addChild("Bitrate",  $info['video']['Bit rate']);
        $x_video->addChild("Width",    $info['video']['Width']);
        $x_video->addChild("Height",   $info['video']['Height']);
        $x_video->addChild("Scan",     $info['video']['Scan type']);
        $x_video->addChild("FPS",      $info['video']['Frame rate']);

        $x_audio = $x_response->addChild("Audio");
        $x_audio->addChild("Format",   $info['audio']['Format']);
        $x_audio->addChild("Bitrate",  $info['audio']['Bit rate']);
        $x_audio->addChild("Sample",   $info['audio']['Sampling rate']);
        $x_audio->addChild("Channel",  $info['audio']['Channel(s)']);
      }
      $x_timestamp = $x_response->addChild("Timestamp", $this->TIMESTAMP);
      return true;
  }

  ///////////////////////////////////////////////////
  // Notification
  ///////////////////////////////////////////////////
  public function response_noti_geturl($command, $result, $url) {
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_response->addChild("Status", $result);
      $x_response->addChild("TriggerUrl", $url);
      $x_timestamp = $x_response->addChild("Timestamp", $this->TIMESTAMP);
      return true;
  }
  public function noti_capacity_update($ev, $unit, $capacity) {
      $x_noti = $this->dom->addChild("Response");
      $x_noti->addAttribute("Command", $ev);
      $x_noti->addChild("Status", "OK");
      if (isset($unit)) {
	    $this->write_capacity($x_noti, $unit, $capacity);
      }
      $x_timestamp = $x_noti->addChild("Timestamp", $this->TIMESTAMP);
      return true;
  }
  public function noti_job_update($ev, $job_id, $meta, $outputs) {
      $x_noti = $this->dom->addChild("Response");
      $x_noti->addAttribute("Command", $ev);
      $x_noti->addChild("Status", "OK");
      $x_noti->addChild("OutputPath", $meta['destination']);

      if (isset($job_id)) {
        $this->write_job_info($x_noti, $job_id, 0);
        $x_output = $x_noti->addChild("Output");
        foreach ($outputs as $o) {
          $this->write_job_output($x_output,
                                  $o['o_num'],
                                  $o['filename'],
                                  $o['container'],
                                  $o['profile'],
                                  $o['stage'],
                                  $o['progress'],
                                  $o['progress_fps'],
                                  $o['progress_mode'],
                                  $o['err'],
                                  $this->TIMEZONE . " " . $o['stime'],
                                  $this->TIMEZONE . " " . $o['ltime'],
                                  $o['length'],
                                  $o['message']);
        }
      }
      $x_timestamp = $x_noti->addChild("Timestamp", $this->TIMESTAMP);
      return true;
  }
  public function noti_worker_dead($ev, $list) {
      $x_noti = $this->dom->addChild("Response");
      $x_noti->addAttribute("Command", $ev);
      $x_noti->addChild("Status", "OK");
      if (isset($list)) {
        foreach ($list as $worker) {
          $x_worker = $x_noti->addChild("Worker");
          $x_worker->addChild("WorkerID", $worker['id']);
          $x_worker->addChild("UUID",     $worker['uuid']);
          $x_worker->addChild("Name",     $worker['name']);
          $x_worker->addChild("DeadTime", $worker['dtime']);
        }
      }
      $x_timestamp = $x_noti->addChild("Timestamp", $this->TIMESTAMP);
      return true;
  }

  ///////////////////////////////////////////////////
  // Helper
  ///////////////////////////////////////////////////
  private function write_capacity($parent, $unit, $capacity) {
      $x_capacity = $parent->addChild("CapacityReport");
      $x_capacity->addAttribute("Unit", $unit);
      $x_capacity->addAttribute("Value", $capacity);
      return $x_capacity;
  }
  private function write_job_info($parent, $job_id, $est_time) {
      $x_job = $parent->addChild("JobID", $job_id);
      // if ( isset($est_time)) {
      //   $x_est = $parent->addChild("EstimatedTime", $est_time);
      // }
      return $x_job;
  }
  private function write_job_output($parent, $o_num, $filename, $container, $profile, $stage, $progress, $progress_fps, $progress_mode, $err, $stime, $ltime, $length, $nodemess) {
      $x_output = $parent->addChild("OutputJob");
      $x_output->addChild("OutputNumber", $o_num);
      $x_output->addChild("OutputFile", $filename);
      if (trim($container) != "")
      	$x_output->addChild("OutputContainer", $container);
      $x_output->addChild("DeviceOutputProfile", $profile);
      $x_output->addChild("EncodeLength", $length);
      if ( $stage == 0) {
        $x_output->addChild("StartEncodeTime", "");
        $x_output->addChild("LastUpdateTime", "");
      } else {
        $x_output->addChild("StartEncodeTime", $stime);
        $x_output->addChild("LastUpdateTime", $ltime);
      }
      $x_output->addChild("JobStatus", $this->STAGE_NAME[$stage+1]);
      if ( $stage >= 0) {
        $x_output->addChild("EncodePass", $progress_mode);
        $x_output->addChild("Value", $progress);
        $x_output->addChild("FramePerSecond", $progress_fps);
      } else if ( isset($err) && $err!=0 ) {
        $e = new VisaError();
        $x_output->addChild("Value", $e->get_desc($err));
	if ($err > 5)
        	$x_output->addChild("ErrorGroup", 500);
	else if (substr($nodemess, 0, 6) == 'error]')
        	$x_output->addChild("ErrorGroup", 500);
	else if (substr($nodemess, 0, 6) == '[error')
        	$x_output->addChild("ErrorGroup", 500);
	else
        	$x_output->addChild("ErrorGroup", 400);
      } else {
        $e = new VisaError();
        $x_output->addChild("Value", $e->get_desc(VisaError::ERR_INTERNAL));
        $x_output->addChild("ErrorGroup", 500);
      }
      if (substr($nodemess, 0, 6) == 'error]') $nodemess = "[" . $nodemess;
      $x_output->addChild("NodeMessage", $nodemess);
      return $x_output;
  }


  ///////////////////////////////////////////////////
  // Worker Specific
  ///////////////////////////////////////////////////
  public function response_worker_heartbeat($command, $result, $shutdown) {
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_response->addChild("Status", $result);
      if ( isset($shutdown) ) {
        $x_response->addChild("Shutdown", $shutdown ? 'true' : 'false');
      }
      return true;
  }
  public function response_worker_getstate($command, $result, $state) {
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_response->addChild("Status", $result);
      if ( isset($state) ) {
        $x_response->addChild("State", $state);
      }
      return true;
  }
  public function response_worker_pick($command, $result, $list) {
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_response->addChild("Status", $result);
      $x_output = $x_response->addChild("Output");
      foreach ($list as $o) {
       if ($o['mode'] == 'S'){
        $x_o = $x_output->addChild("OutputJob");
        $x_o->addChild("JobID", $o['job_id']);
        $x_o->addChild("OutputID", $o['out_id']);
        $x_o->addChild("SplitID", $o['sp_id']);
        $x_o->addChild("SplitSrcType", $o['sp_tstype']);
        $x_o->addChild("SplitSrcTotal", $o['sp_tsttl']);
        $x_o->addChild("SplitSrcPath", $o['sp_srcpath']);
        $x_o->addChild("SplitSrcFile", $o['sp_srcfile']);
        $x_o->addChild("SplitDstPath", $o['sp_dstpath']);
        $x_o->addChild("SplitDstFile", $o['sp_dstfile']);
        $x_o->addChild("OutputNumber", $o['o_num']);
        $x_o->addChild("Profile", $o['profile']);
	$para = "";
	if ($o['videobit'] > 0)
		$para .= '-videobitrate ' . $o['videobit'] . ' ';
	if ($o['audiobit'] > 0)
		$para .= '-audiobitrate ' . $o['audiobit'] . ' ';
	if ($o['resolu'] != '')
		$para .= '-resolution ' . $o['resolu'] . ' ';
	if ($o['deint'] != '')
		$para .= '-deinterlace ' . strtolower($o['deint']) . ' ';
	if ($o['pmode'] != '')
		$para .= '-pass ' . $o['pmode'] . ' ';
        $x_o->addChild("EncodeParameter", $para);
        $x_o->addChild("EncodeMode", $o['encode_mode']);
        $x_o->addChild("EncodeUnit", $o['encode_unit']);
        $x_o->addChild("Mode", $o['mode']);
       } else {
        $x_o = $x_output->addChild("OutputJob");
        $x_o->addChild("OutputID", $o['id']);
        $x_o->addChild("Source", $o['source']);
	if ($o['length'] > 0)
        	$x_o->addChild("Duration", $o['length']);
	else
        	$x_o->addChild("Duration", $o['source_duration']);
        $x_o->addChild("Destination", $o['destination']);
        $x_o->addChild("JobID", $o['job_id']);
        $x_o->addChild("OutputNumber", $o['o_num']);
        $x_o->addChild("Filename", $o['filename']);
        $x_o->addChild("Profile", $o['profile']);
	$para = "";
	if ($o['container'] != '')
		$para .= '-container ' . $o['container'] . ' ';
	if ($o['videobit'] > 0)
		$para .= '-videobitrate ' . $o['videobit'] . ' ';
	if ($o['audiobit'] > 0)
		$para .= '-audiobitrate ' . $o['audiobit'] . ' ';
	if ($o['resolu'] != '')
		$para .= '-resolution ' . $o['resolu'] . ' ';
	if ($o['deint'] != '')
		$para .= '-deinterlace ' . strtolower($o['deint']) . ' ';
	if ($o['pmode'] != '')
		$para .= '-pass ' . $o['pmode'] . ' ';
	if ($o['length'] > 0)
		$para .= '-duration ' . $o['length'] . ' ';
        $x_o->addChild("EncodeParameter", $para);
        $x_o->addChild("Segmentor", $o['segmode']);
        $x_o->addChild("Priority", $o['priority']);
        $x_o->addChild("EncodeMode", $o['encode_mode']);
        $x_o->addChild("EncodeUnit", $o['encode_unit']);
        $x_o->addChild("Mode", $o['mode']);
       }
      }
      return true;
  }
  public function response_worker_getcancel($command, $result, $list) {
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_response->addChild("Status", $result);
      $x_output = $x_response->addChild("Output");
      foreach ($list as $o) {
        $x_o = $x_output->addChild("OutputJob");
        $x_o->addChild("OutputID", $o['id']);
        $x_o->addChild("JobID", $o['job_id']);
        $x_o->addChild("OutputNumber", $o['o_num']);
        $x_o->addChild("Stage", $o['stage']);
      }
      return true;
  }
  public function response_worker_spgetcancel($command, $result, $list) {
      $x_response = $this->dom->addChild("Response");
      $x_response->addAttribute("Command", $command);
      $x_response->addChild("Status", $result);
      $x_output = $x_response->addChild("Output");
      foreach ($list as $o) {
        $x_o = $x_output->addChild("OutputJob");
        $x_o->addChild("SplitID", $o['id']);
        $x_o->addChild("OutputID", $o['output_id']);
        $x_o->addChild("JobID", $o['job_id']);
        $x_o->addChild("TrunkType", $o['trunk_type']);
        $x_o->addChild("Stage", $o['stage']);
      }
      return true;
  }

}

?>
