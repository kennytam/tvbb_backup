<?php
//
// VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
//

class VisaNotification {
  const NOTI_CAPACITY_UPDATE    = 1;
  const NOTI_JOB_UPDATE         = 2;
  const NOTI_WORKER_DEAD        = 3;
  const NOTI_UN_CAPACITY_UPDATE = 4;
  const NOTI_UN_JOB_UPDATE      = 5;
  const NOTI_URL_CAPACITY_UPDATE = 6;
  const NOTI_URL_JOB_UPDATE      = 7;

  protected $TBL;
  function __construct() {
    $this->TBL = array(
    	'Unused',
    	'CapacityUpdateTrigger',
    	'StatusUpdateTrigger',
    	'WorkerDead',
    	'CapacityUpdateUnTrigger',
    	'StatusUpdateUnTrigger',
    	'CapacityUpdateURLTrigger',
    	'StatusUpdateURLTrigger',
    );
  }
  public function get_text($code) {
      foreach ($this->TBL as $key => $value) {
        if ( $key == $code ) return $value;
      }
      return $this->TBL[0];
  }
  public function get_code($text) {
      foreach ($this->TBL as $key => $value) {
        if ( $value == $text ){
	  if ($key == 1 || $key == 4 || $key == 6)
		return 1;
	  else if ($key == 2 || $key == 5 || $key == 7) 
		return 2;
	  else
		return $key;
	}
      }
      return 0;
  }
};

?>
