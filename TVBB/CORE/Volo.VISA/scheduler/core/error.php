<?php
//
// VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
//

class VisaError {
  const ERR_OK             = 0;
  const ERR_INTERNAL       = 1;
  const ERR_UNSUPPORTED    = 2;
  const ERR_INVALID_PARAM  = 3;
  const ERR_JOB_NOTFOUND   = 4;
  const ERR_NOCAPACITY     = 5;
  const ERR_FILE_NOT_FOUND = 6;
  const ERR_JOB_NOTRUNNING = 7;
  const ERR_SOURCE_TIMEOUT = 8;
  const ERR_ENCODE_PARAMETER = 9;

  protected $TBL;
  function __construct() {
    $this->TBL = array(
      array ( 'retry' => true,  'desc' => 'OK' ),
      array ( 'retry' => true,  'desc' => 'Internal Error' ),
      array ( 'retry' => false, 'desc' => 'Invalid Command' ),
      array ( 'retry' => false, 'desc' => 'Invalid Parameters' ),
      array ( 'retry' => false, 'desc' => 'Job Not Found' ),
      array ( 'retry' => true,  'desc' => 'Scheduler No Capacity' ),
      array ( 'retry' => false, 'desc' => 'File Not Found' ),
      array ( 'retry' => false, 'desc' => 'Job Not Queued or Encoding' ),
      array ( 'retry' => false, 'desc' => 'Source Video Analysis Timeout' ),
      array ( 'retry' => false, 'desc' => 'Invalid Encode Parameter Set' ),
    );
  }
  
  public function get_retry($err) { return $this->TBL[$err]['retry']; }
  public function get_desc($err)  { return $this->TBL[$err]['desc'];  }
};

?>
