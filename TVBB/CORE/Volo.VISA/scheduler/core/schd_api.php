<?php
//
// VISA v1.3.4 Build Fri May 01 23:51:12 HKT 2015 (DaoLab)
//
require_once("error.php");
require_once("schd.php");
require_once("media/mediainfo.php");


class VisaSchedulerAPI {
  protected $scheduler;

  //! Constructor & Destructor
  function __construct() {
    $this->scheduler = new VisaScheduler();
  }
  function __destruct() {
      if ( isset( $this->scheduler) ) unset ( $this->scheduler );
  }

  ///////////////////////////////////////////////////
  // Entrance
  ///////////////////////////////////////////////////
  public function process($writer,$request) {
      $cmd = $request['Command'];
      if (strcasecmp($cmd, "CapacityCheck")==0) {
        return $this->do_capacity_check($writer,$request);
      } elseif (strcasecmp($cmd, "JobSubmit")==0) {
        return $this->do_job_submit($writer,$request);
      } elseif (strcasecmp($cmd, "JobStatusQuery")==0) {
        return $this->do_job_query($writer,$request);
      } elseif (strcasecmp($cmd, "JobControl")==0) {
        return $this->do_job_control($writer,$request);
      } elseif (strcasecmp($cmd, "EncodeStart")==0) {
        return $this->do_encode_start($writer,$request);
      } elseif (strcasecmp($cmd, "MediaInfo")==0) {
        return $this->do_media_info($writer,$request);
      } elseif (strcasecmp($cmd, "ClearUpdateTrigger")==0) {
        return $this->do_noti_clear($writer,$request);
      } elseif (strcasecmp($cmd, "StatusUpdateURLTrigger")==0) {
        return $this->do_noti_geturl($writer,$request);
      } elseif (strcasecmp($cmd, "CapacityUpdateURLTrigger")==0) {
        return $this->do_noti_geturl($writer,$request);
      } elseif (strcasecmp($cmd, "StatusUpdateTrigger")==0) {
        return $this->do_noti_register($writer,$request);
      } elseif (strcasecmp($cmd, "CapacityUpdateTrigger")==0) {
        return $this->do_noti_register($writer,$request);
      } elseif (strcasecmp($cmd, "StatusUpdateUnTrigger")==0) {
        return $this->do_noti_unregister($writer,$request);
      } elseif (strcasecmp($cmd, "CapacityUpdateUnTrigger")==0) {
        return $this->do_noti_unregister($writer,$request);
      } else {
        $writer->response_err($cmd, VisaError::ERR_UNSUPPORTED);
        return false;
      }
  }

  ///////////////////////////////////////////////////
  // Misc Functions
  ///////////////////////////////////////////////////
  private function do_media_info($writer, $request) {
      $cmd  = (string)$request['Command'];
      $file = (string)$request->SourceUri;

      $info = MediaInfo::get($file);
      if ( $info['success'] ) {
        $writer->response_media_info($cmd, "OK", $info);
      } else {
        $writer->response_err($cmd, VisaError::ERR_FILE_NOT_FOUND);
      }
      return true;
  }

  ///////////////////////////////////////////////////
  // Capacity Functions
  ///////////////////////////////////////////////////
  private function do_capacity_check($writer, $request) {
      $cmd  = (string)$request['Command'];
      $unit = (string)$request->Unit;
      
      if ( strcasecmp($unit, 'JobSlot') == 0 ) {
        $capacity = $this->scheduler->capacity_jobslot();
        $writer->response_capacity_check($cmd, "OK", $unit, $capacity);
        return true;
      }
      $writer->response_err($cmd, VisaError::ERR_INVALID_PARAM);
      return false;
  }

  ///////////////////////////////////////////////////
  // Job Functions
  ///////////////////////////////////////////////////
  private function do_job_control($writer, $request) {
      $cmd    = (string)$request['Command'];
      $job_id = intval ( $request->JobID );
      $op     = $request->Operation;

      if ( strcasecmp($op, "Abort")==0 ) {
        $rez = $this->scheduler->job_not_cancel($job_id);
        if ( $rez ) {
          $this->scheduler->job_cancel($job_id);
          $writer->response_ok($cmd);
        } else {
          $writer->response_err($cmd, VisaError::ERR_JOB_NOTRUNNING);
        }
      } else if ( strcasecmp($op, "RetryJob")==0 ) {
          $this->scheduler->job_retry($job_id,"RetryJob");
          $writer->response_ok($cmd);
      } else if ( strcasecmp($op, "RetryFail")==0 ) {
          $this->scheduler->job_retry($job_id,"RetryFail");
          $writer->response_ok($cmd);
      } else {
        $writer->response_err($cmd, VisaError::ERR_INVALID_PARAM);
      }

      return true;
  }
  private function do_encode_start($writer, $request) {
      $cmd    = (string)$request['Command'];
      $o_id = intval ( $request->OutputID );
      $this->scheduler->encode_start($o_id);
      $writer->response_ok($cmd);
      return true;
  }
  private function do_job_submit($writer, $request) {
      $cmd         = (string)$request['Command'];
      $source      = (string)$request->SourceUri;
      $destination = (string)$request->OutputPath;
      $split_encode= (string)$request->SplitEncode; // true | false
      $priority    = (string)$request->EncodePriority; // 0 - 99
      $source      = trim($source);
      $destination = trim($destination);
      $split_encode= strtolower(trim($split_encode));
      $priority    = intval(trim($priority));

      $check = $this->scheduler->capacity_check( $request->outoput->count() );
      if ( !$check['allow'] ) {
        $writer->response_err($cmd, VisaError::ERR_NOCAPACITY);
        return false;
      }
      
      // Parse the Source Info by Mediainfo
      $filename = escapeshellarg($source);
      $source_info = shell_exec("mediainfo $filename");

      // Verify Source Info contains Format String
      if ( !strpos($source_info, 'Format') !== false ){
        $writer->response_err($cmd, VisaError::ERR_UNSUPPORTED);
        return false;
      }

      $source_duration = shell_exec("mediainfo --Output=\"Video;%Duration%\" $filename");
      $source_duration = round($source_duration/1000, 0, PHP_ROUND_HALF_UP);

      // Source Detect Timeout Error
      if ( $source_info == '' ) {
        $writer->response_err($cmd, VisaError::ERR_SOURCE_TIMEOUT);
        return false;
      }

      // Create Job
      $job_id = $this->scheduler->job_create($source, $destination, $source_info, $source_duration);

      foreach($request->Output->children() as $o){
	// Init
	$o_num = $vdbit = $adbit = $length = $encode_mode = 0;
	$file = $container = $profile = $resolu = $deint = $pmode = $segmode = '';

        $o_num   = trim($o->OutputNumber);
        $encode_mode = trim($o->EncodeMode); // 0 - H.264, 1 - H.265
        $file    = trim($o->OutputFile);
        $container = trim($o->OutputContainer);
        $profile = trim($o->DeviceOutputProfile);
        $vdbit   = intval(trim($o->VideoBitrate));
        $adbit   = intval(trim($o->AudioBitrate));
        $resolu  = trim($o->Resolution);
        $deint   = strtolower(trim($o->Deinterlace));
        $pmode   = trim($o->PassMode);
        $segmode = trim($o->Segmentor);
        $length  = intval(trim($o->EncodeLength));

	// Field Validation
	if ($vdbit <= 0) $vdbit = 0;
	if ($adbit <= 0) $adbit = 0;
	if ($deint != 'off' && $deint != 'on')
		$deint = '';
	if ($resolu != ''){
		list($width, $height) = split("x", $resolu, 2);
		$width=trim($width);
		$height=trim($height);
		if (intval($width) <= 0)
			$resolu = '';
		else if (intval($height) <= 0 || strtolower($height) == 'auto')
			$resolu = $width . "xAuto";
		else
			$resolu = $width . "x" . $height;
	}
	if ($pmode != '1' && $pmode != '2') $pmode = '';
	if ($split_encode != 'true')
		if ($length < 60 || ($source_duration > 0 && $length >= $source_duration)) $length = 0;

	// Output Job Validation
        if ( $profile == '' || $file == '' ) {
		$writer->response_err($cmd, VisaError::ERR_ENCODE_PARAMETER);
		return false;
        }

	// Submit Output Job
        $oudput_id = $this->scheduler->job_addoutput($job_id, $o_num, $file, $container, $profile, $vdbit, $adbit, $resolu, $deint, $pmode, $segmode, $length, $encode_mode, $priority, $split_encode);
      }

      $unit = "JobSlot";
      $writer->response_job_submit($cmd, "OK", $unit, $check['capacity'], $job_id, $source_duration);
      return true;
  }
  private function do_job_query($writer, $request) {
      $cmd =(string) $request['Command'];
      $job_id = intval ( $request->JobID );

      $meta = $this->scheduler->job_meta($job_id);
      $outputs = $this->scheduler->job_query($job_id);
      if ( count($outputs) > 0 ) {
        $writer->response_job_query($cmd, "OK", $job_id, $meta, $outputs);
      } else {
        $writer->response_err($cmd, VisaError::ERR_JOB_NOTFOUND);
      }
      return true;
  }

  ///////////////////////////////////////////////////
  // Notification Functions
  ///////////////////////////////////////////////////
  private function do_noti_clear($writer, $request) {
      $cmd = (string)$request['Command'];

      $rez = $this->scheduler->noti_clear();
      if ( $rez ) {
        $writer->response_ok($cmd, "OK");
      } else {
        $writer->response_err($cmd, VisaError::ERR_INTERNAL);
      }
      return true;
  }
  private function do_noti_geturl($writer, $request) {
      $cmd = (string)$request['Command'];

      $url = $this->scheduler->noti_geturl($cmd);
      $writer->response_noti_geturl($cmd, "OK", $url);
      return true;
  }
  private function do_noti_register($writer, $request) {
      $cmd = (string)$request['Command'];
      $url = (string)$request->Url;

      $rez = $this->scheduler->noti_register($cmd,$url);
      if ( $rez ) {
        $writer->response_ok($cmd, "OK");
      } else {
        $writer->response_err($cmd, VisaError::ERR_INTERNAL);
      }
      return true;
  }
  private function do_noti_unregister($writer, $request) {
      $cmd = (string)$request['Command'];
      $url = (string)$request->Url;
      
      $rez = $this->scheduler->noti_unregister($cmd,$url);
      if ( $rez ) {
        $writer->response_ok($cmd, "OK");
      } else {
        $writer->response_err($cmd, VisaError::ERR_INTERNAL);
      }
      return true;
  }

};

?>
