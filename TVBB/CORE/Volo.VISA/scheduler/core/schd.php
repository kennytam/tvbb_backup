<?php
//
// VISA v1.3.4 Build Fri May 01 23:51:12 HKT 2015 (DaoLab)
//
require_once("constant.php");
require_once("notification.php");
require_once("proto_xml.php");
require_once("database/schd_db.php");


class VisaScheduler {
  protected $config;
  protected $visadb;
  protected $noti;

  ///////////////////////////////////////////////////
  // Constructor & Destructor
  ///////////////////////////////////////////////////
  function __construct() {
    $this->config = include('config/scheduler.php');
    $this->noti = new VisaNotification();
    $this->visadb = new VisaDB();
  }
  function __destruct() {
      if ( isset( $this->visadb) ) unset ( $this->visadb);
      if ( isset( $this->noti) ) unset ( $this->noti);
      if ( isset( $this->config) ) unset ( $this->config);
  }

  ///////////////////////////////////////////////////
  // Get Capacity with JobSlot unit
  ///////////////////////////////////////////////////
  protected function capacity_raw() {
      $list = $this->visadb->worker_list();
      $capacity = 0;
      foreach ( $list as $worker ) {
        if ( $worker['ready'] ) $capacity ++;
      }
      return $capacity;
  }
  public function capacity_jobslot() {
      $capacity = $this->capacity_raw() - $this->visadb->output_queue_len();
      if ( $capacity < 0 ) $capacity = 0;
      return $capacity;
  }
  public function capacity_check($num_output) {
      $capacity = $this->capacity_raw() - $this->visadb->output_queue_len() - $num_output;
      return array(
          'allow'    => $this->config['schd_queue_size'] > -$capacity,
          'capacity' => $capacity < 0 ? 0 : $capacity
      );
  }
  
  ///////////////////////////////////////////////////
  // Job Functions
  ///////////////////////////////////////////////////
  public function job_create($source,$destination,$source_info,$source_duration) {
      return $this->visadb->job_create($source,$destination,$source_info,$source_duration);
  }
  public function job_not_cancel($job_id) {
      return $this->visadb->job_not_cancel($job_id);
  }
  public function job_cancel($job_id) {
      return $this->visadb->job_cancel($job_id);
  }
  public function job_retry($job_id,$mode) {
      return $this->visadb->job_retry($job_id,$mode);
  }
  public function job_addoutput($job_id,$o_num,$file,$container,$profile,$vdbit,$adbit,$resolu,$deint,$pmode,$segmode,$length,$encode_mode,$priority,$split_encode) {
      return $this->visadb->job_addoutput($job_id,$o_num,$file,$container,$profile,$vdbit,$adbit,$resolu,$deint,$pmode,$segmode,$length,$encode_mode,$priority,$split_encode);
  }
  public function job_meta($job_id) {
      return $this->visadb->job_meta($job_id);
  }
  public function job_query($job_id) {
      return $this->visadb->job_query($job_id);
  }

  ///////////////////////////////////////////////////
  // Output Functions
  ///////////////////////////////////////////////////
  public function encode_start($o_id) {
      return $this->visadb->encode_start($o_id);
  }
  public function output_pick($uuid) {
      $ident = $this->visadb->worker_ident($uuid);
      if ( $ident['id'] == 0 ) return array();
      $list = $this->visadb->output_pick($ident['id'], $this->config['volo_mode'], $this->config['min_slot']);
      if ( count($list) > 0 ) {
        foreach ($list as $o) {
          $this->noti_job_update($o['job_id']);
        }
        $this->visadb->worker_busy($uuid);      
      }
      return $list;
  }
  public function output_update($uuid, $o_id, $stage, $progress, $progress_fps, $progress_mode, $err, $message) {
      $this->visadb->output_update($o_id, $stage, $progress, $progress_fps, $progress_mode, $err, $message);
      $job_id = $this->visadb->output_get_jobid($o_id);
      $this->noti_job_update($job_id);
      return true;
  }
  public function spoutput_update($uuid, $s_id, $trunk_type, $stage, $progress, $progress_fps, $progress_mode, $err, $message) {
      $this->visadb->spoutput_update($s_id, $trunk_type, $stage, $progress, $progress_fps, $progress_mode, $err, $message);
      return true;
  }

  ///////////////////////////////////////////////////
  // Notification Helper
  ///////////////////////////////////////////////////
  protected function noti_send($url, $payload) {
      $options = array(
          'http' => array(
            'header'  => "Content-type: application/xml; charset=utf-8\r\n",
            'method'  => 'POST',
            'content' => $payload,
          ),
      );
      $context  = stream_context_create($options);
      return file_get_contents( $url, false, $context);
  }

  ///////////////////////////////////////////////////
  // Notification Functions
  ///////////////////////////////////////////////////
  public function noti_clear() {
      return $this->visadb->noti_clear();
  }
  public function noti_geturl($ev) {
      $code = $this->noti->get_code($ev);
      $url_list = $this->visadb->noti_geturl($code);
      if ( count($url_list) == 0 ) return '';
      return $url_list[0];
  }
  public function noti_register($ev,$url) {
      $code = $this->noti->get_code($ev);
      if (count($this->visadb->noti_geturl($code)) > 0){
		$this->visadb->noti_unregister($code,$url);
      }
      return $this->visadb->noti_register($code,$url);
  }
  public function noti_unregister($ev,$url) {
      $code = $this->noti->get_code($ev);
      return $this->visadb->noti_unregister($code,$url);
  }
  public function noti_capacity_update() {
      $url_list = $this->visadb->noti_geturl(VisaNotification::NOTI_CAPACITY_UPDATE);
      if ( count($url_list) == 0 ) return true;

      $capacity = $this->capacity_jobslot();

      $writer = new VisaProto_XMLWriter();
      $writer->noti_capacity_update(
          $this->noti->get_text(VisaNotification::NOTI_CAPACITY_UPDATE),
          'JobSlot', $capacity);
      $payload = $writer->toText();
      foreach ($url_list as $url) {
        $this->noti_send($url, $payload);
      }
      return true;
  }
  public function noti_job_update($job_id) {
      if ( $job_id == 0 ) return true;
      $url_list = $this->visadb->noti_geturl(VisaNotification::NOTI_JOB_UPDATE);
      if ( count($url_list) == 0 ) return true;
      // Construct payload
      $meta = $this->job_meta($job_id);
      $outputs = $this->job_query($job_id);
      if ( count($outputs) == 0 ) return true;

      $writer = new VisaProto_XMLWriter();
      $writer->noti_job_update(
          $this->noti->get_text(VisaNotification::NOTI_JOB_UPDATE),
          $job_id, $meta, $outputs);
      $payload = $writer->toText();
      foreach ($url_list as $url) {
        $this->noti_send($url, $payload);
      }
      return true;
  }
  public function noti_worker_dead($list) {
      if ( count($list) == 0 ) return true;
      $url_list = $this->visadb->noti_geturl(VisaNotification::NOTI_WORKER_DEAD);
      if ( count($url_list) == 0 ) return true;
      $writer = new VisaProto_XMLWriter();
      $writer->noti_worker_dead(
          $this->noti->get_text(VisaNotification::NOTI_WORKER_DEAD),
          $list);
      $payload = $writer->toText();
      foreach ($url_list as $url) {
        $this->noti_send($url, $payload);
      }
      return true;
  }

  ///////////////////////////////////////////////////
  // Worker Functions
  ///////////////////////////////////////////////////
  public function worker_deadcheck() {
      $deadlist = $this->visadb->worker_deadcheck();
      foreach ($deadlist as $worker) {
        $this->visadb->log(VisaConstant::LOG_TYPE_WORKER, $worker['id'], VisaConstant::LOG_WORKER_DEAD);
        $outputs = $this->visadb->worker_cleandead($worker['uuid']);
        foreach ($outputs as $o) {
          $this->noti_job_update($o['job_id']);
        }
      }
      $this->noti_worker_dead($deadlist);
      return $deadlist;
  }
  public function worker_join($uuid, $name, $power, $ready) {
      // Clean previous instance
      $outputs = $this->visadb->worker_cleandead($uuid);
      foreach ($outputs as $o) {
        $this->noti_job_update($o['job_id']);
      }      
      $worker_id = $this->visadb->worker_join($uuid, $name, $power, $ready);
      if ( $worker_id ) {
        $this->visadb->log(VisaConstant::LOG_TYPE_WORKER, $worker_id, VisaConstant::LOG_WORKER_UP);
      }
  }
  public function worker_leave($uuid) {
      $worker_id = $this->visadb->worker_leave($uuid);
      if ( $worker_id ) {
        $this->visadb->log(VisaConstant::LOG_TYPE_WORKER, $worker_id, VisaConstant::LOG_WORKER_DOWN);
      }
  }
  public function worker_heartbeat($uuid, $power, $ready) {
      return $this->visadb->worker_heartbeat($uuid, $power, $ready);
  }
  public function worker_getcancel($uuid) {
      return $this->visadb->worker_getcancel($uuid);
  }
  public function worker_spgetcancel($uuid) {
      return $this->visadb->worker_spgetcancel($uuid);
  }
  public function worker_getstate($uuid) {
      return $this->visadb->worker_getstate($uuid);
  }
};

?>
