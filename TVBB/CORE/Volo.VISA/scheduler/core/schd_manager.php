<?php
//
// VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
//
require_once("schd.php");


class VisaSchedulerManager {
  protected $scheduler;

  //! Constructor & Destructor
  function __construct() {
    $this->scheduler = new VisaScheduler();
  }
  function __destruct() {
      if ( isset( $this->scheduler) ) unset ( $this->scheduler);
  }

  //! Command switch
  public function process($writer,$request) {
      $cmd = $request['Command'];
      // $ip = $_SERVER['HTTP_X_FORWARDED_FOR'] ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

      if (strcasecmp($cmd, "Periodic")==0) {
        return $this->do_periodic($writer,$request);
      } elseif (strcasecmp($cmd, "CapacityUpdate")==0) {
        return $this->do_capacity_update($writer,$request);
      } else {
        $writer->response_err($cmd, VisaError::ERR_UNSUPPORTED);
        return false;
      }
  }

  private function do_periodic($writer,$request) {
      $cmd = $request['Command'];
      $this->scheduler->worker_deadcheck();
      $writer->response_ok($cmd);
      return true;
  }

  private function do_capacity_update($writer,$request) {
      $cmd = $request['Command'];
      $this->scheduler->noti_capacity_update();
      $writer->response_ok($cmd);
      return true;
  }
};

?>
