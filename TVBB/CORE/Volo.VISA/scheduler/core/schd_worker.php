<?php
//
// VISA v1.3.4 Build Fri May 01 23:51:12 HKT 2015 (DaoLab)
//
require_once("schd.php");


class VisaSchedulerWorker {
  protected $scheduler;

  //! Constructor & Destructor
  function __construct() {
    $this->scheduler = new VisaScheduler();
  }
  function __destruct() {
      if ( isset( $this->scheduler) ) unset ( $this->scheduler);
  }

  //! Command switch
  public function process($writer,$request) {
      $cmd = $request['Command'];
      if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	$ip = $_SERVER['HTTP_X_FORWARDED_FOR'] ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
      else
	$ip = $_SERVER['REMOTE_ADDR'];

      if (strcasecmp($cmd, "WorkerHeartbeat")==0) {
        return $this->do_worker_heartbeat($writer,$request,$ip);
      } elseif (strcasecmp($cmd, "WorkerGetState")==0) {
        return $this->do_worker_getstate($writer,$request,$ip);
      } elseif (strcasecmp($cmd, "WorkerPick")==0) {
        return $this->do_worker_pick($writer,$request,$ip);
      } elseif (strcasecmp($cmd, "WorkerProgress")==0) {
        return $this->do_worker_progress($writer,$request,$ip);
      } elseif (strcasecmp($cmd, "WorkerSPProgress")==0) {
        return $this->do_worker_spprogress($writer,$request,$ip);
      } elseif (strcasecmp($cmd, "WorkerGetCancel")==0) {
        return $this->do_worker_getcancel($writer,$request,$ip);
      } elseif (strcasecmp($cmd, "WorkerSPGetCancel")==0) {
        return $this->do_worker_spgetcancel($writer,$request,$ip);
      } elseif (strcasecmp($cmd, "WorkerJoin")==0) {
        return $this->do_worker_join($writer,$request,$ip);
      } elseif (strcasecmp($cmd, "WorkerLeave")==0) {
        return $this->do_worker_leave($writer,$request,$ip);
      } else {
        $writer->response_err($cmd, VisaError::ERR_UNSUPPORTED);
        return false;
      }
  }

  // Worker Join
  private function do_worker_join($writer,$request,$uuid) {
      $cmd   = (string)$request['Command'];
      $name  = (string)$request->Name;
      $power = intval($request->Power);
      $ready = (string)$request->Ready;

      $worker_id = $this->scheduler->worker_join($uuid, $name, $power, $ready);
      $writer->response_ok($cmd);
      return true;
  }
  // Worker Leave
  private function do_worker_leave($writer,$request,$uuid) {
      $cmd = (string)$request['Command'];

      $this->scheduler->worker_leave($uuid);
      $writer->response_ok($cmd);
      return true;
  }
  // Heartbeat
  private function do_worker_heartbeat($writer,$request,$uuid) {
      $cmd   = (string)$request['Command'];
      $power = intval($request->Power);
      $ready = (string)$request->Ready;
      
      $shutdown = $this->scheduler->worker_heartbeat($uuid, $power, $ready);
      $writer->response_worker_heartbeat($cmd, "OK", $shutdown);
      return true;
  }
  // State
  private function do_worker_getstate($writer,$request,$uuid) {
      $cmd = (string)$request['Command'];
      
      $state = $this->scheduler->worker_getstate($uuid);     
      $writer->response_worker_getstate($cmd, "OK", $state);
      return true;
  }
  // Pick
  private function do_worker_pick($writer,$request,$uuid) {
      $cmd = (string)$request['Command'];
      
      $list = $this->scheduler->output_pick($uuid);     
      $writer->response_worker_pick($cmd, "OK", $list);
      return true;
  }
  // Progress
  private function do_worker_progress($writer,$request,$uuid) {
      $cmd      = (string)$request['Command'];
      $o_id     = (string)$request->OutputID;
      $stage    = (string)$request->Stage;
      $progress = intval($request->Progress);
      $progress_fps = intval($request->ProgressFPS);
      $progress_mode = intval($request->ProgressMode);
      $err      = intval($request->Error);
      $message  = (string)$request->NodeMessage;
      
      $this->scheduler->output_update($uuid, $o_id, $stage, $progress, $progress_fps, $progress_mode, $err, $message);
      $writer->response_ok($cmd);
      return true;
  }
  // Split Encode Progress
  private function do_worker_spprogress($writer,$request,$uuid) {
      $cmd      = (string)$request['Command'];
      $s_id     = (string)$request->SplitID;
      $trunk_type = (string)$request->TrunkType;
      $stage    = (string)$request->Stage;
      $progress = intval($request->Progress);
      $progress_fps = intval($request->ProgressFPS);
      $progress_mode = intval($request->ProgressMode);
      $err      = intval($request->Error);
      $message  = (string)$request->NodeMessage;
      
      $this->scheduler->spoutput_update($uuid, $s_id, $trunk_type, $stage, $progress, $progress_fps, $progress_mode, $err, $message);
      $writer->response_ok($cmd);
      return true;
  }
  // GetCancel for Normal Encode Job
  private function do_worker_getcancel($writer,$request,$uuid) {
      $cmd   = (string)$request['Command'];
      
      $list = $this->scheduler->worker_getcancel($uuid);
      $writer->response_worker_getcancel($cmd, "OK", $list);
      return true;
  }
  // GetCancel for Normal Encode Job
  private function do_worker_spgetcancel($writer,$request,$uuid) {
      $cmd   = (string)$request['Command'];
      
      $list = $this->scheduler->worker_spgetcancel($uuid);
      $writer->response_worker_spgetcancel($cmd, "OK", $list);
      return true;
  }
};

?>
