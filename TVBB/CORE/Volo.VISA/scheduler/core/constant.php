<?php
//
// VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
//

class VisaConstant {
  const LOG_TYPE_WORKER = 1;
  const LOG_TYPE_JOB    = 2;
  const LOG_TYPE_ENCODE = 3;

  const LOG_WORKER_UP        = 1001;
  const LOG_WORKER_DOWN      = 1002;
  const LOG_WORKER_DEAD      = 1003;
  const LOG_JOB_SCHEDULED    = 2001;
  const LOG_ENCODE_STARTED   = 3001;
  const LOG_ENCODE_COMPLETED = 3002;
  const LOG_ENCODE_FAILED    = 3003;
  
  const STAGE_SCHEDULED = 0;
  const STAGE_PROGRESS  = 1;
  const STAGE_COMPLETED = 2;
  const STAGE_FAILED    = -1;
};

?>
