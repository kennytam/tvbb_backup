<?php
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# VISA Worker System Parameter
#
return array(
  'schd_server'  => 'http://172.30.40.111/scheduler/',
  'schd_api'     => 'api.php',
  'schd_worker'  => 'worker.php',
  'schd_manager' => 'management.php',
  'control_path' => '/opt/Volo.VISA/VISA_Control',
  'batch_path'   => '/dev/shm/Volo.VISA/jobs',
  'batch_cmd'    => '/opt/Volo.VISA/client/helper/batchencode.sh',
  'cancel_cmd'   => '/opt/Volo.VISA/client/helper/cancel.sh',
  'encode_cmd'   => '/opt/Volo.VISA/client/helper/encode.sh',
  'polljob_cmd'  => '/opt/Volo.VISA/control/scripts/VISA.WORKER.PollJob.sh',
  'worker_debug_mode'=> true,   // Print DEBUG info
  'worker_pollrate'  => 1,	// Worker Poll Jobs Rate in sec
  'threshold_cpu'    => 10,
  'default_unit'     => 30, // Process unit 240p - 480p ~ 15, 540p ~ 20, 720p-1080p ~ 30
  // SPLIT ENCODING PARAMETER
  'split_cancel_cmd' => '/opt/Volo.VISA/client/helper/splitencode_cancel.sh', // SPLIT ENCODE CANCEL CMD
  'split_video_cmd'  => '/opt/Volo.VISA/client/helper/splitencode.sh', // SPLIT ENCODE VIDEO CMD
  'split_audio_cmd'  => '/opt/Volo.VISA/client/helper/splitencode_audio.sh', // SPLIT ENCODE AUDIO CMD
  'split_path'       => '/dev/shm/Volo.VISA/splitjobs', // VOLO FARM SPLIT WORK PATH
  'audio_map_file'   => 'AUDIO_MAP.INFO', // ENCODE AUDIO MAPPING INDEX
);
?>
