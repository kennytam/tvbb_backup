#!/usr/bin/python
import os

with open('/opt/Volo.VISA/client/helper/mount.csv') as f:
	for line in f.readlines():
		if line.strip() is not '':
			line_split = line.split('|',5)
			if not os.path.ismount(str(line_split[0])):
				os.system('sudo mkdir -p '+line_split[0])
				os.system(line_split[2])
				#print line_split[2]
			if not os.path.ismount(str(line_split[1])):
				os.system('sudo mkdir -p '+line_split[1])
                                os.system(line_split[3])
                                #print line_split[3]
