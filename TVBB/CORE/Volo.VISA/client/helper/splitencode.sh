#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
# Volo.Worker :: Scripts to Spawn Volo Split Encoder Job
# Skip Checking input Parameter due to Batch job Sync
# "USAGE: splitencode.sh [JOB ID] [OUTPUT ID] [SPLIT ID] [SOURCE] [DEST] [PROFILE] [MODE H.264|H.265]"
#
 
cd /opt/Volo.VISA/client/helper
JID=$1
OID=$2
SID=$3
SOURCE=$4
DEST=$5
PROFILE=$6
ENCMODE=$7
PARAFILE=/dev/shm/Volo.VISA/splitjobs/${JID}_${OID}_${SID}.para
 
# Read Optional Paramter from File ** Must Add -vsync 0 for Split Encode
if [ -f "${PARAFILE}" ]
then
	PARA=`cat ${PARAFILE}`
else
	PARA="-vsync 0 -container ts -audio OFF -stitchable Y "
fi
 
# H.264
if [ "${ENCMODE}" == "0" ]
then
#kenny hack star, edit binary
#	VOLO_BIN=/opt/volo/script/volo
	VOLO_BIN=/opt/volo/bin/volo-1.4.1
#kenny hack end
	VOLO_PROBE=/opt/volo/bin/vprobe
	VOLO_BASE=/opt/volo
else
#kenny hack star, edit binary
	VOLO_BIN=/opt/volo-hevc/bin/volo-1.4.1
#	VOLO_BIN=/opt/volo-hevc/script/volo
#kenny hack end
	VOLO_PROBE=/opt/volo-hevc/bin/vprobe-1.4.1
	VOLO_BASE=/opt/volo-hevc
fi
JOB_LABEL=F_${SID}
JOB_PATH=/dev/shm/Volo.VISA/splitjobs/${JOB_LABEL}
JOB_LOG='/opt/Volo.VISA/logs/VISA.JobEncode'.`hostname`.`date +%Y%m%d`.'log'
ERRSTR=""

echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Split Encode Job Submit :: JID[${1}] OID[${2}] SID[${3}] SOURCE[${4}] DEST[${5}] PROFILE[${6}] MODE[${7}] ENCODE PARA[${PARA}] >> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Split Encode Command :: ./splitencode.sh ${1} ${2} ${3} ${4} ${5} ${6} ${7} >> ${JOB_LOG}

#kenny hack start, no need to check
# Profile Not Exist
#if [ ! -f "${VOLO_BASE}/config/users/${PROFILE}.xml" ] && [ ! -f "${VOLO_BASE}/config/samples/${PROFILE}.xml" ]
#then
#        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Split Encode Job Error :: JOB[${OID}] Device Profile[${PROFILE}.xml] Not Found >> ${JOB_LOG}
#        ERRSTR="[ERROR] Device Profile [${PROFILE}.xml] Not Found"
#fi
#kenny hack end
 
# Create Encode Working Path
if [ -d "${JOB_PATH}" ]; then
	mv -f ${JOB_PATH} /opt/Volo.VISA/splitjobs/SPO_Unknown/${JOB_LABEL}.`hostname`.`date +"%Y%m%d%H%M%S"`
fi
mkdir -p ${JOB_PATH}
mv ${PARAFILE} ${JOB_PATH}/
echo ${OID} > ${JOB_PATH}/output_id

# Mark Job ERROR
if [ ! -z "${ERRSTR}" ]; then
	echo ${ERRSTR} > ${JOB_PATH}/encode.log
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

EXT=${SOURCE##*.}
if [ "$EXT" == "jpg" ] || [ "$EXT" == "txt" ]; then
	echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Split Encode Job Error :: JOB[${OID}] Invalid Source Video Format >> ${JOB_LOG}
	ERRSTR="[ERROR] Invalid Source Video Format"
	echo ${ERRSTR} > ${JOB_PATH}/encode.log
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

# Housekeep Destination Path
OUTPATH=`dirname ${DEST}`
if [ ! -d "${OUTPATH}" ]; then
        mkdir -p ${OUTPATH}
else
        rm -rf ${DEST}
fi
N_THREAD=$(less /proc/cpuinfo | grep processor | wc -l)

# Split TS Encode (Workaround OCT-21 for HEVC CUTREE Retry)
echo [`hostname`]Split Encode Start :: `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.start
ls -lh --full-time ${SOURCE} >> ${JOB_PATH}/job.start 2>&1

# Standalone Test
# ODEST=${DEST}
# FNAME="${SOURCE##*/}"
# DNAME="${DEST##*/}"
# SRCLOCAL=/home/daolab/Video.Local
# if [ ! -d "${SRCLOCAL}" ]; then
#         mkdir -p ${SRCLOCAL}
# fi
# CTIME=`date +"%Y-%m-%d %H:%M:%S"`
# cp ${SOURCE} ${SRCLOCAL}/S${SID}_${FNAME}
# echo [`hostname`]Copy Source to Local :: ${CTIME} - `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
# SOURCE=${SRCLOCAL}/S${SID}_${FNAME}
# DEST=${SRCLOCAL}/${DNAME}
# Standalone Test End

for(( s=1; s<4; s++ ))
do
        #kenny hack start, edit output for XDCAM
	#echo [`date +"%Y-%m-%d %H:%M:%S"`] Split Encode Command :: ${VOLO_BIN} -label SPLIT.F_${OID}_${SID} -f ${SOURCE} -o ${DEST} -workpath ${JOB_PATH} -do ${PROFILE}.xml ${PARA} -threads ${N_THREAD} >> ${JOB_PATH}/encode.log
	#${VOLO_BIN} -label SPLIT.F_${OID}_${SID} -f ${SOURCE} -o ${DEST} -workpath ${JOB_PATH} -do ${PROFILE}.xml ${PARA} -threads ${N_THREAD} >> ${JOB_PATH}/encode.log 2>&1
        
        SRCPATH=$(dirname ${SOURCE})
        SRCFILE_ORI=$(cat ${SRCPATH}/SOURCE.txt)
        SRCFILE_PATH=$(dirname ${SRCFILE_ORI})
        SEGTIME_SEQ=$(($(echo $(basename ${SOURCE}) | sed -e  "s/SPTS_[0-9]*_//g" | sed -e "s/.ts//g")-1))
        DURATION=60
        DURATION_BASE=$(cat ${SRCPATH}/DURBASE)
#kenny, please case durcation

        if [ $SEGTIME_SEQ -eq 0 ]
        then
                j1="0"
                j2="0"

        else
		j1=`expr "${SEGTIME_SEQ}"\*"${DURATION}"-"1" | bc`
		j2="1"
        fi

#        echo [`date +"%Y-%m-%d %H:%M:%S"`] Split Encode Command :: ${VOLO_BIN} -label SPLIT.F_${OID}_${SID} -vsync 0 -ss $j1 -i ${SRCFILE_ORI} -ss $j2 -vcodec mpeg2video -minrate 50M -maxrate 50M -flags +ildct+ilme -top 1 -b:v 50M -t ${DURATION} -an -threads 1 -y ${DEST} >> ${JOB_PATH}/encode.log
#        ${VOLO_BIN} -label SPLIT.F_${OID}_${SID} -vsync 0 -ss $j1 -i ${SRCFILE_ORI} -ss $j2 -vcodec mpeg2video -minrate 50M -maxrate 50M -flags +ildct+ilme -top 1 -b:v 50M -t ${DURATION} -an -threads 1 -y ${DEST} >> ${JOB_PATH}/encode.log 2>&1

        if [ ! -f ${SRCPATH}/NOENCODE ]; then
            DURSOURCE=$(cat ${SRCPATH}/DURSOURCE)
            REMAIN=$((DURSOURCE/DURATION+1))
            if [ $REMAIN -lt $DURATION ];then
                DURATION = ${REMAIN}
            fi
            
            #for duration base = 1.001
            j1=$(expr ${j1}*${DURATION_BASE}|bc)
            j2=$(expr ${j2}*${DURATION_BASE}|bc)
            DURATION=$(expr ${DURATION}*${DURATION_BASE}|bc)
            
            #kenny, support IMX50,20180122
            if [ -f ${SRCPATH}/IMX50 ]; then
                CMD="php /opt/Volo.VISA/client/helper/audio_mapping.php -in ${SRCFILE_ORI} -out ${DEST} -map_rule ${SRCPATH}/MAP.INFO -label SPLIT.F_${OID}_${SID} -threads 2 -ss ${j1} -ss2 ${j2} -t ${DURATION} -an -imx"
            else
                CMD="php /opt/Volo.VISA/client/helper/audio_mapping.php -in ${SRCFILE_ORI} -out ${DEST} -map_rule ${SRCPATH}/MAP.INFO -label SPLIT.F_${OID}_${SID} -threads 2 -ss ${j1} -ss2 ${j2} -t ${DURATION} -an -xdcam"
            fi
            #kenny end, support IMX50,20180122

        else
            #this is for fast audio mapping,a hard code, stupid and crazy method, find other method to fix and kill me pls
            #volo encode and pass to mbc by pipe
            DURSOURCE=$(cat ${SRCPATH}/DURSOURCE)
            OLD_DEST=$DEST
            TMP=$(cat ${SRCPATH}/DESTINATION)'/VOLO_TMP/'
            mkdir -p $TMP >> ${JOB_PATH}/encode.log 2>&1
            DEST=${TMP}'/'$(basename ${DEST})
            echo [`date +"%Y-%m-%d %H:%M:%S"`] Split Encode Command :: /opt/Volo.VISA/client/helper/audio_mapping.php -in ${SRCFILE_ORI} -out - -oformat mxf -map_rule ${SRCPATH}/MAP.INFO -cmd_mode -silent_dur ${DURSOURCE} -video_control_copy 1 >> ${JOB_PATH}/encode.log 2>&1
            CMD=$(php /opt/Volo.VISA/client/helper/audio_mapping.php -in ${SRCFILE_ORI} -out - -oformat mxf -map_rule ${SRCPATH}/MAP.INFO -label SPLIT.F_${OID}_${SID} -cmd_mode -silent_dur ${DURSOURCE} -video_control_copy 1 -t ${DURSOURCE} )
            CTIME=$(date +"%F %T.000")
            #CMD+=" | /opt/volo/bin/volombc-1.0 -i - -target xdcamhd422 -vcodec copy -label SPLIT.F_${OID}_${SID} -timecode 01:00:00:00 -metadata creation_time=\"${CTIME}\" -f mxf -an -y ${DEST} -acodec copy -newaudio -map_audio_channel 0:1:0:0:1:0 -acodec copy -newaudio -map_audio_channel 0:2:0:0:2:0 -acodec copy -newaudio -map_audio_channel 0:3:0:0:3:0 -acodec copy -newaudio -map_audio_channel 0:4:0:0:4:0 -acodec copy -newaudio -map_audio_channel 0:5:0:0:5:0 -acodec copy -newaudio -map_audio_channel 0:6:0:0:6:0 -acodec copy -newaudio -map_audio_channel 0:7:0:0:7:0 -acodec copy -newaudio -map_audio_channel 0:8:0:0:8:0"
	    TIMECODE=$(echo -n $(mediainfo ${SRCFILE_ORI} | grep 'Time code of first frame' | tail -1 | sed -e 's/T.*: //g'))

            #kenny, support IMX50,20180122
            if [ -f ${SRCPATH}/IMX50 ]; then
                #/opt/volo_bk/bin/volombc-1.0 -i - -target imx50 -vcodec mpeg2video -acodec copy -y test.mxf
                CMD+=" | /opt/volo/bin/volombc-1.0 -i - -target imx50 -vcodec copy -acodec copy -label SPLIT.F_${OID}_${SID} -timecode ${TIMECODE} -metadata creation_time=\"${CTIME}\" -f mxf -y ${DEST}"
            else
                #xdcam50
                CMD+=" | /opt/volo/bin/volombc-1.0 -i - -target xdcamhd422 -vcodec copy -label SPLIT.F_${OID}_${SID} -timecode ${TIMECODE} -metadata creation_time=\"${CTIME}\" -f mxf -an -y ${DEST} -acodec copy -newaudio -map_audio_channel 0:1:0:0:1:0 -acodec copy -newaudio -map_audio_channel 0:2:0:0:2:0 -acodec copy -newaudio -map_audio_channel 0:3:0:0:3:0 -acodec copy -newaudio -map_audio_channel 0:4:0:0:4:0 -acodec copy -newaudio -map_audio_channel 0:5:0:0:5:0 -acodec copy -newaudio -map_audio_channel 0:6:0:0:6:0 -acodec copy -newaudio -map_audio_channel 0:7:0:0:7:0 -acodec copy -newaudio -map_audio_channel 0:8:0:0:8:0"
            fi
            #kenny end, support IMX50,20180122
        fi

        echo [`date +"%Y-%m-%d %H:%M:%S"`] Split Encode Command :: ${CMD} >> ${JOB_PATH}/encode.log 2>&1

        eval ${CMD} >> ${JOB_PATH}/encode.log 2>&1
        #ln output file if run NOENCODE mode
        if [ -f ${SRCPATH}/NOENCODE ]; then
            ln -s ${DEST} ${OLD_DEST}
        fi
        #kenny hack end

	if [ -f "${DEST}" ]; then
                #20180222, kenny, fast way to detect error
                if [ $(wc -c < "${DEST}") -lt 50 ]; then

                    #kenny start,20180313, move file to error space if setting exists
                    if [ -f ${SRCPATH}/SRCERRPATH ]; then mv ${SRCFILE_ORI} $(cat ${SRCPATH}/SRCERRPATH)/ ; fi
                    #kenny end,20180313

                    rm -rf ${JOB_PATH}/volo_high_compression* 2>&1
                    rm -rf ${JOB_PATH}/*.ts 2>&1
                    echo [ERROR] Split Encode Error1 :: Fail to Encode Output Trunk, Maybe Using Error Setting Profile Or Error Video Source File > ${JOB_PATH}/job.error
                    exit 1
                fi
                #20180222 end
                echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.done
		echo [`hostname`]Split Encode Done :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
		ls -lh --full-time ${DEST} >> ${JOB_PATH}/job.start 2>&1
		# Standalone Test
		# echo [`hostname`]Remove Local Source Start :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
		# rm -rf ${SOURCE}
		# echo [`hostname`]Remove Local Source End :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
		# CTIME=`date +"%Y-%m-%d %H:%M:%S"`
		# mv -f ${DEST} ${ODEST}
		# echo [`hostname`]Copy Encoded TS to Destination :: ${CTIME} - `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
		# Standalone Test End
		exit 0
	fi

	if [ ${s} -lt 3 ]; then
		echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Split Encode Retry Job :: JID[${1}] OID[${2}] SID[${3}] SOURCE[${4}] DEST[${5}] PROFILE[${6}] MODE[${7}] ENCODE PARA[${PARA}] RETRY[${s}] >> ${JOB_LOG}
		echo [`hostname`]Split Encode Retry [$s] :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
		echo [`hostname`]Split Encode Retry [$s] :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/encode.log
		df -kh >> ${JOB_PATH}/job.start 2>&1
		ls -lh --full-time ${JOB_PATH}/ >> ${JOB_PATH}/job.start 2>&1
		tar -zcvf ${JOB_PATH}/SPLIT.${s}.tgz ${JOB_PATH}/volo_high* ${JOB_PATH}/SPLIT.*.log ${JOB_PATH}/job.encode.* >> ${JOB_PATH}/job.start 2>&1
		cp ${JOB_PATH}/SPLIT.${s}.tgz /opt/Volo.VISA/errorjobs/SP_${1}_${2}_${3}-Retry.${s}.tgz
		rm -rf ${JOB_PATH}/SPLIT.*.log 2>&1
		rm -rf ${JOB_PATH}/job.encode.* 2>&1
		# rm -rf ${JOB_PATH}/volo_high_compression* 2>&1
		rm -rf ${JOB_PATH}/*.ts 2>&1
#		sleep $((s*5))
                sleep 5
		ls -lh --full-time ${SOURCE} >> ${JOB_PATH}/job.start 2>&1
		${VOLO_PROBE} -i ${SOURCE} >> ${JOB_PATH}/job.start 2>&1
                echo ${s} > ${JOB_PATH}/job.time
	fi
done

#kenny start,20180313, move file to error space if setting exists
if [ -f ${SRCPATH}/SRCERRPATH ]; then mv ${SRCFILE_ORI} $(cat ${SRCPATH}/SRCERRPATH)/ ; fi
#kenny end,20180313

rm -rf ${JOB_PATH}/volo_high_compression* 2>&1
rm -rf ${JOB_PATH}/*.ts 2>&1
#echo [ERROR] Split Encode Error :: Fail to Encode Output Video Trunk > ${JOB_PATH}/job.error
echo [ERROR] Split Encode Error2 :: Fail to Encode Output Trunk, Maybe Using Error Setting Profile Or Error Video Source File > ${JOB_PATH}/job.error
exit 1

