<?php

//define("VOLOBIN", "/opt/volo/bin/volo-1.4.1");
define('VOLOBIN', '/opt/volo/bin/volo-1.4.1');
define('MBCBIN', '/opt/volo/bin/volombc-1.0');
define('FFSILENTSRC', ' -acodec pcm_s24le -f lavfi -i aevalsrc=0:nb_samples=1920:channel_layout=mono:sample_rate=48000');
//define('ZEROSILENTSRC',' -ar 48000 -acodec pcm_s24le -f s24le -ac 1 -i /dev/zero -target xdcamhd422 ');
define('EBUR', 'loudnorm=I=-23:LRA=1:tp=-1');
define('XDCAM50', '-pix_fmt yuv422p -s 1920x1080 -vcodec mpeg2video -flags +ildct+ilme+cgop -top 1 -dc 10 -intra_vlc 1 -non_linear_quant 1 -qmin 1 -qmax 10 -b:v 50M -minrate 50M -maxrate 50M -bufsize 36408333 -rc_init_occupancy 36408333 -rc_max_vbv_use 1 -g 12 -bf 2 -mpv_flags +strict_gop -aspect 16:9 -sc_threshold 1000000000');
define('IMX50',   '-pix_fmt yuv422p -s 720x608 -vcodec mpeg2video -r 25 -pix_fmt yuv422p -minrate 50M -maxrate 50M -b 50M -intra -flags +ildct+low_delay -dc 10 -intra_vlc 1 -non_linear_quant 1 -ps 1 -qmin 1 -qmax 3 -top 1 -bufsize 2000000 -rc_init_occupancy 2000000 -rc_buf_aggressivity 0.25');


class audioDetails {

    public $sampleRate;
    public $codec;
    public $channels;
    public $bitrate;
    public $selector;
    public $target;
    public $streamId;
    public $src = array();
    public $srcNum;
    public $audioAction;
    public $volume;
    public $adjustment;
}

$audioIn = array();
$audioOut = array();
//$audioAdjustment = "";
$audioAdjustment = array();
$FFCMD = "";

function get_codec_name($tmp) {
    $codec = "";
    switch ($tmp) {
        case 'pcm24':
            $codec = "pcm_s24le";
            break;
        case 'pcm16':
            $codec = "pcm_s16le";
            break;
        case 'pcm32':
            $codec = "pcm_s32le";
            break;
        case 'aac':
            $codec = "libfdk_aac";
            break;
        default :
            $codec = "";
            break;
    }
    return $codec;
}

function get_channel_num($tmp) {
    $channelsNum = 0;
    switch ($tmp) {
        case 'mono':
            $channelsNum = 1;
            break;
        case 'stereo':
            $channelsNum = 2;
            break;
        default :
//            $channelsNum = 0;
            $channelsNum = intval($tmp);
            break;
    }
    return $channelsNum;
}

function get_sample_rate($tmp) {
    $sampleRate = str_replace("khz", "000", $tmp);
    return is_numeric($sampleRate) ? intval($sampleRate) : 0;
}

function get_bitrate($tmp) {
    $sampleRate = str_replace("kbps", "000", $tmp);
    return is_numeric($sampleRate) ? $sampleRate : 0;
}

function read_xml($xml_file) {
    $xml = simplexml_load_file($xml_file) or die("Read XML ERROR: Cannot create object");
//    return strtolower($xml);
    return $xml;
}

function get_ini_val($ch) {
    $details = new audioDetails();
    $details->codec = get_codec_name(strtolower($ch['codec']));
    $details->channels = get_channel_num(strtolower($ch['scope']));
    $details->sampleRate = get_sample_rate(strtolower($ch['samplingRate']));
    $details->bitrate = get_bitrate(strtolower($ch['bitRate']));
    $details->streamId = is_numeric((string) $ch['track']) ? intVal($ch['track']) - 1 : 0;
    //add volume here,20171211
    $details->volume = strcmp($ch['volume'], "") == 0 ?  '' : $ch['volume'];
    $details->adjustment = strcmp($ch['adjustment'], "") == 0 ?  '' : strtolower($ch['adjustment']);		
    $details->srcNum = 0; //assume only one input src now
    return $details;
}

function empty_audio_src() {
    $details = new audioDetails();
    $details->srcNum = 1;
    $details->streamId = 0;
    return $details;
}

function parse_xml_data($xml) {

    global $audioOut;
    global $audioIn;
    global $audioAdjustment;

    foreach ($xml->children() as $child) {
        if (strcmp($child->getName(), "buses") == 0 && strcmp(strtolower($child->children()->getName()), "channelmap") == 0) {

            //get audio justment string
            if (strcmp($child->children()[0]['volume'], "") != 0)
                //$audioAdjustment .= strcmp($audioAdjustment, "") == 0 ? 'volume=' . $child->children()[0]['volume'] : ',volume=' . $child->children()[0]['volume'];
		$audioAdjustment['volume'] = $child->children()[0]['volume'];
            if (strcmp($child->children()[0]['adjustment'], "") != 0)
                //$audioAdjustment .= strcmp($audioAdjustment, "") == 0 ? EBUR : ',' . EBUR;
		$audioAdjustment['ebur'] = EBUR;

            foreach ($child->children()->children() as $ch) {
                $details = get_ini_val($ch);

                //add silent if no map rule
                if (strcmp(strtolower($ch->children()->getName()), "maprule") != 0) {
                    fwrite(STDERR, "[WARN] no map rule,use silent source\n");
                    if (count($details->src) == 0)
                        $details->src[] = (array) (empty_audio_src()); //0 is silent
                    $audioOut[] = $details;
                    continue;
                }

                $mapRule = $ch->children();

                foreach ($mapRule->children() as $in) {
                    $track = (string) $in['track'];
                    if (strcmp(strtolower($track), "silent") == 0) {
                        $details->src[] = (array) (empty_audio_src());
                    } else if (is_numeric($track)) {
                        $in_src = new audioDetails();
                        $in_src->srcNum = 0;
                        $in_src->streamId = intval($track) - 1;
                        $in_src->selector = is_numeric((string) $in['selector']) ? intval($in['selector'])-1 : 0;
                        $details->src[] = (array) ($in_src);
                    }
                }

                $num_src = count($details->src);
                if ($num_src == 0) {
                    $details->src[] = (array) (empty_audio_src()); //0 is silent
                }

                if ($num_src == 1)
                    $details->audioAction = "redirect";
                else if ($num_src > 1)
                    $details->audioAction = "mix";
                $audioOut[] = $details;
            }
        }
    }
}

function parse_ffmpeg_audio_details_to_output($out) {
    $outStr = "";

    if (!is_null($out['codec']))
        $outStr .= ' -c:a:' . $out['streamId'] . ' ' . $out['codec'];

    if ($out['sampleRate'] != 0)
        $outStr .= ' -ar:a:' . $out['streamId'] . ' ' . $out['sampleRate'];

    if ($out['channels'] != 0)
        $outStr .= ' -ac:a:' . $out['streamId'] . ' ' . $out['channels'];

    if ($out['bitrate'] != 0)
        $outStr .= ' -ab:a:' . $out['streamId'] . ' ' . str_replace("000", "k", $out['bitrate']);

    return $outStr;
}

function parse_to_ffmpeg_cmd_ini_output_simple($out) {
    $output = "";

    $src = $out['src'];
    $output .= $src[0] == 0 ? ' -map 1:a:0 ' : ' -map ' . $src[0]['srcNum'] . ':a:' . $src[0]['streamId'];
    $output .= parse_ffmpeg_audio_details_to_output($out);


    return $output;
}

function parse_to_ffmpeg_cmd_ini_output_complex($out) {
    $output = "";

    $output .= ' -map "[aout' . $out['streamId'] . ']" ';
    $output .= parse_ffmpeg_audio_details_to_output($out);

    return $output;
}

function parse_to_ffmpeg_cmd_ini_filtercomplex($out) {
    $filter_complex_str = "";

    $src = $out['src'];
    global $args;
    global $audioAdjustment;
    
    if(count($src) > 0){
        
        if($out['channels'] < 3){//a stupid method to deteminer XDCAM50 and IMX50
            foreach ($src as $tmp){
                $filter_complex_str .= ' [' . $tmp['srcNum'] . ':a:' . $tmp['streamId'] . ']';
            }
            if(count($src) > 1 )
                $filter_complex_str .= 'amix=inputs='.count($src);
        }else {
            $filter_complex_str .= '[' . $src[0]['srcNum'] . ':a:' . $src[0]['streamId'] . ']';
            $filter_complex_str .= 'pan='.$out['channels'].'c';
            for($i=0;$i<$out['channels'];$i++){
                $filter_complex_str .= '|c'.$i.'=c'.$src[$i]['selector'];
            }
        }
        
        
        
        //if (strcmp($audioAdjustment, "") != 0)
        //    $filter_complex_str .= (count($src) > 1 ) ? ',' . $audioAdjustment : $audioAdjustment ;

	//add audio adjustment	
        //volume
        if(isset($out['volume']) && strlen($out['volume'])){
            $filter_complex_str .= (count($src) > 1 ) ? ',volume=' . $out['volume'] : 'volume=' . $out['volume'];
        }else if(isset($audioAdjustment['volume'])){
            $filter_complex_str .= (count($src) > 1 ) ? ',volume=' . $audioAdjustment['volume'] : 'volume=' . $audioAdjustment['volume'];
	}
	//ebur
	if(isset($out['adjustment']) && strlen($out['adjustment']) ){
            $filter_complex_str .= (count($src) > 1 ) ? ',' . EBUR : EBUR;
	}else if(isset($audioAdjustment['ebur'])){
            $filter_complex_str .= (count($src) > 1 ) ? ',' . EBUR : EBUR;
	}
        $filter_complex_str .= '[aout' . $out['streamId'] . ']';
    }else{
        fwrite(STDERR, '[ERR]Filter_complex Error, Empty Input Source' . PHP_EOL);
    }

//    if (count($src) == 1) {
//        $filter_complex_str .= '[' . $src[0]['srcNum'] . ':a:' . $src[0]['streamId'] . ']';
//        if ($src[0]['selector'] > 0)
//            $filter_complex_str .= 'pan=1c|c0=c' . ($src[0]['selector'] - 1);
//        if (strcmp($audioAdjustment, "") != 0) {
//            if ($src[0]['selector'] > 0)
//                $filter_complex_str .= ',';
//            $filter_complex_str .= $audioAdjustment;
//        }
//        $filter_complex_str .= '[aout' . $out['streamId'] . ']';
//    }else if (count($src) == 2) {
//        if ($src[0]['streamId'] != $src[1]['streamId']) {//not in a sample stream
//            $filter_complex_str .= ' [' . $src[0]['srcNum'] . ':a:' . $src[0]['streamId'] . ']';
//            $filter_complex_str .= '[' . $src[1]['srcNum'] . ':a:' . $src[1]['streamId'] . ']';
//            $filter_complex_str .= 'amerge=inputs=2';
//        } else {//more than one input src in sample stream
//            $filter_complex_str .= '[' . $src[0]['srcNum'] . ':a:' . $src[0]['streamId'] . ']';
//            $filter_complex_str .= 'pan=stereo|c0=c' . $src[0]['selector'] . '|c1=c' . $src[1]['selector'];
//        }
//        if (strcmp($audioAdjustment, "") != 0)
//            $filter_complex_str .= ',' . $audioAdjustment;
//        $filter_complex_str .= '[aout' . $out['streamId'] . ']';
//    } else {
//        fwrite(STDERR, '[ERR]filter_complex not support, input src over 2' . PHP_EOL);
//    }

    return $filter_complex_str;
}

function parse_to_ffmpeg_cmd() {
    global $audioOut;
    global $FFCMD;
    global $args;
    global $audioAdjustment;

    $FFGlobalObj = "";
    $FFSilentSrc = "";
    $FFFilterComplex = "";
    $FFOutput = "";

    if (!isset($args['an'])) {
        foreach ($audioOut as $tmp) {
            $out = (array) $tmp;

            if (count($audioAdjustment) != 0 || strlen($out['volume']) || strlen($out['adjustment'])) { //audio adjustment must use filter_complex
                $FFFilterComplex .= strcmp($FFFilterComplex, "") == 0 ? parse_to_ffmpeg_cmd_ini_filtercomplex($out) : ',' . parse_to_ffmpeg_cmd_ini_filtercomplex($out);
                $FFOutput .= parse_to_ffmpeg_cmd_ini_output_complex($out);
            } else {
                if (strcmp($out['audioAction'], "mix") == 0) {
                    $FFFilterComplex .= strcmp($FFFilterComplex, "") == 0 ? parse_to_ffmpeg_cmd_ini_filtercomplex($out) : ',' . parse_to_ffmpeg_cmd_ini_filtercomplex($out);
                    $FFOutput .= parse_to_ffmpeg_cmd_ini_output_complex($out);
                }

                if (strcmp($out['audioAction'], "redirect") == 0) {
                    $FFOutput .= parse_to_ffmpeg_cmd_ini_output_simple($out);
                }
            }
            if ($out['src'][0]['srcNum'] == 1)//1 is empty audio src
                $FFSilentSrc = FFSILENTSRC;
        }
    }

    if (strcmp($FFFilterComplex, "") != 0)
        $FFFilterComplex = ' -filter_complex "' . $FFFilterComplex . '"';

    //add duration for slient source, shortest is dangours
    if(isset($args['silent_dur']) && $FFSilentSrc)
        $FFSilentSrc .= $args['silent_dur'] ? ':d=' . $args['silent_dur'] . ' ' : ' ';

    //generate cmd
    $FFCMD .= VOLOBIN . $FFGlobalObj;
    if(isset($args['ss'])) $FFCMD .=' -ss ' . $args['ss'];
    $FFCMD .= ' -i ' . $args['videoSrc'];
    if(isset($args['label'])) $FFCMD .=' -label ' . $args['label'];
    if(strcmp($FFSilentSrc, "") != 0) $FFCMD .= $FFSilentSrc;
    if(isset($args['ss2'])) $FFCMD .=' -ss ' . $args['ss2'];
    if(isset($args['xdcam'])) $FFCMD .=' -map 0:v ' . XDCAM50 . ' ';
    if(isset($args['imx'])) $FFCMD .=' -map 0:v ' . IMX50 . ' ';
    if(isset($args['threads'])) $FFCMD .= ' -threads ' . $args['threads'];
    if(isset($args['t'])) $FFCMD .= ' -t ' . $args['t'];
    if(isset($args['videoControlCopy'])) $FFCMD .= ' -map 0:v -vcodec copy ';
    if(isset($args['vn'])) $FFCMD .= ' -map 0:v -vn ';
//    $FFCMD .= $FFFilterComplex;
    $FFCMD .= isset($args['an']) ? ' -map 0:a -an ' : $FFFilterComplex;
    $FFCMD .= $FFOutput;
//    $FFCMD .= strcmp($FFSilentSrc, "") != 0 ? ' -shortest ' : '';//don't use shortest, it will cause frame loss
    if(isset($args['oformat'])) $FFCMD .= ' -f '.$args['oformat'];
    $FFCMD .= ' -y ' . $args['videoOut'];
}

//function parse_mbc_audio_details_to_output($out) {
//    $outStr = "";
//    $outStr .= ' -acodec copy';
//    
////$outStr .= ' -ab:a:' . $out['streamId'] . ' ' . str_replace("000", "k", $out['bitrate']);
//    return $outStr;
//}

//function parse_to_mbc_cmd_ini_output_simple($out) {
//    $output = "";
//    $output .= parse_mbc_audio_details_to_output($out);
//    
//    $src = $out['src'];
//    if($src[0]['srcNum'] != 1)//1 is null src
//        $output .= ' -newaudio -map_audio_channel 0:'.($src[0]['streamId']+1).':0:0:'.($out['streamId']+1).':0 ';
//    else
//        $output .= ' -newaudio -map_audio_channel 1:0:0:0:'.($out['streamId']+1).':0 ';
//
//    return $output;
//}

//function parse_to_mbc_cmd() {
//    global $audioOut;
//    global $FFCMD;
//    global $args;
//
//    $FFOutput = "";
//    $FFSilentSrc = "";
//
//    foreach ($audioOut as $tmp) {
//        $out = (array) $tmp;
//        $FFOutput .= parse_to_mbc_cmd_ini_output_simple($out);
//        
//        if ($out['src'][0]['srcNum'] == 1)//1 is empty audio src
//           $FFSilentSrc = ZEROSILENTSRC;
//    }
//
//    //generate cmd
//    $FFCMD .= MBCBIN;
//    $FFCMD .= $args['label'] ? ' -label ' . $args['label'] : ' ';
//    $FFCMD .= ' -i ' . $args['videoSrc'];
//    $FFCMD .= strcmp($FFSilentSrc, "") != 0 ? $FFSilentSrc.' -shortest ' : ' ';
//    $FFCMD .= ' -target xdcamhd422 -vcodec copy -timecode 01:00:00:00 ';
//    $FFCMD .= $args['creation_time'] ? ' -metadata creation_time=' . $args['creation_time'] : ' ';
//    $FFCMD .= $args['threads'] ? ' -threads ' . $args['threads'] : '';
//    $FFCMD .= ' -f mxf -an -y ' . $args['videoOut'];
//    $FFCMD .= ' '.$FFOutput;
//}

function get_args($argv) {

    $args = array();

    for ($i = 1; $i < count($argv); $i++) {
        switch ($argv[$i]) {
            case "-map_rule":
                $args['mapFile'] = $argv[++$i];
                break;

            case "-in":
                $args['videoSrc'] = $argv[++$i];
                break;

            case "-out":
                $args['videoOut'] = $argv[++$i];
                break;

            case "-video_control_copy":
                $args['videoControlCopy'] = intVal($argv[++$i]);
                break;

            case "-label":
                $args['label'] = $argv[++$i];
                break;

            case "-threads":
                $args['threads'] = intVal($argv[++$i]);
                break;

            case "-ss":
                $args['ss'] = floatval($argv[++$i]);
                break;

            case "-ss2":
                $args['ss2'] = floatval($argv[++$i]);
                break;

            case "-t":
                $args['t'] = floatval($argv[++$i]);
                break;

            case "-xdcam":
                $args['xdcam'] = true;
                break;
            
            case "-imx":
                $args['imx'] = true;
                break;

            case "-silent_dur":
                $args['silent_dur'] = intVal($argv[++$i]);
                break;

//            case "-creation_time":
//                $args['creation_time'] = $argv[++$i].' '.$argv[++$i];
//                break;

            case "-vn":
                $args['vn'] = true;
                break;

            case "-an":
                $args['an'] = true;
                break;
            
            case "-oformat":
                $args['oformat'] = $argv[++$i];
                break;
            
            case "-cmd_mode":
                $args['cmd_mode'] = true;
                break;

            case "--help":
                echo 'Usage: ' . PHP_EOL . $argv[0] . ' -map_rule {mapping rule xml} -in {input video file} -out {output video file} -video_control_copy {0|1}' . PHP_EOL;
                exit(0);
                break;

            default:
                break;
        }
    }

    return $args;
}

//pre-check
function pre_check($args) {
    $flag = false;
    $errorMsg = "";

    if (empty($args['mapFile'])) {
        $flag = true;
        $errorMsg .= '[ERR]Mapping rule is empty' . PHP_EOL;
    }

    if (empty($args['videoSrc'])) {
        $flag = true;
        $errorMsg .= '[ERR]Video input is empty' . PHP_EOL;
    }

    if (empty($args['videoOut'])) {
        $flag = true;
        $errorMsg .= '[ERR]Video output is empty' . PHP_EOL;
    }

    if ($flag) {
        fwrite(STDERR, $errorMsg);
        echo 'Usage: ' . PHP_EOL . $args[0] . ' -map_rule {mapping rule xml} -in {input video file} -out {output video file} -video_control_copy {0|1}' . PHP_EOL;
        exit(0);
    }
}

//start
$args = get_args((array) $argv);
pre_check($args);

parse_xml_data(read_xml($args['mapFile'])); //read maping matrix
parse_to_ffmpeg_cmd(); //create cmd

echo $FFCMD . PHP_EOL;
if(!$args['cmd_mode'])
    system($FFCMD);
?>
