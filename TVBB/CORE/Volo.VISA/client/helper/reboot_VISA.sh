#!/bin/bash

#mount pkg NAS
TMP_ARG1=$1
if [ "$TMP_ARG1" == "SCHD" ]; then
    service nginx start
    service php-fpm start 
    service postgresql-9.4 start 
#else
    #mount scheduler NAS, todo
#    mount -t nfs 192.168.3.121:/data/visa/Volo.NAS /mnt/Volo.NAS
fi

#network restart
service network restart
#mount NAS driver, todo
#mount -t cifs //192.168.3.201/pcie /mnt/WINNAS -o username=daolab,domain=my_domain,sec=ntlmssp,iocharset=utf8,rw,file_mode=0777,dir_mode=0777,password=ichi2san4

#VISA start
bash /opt/Volo.VISA/client/helper/restart_VISA.sh $TMP_ARG1