#!/bin/sh
#
# Volo Split Encoder :: Scripts to Encode Split Audio Track
# VISA v1.4 Build Sat Sep  5 02:01:31 HKT 2015 (DaoLab)
#
# "USAGE: splitencode_audio.sh [JOB ID] [OUTPUT ID] [SPLIT ID] [SOURCE] [DEST] [PROFILE] [MODE H.264|H.265]"
#
 
cd /opt/Volo.VISA/client/helper
JID=$1
OID=$2
SID=$3
SOURCE=$4
DEST=$5
PROFILE=$6
ENCMODE=$7
 
#kenny hack start
#exit 0

JOB_LABEL=F_${SID}
JOB_PATH=/dev/shm/Volo.VISA/splitjobs/${JOB_LABEL}
JOB_LOG='/opt/Volo.VISA/logs/VISA.JobEncode'.`hostname`.`date +%Y%m%d`.'log'
ERRSTR=""
 
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Split Encode Audio Job Submit :: JID[${1}] OID[${2}] SID[${3}] SOURCE[${4}] DEST[${5}] PROFILE[${6}] MODE[${7}] >> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Split Encode Audio Command :: ./splitencode_audio.sh ${1} ${2} ${3} ${4} ${5} ${6} ${7} >> ${JOB_LOG}

# Create Encode Working Path
if [ -d "${JOB_PATH}" ]; then
	mv -f ${JOB_PATH} /opt/Volo.VISA/splitjobs/SPO_Unknown/${JOB_LABEL}.`hostname`.`date +"%Y%m%d%H%M%S"`
fi
mkdir -p ${JOB_PATH}
echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.audio
echo ${OID} > ${JOB_PATH}/output_id
 
# H.264
if [ "${ENCMODE}" == "0" ]
then
#kenny hack star, edit binary
#	VOLO_BIN=/opt/volo/script/volo
	VOLO_BIN=/opt/volo/bin/volo-1.4.1
#kenny hack end
	VOLO_PROBE=/opt/volo/bin/vprobe
	VOLO_BASE=/opt/volo
else
#kenny hack star, edit binary
	VOLO_BIN=/opt/volo-hevc/bin/volo-1.4.1
#	VOLO_BIN=/opt/volo-hevc/script/volo
#kenny hack end
	VOLO_PROBE=/opt/volo-hevc/bin/vprobe-1.4.1
	VOLO_BASE=/opt/volo-hevc
fi

# Split Encode Audio Trunk
# Housekeep Destination Path
OUTPATH=`dirname ${DEST}`
if [ ! -d "${OUTPATH}" ]; then
        mkdir -p ${OUTPATH}
else
        rm -rf ${DEST}
fi
N_THREAD=$(less /proc/cpuinfo | grep processor | wc -l)
echo [`hostname`]Split Audio Encode Start :: `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.start
ls -lh --full-time ${SOURCE} >> ${JOB_PATH}/job.start 2>&1
TMPDST=${DEST}.TEMP.mov
for(( s=1; s<4; s++ ))
do

        #kenny hack start, for fast audio mapping
#	echo [`date +"%Y-%m-%d %H:%M:%S"`] Split Audio Command :: ${VOLO_BIN} -label SPLIT.F_${OID}_${SID} -f ${SOURCE} -o ${TMPDST} -workpath ${JOB_PATH} -do ${PROFILE}.xml -container mov -vcodec NONE -pass 1 -threads ${N_THREAD} >> ${JOB_PATH}/encode.log
#	${VOLO_BIN} -label SPLIT.F_${OID}_${SID} -f ${SOURCE} -o ${TMPDST} -workpath ${JOB_PATH} -do ${PROFILE}.xml -container mov -vcodec NONE -pass 1 -threads ${N_THREAD} >> ${JOB_PATH}/encode.log 2>&1
#        ${VOLO_BIN} -label SPLIT.F_${OID}_${SID} -i ${SOURCE} -map 0:a -acodec pcm_s16le -threads ${N_THREAD} -vcodec copy -y ${TMPDST} >> ${JOB_PATH}/encode.log 2>&1
        
        #fast
        #kenny, outdate, not use
        #this for audio split encode
#        TRUNK_NB=$(($(echo $(basename ${DEST})| sed -e "s/SPTS_[0-9]*_[0-9]*_//g"| sed -e "s/_audio.mov//g" )-1))
#        DURATION=60
#	SEEK=`expr "${TRUNK_NB}"\*"${DURATION}" | bc`

#        SRC_PATH=$(dirname ${SOURCE})
#        SRCFILE_ORI=$(cat ${SRC_PATH}/SOURCE.txt)
#        DURSOURCE=$(cat ${SRC_PATH}/DURSOURCE)
#        if [ -f ${SRC_PATH}/MAP.INFO ]; then
#            echo [`date +"%Y-%m-%d %H:%M:%S"`] Split Audio Command :: /opt/Volo.VISA/client/helper/audio_mapping.php -in ${SOURCE} -out ${TMPDST} -map_rule ${SRC_PATH}/MAP.INFO -video_control_copy 0 -label SPLIT.F_${OID}_${SID} -threads ${N_THREAD} -ss ${SEEK} -t ${DURATION} >> ${JOB_PATH}/encode.log 2>&1
#            php /opt/Volo.VISA/client/helper/audio_mapping.php -in ${SOURCE} -out ${TMPDST} -map_rule ${SRC_PATH}/MAP.INFO -video_control_copy 0 -label SPLIT.F_${OID}_${SID} -threads ${N_THREAD} -ss ${SEEK} -t ${DURATION} >> ${JOB_PATH}/encode.log 2>&1
#        else
#            echo [`date +"%Y-%m-%d %H:%M:%S"`] Split Audio Command :: ${VOLO_BIN} -label SPLIT.F_${OID}_${SID} -i ${SOURCE} -map 0:a -acodec pcm_s16le -threads ${N_THREAD} -vcodec copy -y ${TMPDST} >> ${JOB_PATH}/encode.log 2>&1
#            ${VOLO_BIN} -label SPLIT.F_${OID}_${SID} -i ${SOURCE} -map 0:a -acodec pcm_s24le -threads ${N_THREAD} -vcodec copy -y ${TMPDST} >> ${JOB_PATH}/encode.log 2>&1
#        fi
        SRC_PATH=$(dirname ${SOURCE})
        SRCFILE_ORI=$(cat ${SRC_PATH}/SOURCE.txt)
        DURSOURCE=$(cat ${SRC_PATH}/DURSOURCE)
        echo $SRC_PATH >> $JOB_LOG
        if [ -f ${SRC_PATH}/MAP.INFO ]; then
            echo [`date +"%Y-%m-%d %H:%M:%S"`] Split Audio Command :: /opt/Volo.VISA/client/helper/audio_mapping.php -in ${SRCFILE_ORI} -out ${TMPDST} -map_rule ${SRC_PATH}/MAP.INFO -vn -label SPLIT.F_${OID}_${SID} -threads ${N_THREAD} >> ${JOB_PATH}/encode.log 2>&1
            php /opt/Volo.VISA/client/helper/audio_mapping.php -in ${SRCFILE_ORI} -out ${TMPDST} -map_rule ${SRC_PATH}/MAP.INFO -vn -label SPLIT.F_${OID}_${SID} -silent_dur ${DURSOURCE} >> ${JOB_PATH}/encode.log 2>&1
        else
            echo [`date +"%Y-%m-%d %H:%M:%S"`] Split Audio Command :: ${VOLO_BIN} -label SPLIT.F_${OID}_${SID} -i ${SOURCE} -map 0:a -acodec pcm_s16le -threads ${N_THREAD} -vcodec copy -y ${TMPDST} >> ${JOB_PATH}/encode.log 2>&1
            ${VOLO_BIN} -label SPLIT.F_${OID}_${SID} -i ${SOURCE} -map 0:a -acodec pcm_s24le -threads ${N_THREAD} -vn -y ${TMPDST} >> ${JOB_PATH}/encode.log 2>&1
        fi
        #kenny hack end

	if [ -f "${TMPDST}" ]; then
		mv -f ${TMPDST} ${DEST} >> ${JOB_PATH}/job.start 2>&1
		ls -lh --full-time ${DEST} >> ${JOB_PATH}/job.start 2>&1
		echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.done
                echo [`hostname`]Split Audio Encode Done :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
		exit 0
	fi

	if [ ${s} -lt 3 ]; then
                echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Split Encode Audio Retry Job :: SOURCE[${SOURCE}] TEMPDEST[${TMPDST}] DEST[${DEST}] RETRY[${s}] >> ${JOB_LOG}
                echo [`hostname`]Split Audio Encode Retry [$s] :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/encode.log
                echo [`hostname`]Split Audio Encode Retry [$s] :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
		ls -lh --full-time ${JOB_PATH}/ >> ${JOB_PATH}/job.start 2>&1
		tar -zcvf ${JOB_PATH}/SPLIT.${s}.tgz ${JOB_PATH}/SPLIT.*.log ${JOB_PATH}/job.encode.* >> ${JOB_PATH}/job.start 2>&1
                rm -rf ${JOB_PATH}/SPLIT.*.log 2>&1
                rm -rf ${JOB_PATH}/job.encode.* 2>&1
                rm -rf ${JOB_PATH}/volo_high_compression* 2>&1
                rm -rf ${JOB_PATH}/*.mov 2>&1
                sleep $((s*5))
		ls -lh --full-time ${SOURCE} >> ${JOB_PATH}/job.start 2>&1
                ${VOLO_PROBE} -i ${SOURCE} >> ${JOB_PATH}/job.start 2>&1
        fi
done

rm -rf ${JOB_PATH}/volo_high_compression* 2>&1
rm -rf ${JOB_PATH}/*.mov 2>&1
#echo [ERROR] Split Audio Encode Error :: Fail to Encode Output Audio Trunk > ${JOB_PATH}/job.error
echo [ERROR] Split Audio Encode Error :: Fail to Encode Output Audio Trunk, Maybe Error Using Setting Profile > ${JOB_PATH}/job.error
exit 1
