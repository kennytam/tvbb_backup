#!/bin/sh
# Volo HLS Generator Script
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
#
if [ $# -lt 2 ]
  then
    echo "USAGE: encodeTS.sh [SOURCE MP4 FILE] [DEST HLS PATH]"
    exit 1
fi

cd /opt/Volo.VISA/client/helper
JOB_LOG='/opt/Volo.VISA/logs/VISA.GenHLS'.`hostname`.`date +%Y%m%d`.'log'
SRC=$1
DESC=$2

echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER HLS Generation Job Submit :: SOURCE[${1}] DEST[${2}] >> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER HLS Generation Command :: ./encodeTS.sh ${1} ${2} >> ${JOB_LOG}
 
# Verify Source Exist
if [ ! -f "${SRC}" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER HLS Generation Error :: SOURCE[${SRC}] Not Found >> ${JOB_LOG}
	exit 1
fi

# Prepare the M3U8
FILENAME=$(basename "${SRC}" .mp4)
LEVEL=${FILENAME:(-1)}
HLSIDX=${FILENAME}_vod.m3u8
SUBPATH=${FILENAME}/Period1
M3U8MAIN='#EXTM3U'
if [ "${LEVEL}" == "1" ];then
	M3U8FILE=$(basename "${FILENAME}" _Layer1).m3u8
	M3U8BODY='#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=300000'
elif [ "${LEVEL}" == "2" ];then
	M3U8FILE=$(basename "${FILENAME}" _Layer2).m3u8
	M3U8BODY='#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=800000'
elif [ "${LEVEL}" == "3" ];then
	M3U8FILE=$(basename "${FILENAME}" _Layer3).m3u8
	M3U8BODY='#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=1600000'
else
	M3U8FILE=$(basename "${FILENAME}" _Layer4).m3u8
	M3U8BODY='#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=3000000'
fi


# Segment the HLS
if [ ! -d "${DESC}/${SUBPATH}" ]; then
	mkdir -p ${DESC}/${SUBPATH}/
fi
/opt/volo/bin/volo-1.4.1 -i "${SRC}" -map 0:v -vcodec copy -map 0:a -acodec copy -bsf h264_mp4toannexb -f segment -segment_time 5 -segment_format mpegts -segment_list "${DESC}/${HLSIDX}" -segment_list_entry_prefix "${SUBPATH}/" "${DESC}/${SUBPATH}/live_%08d.ts" > /dev/null 2>&1

# Generate the Main M3U8 Index
if [ ! -f "${DESC}/${M3U8FILE}" ]
then
	echo ${M3U8MAIN} > ${DESC}/${M3U8FILE}
fi
echo ${M3U8BODY} >> ${DESC}/${M3U8FILE}
echo ${HLSIDX} >> ${DESC}/${M3U8FILE}

