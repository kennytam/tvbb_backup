#!/bin/sh
# Volo.Worker :: Scripts to Cancel Volo Encoder Output Job
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
#
 
if [ $# -lt 2 ]
  then
    echo "USAGE: cancel.sh [JOB ID] [OUTPUT ID]"
    exit 1
fi

cd /opt/Volo.VISA/client/helper
JID=$1
OID=$2
JOB_LABEL=F_${OID}
JOB_PATH=/dev/shm/Volo.VISA/jobs/${JOB_LABEL}
JOB_CANCEL_FILE=/dev/shm/Volo.VISA/jobs/${JOB_LABEL}/job.cancel
JOB_LOG='/opt/Volo.VISA/logs/VISA.JobCancel'.`hostname`.`date +%Y%m%d`.'log'

echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Output Job Cancel :: JID[${1}] OID[${2}] >> ${JOB_LOG}

# Loop till no Volo Encode Script
while true; do
  jobStr=`ps -Alf | grep "encode.sh" | grep "${OID}" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`

  if [ -z "${jobStr}" ]
  then
	break
  else
	jobs=( ${jobStr} )
	if [ ${#jobs[@]} -gt 0 ]
	then
		kill -9 ${jobs[3]}
		sleep 1
	fi
  fi
done
# Loop till no Volo Process
while true; do
  jobStr=`ps -Alf | grep "bin/volo\-1.4" | grep "SCHEDULAR.${JOB_LABEL}" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`

  if [ -z "${jobStr}" ]
  then
	break
  else
	jobs=( ${jobStr} )
	if [ ${#jobs[@]} -gt 0 ]
	then
		kill -9 ${jobs[3]}
		sleep 1
	fi
  fi
done

# Let PollJobs clean
sleep 5

# Set CANCEL for Cancel Jobs
if [ -d "${JOB_PATH}" ]; then
	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Volo Job Stop :: JID[${JID}] OID[${OID}] Volo PID[${jobs[3]}] >> ${JOB_LOG}
	echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_CANCEL_FILE}
	rm -rf ${JOB_PATH}/volo_high_compression* 2>&1
	rm -rf ${JOB_PATH}/*.mp4 2>&1
	rm -rf ${JOB_PATH}/*.ts 2>&1
fi
exit 0
