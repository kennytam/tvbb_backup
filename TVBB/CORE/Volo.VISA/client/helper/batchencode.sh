#!/bin/sh
#
# Volo.Worker :: Scripts to Spawn Volo Batch Encoder Job
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
#

if [ $# -lt 1 ]
  then
    echo "USAGE: batchencode.sh [BATCH JOB FILE FULL PATH]"
    exit 1
fi

JOBFILE=$1
JOB_LOG='/opt/Volo.VISA/logs/VISA.JobBatch'.`hostname`.`date +%Y%m%d`.'log'
JOBPATH=/opt/Volo.VISA/client/helper/
BAKPATH=/opt/Volo.VISA/results/batch/
ENCBIN=/opt/Volo.VISA/client/helper/encode.sh
RUNFILE=/dev/shm/Volo.VISA/jobs/batch.run

echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Batch Encode Job Submit :: JOB FILE[${1}] >> ${JOB_LOG}

# Batch File Not Exist
if [ ! -f "${JOBFILE}" ]
then
	echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Batch Encode Job Error :: JOB FILE[${1}] Not Found >> ${JOB_LOG}
	exit 1
fi

# Submit Encode Job
cd ${JOBPATH}
ERROR=0
while read line
do
 job=${line}
 # echo 'START -> '${line}
 JOBID=`echo ${job} | cut -d' ' -f1`
 OID=`echo ${job} | cut -d' ' -f2`

 if [ ${ERROR} -lt 0 ]
 then
	# Cancel Rest of Job if ERROR
	curl --data "JID=${JOBID}" http://10.12.6.100/scheduler/jobcancel.php 2>&1 > /dev/null
	# echo 'CALL CANCEL JOB -> '${line}
	mv ${JOBFILE} ${BAKPATH}
	exit 1
 else
	# Mark Batch Job Running
	echo `date` > ${RUNFILE}
	curl --data "OID=${OID}" http://10.12.6.100/scheduler/encodestart.php 2>&1 > /dev/null
	${ENCBIN} ${job}
	# echo 'CALL ENCODE JOB -> '${line}

	# Wait Polljob Progress for job DONE
	while [ ! -f "${RUNFILE}.done" ] && [ ! -f "${RUNFILE}.error" ] && [ ! -f "${RUNFILE}.cancel" ]
	do
		# echo 'WAIT JOB POLLING SIGNAL ... '
		sleep 1
	done

	# Stop for Job ERROR
	if [ -f "${RUNFILE}.error" ]
	then
		rm ${RUNFILE}.error
		ERROR=-1
		# echo 'JOB ERROR -> '${line}
	fi

	# Stop for Job CANCEL
	if [ -f "${RUNFILE}.cancel" ]
	then
		rm ${RUNFILE}.cancel
		mv ${JOBFILE} ${BAKPATH}
		# echo 'JOB CANCEL ... exiting'
		exit 1
	fi

	# Prepare Next Job
	if [ -f "${RUNFILE}.done" ]
	then
		rm ${RUNFILE}.done
		# echo 'JOB DONE ... exiting'
	fi
 fi
 # echo 'PROCESS NEXT ... '
done < ${JOBFILE}

mv ${JOBFILE} ${BAKPATH}
# echo 'FINISH ... '

