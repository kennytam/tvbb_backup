#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
# Volo.Worker :: Scripts to Generate HLS Playlist based on Encoded MP4
#
if [ $# -lt 3 ]
then
	echo "Usage: encodePlaylistGENERAL.sh <INPUT PATH> <ENCODED FILE PREFIX> <OUTPUT PLAYLIST NAME>"
	exit
fi

INPUT=$1
PREFIX=$2
OUTPUT=$3

if [ ! -d ${INPUT}  ]; then
	echo ${INPUT} Not Exist!! Exit!!
	exit 1	
fi

playlist="#EXTM3U"

for d in "$INPUT/$PREFIX"*
do
	if [ -d $d ]
	then
		#echo "Folder = $d"

		for m in "$d"*.m3u8
		do

			m3u8=$(basename $m)
			#echo "M3U8 = $m3u8"

			break;
		
		done

		for src in "$d"*.mp4
		do

			if [ -f $src ]
			then
				#echo "Src = $src"
				bitrate=`mediainfo --Inform="Video;%BitRate%" $src`
				#echo "Bitrate = $bitrate"
				resolution=`mediainfo --Inform="Video;%Width%x%Height%" $src`

				break;

			fi

		done


		playlist="$playlist\n#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=$bitrate,RESOLUTION=$resolution\n$m3u8"

	fi

done

echo -e "Playlist:\n$playlist"
echo -e $playlist > "$INPUT/$OUTPUT"

exit 0
