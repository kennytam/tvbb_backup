#!/bin/bash
# Volo DRM and Generate Multiple Language HLS
# NODRM DEBUG MODE, Y=ON, ** MUST CREATE VIDEO PATH, AUDIO SYMBOLIC LINK MANUALLY **
# eg. /home/html/nodrm/smil:unq.smil link to -> /home/html/unq/unqhls/nodrm/
#
if [ $# -lt 4 ]
  then
    echo "USAGE: DRM_VOD.sh [Source HLS Full Path with M3U8] [Destination HLS Full Path] [Resource ID] [Output ID]"
    echo "eg. ./DRM_VOD.sh /home/daolab/DAOLAB_DRM_TEST/playlist.m3u8 /tmp/test3 Demo 123"
    exit 1
fi

SRCFULL=$1
SRCBASE=`echo ${SRCFULL%/*}`
MASTERPLAYLIST=`echo ${SRCFULL##*/}`
OUTPUTBASE=$2
OUTAAC=$2/audios
NODRMPATH=$2/nodrm
noOfLang=0
FILTER=/opt/Volo.LIVE/${noOfLang}audios.txt
AUDIOFILT=N
VIDEOONLY=N
DEBUGMODE=N
PROFILE=
#RES=res_pccw_global_stream_1
#RES=goliveconcert_staging_res_1
#RES=goliveconcert_staging_res2_1
#RES=goliveconcert_res2_1
#RES=goliveconcert_staging_res2_100000
#RES=goliveconcert_res2_100000
RES=$3
OUTPUTID=$4
DUR=5
MAXKEY=60
TIME2SHIFT=60
KEY=0
TIME=0
KEYS=()
MTIME=0
TYPE=VOD
#APPBASE=/opt/Volo.CTSLIVE
#JOB_LOG=${APPBASE}'/logs/VoloLIVE.DRM'.`hostname`.`date +%Y%m%d`.'log'
#DRM_CONTROL=${APPBASE}/etc/drm.seq
#kenny
APPBASE=/opt/Volo.VISA
JOB_LOG=${APPBASE}'/logs/VISA.DRM'.`hostname`.`date +%Y%m%d`.'log'
DRM_CONTROL=${APPBASE}/client/helper/DRM_KEY/drm.seq

#KEYURL=http://swiftdrm.conversant.com.sg:12684/CAB/keyfile
KEYURL=http://172.16.4.246:12684/CAB/keyfile
#DECRYPTURL=http://199.203.83.216:12684/CAB/keyfile
shift 2

#Check DRM control file
if [ ! -f ${DRM_CONTROL} ]
then
	echo 1 > ${DRM_CONTROL}

fi

# Housekeeping Video TS
if [ -d "$OUTPUTBASE" ]
then
	rm -f $OUTPUT/*.ts
	rm -f $SRCBASE/*.key
	#if [ "$DEBUGMODE" == "Y" ]
	#then
	#	rm -f $NODRMPATH/*.ts
	#fi
else
	mkdir -p $OUTPUTBASE
	#mkdir -p $NODRMPATH
fi

# Housekeeping Audio AAC
for((i=0;i<$noOfLang;i++))
do
	langOutput=$OUTAAC/lang$((i+1))
 
	if [ -d "$langOutput" ]
        then
                rm -f $langOutput/*.aac
        else
                mkdir -p $langOutput
        fi
done


# Get DRM Key from Key Server
#for drmkey in $(seq 0 $((MAXKEY-1)))
#do
#	keyFile=$SRCBASE/enc$((drmkey+1)).key
#	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VOLO.LIVE Retrieve DRM KEY No.${drmkey} File :: ${keyFile} >> ${JOB_LOG}
#	curl -v "$KEYURL?r=$RES&t=${TYPE}&p=$drmkey" -o $keyFile >> ${JOB_LOG} 2>&1
#	KEYS[$drmkey]=$(cat $keyFile | hexdump -e '16/1 "%02x"')
#	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VOLO.LIVE DRM KEY No. ${drmkey} KEY :: ${KEYS[$drmkey]} >> ${JOB_LOG}
#done

# Function to get current key
keyIdx=0
getDRMKey() {
	#keyIdx=`cat ${DRM_CONTROL}`
	
	#keyTime=`stat -c %Y "${DRM_CONTROL}"`
	#now=`date +%s`

	#newKey=false

	#if [ $now -gt $((keyTime+TIME2SHIFT)) ]
	#then
		keyIdx=$((keyIdx+1))
	
		if [ $keyIdx -gt $MAXKEY ]
		then
			keyIdx=1

		fi

		newKey=true

		echo $keyIdx > ${DRM_CONTROL}

	#fi

	keyFile=${APPBASE}/client/helper/DRM_KEY/${OUTPUTID}_enc${keyIdx}.key
	locFile=${APPBASE}/client/helper/DRM_KEY/${OUTPUTID}_enc${keyIdx}.loc

	#if [ $newKey ] || [ ! -f $keyFile ]
	#then
	#	if [ -f $keyFile ]
	#	then
	#		rm -f $keyFile

	#	fi

		echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VOLO.LIVE Retrieve DRM KEY No.${keyIdx} File :: ${keyFile} >> ${JOB_LOG}
		echo "curl -v $KEYURL?r=$RES&t=${TYPE}&p=$keyIdx -o $keyFile" >> ${JOB_LOG}
		(curl --connect-timeout 5 -v "$KEYURL?r=$RES&t=${TYPE}&p=$keyIdx" -o $keyFile 2>&1 | grep Content\-Location | sed -e 's/^.*Location: //' -e 's///') > $locFile
		#curl -v "http://swiftdrm.conversant.com.sg:12684/CAB/keyfile?r=goliveconcert_res2_100000&t=${TYPE}&p=1" -o $keyFile >> ${JOB_LOG} 2>&1
	#fi

	#if [ ! -f $keyFile ]
	#then
	#	echo 0 0
	#	exit 1

	#fi

	key=$(cat $keyFile | hexdump -e '16/1 "%02x"')
	location=$(cat $locFile)

	echo -n $keyIdx $key "$location"
};

sublist=()

if [ -z `cat $SRCFULL | grep "EXT\-X\-STREAM\-IN" | head -1` ] 
then
	echo "Single Playlist"
	sublist=($MASTERPLAYLIST)

else
	echo "Multiple Playlists"

	while read main
	do
		if [ ! -z `echo $main | grep "\.m3u8"` ]
		then
			sublist+=($main)

		fi

	done < $SRCFULL

	cp -f $SRCFULL $OUTPUTBASE

fi

#echo "Size = ${#sublist[@]}"

# Process Source HLS TS
for((s=0;s<${#sublist[@]};s++))
do
	SRCFULL=${SRCBASE}/${sublist[$s]}
	SRC=`echo ${SRCFULL%/*}`
	PLAYLIST=`echo ${SRCFULL##*/}`
	OUTPUTFULL=${OUTPUTBASE}/${sublist[$s]}
	OUTPUT=`echo ${OUTPUTFULL%/*}`

	CDuration=0

	#echo $SRCFULL
	#echo $SRC
	#echo $OUTPUT

	mkdir -p $OUTPUT

        if [ -f "$SRCFULL" ]
        then
		if [ -f "$OUTPUTFULL" ]
		then
			rm -f "$OUTPUTFULL"

		fi

		LASTKEY=-1

		while read p
		do
			echo "Processing $p..."

			#echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VOLO.LIVE Processing M3U8 :: [$SRC/index.m3u8]
			echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VOLO.LIVE Processing M3U8 :: [$SRC/$PLAYLIST] >> ${JOB_LOG}

			ts=`echo $p | grep "\.ts"`

			if [ -z "$ts" ]
			then
				INF=`echo $p | grep "#EXTINF:"`

				if [ -z "$INF" ]
				then
					echo $p >> "$OUTPUTFULL"

				fi

				continue
			
			fi

			TS=`basename $ts`
			SRCTS=$TS

			if [ "$VIDEOONLY" == "Y" ]
			then
				SRCTS="video_only_$TS"
				echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VOLO.LIVE Spliting Vides TS :: [$SRC/$TS] [$SRC/$SRCTS] >> ${JOB_LOG}

				# With Variable time code
				/opt/volo/bin/volo-1.4.1 -i "$SRC/$TS" -map 0:v -vcodec copy -vsync 0 -max_delay 0 -copyts -an "$SRC/$SRCTS" >> ${JOB_LOG} 2>&1
				# Without Variable time code
				# /opt/volo/bin/volo-1.4.1 -i "$SRC/$TS" -map 0:v -vcodec copy -vsync 0 -an "$SRC/$SRCTS" >> ${JOB_LOG} 2>&1

			fi

			# Multiple Audio Handling
                        outStr=""
                        for((i=0;i<$noOfLang;i++))
                        do
echo "LANG"
                                langOutput=$OUTAAC/lang$((i+1))
 
                                AAC=`echo $TS | sed 's/\.ts/\.aac/'`
                                #/opt/volo/bin/volo-1.4.1 -i "$SRC/$TS" -map 0:a:$i -acodec copy "$langOutput/$AAC"
 
                                #cat "$SRC/index.m3u8" | sed 's/\.ts/\.aac/g' > "$langOutput/index.m3u8"
 
				if [ "$AUDIOFILT" == "Y" ]
				then
                                  # With Audio Filter
                                  outStr="$outStr -map [aout$((i+1))] $langOutput/$AAC"
				else
                                  # Without Audio Filter
                                  outStr="$outStr -map 0:a:$i -acodec copy $langOutput/$AAC"
				fi
 
                        done
 
			#echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VOLO.LIVE Spliting Audio AAC :: ["$SRC"/"$TS"] ["$OUTAAC"] >> ${JOB_LOG}
			#if [ "$AUDIOFILT" == "Y" ]
			#then
                          # With Audio Filter
                        # /opt/volo/bin/volo-1.4.1 -i "$SRC/$TS" -filter_complex "`cat $FILTER`" $outStr >> ${JOB_LOG} 2>&1
			#else
                        #  # No Audio Filter
                        #  /opt/volo/bin/volo-1.4.1 -i "$SRC/$TS" $outStr >> ${JOB_LOG} 2>&1
                        #  #echo "/opt/volo/bin/volo-1.4.1 -i \"$SRC/$TS\" -filter_complex \"`cat $FILTER`\" $outStr"
			#fi
 
                        # Add ID3 Tag to AAC
                        #for((i=0;i<$noOfLang;i++))
                        #do
                         # langOutput=$OUTAAC/lang$((i+1))

                          # With Audio Filter
			  #if [ "$AUDIOFILT" == "Y" ]
			  #then
                                # DIRTY METHOD
                                #if [ "$i" -lt 2 ]
                                #then
                                #        tspid=0
                                #elif [ "$i" -lt 4 ]
                                #then
                                #        tspid=1
                                #elif [ "$i" -lt 6 ]
                                #then
                                #        tspid=2
                                #else
                                #        tspid=3
                                #fi
			  #else
               #                 tspid=$i
			  #fi

			  #echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VOLO.LIVE ID3 Audio AAC :: [$SRC/$TS] [$langOutput/$AAC.tmp] >> ${JOB_LOG}
               #           /opt/volo/bin/changeTsAptsToAACpts "$SRC/$TS" $((257+$tspid)) $langOutput/$AAC $langOutput/$AAC.tmp >> ${JOB_LOG} 2>&1
               #           mv $langOutput/$AAC.tmp $langOutput/$AAC
                #        done

			echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VOLO.LIVE DRM Video TS :: [$SRC/$SRCTS] [$OUTPUT/$TS] >> ${JOB_LOG}
			
			if [ $((CDuration % 30)) -eq 0 ]
			then
				read keyIdx key location <<<$(getDRMKey)

			fi

			CDuration=$((CDuration+DUR))

			if [ $keyIdx -eq 0 ]
			then
				echo "Error: Cannot get DRM key from $KEYURL"
				exit 1

			fi

			#keyFile=${APPBASE}/etc/${OUTPUTID}_enc${keyIdx}.key
			#key=$(cat $keyFile | hexdump -e '16/1 "%02x"')
			init_vector=`printf '%032x' $keyIdx`
			echo "openssl aes-128-cbc -e -in $SRC/$SRCTS -out $OUTPUT/$TS -p -nosalt -iv ${init_vector} -K $key" >> ${JOB_LOG}
			openssl aes-128-cbc -e -in $SRC/$SRCTS -out $OUTPUT/$TS -p -nosalt -iv ${init_vector} -K $key>> ${JOB_LOG} 2>&1

			if [ $keyIdx -ne $LASTKEY ]
			then
				echo "#EXT-X-KEY:METHOD=AES-128,URI=\"${location}\",IV=0x${init_vector}" >> "$OUTPUTFULL"

				LASTKEY=$keyIdx
			fi

			echo $INF >> "$OUTPUTFULL"
			echo $p >> "$OUTPUTFULL"

			# Housekeeping
			if [ "$VIDEOONLY" == "Y" ] && [ "$DEBUGMODE" != "Y" ]
			then
				rm $SRC/$SRCTS
			else
				sleep 0
				#mv $SRC/$SRCTS $NODRMPATH/$TS
				#cp $SRC/$SRCTS $NODRMPATH/$TS
				#head -5 "$SRC/index.m3u8" > "$NODRMPATH/index.m3u8"
				#tail -n +6 "$SRC/index.m3u8" >> "$NODRMPATH/index.m3u8"


				#mv $SRC/$SRCTS $NODRMPATH/$TS
			fi

			#TIME=$((TIME+DUR))
			#LASTKEY=$KEY


		done < "$SRCFULL"

	fi
done

#rm tmp file
rm ${APPBASE}/client/helper/DRM_KEY/${OUTPUTID}_*.key
rm ${APPBASE}/client/helper/DRM_KEY/${OUTPUTID}_*.loc
