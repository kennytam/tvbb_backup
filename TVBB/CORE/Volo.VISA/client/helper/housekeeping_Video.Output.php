<?php

function watch_folder($root = '.') {
    $directories = array();
    $last_letter = $root[strlen($root) - 1];
    $root = ($last_letter == '\\' || $last_letter == '/') ? $root : $root . DIRECTORY_SEPARATOR;
    $directories[] = $root;

    while (sizeof($directories)) {
        $dir = array_pop($directories);
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file == '.' || $file == '..')
                    continue;
                $file = $dir . $file;
                if (is_dir($file)) {
                    $directory_path = $file . DIRECTORY_SEPARATOR;
                    array_push($directories, $directory_path);

                    //create a job file
                    if (basename($directory_path) == 'VOLO_TMP') {
                        if ($handle_a = opendir($directory_path)) {
                            while (false !== ($entry = readdir($handle_a))) {
                                if ($entry == '.' || $entry == '..')
                                    continue;
                                $v_file = $directory_path . $entry;
                                if (is_file($v_file) && (time() - filemtime($v_file)) > 600) {

                                    // use size to comfirm again
                                    $stat = stat($v_file);
                                    $size1 = $stat['size'];
                                    sleep(15);
                                    $stat = stat($v_file);
                                    $size2 = $stat['size'];

                                    if ($size1 == $size2) {
                                        unlink($v_file);
                                        echo 'rm file: ' . $v_file . PHP_EOL;
                                    }
                                }
                            }
                            closedir($handle_a);
                        }
                    }
                }
            }
            closedir($handle);
        }
    }
}

watch_folder('/home/Video.Output/')
?>