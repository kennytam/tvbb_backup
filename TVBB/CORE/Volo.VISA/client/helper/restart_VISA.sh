#!/bin/bash


#kill all VISA
echo volo | su volo -c "ps -def | grep VISA | awk '{system(\"kill -9 \"\$2)}' >> /dev/null 2>&1" >> /dev/null 2>&1

#setup worker.ctl
echo volo | su volo -c "sed -i 's/^[A-Z]/U/g' /opt/Volo.VISA/control/scheduler.ctl >> /dev/null 2>&1" >> /dev/null 2>&1
echo volo | su volo -c "sed -i 's/^[A-Z]/U/g' /opt/Volo.VISA/control/splitter.ctl >> /dev/null 2>&1" >> /dev/null 2>&1
echo volo | su volo -c "sed -i 's/^[A-Z]/U/g' /opt/Volo.VISA/control/worker.ctl >> /dev/null 2>&1" >> /dev/null 2>&1

TMP_ARG1=$1
if [ "$TMP_ARG1" == "SCHD" ]; then

#Start all VISA, user schd
echo volo | su volo -c '/opt/Volo.VISA/control/scripts/VISA.CPU.Config.sh >> /dev/null 2>&1' >> /dev/null 2>&1
echo volo | su volo -c '/opt/Volo.VISA/control/scripts/VISA.CPU.Usage.sh >> /dev/null 2>&1 &' >> /dev/null 2>&1
echo volo | su volo -c '/opt/Volo.VISA/control/scripts/VISA.SCHD.Monitor.sh >> /dev/null 2>&1 &' >> /dev/null 2>&1
echo volo | su volo -c '/opt/Volo.VISA/control/scripts/VISA.WORKER.Monitor.sh >> /dev/null 2>&1 &' >> /dev/null 2>&1
echo volo | su volo -c '/opt/Volo.VISA/control/scripts/VISA.CTRL.Monitor.sh >> /dev/null 2>&1 &' >> /dev/null 2>&1
echo volo | su volo -c '/opt/Volo.VISA/control/scripts/VISA.PUBS.Monitor.sh >> /dev/null 2>&1 &' >> /dev/null 2>&1
echo volo | su volo -c '/opt/Volo.VISA/control/scripts/VISA.SPLIT.Monitor.sh >> /dev/null 2>&1 &' >> /dev/null 2>&1

else

echo volo | su volo -c '/opt/Volo.VISA/control/scripts/VISA.CPU.Config.sh >> /dev/null 2>&1 ' >> /dev/null 2>&1
echo volo | su volo -c '/opt/Volo.VISA/control/scripts/VISA.CPU.Usage.sh >> /dev/null 2>&1 &' >> /dev/null 2>&1
echo volo | su volo -c '/opt/Volo.VISA/control/scripts/VISA.WORKER.Monitor.sh >> /dev/null 2>&1 &' >> /dev/null 2>&1

#test
#echo volo | su volo -c '/opt/Volo.VISA/control/scripts/VISA.PUBS.Monitor.sh >> /dev/null 2>&1 &' >> /dev/null 2>&1

fi
