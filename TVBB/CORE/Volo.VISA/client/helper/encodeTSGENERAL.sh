#!/bin/sh
# Volo HLS Generator Script
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
if [ $# -lt 2 ]
  then
    echo "USAGE: encodeTSGENERAL.sh [SOURCE MP4 FILE] [DEST HLS PATH]"
    exit 1
fi

cd /opt/Volo.VISA/client/helper
JOB_LOG='/opt/Volo.VISA/logs/VISA.GenHLS'.`hostname`.`date +%Y%m%d`.'log'
SRC=$1
DESC=$2

echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER HLS Generation Job Submit :: SOURCE[${1}] DEST[${2}] >> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER HLS Generation Command :: ./encodeTSGENERAL.sh ${1} ${2} >> ${JOB_LOG}
 
# Verify Source Exist
if [ ! -f "${SRC}" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER HLS Generation Error :: SOURCE[${SRC}] Not Found >> ${JOB_LOG}
	exit 1
fi

# Prepare the M3U8
FILENAME=$(basename "${SRC}" .mp4)
LEVEL=${FILENAME:(-1)}
HLSIDX=${FILENAME}.m3u8
HLSDESC=${DESC}/${FILENAME}
# Segment the HLS
if [ -d "${HLSDESC}" ]; then
	rm -rf ${HLSIDX}
	rm -rf ${HLSDESC}
else
	mkdir -p ${HLSDESC}
fi
#/opt/volo/bin/volo-1.4.1 -i "/tmp/VTEST/Dao_360p.mp4" -map 0:v -vcodec copy -map 0:a -acodec copy -bsf h264_mp4toannexb -f segment -segment_time 5 -segment_format mpegts -segment_list "/tmp/VTEST/360p/index.m3u8" -segment_list_entry_prefix "./" "/tmp/VTEST/360p/live_%08d.ts"
/opt/volo/bin/volo-1.4.1 -i "${SRC}" -map 0:v -vcodec copy -map 0:a -acodec copy -bsf h264_mp4toannexb -f segment -segment_time 5 -segment_format mpegts -segment_list "${DESC}/${HLSIDX}" -segment_list_entry_prefix "./${FILENAME}/" "${HLSDESC}/vod_%08d.ts" >> ${JOB_LOG} 2>&1

exit 0
