#!/bin/sh
#
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
#
cd /opt/Volo.VISA/client/helper

php /opt/volo/script/mono_2_stereo_prepare.php /media/volo_source/multiaudio1/101/Testing_XDCAMHD422_CAMA.mov
/opt/volo/script/volo -f /media/volo_source/multiaudio1/101/Testing_XDCAMHD422_CAMA.mov -o /tmp/Testing_XDCAMHD422_CAMA_P5.mp4 -do tvb_device_5 -duration 60
/opt/volo/script/volo -f /opt/volo/extras/EF_8mono.mp4 -o /tmp/ef_P5.mp4 -do tvb_device_5
 
php /opt/volo/script/mono_2_stereo.php /tmp/Testing_XDCAMHD422_CAMA_P5.mp4 /tmp/Testing_XDCAMHD422_CAMA_P5_merged.ts
php /opt/volo/script/mono_2_stereo.php /tmp/ef_P5.mp4 /tmp/ef_P5_merged.ts
 
/opt/volo/script/add_end_flip.sh /tmp/Testing_XDCAMHD422_CAMA_P5_merged.ts /tmp/ef_P5_merged.ts /tmp/Testing_XDCAMHD422_CAMA_P5_combined.mp4

