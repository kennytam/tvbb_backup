#!/bin/sh
# Volo HLS Generator Script
# VISA v1.3.4 Build Mon Jul 20 23:25:54 HKT 2015 (DaoLab)
#
if [ $# -lt 2 ]
  then
    echo "USAGE: encodeETS.sh [SOURCE MP4 FILE] [DEST HLS PATH]"
    exit 1
fi

cd /opt/Volo.VISA/client/helper
JOB_LOG='/opt/Volo.VISA/logs/VISA.GenEHLS'.`hostname`.`date +%Y%m%d`.'log'
SRC=$1
DESC=$2

echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER EHLS Generation Job Submit :: SOURCE[${1}] DEST[${2}] >> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER EHLS Generation Command :: ./encodeETS.sh ${1} ${2} >> ${JOB_LOG}
 
# Verify Source Exist
if [ ! -f "${SRC}" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER EHLS Generation Error :: SOURCE[${SRC}] Not Found >> ${JOB_LOG}
	exit 1
fi

# Prepare the M3U8
FILENAME=$(basename "${SRC}" .mp4)
LEVEL=${FILENAME:(-1)}
TMP_HLSIDX=${FILENAME}_vod_tmp.m3u8
HLSIDX=${FILENAME}_vod.m3u8
SUBPATH=${FILENAME}/Period1
M3U8MAIN='#EXTM3U'
if [ "${LEVEL}" == "1" ];then
	M3U8FILE=$(basename "${FILENAME}" _Layer1).m3u8
	M3U8BODY='#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=300000'
elif [ "${LEVEL}" == "2" ];then
	M3U8FILE=$(basename "${FILENAME}" _Layer2).m3u8
	M3U8BODY='#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=800000'
elif [ "${LEVEL}" == "3" ];then
	M3U8FILE=$(basename "${FILENAME}" _Layer3).m3u8
	M3U8BODY='#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=1600000'
else
	M3U8FILE=$(basename "${FILENAME}" _Layer4).m3u8
	M3U8BODY='#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=3000000'
fi


# Segment the HLS
if [ ! -d "${DESC}/${SUBPATH}" ]; then
	mkdir -p ${DESC}/${SUBPATH}/
fi
if [ ! -d "/tmp/Volo.HLS/${SUBPATH}_TMP" ]; then
	mkdir -p /tmp/Volo.HLS/${SUBPATH}_TMP/
fi
/opt/volo/bin/volo-1.4.1 -i "${SRC}" -map 0:v -vcodec copy -map 0:a -acodec copy -bsf h264_mp4toannexb -f segment -segment_time 10 -segment_format mpegts -segment_list "${DESC}/${TMP_HLSIDX}" -segment_list_entry_prefix "${SUBPATH}_TMP/" "/tmp/Volo.HLS/${SUBPATH}_TMP/live_%08d.ts" > /dev/null 2>&1

# Encrypt the TS by key https://stream.ott.now.com/hls/vodcp124/vodcp124.key
KEYFILE=vodcp124.key
KEY=$(cat $KEYFILE | hexdump -e '16/1 "%02x"')
DUR=10
TMPSRC=/tmp/Volo.HLS/${SUBPATH}_TMP
#echo -e "#EXTM3U\n#EXT-X-VERSION:3\n#EXT-X-TARGETDURATION:$DUR\n#EXT-X-MEDIA-SEQUENCE:0\n#EXT-X-ALLOW-CACHE:YES" > ${DESC}/${HLSIDX}
head -5 "${DESC}/${TMP_HLSIDX}" > ${DESC}/${HLSIDX}
echo "#EXT-X-KEY:METHOD=AES-128,URI=\"https://stream.ott.now.com/hls/vodcp124/vodcp124.key\"" >> ${DESC}/${HLSIDX}

for ts in ${TMPSRC}/*.ts
do
        TS=`basename $ts`
 
        init_vector=`printf '%032x' $i`
        openssl aes-128-cbc -e -in $TMPSRC/$TS -out ${DESC}/${SUBPATH}/${TS} -p -nosalt -iv ${init_vector} -K ${KEY} > /dev/null 2>&1
 
	DURATION=`cat ${DESC}/${TMP_HLSIDX} | grep -B 1 $TS | head -1 | sed -e 's/#EXTINF://' -e 's/,//'`

        echo -e "#EXTINF:${DURATION},\n${SUBPATH}/${TS}" >> ${DESC}/${HLSIDX}
 
done
echo "#EXT-X-ENDLIST" >> ${DESC}/${HLSIDX}
rm -rf "/tmp/Volo.HLS/${FILENAME}"
rm "${DESC}/${TMP_HLSIDX}"

exit 0
