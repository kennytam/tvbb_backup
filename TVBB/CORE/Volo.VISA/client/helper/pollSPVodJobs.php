<?php
//////////////////////////////////////////////////////////////////////////////////////
// DaoLab :: Volo VISA Client (Polling Volo Split Encode Trunk Status)              //
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)                     //
//////////////////////////////////////////////////////////////////////////////////////
// SPLIT ENCODE
define('UPDATER', "/opt/Volo.VISA/client/worker_spprogress.php");
define('JOB_BASE_FOLDER', "/dev/shm/Volo.VISA/splitjobs");
define('JOB_DONE_FOLDER', "/opt/Volo.VISA/splitjobs/encode");
define('JOB_CANCEL_FILE', "job.cancel");
define('JOB_ENCCNX_FILE', "job.encode.cancel");
define('JOB_ERROR_FILE',  "job.error");
define('JOB_ENCERR_FILE', "job.encode.error");
define('JOB_DONE_FILE',   "job.done");
define('JOB_ENCDONE_FILE',"job.encode.done");
define('JOB_AUDIO_FILE',  "job.audio");
define('JOB_FPS_FILE',    "job.fps");
define('JOB_2PASS_FILE',  "report_1.log");
define('JOB_TIMEOUT',     1800);
define('JOB_TIMEDONE',    1);

// Prepare the JOB Result Folder
if (!file_exists(JOB_DONE_FOLDER)) {
	popen("mkdir -p " . JOB_DONE_FOLDER, "r");
}

// Global Variable
$progress_fps=$progress_pcent=$source_duration=0;
$progress_mode=1;

//////////////////////////////////////////////////////////////////////////////////////
// Get Duration, FPS of Running Split Encode Job                                    //
// Parse the JOB Progress by string ::                                              //
// frame= 2968 fps= 24 q=26.0 size=6267kB time=00:01:59.57 ...............          //
// rawTime is in 00:00:00.00 format. This converts it to seconds.                   //
//////////////////////////////////////////////////////////////////////////////////////
function getSPEncodeStatus($logFile, $SID){
global $progress_fps,$progress_mode,$progress_pcent,$source_duration;
  $progress_fps=$progress_pcent=$duration=0;
  $base=1;
  $frameLINE = $rawField = $durField = "";
 
  // Mark 1-Pass / 2-Pass Mode
  if(file_exists(JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_2PASS_FILE))
	$progress_mode = '2';
  else
	$progress_mode = '1';

  // Get the Encode Reesult
  $loghd = fopen($logFile, "r");
  if ($loghd){
	while(($frameLINE = fgets($loghd)) !== false) {
	  $frameLINE = str_replace("\n", "", $frameLINE);
	  if(substr($frameLINE,1,4) == "Pass") {
		$progress_pass=substr($frameLINE,0,1);
	  } else if(substr($frameLINE,1,5) == "state") {
		if (preg_match_all("/fps=(.*?) q/", $frameLINE, $rawField)){
			$rawField= trim($rawField[1][0]);
                	if ($rawField != '' && $rawField > 0) $progress_fps = $rawField;
		}
                if (preg_match_all("/time=(.*?) bitrate/", $frameLINE, $rawField)){
                	$rawField= trim($rawField[1][0]);
                	if ($rawField != '') $durField = $rawField;
		}
                //kenny hacked start, change progress
                if (preg_match_all("/ progress=(.*?) /", $frameLINE, $rawField,PREG_SET_ORDER)){
                    $tmp = floatval(trim($rawField[count($rawField)-1][1]))*100;
                    $progress_pcent = ($tmp == 0) ? 100 : $tmp;
                }
                //kenny hacked end

	  }
	}
	fclose($loghd);
  }
//kenny hacked start, change progress
//  // Calculate the Progress %
//  $ar = array_reverse(explode(":", substr($durField,0,8)));
//  $time=0;
//  $base=1;
//  for ($tt=0; $tt<sizeof($ar); $tt++){
//	if ($base == 1)
//		$duration += floatval($ar[$tt]);
//	else
//		$duration += ( $base * floatval($ar[$tt]) );
//		$base *= 60;
//  }
//  if ($duration > $source_duration)
//	$progress_pcent = 100;
//  else
//	$progress_pcent = round(($duration/$source_duration) * 100, 0, PHP_ROUND_HALF_UP);
//kenny hacked end

  return true;
}
//////////////////////////////////////////////////////////////////////////////////////
// End Function                                                                     //
//////////////////////////////////////////////////////////////////////////////////////
 
//////////////////////////////////////////////////////////////////////////////////////
// Verify the Volo Split Output Job Command Progress from Volo Log File             //
//////////////////////////////////////////////////////////////////////////////////////
function checkSPVODProgress($SID, $SPType, $logFile, $encLOG, $startLOG){
global $progress_fps,$progress_mode,$progress_pcent,$source_duration;
  if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
  $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
  $fileTime = date("YmdHis", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

  // Verify Cancel Job
  $workFile = JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_CANCEL_FILE;
  if(file_exists($workFile)) {
	echo "[INFOS][" . $curTime . "][" . gethostname() . "] Split Encode State :: STATUS[Trunk Encode Process Cancelled]" . "\n";
	popen("mv -f " . JOB_BASE_FOLDER . "/F_{$SID} " . JOB_DONE_FOLDER . "/F_{$SID}." . gethostname() . "." . $fileTime . ".cancel", "r");
	return false;
  }

  // Verify Encode Cancel -- Handle Retry by Encode Script
  $workFile = JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_ENCCNX_FILE;
  if(file_exists($workFile)) {
	return false;
  }

  // Verify Encode Error -- Handle Retry by Encode Script
  $workFile = JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_ENCERR_FILE;
  if(file_exists($workFile)) {
	return false;
  }

  // Verify Encode Error by Encode Script, ** Update Split Status Here Only
  $workFile = JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_ERROR_FILE;
  if(file_exists($workFile)) {
	$errSTR = preg_replace("/[\n]/", "", file_get_contents($workFile));
	echo "[ERROR][" . $curTime . "][" . gethostname() . "] Split Encode State :: ERROR[" . $errSTR . "]" . "\n";
	popen("php " . UPDATER . " sid={$SID} trunktype={$SPType} stage=-1 err=6 message=\"" . $errSTR . "\"", "r");
	popen("mv -f  " . JOB_BASE_FOLDER . "/F_{$SID} " . JOB_DONE_FOLDER . "/F_{$SID}." . gethostname() . "." . $fileTime . ".error", "r");
	return false;
  }

  // Housekeep DONE Job
  $workFile = JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_ENCDONE_FILE;
  if(file_exists($workFile)) {
    $workFile = JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_DONE_FILE;
    if(file_exists($workFile)) {
	echo "[INFOS][" . $curTime . "][" . gethostname() . "] Split Encode State :: STATUS[Trunk Encode Process Completed]" . "\n";
	popen("mv -f " . JOB_BASE_FOLDER . "/F_{$SID} " . JOB_DONE_FOLDER . "/F_{$SID}." . gethostname() . "." . $fileTime . ".done", "r");
    }
    return true;
  }

  // Verify Job Status : Complete, Processing, Fail
  $progress_fps = 0;
  if(file_exists($encLOG)) {
      // Parsing Encode Core Log (SPLIT.F_{SID}_e.log)
      if(file_exists($logFile)) {
	// Get FPS, Progress and write the fps
	getSPEncodeStatus($logFile, $SID);
	$workFile = JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_FPS_FILE;
	file_put_contents($workFile, $progress_fps);
	 
	// Get Last Status
	$logTails = tail($logFile, 2, "\n");
	for($t = count($logTails)-1; $t >= 0; $t--) {
		$logTail = $logTails[$t];
		// Core Encode Done
		$summaryPos = strpos($logTail, "[state] Conversion= Done");
		if($summaryPos !== false) {
		   $workFile = JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_DONE_FILE;
		   if(file_exists($workFile)) {
		      // Mark DONE
		      file_put_contents(JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_ENCDONE_FILE, $logTail);
		      popen("php " . UPDATER . " sid={$SID} trunktype={$SPType} stage=2 progress=100 progress_fps=" . $progress_fps . " progress_mode=" . $progress_mode . " message=\"[STATUS] Trunk Encode Process Completed\"", "r");
		   } else {
		      // 1-Pass to 2-Pass Gap
		      echo "[INFOS][" . $curTime . "][" . gethostname() . "] Split Encode State :: PROGRESS[99%] FPS[" . $progress_fps . "] MODE[" . $progress_mode . "]\n";
		      popen("php " . UPDATER . " sid={$SID} trunktype={$SPType} stage=1 progress=99 progress_fps=" . $progress_fps . " progress_mode=" . $progress_mode . " message=\"[STATUS] Trunk Encode in Progress\"", "r");
		   }
		  return true;
		} 
		// Core Encode Cancel
		$summaryPos = strpos($logTail, "[state] Conversion= Canceled");
		if($summaryPos !== false) {
		  file_put_contents(JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_ENCCNX_FILE, $logTail);
		  popen("php " . UPDATER . " sid={$SID} trunktype={$SPType} stage=3 progress=0 progress_fps=" . $progress_fps . " progress_mode=" . $progress_mode . " message=\"[STATUS] Trunk Encode Process Cancelled\"", "r");
		  return false;
		}
		// Core Encode Error
		$summaryPos = strpos($logTail, "[state] Conversion= Failed");
		if($summaryPos !== false) {
		  file_put_contents(JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_ENCERR_FILE, $logTail);
		  // popen("php " . UPDATER . " sid={$SID} trunktype={$SPType} stage=-1 err=1 message=\"[ERROR] Trunk Encode Process Failed\"", "r");
		  return false;
		}
	} // End for

	// Job Time Out Error
	$workFile = JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_ENCERR_FILE;
	if(time() - filemtime($logFile) > JOB_TIMEOUT) {
		file_put_contents($workFile, "Volo[state] Conversion= Timeout");
		echo "[ERROR][" . $curTime . "][" . gethostname() . "] Split Enocde State :: ERROR[Trunk Encode Timeout]" . "\n";
		// Disable Split Update due to Retry Mechanism
		// popen("php " . UPDATER . " sid={$SID} stage=-1 err=1 message=\"[ERROR] Trunk Encode Timeout\"", "r");
		return false;
	} else {
		echo "[INFOS][" . $curTime . "][" . gethostname() . "] Split Encode State :: PROGRESS[" . $progress_pcent . "%] FPS[" . $progress_fps . "] MODE[" . $progress_mode . "]\n";
		popen("php " . UPDATER . " sid={$SID} trunktype={$SPType} stage=1 progress=" . $progress_pcent . " progress_fps=" . $progress_fps . " progress_mode=" . $progress_mode . " message=\"[STATUS] Trunk Encode in Progress\"", "r");
		return true;
	
	}
        // End $logFile Check, Volo Encoding here ...... NO STATUS UPDATE
      } else {
        // MiddleWare encode.log ERROR at last check
        $logTails = tail($encLOG, 2, "\n");
        for($t = count($logTails)-1; $t >= 0; $t--) {
	  $logTail = $logTails[$t];
	  $summaryPos = strpos($logTail, "Error");
	  if($summaryPos !== false) {
		// Encode Error
        	$workFile = JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_ENCERR_FILE;
		file_put_contents($workFile, "Volo[state] Conversion= Probe Source Media Timeout");
		echo "[ERROR][" . $curTime . "][" . gethostname() . "] Split Encode State :: ERROR[Failed to Probe Source Trunk, Encode Aborted]" . "\n";
		// popen("php " . UPDATER . " sid={$SID} trunktype={$SPType} stage=-1 err=1 message=\"[ERROR] Failed to Probe Source Trunk, Encode Aborted\"", "r");
		return false;
	  }
        }
        // Job Time Out Error
        if(time() - filemtime($encLOG) > JOB_TIMEOUT) {
          $workFile = JOB_BASE_FOLDER . "/F_{$SID}/" . JOB_ENCERR_FILE;
	  file_put_contents($workFile, "Volo[state] Conversion= Volo Encode Timeout");
	  echo "[ERROR][" . $curTime . "][" . gethostname() . "] Split Encode State :: ERROR[Trunk Encode Timeout]" . "\n";
	  // popen("php " . UPDATER . " sid={$SID} trunktype={$SPType} stage=-1 err=1 message=\"[ERROR] Trunk Encode Timeout\"", "r");
	  return false;
        }
      }
      // Job Initializing
      echo "[INFOS][" . $curTime . "][" . gethostname() . "] Split Encode State :: STATUS[Starting Split Encode Core]" . "\n";
      popen("php " . UPDATER . " sid={$SID} trunktype={$SPType} stage=1 progress=1 progress_mode=" . $progress_mode . " message=\"[STATUS] Starting Split Encode Core\"", "r");
  } else if (time() - filemtime($startLOG) > JOB_TIMEOUT) {
	// No Encode Logs Inside, JUST LOOP??
	echo "[ERROR][" . $curTime . "][" . gethostname() . "] Split Encode State :: ERROR[Split Encode Initialization Timeout]" . "\n";
	popen("php " . UPDATER . " sid={$SID} trunktype={$SPType} stage=-1 err=1 message=\"[ERROR] Split Encode Initialization Timeout\"", "r");
	return false;
  } // end $encLOG Check

  return true;
} // end function
//////////////////////////////////////////////////////////////////////////////////////
// End Function                                                                     //
//////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////
// Get last n lines of Encode Log File                                              //
//////////////////////////////////////////////////////////////////////////////////////
function tail($filename, $lines_to_read, $line_delimiter="\n") {
        $tails = array();
        $text = "";
        $pos = -1;
        $handle = fopen($filename, "r");

        while ($lines_to_read > 0) {
                --$pos;

                if(fseek($handle, $pos, SEEK_END) !== 0) {
                        rewind($handle);
                        $lines_to_read = 0;
                } elseif (fgetc($handle) === $line_delimiter) {
                        --$lines_to_read;
                }

                $block_size = (-$pos) % 8192;
                //if ($block_size === 0 || $lines_to_read === 0) {
                if ($block_size > 0 && $lines_to_read > 0) {
                        //$text = fread($handle, ($block_size === 0 ? 8192 : $block_size)) . $text;
                        $text = fread($handle, ($block_size === 0 ? 8192 : $block_size));
                        $lines[] = $text;
                }
        }

        fclose($handle);
        //return $text;
        return $lines;
}
//////////////////////////////////////////////////////////////////////////////////////
// End Function                                                                     //
//////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////
// VISA Volo JobPolling START                                                       //
//////////////////////////////////////////////////////////////////////////////////////
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$jobList = array();

foreach(glob(JOB_BASE_FOLDER . "/" . "F_*") as $folder) {
	//echo "Folder: " . $folder . ", Type = " . (is_dir($folder) ? "Y": "N") . "\n";
	if(is_dir($folder))
		$jobList[] = basename($folder);
}

if (!empty($jobList)){
   foreach($jobList as $job) {
	$jobParts = split("_", $job);
	$splitID = $jobParts[1];
	$outputID = preg_replace("/[\n]/", "", file_get_contents(JOB_BASE_FOLDER . "/" . $job . "/output_id"));
	// SPLIT.F_1261_34023_e.log
	$logFile  =  "SPLIT.F_" . $outputID . "_" . $splitID . "_e.log";
        
	$probeLog = JOB_BASE_FOLDER . "/" . $job . "/vprobe_source.log";
	$startLog = JOB_BASE_FOLDER . "/" . $job . "/job.start";
        $errorLog = JOB_BASE_FOLDER . "/" . $job . "/job.error";
        $cancelLog= JOB_BASE_FOLDER . "/" . $job . "/job.cancel";
        $audioLog = JOB_BASE_FOLDER . "/" . $job . "/job.audio";
        $pollERR = false;
 
        if(file_exists($audioLog))
		$SPType = "A";
	else
		$SPType = "V";
        $logFile = "encode.log";//kenny
        
 
        if(file_exists($errorLog))
                $pollERR = false;
        else if(file_exists($cancelLog))
                $pollERR = false;
        else if(!file_exists($startLog))
                $pollERR = true;
        else if ($splitID == "")
                $pollERR = true;

	if ($pollERR){
	  echo "[INFOS][" . $curTime . "][" . gethostname() . "] Split Encode State :: STATUS[Work Path Preparing OID($outputID) SID($splitID)]" . "\n";
	} else {
	  $source_duration = 40;
	  if(file_exists($probeLog)) {
		$pLog = file_get_contents($probeLog);
		preg_match('/duration=[0-9.]*/', $pLog, $matches);
		if(count($matches) > 0) {
			$durationTmp = explode("=", $matches[0]);
			$source_duration = round($durationTmp[1]);
		}
	  }
	  // echo "START DURATION:: " . $source_duration . " => " . $durationTmp[1] . "\n";

	  echo "[INFOS][" . $curTime . "][" . gethostname() . "] Split Encode State :: STATUS[Processing Trunk :: OID($outputID) SID($splitID)]" . "\n";

	  popen("tail -50 " . JOB_BASE_FOLDER . "/" . $job . "/" . $logFile . " > " . JOB_BASE_FOLDER . "/" . $job . "/job.info", "r");
       // checkSPVODProgress($SID, $SPType, $logFile, $encLOG, $startLOG)
          echo $splitID, $SPType, JOB_BASE_FOLDER . "/" . $job . "/job.info", JOB_BASE_FOLDER . "/" . $job . "/" . "encode.log", $startLog;//kenny
	  checkSPVODProgress($splitID, $SPType, JOB_BASE_FOLDER . "/" . $job . "/job.info", JOB_BASE_FOLDER . "/" . $job . "/" . "encode.log", $startLog);
	}
   }
} // End Split Encode Trunk Polling

?>
