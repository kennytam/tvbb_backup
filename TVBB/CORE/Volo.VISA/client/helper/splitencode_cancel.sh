#!/bin/sh
# Volo.Worker :: Scripts to Cancel Volo Split Encoder Output Job
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
#
 
if [ $# -lt 4 ]
  then
    echo "USAGE: cancel.sh [JOB ID] [OUTPUT ID] [SPLIT ID] [TRUNK TYPE]"
    exit 1
fi

cd /opt/Volo.VISA/client/helper
JID=$1
OID=$2
SID=$3
TTYPE=$4
JOB_LABEL=SPLIT.F_${OID}_${SID} 
JOB_PATH=/dev/shm/Volo.VISA/splitjobs/F_${SID}
JOB_CANCEL_FILE=/dev/shm/Volo.VISA/splitjobs/F_${SID}/job.cancel
JOB_LOG='/opt/Volo.VISA/logs/VISA.JobCancel'.`hostname`.`date +%Y%m%d`.'log'

echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Split Output Job Cancel :: JID[${1}] OID[${2}] SID[${3}] TRUNKTYPE[${4}] >> ${JOB_LOG}

if [ "$TTYPE" == "V" ]; then
 # Loop till no Volo Split Encode Video Script
 while true; do
  jobStr=`ps -Alf | grep "splitencode.sh" | grep "${SID}" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`

  if [ -z "${jobStr}" ]
  then
	break
  else
	jobs=( ${jobStr} )
	if [ ${#jobs[@]} -gt 0 ]
	then
		kill -9 ${jobs[3]}
		sleep 1
	fi
  fi
 done
else
 # Loop till no Volo Split Encode Video Script
 while true; do
  jobStr=`ps -Alf | grep "splitencode_audio.sh" | grep "${SID}" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`

  if [ -z "${jobStr}" ]
  then
	break
  else
	jobs=( ${jobStr} )
	if [ ${#jobs[@]} -gt 0 ]
	then
		kill -9 ${jobs[3]}
		sleep 1
	fi
  fi
 done
fi

# Loop till no Volo Core Process
while true; do
  jobStr=`ps -Alf | grep "bin/volo\-1.4" | grep "${JOB_LABEL}" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`

  if [ -z "${jobStr}" ]
  then
	break
  else
	jobs=( ${jobStr} )
	if [ ${#jobs[@]} -gt 0 ]
	then
		kill -9 ${jobs[3]}
		sleep 2
	fi
  fi
done

# Let PollJobs clean
sleep 5

# Set CANCEL for Cancel Jobs
if [ -d "${JOB_PATH}" ]; then
	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Volo Split Encode Stop :: SID[${SID}] Volo PID[${jobs[3]}] >> ${JOB_LOG}
	echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_CANCEL_FILE}
	rm -rf ${JOB_PATH}/volo_high_compression* 2>&1
	rm -rf ${JOB_PATH}/*.mov 2>&1
	rm -rf ${JOB_PATH}/*.ts 2>&1
fi
exit 0
