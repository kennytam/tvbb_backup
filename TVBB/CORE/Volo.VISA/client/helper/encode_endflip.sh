#!/bin/sh
#
# Volo.Worker :: Scripts to Spawn Volo Encoder Job
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
#
# Skip Checking input Parameter due to Batch job Sync
# "USAGE: encode.sh [JOB ID] [OUTPUT ID] [SOURCE] [DURATION] [DEST] [PROFILE]"
#
 
cd /opt/Volo.VISA/client/helper
JID=$1
OID=$2
SOURCE=$3
LENGTH=$4
DEST=$5
PROFILE=$6
PARAFILE=/dev/shm/Volo.VISA/jobs/${JID}_${OID}.para
 
# Read Optional Paramter from File
PARA=`cat ${PARAFILE}`
 
#VOLO_BIN=/opt/volo/bin/volo-1.4
VOLO_BIN=/opt/volo/script/volo
JOB_LABEL=F_${OID}
JOB_PATH=/dev/shm/Volo.VISA/jobs/${JOB_LABEL}
JOB_LOG='/opt/Volo.VISA/logs/VISA.JobEncode'.`hostname`.`date +%Y%m%d`.'log'
VISA_BASE=/opt/Volo.VISA/client
TMPOUT=${JOB_PATH}/voloout.mp4
ERRSTR=""
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Submit :: JID[${1}] OID[${2}] SOURCE[${3}] DURATION[${4}] DEST[${5}] PROFILE[${6}] ENCODE PARA[${PARA}] >> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Command :: ./encode.sh ${1} ${2} ${3} ${4} ${5} ${6} >> ${JOB_LOG}
 
# Source Not Exist
if [ ! -f "${SOURCE}" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] SOURCE[${SOURCE}] Not Found >> ${JOB_LOG}
        ERRSTR="[ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] SOURCE[${SOURCE}] Not Found"
fi
 
# Profile Not Exist
if [ ! -f "/opt/volo/config/users/${PROFILE}.xml" ] && [ ! -f "/opt/volo/config/samples/${PROFILE}.xml" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] Device Profile[${PROFILE}.xml] Not Found >> ${JOB_LOG}
        ERRSTR="[ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] Device Profile[${PROFILE}.xml] Not Found"
fi
 
# Create Encode Working Path
mkdir -p ${JOB_PATH}
mv ${PARAFILE} ${JOB_PATH}/

# Mark Job ERROR
if [ ! -z "${ERRSTR}" ]; then
	cd ${JOB_PATH}
	echo ${ERRSTR} > ./encode.log
	echo ${ERRSTR} > ./job.error
	exit 1
fi

# Create Encode Destination Path
OUTPATH=`dirname ${DEST}`
if [ ! -d "${OUTPATH}" ]; then
	mkdir -p ${OUTPATH}
else
        rm -rf ${DEST}
fi

# Parse the Video Duration by MediaInfo
echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.start
if [ ${LENGTH} -eq 0 ]; then
        mediainfo --Output="Video;%Duration%" ${SOURCE} > ${JOB_PATH}/videolength.log
else
        echo ${LENGTH} > ${JOB_PATH}/videolength.log
fi

# Prepare TVB.COM Audio Track Mixing
php /opt/volo/script/mono_2_stereo_prepare.php ${SOURCE} ${JOB_PATH} > ${JOB_PATH}/audioparse.log 2>&1
if [ ! -f "${JOB_PATH}/merge_filter.log" ]
then
	echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] Source Video Contains One Audio Track Only >> ${JOB_LOG}
	ERRSTR="Source Video Contains One Audio Track Only"
	cd ${JOB_PATH}
	echo ${ERRSTR} > ./job.error
	exit 1
fi

# General Encode
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Volo Encode Command :: ${VOLO_BIN} -label SCHEDULAR.F_${OID} -f ${SOURCE} -o ${TMPOUT} -workpath ${JOB_PATH} -do ${PROFILE}.xml ${PARA} >> ${JOB_LOG}
${VOLO_BIN} -label SCHEDULAR.F_${OID} -f ${SOURCE} -o ${TMPOUT} -workpath ${JOB_PATH} -do ${PROFILE}.xml ${PARA} > ${JOB_PATH}/encode.log 2>&1

# TVB.COM Audio and End Flip Handler
if [ -f "${TMPOUT}" ]
then
	/opt/volo/script/volo -f /opt/volo/extras/EF_8mono.mp4 -o ${JOB_PATH}/endflip.mp4 -do ${PROFILE}.xml >> ${JOB_PATH}/audioparse.log 2>&1
	php /opt/volo/script/mono_2_stereo.php ${TMPOUT} ${JOB_PATH}/source.ts ${JOB_PATH} >> ${JOB_PATH}/audioparse.log 2>&1
	php /opt/volo/script/mono_2_stereo.php ${JOB_PATH}/endflip.mp4 ${JOB_PATH}/endflip.ts ${JOB_PATH} >> ${JOB_PATH}/audioparse.log 2>&1
	/opt/volo/script/add_end_flip.sh ${JOB_PATH}/source.ts ${JOB_PATH}/endflip.ts ${DEST} >> ${JOB_PATH}/audioparse.log 2>&1
	rm -rf ${TMPOUT} ${JOB_PATH}/source.ts ${JOB_PATH}/endflip.mp4 ${JOB_PATH}/endflip.ts >> ${JOB_PATH}/audioparse.log 2>&1
	if [ ! -f "${DEST}" ]; then
		cd ${JOB_PATH}
		echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] Fail to Merge Output Video with End Flip > ./job.error
		exit 1
	fi
fi

