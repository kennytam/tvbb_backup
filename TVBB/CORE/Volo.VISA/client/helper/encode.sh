#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
# Volo.Worker :: Scripts to Spawn Volo Encoder Job
# Skip Checking input Parameter due to Batch job Sync
# "USAGE: encode.sh [JOB ID] [OUTPUT ID] [SOURCE] [DURATION] [DEST] [PROFILE] [MODE H.264|H.265] [AUDIO PARA] [SEGMENTOR]"
#
 
cd /opt/Volo.VISA/client/helper
JID=$1
OID=$2
SOURCE=$3
LENGTH=$4
DEST=$5
PROFILE=$6
ENCMODE=$7
PARAFILE=/dev/shm/Volo.VISA/jobs/${JID}_${OID}.para
AUDIOMAP=$8
SEG=$9
 
# Read Optional Paramter from File
if [ -f "${PARAFILE}" ]
then
	PARA=`cat ${PARAFILE}`
else
	PARA=""
fi
 
# H.264
if [ "${ENCMODE}" == "0" ]
then
        VOLO_BIN=/opt/volo/script/volo
        VOLO_BASE=/opt/volo
	VOLO_PROBE=/opt/volo/bin/vprobe
else
        VOLO_BIN=/opt/volo-hevc/script/volo
        VOLO_BASE=/opt/volo-hevc
	VOLO_PROBE=/opt/volo-hevc/bin/vprobe-1.4.1
fi

JOB_LABEL=F_${OID}
JOB_PATH=/dev/shm/Volo.VISA/jobs/${JOB_LABEL}
JOB_LOG='/opt/Volo.VISA/logs/VISA.JobEncode'.`hostname`.`date +%Y%m%d`.'log'
TMPOUT=${JOB_PATH}/voloout.mp4
TMPDST=${JOB_PATH}/volomux.mp4
ERRSTR=""

################ QA SETTING START
## JOB_PATH=/dev/shm/volo/${JOB_LABEL}
## PARA="-duration 10"
## TMPOUT=${JOB_PATH}/voloout.mp4
## TMPDST=${JOB_PATH}/volomux.mp4
################ QA SETTING END

echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Submit :: JID[${1}] OID[${2}] SOURCE[${3}] DURATION[${4}] DEST[${5}] PROFILE[${6}] H.264[${7}] ENCODE PARA[${PARA}] AUDIOMAP[${AUDIOMAP}] SEGMENTOR[${SEG}] >> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Command :: ./encode.sh ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} >> ${JOB_LOG}
 
# Source Not Exist
if [ ! -f "${SOURCE}" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] SOURCE[${SOURCE}] Not Found >> ${JOB_LOG}
        ERRSTR=" Video Source - ${SOURCE} Not Found"
fi
 
# Profile Not Exist
if [ ! -f "${VOLO_BASE}/config/users/${PROFILE}.xml" ] && [ ! -f "${VOLO_BASE}/config/samples/${PROFILE}.xml" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] Device Profile[${PROFILE}.xml] Not Found >> ${JOB_LOG}
        ERRSTR=" Device Profile - ${PROFILE}.xml Not Found"
fi
 
# Create Encode Working Path
if [ ! -d "${JOB_PATH}" ]; then
	mkdir -p ${JOB_PATH}
else
	rm -rf ${JOB_PATH}/*
fi
mv ${PARAFILE} ${JOB_PATH}/

# Mark Job ERROR
if [ ! -z "${ERRSTR}" ]; then
	echo ${ERRSTR} > ${JOB_PATH}/encode.log
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

EXT=${SOURCE##*.}
if [ "$EXT" == "jpg" ] || [ "$EXT" == "txt" ]; then
	echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] Invalid Source Video Format ${EXT} >> ${JOB_LOG}
	ERRSTR=" Invalid Source Video Format - "${EXT}
	echo ${ERRSTR} > ${JOB_PATH}/encode.log
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

# Create Encode Destination Path (with directory 755 and file 664)
echo Volo Encode Start - `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.start
echo Encode Command :: ./encode.sh ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} >> ${JOB_PATH}/job.start
umask ug=rwx,o=rx
OUTPATH=`dirname ${DEST}`

echo "Output Directory Check Before Encode -" >> ${JOB_PATH}/job.start
echo "Output File :: "${DEST} >> ${JOB_PATH}/job.start
echo "Output Path :: "${OUTPATH} >> ${JOB_PATH}/job.start
ls -ltd ${OUTPATH} >> ${JOB_PATH}/job.start 2>&1
ls -lh --full-time ${DEST} >> ${JOB_PATH}/job.start 2>&1
if [ ! -d "${OUTPATH}" ]; then
	echo "Make Output Directory .... " >> ${JOB_PATH}/job.start
	mkdir -p ${OUTPATH} >> ${JOB_PATH}/job.start 2>&1
else
	echo "Remove Old File .... " >> ${JOB_PATH}/job.start
        rm -rf ${DEST} >> ${JOB_PATH}/job.start 2>&1
fi
echo "Output Directory Creation Check -" >> ${JOB_PATH}/job.startt
ls -ltd ${OUTPATH} >> ${JOB_PATH}/job.start 2>&1
ls -lh --full-time ${DEST} >> ${JOB_PATH}/job.start 2>&1

# Parse the Video Duration by MediaInfo
if [ ${LENGTH} -eq 0 ]; then
        mediainfo --Output="Video;%Duration%" ${SOURCE} > ${JOB_PATH}/videolength.log
else
        echo ${LENGTH} > ${JOB_PATH}/videolength.log
fi

# Direct Encode for mp4, No Audio Mapping
EXT=${SOURCE##*.}
if [ "$EXT" != "mxf" ] && [ "$EXT" != "MXF" ]; then
	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Volo Non MXF Encode Command :: ${VOLO_BIN} -label SCHEDULAR.F_${OID} -f ${SOURCE} -o ${DEST} -workpath ${JOB_PATH} -do ${PROFILE}.xml ${PARA} > ${JOB_PATH}/encode.log
	${VOLO_BIN} -label SCHEDULAR.F_${OID} -f ${SOURCE} -o ${DEST} -workpath ${JOB_PATH} -do ${PROFILE}.xml ${PARA} >> ${JOB_PATH}/encode.log 2>&1
	${VOLO_PROBE} -i ${DEST} >> ${JOB_PATH}/job.start 2>&1
        mediainfo ${DEST} > ${JOB_PATH}/${OID}.minfo 2>&1
else
  # Prepare Audio Track Mapping
  echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job :: JOB[${OID}] Verifing Source Audio Track >> ${JOB_LOG}
  php /opt/volo/script/mono_2_stereo_prepare.php ${SOURCE} ${JOB_PATH} ${AUDIOMAP} > ${JOB_PATH}/audioparse.log 2>&1
  if [ ! -f "${JOB_PATH}/merge_filter.log" ]
  then
	echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] Failed to Probe Audio Track >> ${JOB_LOG}
	ERRSTR=" Failed to Probe Audio Track"
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
  fi

  # General Encode
  echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Volo Encode Command :: ${VOLO_BIN} -label SCHEDULAR.F_${OID} -f ${SOURCE} -o ${TMPOUT} -workpath ${JOB_PATH} -do ${PROFILE}.xml ${PARA} > ${JOB_PATH}/encode.log
  ${VOLO_BIN} -label SCHEDULAR.F_${OID} -f ${SOURCE} -o ${TMPOUT} -workpath ${JOB_PATH} -do ${PROFILE}.xml ${PARA} >> ${JOB_PATH}/encode.log 2>&1

  # Audio Mapping
  if [ -f "${TMPOUT}" ]
  then
	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job :: JOB[${OID}] Mapping Encode Output Audio Channel >> ${JOB_LOG}
        mediainfo ${TMPOUT} > ${JOB_PATH}/${OID}.minfo 2>&1
	php /opt/volo/script/mono_2_stereo.php ${TMPOUT} ${TMPDST} ${JOB_PATH} >> ${JOB_PATH}/audioparse.log 2>&1
	if [ ! -f "${TMPDST}" ]; then
		echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] Fail to Remap Audio Channel >> ${JOB_PATH}/job.error
		exit 1
	fi
  else
	echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Encode Output Job Error :: JOB[${OID}] Fail to Encode Source >> ${JOB_PATH}/job.error
	exit 1
  fi
fi

# End Encode Job
umask ug=rwx,o=rx
OUTPATH=`dirname ${DEST}`
if [ ! -d "${OUTPATH}" ]; then
	mkdir -p ${OUTPATH}
fi
mv -f ${TMPDST} ${DEST}
rm -rf ${TMPOUT}
echo "Normal Encode Process Done - "`date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
echo "Verify Encoded Output ...... " >> ${JOB_PATH}/job.start
${VOLO_PROBE} -i ${DEST} >> ${JOB_PATH}/job.start 2>&1
mediainfo ${DEST} > ${JOB_PATH}/${OID}.minfo 2>&1

# Generate HLS / Encrypt HLS
if [ "$SEG" == "HLS" ] || [ "$SEG" == "hls" ]; then
	echo "Generate HLS " ${DEST} ${OUTPATH} >> ${JOB_PATH}/job.start
	./encodeTSGENERAL.sh ${DEST} ${OUTPATH}
fi

echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.done
exit 0
