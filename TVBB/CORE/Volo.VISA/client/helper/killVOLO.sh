#!/bin/sh
#
# Volo.Worker :: Scripts to Kill Volo Encoder Process by LABEL
# VISA v1.3.4 Build Mon Sep  7 02:08:20 HKT 2015 (DaoLab)
#
cd /opt/Volo.VISA/client/helper
LABEL=$1

# Loop till no Volo Process
while true; do
  jobStr=`ps -Alf | grep "volo" | grep "${LABEL}" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
  if [ -z "${jobStr}" ]
  then
        break
  else
        jobs=( ${jobStr} )
        if [ ${#jobs[@]} -gt 0 ]
        then
                echo "Killing PID ["${jobs[3]}"] ......"
                kill -9 ${jobs[3]}
                sleep 2
        fi
  fi
done
