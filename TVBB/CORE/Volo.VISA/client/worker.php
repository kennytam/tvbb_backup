<?php
//
// VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
//

require_once("client/visa.php");
require_once("logic/workload.php");

declare(ticks=1);
pcntl_signal(SIGTERM, "signal_handler");
pcntl_signal(SIGINT, "signal_handler");

function signal_handler($signal) {
    $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
    switch($signal) {
    case SIGTERM:
    case SIGKILL:
    case SIGINT:
      {
        $visa = new VisaClient();
        $xml = $visa->worker_leave();
        echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.WORKER Leave :: STATUS[" . $xml->Response->Status . "]\n";
        unset ($visa);
      } exit;
    }
}

# Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

$workload = new MachineWorkload();
$visa = new VisaClient();

// Join
// $load = sys_getloadavg(); // return array of average 1, 5, 15 minutes
$load[0]=1.0;
$xml = $visa->worker_join(gethostname(), (int) (100 - $load[0]), false);
echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.WORKER Join :: STATUS[" . $xml->Response->Status . "]\n";

// Get Worker Working State (A = Active, S = Suspend)
$ready = $workload->is_ready();
$xml = $visa->worker_heartbeat((int) (100 - $load[0]), $ready);
echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.WORKER Heartbeat :: STATUS[" . $xml->Response->Status . "] READY[" . ($ready?"true":"false") . "]\n";
$xml = $visa->worker_getstate();
echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.WORKER State :: STATUS[" . $xml->Response->State . "]\n";
if (!file_exists($visa->batchpath)) popen("mkdir -p " . $visa->batchpath, "r");
if (!file_exists($visa->splitpath)) popen("mkdir -p " . $visa->splitpath, "r");

while ( true ) {
  $curTime = $procStart = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
  // Get Cancelled Encode for Whatever Status
  $xml = $visa->worker_getcancel();
  if ( strcasecmp("OK", $xml->Response->Status) == 0 ) {
    $output = $xml->Response->Output;
    if ( $output->count() > 0 ) {
      foreach ($output->children() as $o) {
        $id      = intval($o->OutputID);
        $job_id  = intval($o->JobID);
        $o_num   = intval($o->OutputNumber);
        $stage   = intval($o->Stage);

        if ( $stage == VisaClient::STAGE_PROGRESS ) {
		echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.WORKER Encode Output Job Cancel :: JID[" . $job_id . "] OID[" . $id . "]\n";
		exec("$visa->cancelcmd $job_id $id");
	}

       	$visa->worker_update($id, VisaClient::STAGE_CANCELLED, 0, 0, '1', 0, '[STATUS] Normal Encode Process Cancelled');
        $workload->set_busy();
  	$visa->sleep();
      }
    }
  }
  $xml = $visa->worker_spgetcancel();
  var_dump($xml);
  if ( strcasecmp("OK", $xml->Response->Status) == 0 ) {
    $output = $xml->Response->Output;
    if ( $output->count() > 0 ) {
      foreach ($output->children() as $o) {
        $id         = intval($o->SplitID);
        $output_id  = intval($o->OutputID);
        $job_id     = intval($o->JobID);
        $trunk_type = trim($o->TrunkType);
        $stage      = intval($o->Stage);

        //kenny
        if ( $stage == VisaClient::STAGE_PROGRESS ) {
		echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.WORKER Split Encode Output Job Cancel :: JID[" . $job_id . "] OID[" . $output_id . "] SID[" . $id . "] TRUNKTYPE[" . "]\n";
		exec("$visa->splitcancelcmd $job_id $output_id $id $trunk_type");
	}
        var_dump($stage);
       	$visa->worker_spupdate($id, $trunk_type, VisaClient::STAGE_CANCELLED, 0, 0, '1', 0, '[STATUS] Trunk Encode Process Cancelled');
        $workload->set_busy();
  	$visa->sleep();
      }
    }
  }

  // Poll Job Status
  exec("$visa->polljobcmd > /dev/null 2>&1 &");

  // Worker Status for Pick Jobs
  $ready = $workload->is_ready();
  $batch = false;
  $xml = $visa->worker_heartbeat((int) (100 - $load[0]), $ready);

  // Get Worker Working State (A = Active, S = Suspend)
  $xml = $visa->worker_getstate();

  if ( $ready && ($xml->Response->State == "A") ) {

    // Get Single New Encode Job
    $xml = $visa->worker_pick();
    // DEBUG
    if ($visa->debug){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VISA.WORKER worker_pick() :: TIME[" . $procStart . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($procStart,0,19)) ) . ")\n";
    }
    
    // DEBUG END
    if ( strcasecmp("OK", $xml->Response->Status) == 0 ) {
      $output = $xml->Response->Output;

      if ( $output->count() > 0 ) {
        foreach ($output->children() as $o) {
	 if ($o->Mode == "S"){
          $spid     = $o->SplitID;
          $id       = $o->OutputID;
          $jobid    = $o->JobID;
          $srctype  = $o->SplitSrcType;
          $srctotal = $o->SplitSrcTotal;
          $path     = $o->SplitSrcPath;
          if ( substr($path, -1) != '/' ) $path = $path . '/';
          $source   = $path . $o->SplitSrcFile;
          $path     = $o->SplitDstPath;
          if ( substr($path, -1) != '/' ) $path = $path . '/';
          $dest     = $path . $o->SplitDstFile;
          $profile  = $o->Profile;     
          $para     = "-vsync 0 -container ts -audio OFF -stitchable Y " . $o->EncodeParameter;     
          $mode     = $o->Mode;     
          $encodemode = $o->EncodeMode;
          $encodeunit = $o->EncodeUnit;
	 } else {
          $id       = $o->OutputID;
          $jobid    = $o->JobID;
          $source   = $o->Source;
          $duration = $o->Duration * 1000;
          $path     = $o->Destination;
          if ( substr($path, -1) != '/' ) $path = $path . '/';
          $dest     = $path . $o->Filename;
          $profile  = $o->Profile;     
          $para     = $o->EncodeParameter;     
          $mode     = $o->Mode;     
          $segmentor= $o->Segmentor;     
	  $priority = $o->Priority;
          $encodemode = $o->EncodeMode;
          $encodeunit = $o->EncodeUnit;
	 }

          $workload->set_busy();

	  // Split Encode Mode
	  if ($mode == "S"){
	    if ($srctype == "V"){
		echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.WORKER Split Encode Video Output Job Submit :: JID[$jobid] OID[$id] SID[$spid] SOURCE[$source] DEST[$dest] PROFILE[$profile] PARA[" . $visa->splitpath . "/" . $jobid . "_" . $id . "_" . $spid . ".para][" . $para . "] H.264[$encodemode] UNIT[$encodeunit]" . "\n";
		file_put_contents($visa->splitpath . '/' . $jobid . '_' . $id . '_' . $spid . '.para', $para);
		exec("$visa->splitvideocmd $jobid $id $spid $source $dest $profile $encodemode > /dev/null 2>&1 &");
	    } else {
		echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.WORKER Split Encode Audio Output Job Submit :: JID[$jobid] OID[$id] SID[$spid] SOURCE[$source] DEST[$dest] PROFILE[$profile] H.264[$encodemode] UNIT[$encodeunit]" . "\n";
		exec("$visa->splitaudiocmd $jobid $id $spid $source $dest $profile $encodemode > /dev/null 2>&1 &");
	    }
	  } else if ($mode == "B"){
	    // Batch Encode Mode
	    $batch = true;
	    $batchstr = $jobid . ' ' . $id . ' ' . $source . ' ' . $duration . ' ' . $dest . ' ' . $profile . ' ' . "\n";
	    file_put_contents($visa->batchpath . '/' . $jobid . '.job', $batchstr, FILE_APPEND);
	    file_put_contents($visa->batchpath . '/' . $jobid . '_' . $id . '.para', $para);
	  } else {
	    // Single Encode Mode

	    // Check Audio Mapping Control File
            $dirPath = pathinfo($source, PATHINFO_DIRNAME);
            $audioCtl = $audioPARA = $curlist = "";
            $pathLoop = explode("/", $dirPath);
            for ($i=1; $i<sizeof($pathLoop); $i++){
                 $curlist .= "/" . $pathLoop[$i];
                 $audioCtl = $curlist . "/" . $visa->audiomap;
                 if (file_exists($audioCtl)){
                     $austart= true;
                     $aufile = fopen($audioCtl, "r");
                     if ($aufile){
                         while(is_resource($aufile) && ($line = fgets($aufile)) !== false) {
                               $line = str_replace("\n", "", $line);
                               $line = str_replace(",", "_", $line);
                               if ($austart){
                                   $audioPARA = $line;
                                   $austart = false;
                               } else {
                                   $audioPARA .= "," . $line;
                               }
                         }
                         fclose($aufile);
                     }
                 }
            }
	    if ($audioPARA == "") $audioPARA = "NULL";
	    if ($segmentor == "") $segmentor = "NULL";

            echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.WORKER Normal Encode Output Job Submit :: JID[$jobid] OID[$id] SOURCE[$source] DURATION[$duration] DEST[$dest] PROFILE[$profile] PARA[" . $visa->batchpath . "/" . $jobid . "_" . $id . ".para][" . $para . "] SEGMENTOR[" . $segmentor . "] H.264[$encodemode] UNIT[$encodeunit] AUDIOCTL[$audioCtl] AUDIOMAP[$audioPARA]" . "\n";
	    file_put_contents($visa->batchpath . '/' . $jobid . '_' . $id . '.para', $para);
	    exec("$visa->encodecmd $jobid $id $source $duration $dest $profile $encodemode $audioPARA $segmentor > /dev/null 2>&1 &");
          }
        } // end foreach
      }

      if ($batch){
            echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.WORKER Batch Encode Job Submit :: JOB FILE[$visa->batchpath/$jobid.job]\n";
	    exec("$visa->batchcmd $visa->batchpath/$jobid.job > /dev/null 2>&1 &");
      }
    } // end STATUS
  } // end ready

  $visa->sleep();
}
unset ($visa);
?>

