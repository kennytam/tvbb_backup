<?php
//
// VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
//

require_once("client/visa.php");

$visa = new VisaClient();

# Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$xml = $visa->manage_capacity_update();
$xml = $visa->manage_periodic();
echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.PERIODIC :: STATUS[" . $xml->Response->Status . "]\n";

while ( true ) {
  $xml = $visa->manage_capacity_update();
  $xml = $visa->manage_periodic();
  sleep (60);
}

unset ($visa);
?>
