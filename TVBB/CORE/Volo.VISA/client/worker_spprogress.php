<?php
//
// VISA v1.3.4 Build Fri May 01 23:51:12 HKT 2015 (DaoLab)
//
require_once("client/visa.php");

parse_str(implode('&', array_slice($argv, 1)), $_GET);
if ( !isset($_GET['sid']) || !isset($_GET['trunktype']) || !isset($_GET['stage']) ) {
  echo "USAGE: worker_spprogress.php [sid=(Split Output ID)] [trunktype=V|A] [stage=1(progress)|2(completed)|3(cancelled)|-1(failed)] [progress=0-100] [progressfps=999] [progress=1,2] [error=integer] [message=Message String]\n";
  exit (1);
}
if (!isset($_GET['progress'])) $_GET['progress'] = 0;
if (!isset($_GET['progress_fps'])) $_GET['progress_fps'] = 0;
if (!isset($_GET['progress_mode'])) $_GET['progress_mode'] = '1';
if (!isset($_GET['err'])) $_GET['err'] = '';
if (!isset($_GET['message'])) $_GET['message'] = '';

$visa = new VisaClient();
$xml = $visa->worker_spupdate($_GET['sid'], $_GET['trunktype'], $_GET['stage'], $_GET['progress'], $_GET['progress_fps'], $_GET['progress_mode'], $_GET['err'], $_GET['message']);
unset ($visa);

# Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

echo "[INFOS][" . $curTime . "  ][" . gethostname() . "] SPLIT OUTPUT JOB PROGRESS UPDATE :: STATUS[" . $xml->Response->Status . "]\n";
?>

