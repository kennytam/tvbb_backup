<?php
//
// VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
//

require_once("client/visa.php");

if ($argc < 4){
        echo "USAGE: publish.php [Source Video] [Destination Full Path] [Plan XML Filename]\n";
        exit(1);
}
$source = $argv[1];
$dest   = $argv[2];
$plan   = $argv[3];

# Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

# Get target basename without extension
$ext = pathinfo($source, PATHINFO_EXTENSION);
$file = basename($source, ".".$ext);

$planxml = 'plan/' . $plan . '.xml';
if (file_exists($planxml)) {
    $outputs = array();

	$xml = simplexml_load_file($planxml);  
	foreach($xml->children() as $o){
		if ( strcasecmp($o->getName(), "output")!= 0 ) continue;

		array_push($outputs, array(
				'OutputFile'    => $file . $o->OutputSuffix . '.' . $o->OutputExtension,
				'DeviceOutputProfile' => $o->DeviceOutputProfile,
				'OutputContainer' => $o->OutputContainer,
				'VideoBitrate' => $o->VideoBitrate,
				'AudioBitrate' => $o->AudioBitrate,
				'Deinterlace' => $o->Deinterlace,
				'PassMode' => $o->PassMode,
				'Segmentor' => $o->Segmentor,
				'EncodeLength' => $o->EncodeLength
				));
	}
	$visa = new VisaClient();
	$xml = $visa->job_submit($source, $dest, 0, 0, 'true', $outputs);

    if ( strcasecmp($xml->Response->Status, "OK") == 0 ) {
      echo "[INFOS][" . $curTime . "][" . gethostname() . "] JOB PUBLISH OK :: PLAN[$plan] SOURCE[$source] DEST[$dest] JOBID[" . $xml->Response->JobID[0] . "]\n";
    } else {
      echo "[INFOS][" . $curTime . "][" . gethostname() . "] JOB PUBLISH FAIL :: PLAN[$plan] SOURCE[$source] DEST[$dest] ERROR[" . $xml->Response->Reason[0] . "]\n";
    } 
    // echo $xml->asXML();
    unset ($visa);
} else {
    echo "[INFOS][" . $curTime . "][" . gethostname() . "] JOB PUBLISH FAIL :: PLAN[$plan] SOURCE[$source] DEST[$dest] ERROR[$plan.xml Not Found]\n";
}
?>
