<?php
//
// VISA v1.3.4 Build Sun Sep  6 02:53:30 HKT 2015 (DaoLab)
//
// TODO: change to check JobSlot usage in (v1.3.5)

class MachineWorkload {
  const BUSY_TIMER = 5;  
  protected $config;
  protected $busyflag;
  protected $busytimer;
  
  ///////////////////////////////////////////////////
  // Constructor & Destructor
  ///////////////////////////////////////////////////
  function __construct() {
      $this->config = include('config/config.php');
      $this->set_ready();
  }
  function __destruct() {
      if ( isset( $this->config) ) unset ( $this->config);
      $this->set_ready();
  }

  ///////////////////////////////////////////////////
  // Public Functions
  ///////////////////////////////////////////////////
  public function set_busy() {
      $this->busyflag = true;
      $this->busytimer = time();
  }
  public function set_ready() {
      $this->busyflag = false;
  }
  public function is_ready() {
      // TODO: change to check JobSlot usage in (v1.3.5)
      // $this->config = include('config/config.php');
      // $cpu = round($this->cpu_load(), 0, PHP_ROUND_HALF_UP);
      // return $cpu < $this->config['threshold_cpu'];
      // Disabled Process Check :: Sun Sep  6 02:53:30 HKT 2015
      // $jobStr='ps -Alf | grep "encode.sh" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1';
      // $RUN = exec($jobStr);
      // if (substr($RUN, 0, 1) != ""){
      // $this->set_busy();
      //     return false;
      // }
      if ( $this->busyflag ) {
        if ( time() - $this->busytimer >= self::BUSY_TIMER ) {
          $this->set_ready();
        } else {
          return false;
        }
      }
      return true;
  }

  ///////////////////////////////////////////////////
  // Helper
  ///////////////////////////////////////////////////
  protected function cpu_load() {
      $load = sys_getloadavg(); // return array of average 1, 5, 15 minutes
      return $load[0];
  }

};

?>
