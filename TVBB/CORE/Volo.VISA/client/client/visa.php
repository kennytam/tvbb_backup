<?php
//
// VISA v1.3.4 Build Fri May 01 23:51:12 HKT 2015 (DaoLab)
//

class VisaClient {
  const ENDPOINT_API     = 'schd_api';
  const ENDPOINT_WORKER  = 'schd_worker';
  const ENDPOINT_MANAGER = 'schd_manager'; 
  const STAGE_SCHEDULED = 0;
  const STAGE_PROGRESS  = 1;
  const STAGE_COMPLETED = 2;
  const STAGE_CANCELLED = 3;

  protected $config;
  
  ///////////////////////////////////////////////////
  // Constructor & Destructor
  ///////////////////////////////////////////////////
  function __construct() {
    $this->config = include('config/config.php');
    $this->ctlpath   = $this->config['control_path'];
    $this->batchpath = $this->config['batch_path'];
    $this->batchcmd  = $this->config['batch_cmd'];
    $this->cancelcmd = $this->config['cancel_cmd'];
    $this->encodecmd = $this->config['encode_cmd'];
    $this->splitpath = $this->config['split_path'];
    $this->splitvideocmd  = $this->config['split_video_cmd'];
    $this->splitaudiocmd  = $this->config['split_audio_cmd'];
    $this->splitcancelcmd = $this->config['split_cancel_cmd'];
    $this->polljobcmd = $this->config['polljob_cmd'];
    $this->audiomap   = $this->config['audio_map_file'];
    $this->debug      = $this->config['worker_debug_mode'];
  }
  function __destruct() {
      if ( isset( $this->config) ) unset ( $this->config);
      if ( isset( $this->ctlpath ) ) unset ( $this->ctlpath);
      if ( isset( $this->batchpath ) ) unset ( $this->batchpath);
      if ( isset( $this->batchcmd ) ) unset ( $this->batchcmd);
      if ( isset( $this->cancelcmd ) ) unset ( $this->cancelcmd);
      if ( isset( $this->encodecmd ) ) unset ( $this->encodecmd);
      if ( isset( $this->splitpath ) ) unset ( $this->splitpath);
      if ( isset( $this->splitvideocmd ) ) unset ( $this->splitvideocmd);
      if ( isset( $this->splitaudiocmd ) ) unset ( $this->splitaudiocmd);
      if ( isset( $this->splitcancelcmd ) ) unset ( $this->splitcancelcmd);
      if ( isset( $this->polljobcmd ) ) unset ( $this->polljobcmd);
      if ( isset( $this->audiomap) ) unset ( $this->audiomap);
      if ( isset( $this->debug) ) unset ( $this->debug);
  }
  
  public function sleep() {
      $this->config = include('config/config.php');
      sleep ($this->config['worker_pollrate']);
  }
  
  ///////////////////////////////////////////////////
  // Helper
  ///////////////////////////////////////////////////
  protected function send($endpoint, $cmd, $param) {
      $payload = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<VISA>\n<Request Command=\"$cmd\">\n";
      foreach ($param as $k => $v) {
        $payload = $payload . "<$k>$v</$k>\n";
      }
      $payload = $payload . "</Request></VISA>";
      return $this->send_raw($endpoint, $payload);
  }
  protected function send_raw($endpoint, $payload) {
      $options = array(
          'http' => array(
            'header'  => "Content-type: application/xml; charset=utf-8\r\n",
            'method'  => 'POST',
            'content' => $payload,
          ),
      );
      $url = $this->config['schd_server'] . $this->config[$endpoint];
      $context  = stream_context_create($options);
      $result = file_get_contents( $url, false, $context);
      try {
        $xml = new SimpleXMLElement( $result );
      } catch (Exception $e) {
        echo $e;
        echo "\nResponse Detail:\n" . $result . "\n";
        exit;
      }
      return $xml;
  }

  ///////////////////////////////////////////////////
  // Client API
  ///////////////////////////////////////////////////
  public function job_submit($source,$dest,$encode_priority,$split_encode,$outputs) {
      if ( !isset( $encode_priority ) ) $encode_priority=0;
      if ( !isset( $split_encode ) ) $split_encode='false';
      $dom = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><VISA/>');
      $x_request = $dom->addChild("Request");
      $x_request->addAttribute("Command", "JobSubmit");
      $x_request->addChild("SourceUri", $source);
      $x_request->addChild("OutputPath", $dest);
      $x_request->addChild("EncodePriority", $encode_priority);
      $x_request->addChild("SplitEncode", $split_encode);

      $x_output = $x_request->addChild("Output");
      $num = 1;
      foreach ($outputs as $o) {
      	$x_o = $x_output->addChild("OutputJob");
      	$x_o->addChild("OutputNumber", $num);
        $x_o->addChild("EncodeMode", $o['EncodeMode']);
      	$x_o->addChild("OutputFile", $o['OutputFile']);
      	$x_o->addChild("DeviceOutputProfile", $o['DeviceOutputProfile']);
      	$x_o->addChild("OutputContainer", $o['OutputContainer']);
      	$x_o->addChild("VideoBitrate", $o['VideoBitrate']);
      	$x_o->addChild("AudioBitrate", $o['AudioBitrate']);
      	$x_o->addChild("Deinterlace", $o['Deinterlace']);
      	$x_o->addChild("PassMode", $o['PassMode']);
      	$x_o->addChild("Segmentor", $o['Segmentor']);
	if ($o['EncodeLength'] == "")
      		$x_o->addChild("EncodeLength", '0');
	else
      		$x_o->addChild("EncodeLength", $o['EncodeLength']);
      	$num = $num +1;
      }
      // file_put_contents('/tmp/visa.log', $dom->asXML());
      return $this->send_raw(VisaClient::ENDPOINT_API, $dom->asXML());
  }

  ///////////////////////////////////////////////////
  // Worker
  ///////////////////////////////////////////////////
  public function worker_join($name, $power, $ready) {
      return $this->send(VisaClient::ENDPOINT_WORKER, "WorkerJoin", array(
          'Name'  => $name,
          'Power' => $power,
          'Ready' => $ready ? "true" : "false"
          ));
  }
  public function worker_leave() {
      return $this->send(VisaClient::ENDPOINT_WORKER, "WorkerLeave", array());
  }
  public function worker_heartbeat($power, $ready) {
      return $this->send(VisaClient::ENDPOINT_WORKER, "WorkerHeartbeat", array(
          'Power' => $power,
          'Ready' => $ready ? "true" : "false"
          ));
  }
  public function worker_getstate() {
      return $this->send(VisaClient::ENDPOINT_WORKER, "WorkerGetState", array());
  }
  public function worker_pick() {
      return $this->send(VisaClient::ENDPOINT_WORKER, "WorkerPick", array());
  }
  public function worker_getcancel() {
      return $this->send(VisaClient::ENDPOINT_WORKER, "WorkerGetCancel", array());
  }
  public function worker_spgetcancel() {
      return $this->send(VisaClient::ENDPOINT_WORKER, "WorkerSPGetCancel", array());
  }
  public function worker_update($oid, $stage, $progress, $progress_fps, $progress_mode, $err, $message) {
      return $this->send(VisaClient::ENDPOINT_WORKER, "WorkerProgress", array(
        'OutputID'    => $oid,
        'Stage'       => $stage,
        'Progress'    => $progress,
        'ProgressFPS' => $progress_fps,
        'ProgressMode'=> $progress_mode,
        'Error'       => $err,
        'NodeMessage' => $message
        ));
  }
  public function worker_spupdate($sid, $trunk_type, $stage, $progress, $progress_fps, $progress_mode, $err, $message) {
      return $this->send(VisaClient::ENDPOINT_WORKER, "WorkerSPProgress", array(
        'SplitID'     => $sid,
        'TrunkType'   => $trunk_type,
        'Stage'       => $stage,
        'Progress'    => $progress,
        'ProgressFPS' => $progress_fps,
        'ProgressMode'=> $progress_mode,
        'Error'       => $err,
        'NodeMessage' => $message
        ));
  }

  ///////////////////////////////////////////////////
  // Management
  ///////////////////////////////////////////////////
  public function manage_capacity_update() {
      return $this->send(VisaClient::ENDPOINT_MANAGER, "CapacityUpdate", array());
  }
  public function manage_periodic() {
      return $this->send(VisaClient::ENDPOINT_MANAGER, "Periodic", array());
  }

};

?>
