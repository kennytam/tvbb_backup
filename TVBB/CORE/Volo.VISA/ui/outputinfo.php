<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
if (isset($_GET["date"]))
        $jdate = htmlspecialchars($_GET["date"]);
else
        $jdate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
?>
<html>
  <head>
   <title>Volo.VISA</title>
  </head>
  <body bgcolor="grey">
<?php
  echo "<p><b>[ Encode Output Status ]</b>&nbsp;&nbsp;&nbsp;&nbsp;System Time - " . $para['time_zone'] . " " . $curtime . "</p><hr>";
  echo '<table border="0" bgcolor="white" width="100%"><tr><td>';
if (isset($_GET["OID"])){
        $OID = htmlspecialchars($_GET["OID"]);
} else {
        echo "VISA Reponse ::<br />ERROR REQUEST";
        exit();
}
 
$link = pg_Connect("host=localhost dbname=visadb user=visa password=1234");
$src  = pg_exec($link, "SELECT profile,videobit,audiobit,resolu,deint,pmode,container,segmode,encode_mode,length,output_info FROM output WHERE id=" . $OID . ";");
$nrow = pg_fetch_array($src);
echo '[ Encode Parameter ]<br />';
echo '<pre>';
echo str_pad('Volo Profile', 41) . ": " . $nrow["profile"] . "<br />";
echo str_pad('Video Codec', 41) . ": ";
if ($nrow["encode_mode"] == "0")
	echo "H.264";
else
	echo "HEVC";
echo "<br />";
echo str_pad('Segmentor', 41) . ": ";
if ($nrow["segmode"] == "")
	echo "No";
else
	echo $nrow["segmode"];
echo "<br /><br />";
echo 'Customer Encode Parameter' . "<br />";
echo str_pad('Output Resolution', 41) . ": ";
if ($nrow["resolu"] == "")
	echo "Auto";
else
	echo $nrow["resolu"];
echo "<br />";
echo str_pad('Video Bitrate (kbps)', 41) . ": ";
if ($nrow["videobit"] > 0)
	echo $nrow["videobit"];
else
	echo "Same as Profile";
echo "<br />";
echo str_pad('Audio Bitrate (kbps)', 41) . ": ";
if ($nrow["audiobit"] > 0)
	echo $nrow["audiobit"];
else
	echo "Same as Profile";
echo "<br />";
echo str_pad('Container', 41) . ": ";
if ($nrow["container"] == "")
	echo "Same as Profile";
else
	echo $nrow["container"];
echo "<br />";
echo str_pad('Encode Duration', 41) . ": ";
if ($nrow["length"] > 0)
	echo $nrow["length"];
else
	echo "Full Length";
echo "<br />";
echo str_pad('Deinterlace', 41) . ": ";
if ($nrow["deint"] == "off")
	echo "Off";
else
	echo "Same as Profile";
echo "<br />";
echo str_pad('Encode Pass Mode', 41) . ": ";
if ($nrow["pmode"] == "1")
	echo "One Pass Mode";
else
	echo "Same as Profile";
echo '<br /><br /></pre>';
echo '[ Output Video Information by Mediainfo Tool ]<br />';
if (trim($nrow["output_info"]) == ""){
	echo '<pre>';
	echo str_pad('Output Mediainfo', 41) . ": ";
	echo 'Output Not Ready .....</pre>';
} else {
	echo '<pre>', htmlentities($nrow["output_info"]), '</pre>';
}
echo "<br />";
pg_close($link);
?>
  </td></tr></table>
  <p><p>
  </body>
</html>
