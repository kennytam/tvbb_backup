<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.4.8 Build Tue Jan 17 16:17:46 HKT 2017 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
?>
<html>
  <head>
   <title>Volo.VISA</title>
   <meta http-equiv="refresh" content="300">
   <link rel="stylesheet" href="./template/styles.css">
  </head>
  <body style="background:rgba(65,65,65,1)">
  <div id='header'>
<?php
  echo "<p><b>VISA Admin Console v1.4.8 [Node : " . $para['volo_node'] . "] &nbsp;&nbsp;&nbsp;</b></p>";
?>

  </div>
  <div id='cssmenu'>
  <ul>
   <li class='active'><a href='./index_user.php'><span>Job Queue</span></a></li>
   <li><a href='./job_filter_user.php'><span>Job Filter</span></a></li>
  </ul>
  </div>
  <div id='joblist'>
  <table border="0" bgcolor="white"><tr><td>
<?php
  // Input Parameter
  $main_queue=true;
  if (isset($_GET["jobid"]))
        $sJID = (integer) htmlspecialchars($_GET["jobid"]);
  else
        $sJID = 0;

  if ($sJID > 0){
        $edate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	$jdate = "";
  } else {
   if (isset($_GET["sdate"]))
        $sdate = htmlspecialchars($_GET["sdate"]);
   else
        $sdate = "";
   if (isset($_GET["edate"]))
        $edate = htmlspecialchars($_GET["edate"]);
   else
        $edate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
   if ($sdate == "")
	$jdate = "";
   else
	$jdate = "(date_trunc('day',job.ctime) >= '" . $sdate . "' AND date_trunc('day',job.ctime) <= '" . $edate . "')";

   if (isset($_GET["sid"]))
	$sid = htmlspecialchars($_GET["sid"]);
   else
	$sid = 1;

   if (isset($_GET["sname"]))
	$sname = trim(htmlspecialchars($_GET["sname"]));
   else
	$sname = "*";
   if ($sname == "*")
	$jname = "";
   else 
	$jname = "(source like '%" . $sname . "%')";

   if (isset($_GET["dest"]))
	$dest = trim(htmlspecialchars($_GET["dest"]));
   else
	$dest = "*";
   if ($dest == "*")
	$jdest = "";
   else 
	$jdest = "(destination like '%" . $dest . "%')";

   if (isset($_GET["sprior"]))
	$sprior = htmlspecialchars($_GET["sprior"]);
   else
	$sprior = 0;
   if (isset($_GET["eprior"]))
	$eprior = htmlspecialchars($_GET["eprior"]);
   else
	$eprior = 999999;
   $jpriority = "(priority >= " . $sprior . " AND priority <= " . $eprior . ")";
 
   if (isset($_GET["split"]))
	$split = htmlspecialchars($_GET["split"]);
   else
	$split = "";
   if ($split == "Y")
	$jsplit = "(split_encode = true)";
   else if ($split == "N")
	$jsplit = "(split_encode = false)";
   else
	$jsplit = "";

   if (isset($_GET["status"]))
	$status = trim(htmlspecialchars($_GET["status"]));
   else
	$status = "DEFAULT";
   if ($status == "ALL")
	$jstatus = "";
   else if ($status == "WAIT")
	$jstatus = "(stage = 0)";
   else if ($status == "QUEUE")
	$jstatus = "(stage = 1 AND progress < 3)";
   else if ($status == "RUN")
	$jstatus = "(stage = 1 AND progress > 2)";
   else if ($status == "DONE")
	$jstatus = "(stage = 2)";
   else if ($status == "FAIL")
	$jstatus = "(stage < 0)";
   else if ($status == "CANCEL")
	$jstatus = "(stage = 3)";
   else
	$jstatus = "(stage IN (-1,0,1))";
  }

  $config = include('../scheduler/config/database.php');
  $connstr = "host=" . $config['db_server'] .  " user=". $config['db_user'] .  " password=". $config['db_pass'] .  " dbname=".$config['db_name'];
  $link = pg_connect($connstr)
	or die('DB Connection Error : ' . pg_last_error());

  if ($sJID > 0){
	$jquery = "SELECT * FROM JOB WHERE id = " . $sJID . ";";
	$main_queue = false;
  } else {
   $withEND = false;
   $jquery = "SELECT * FROM JOB WHERE ";
   if ($jdate != ""){
	$jquery .= $jdate;
	$withEND = true;
	$main_queue = false;
   }
   if ($jname != ""){
	if ($withEND) $jquery .= " AND ";
	$jquery .= $jname;
	$withEND = true;
	$main_queue = false;
   }
   if ($jdest != ""){
	if ($withEND) $jquery .= " AND ";
	$jquery .= $jdest;
	$withEND = true;
	$main_queue = false;
   }
   if ($withEND)
	$jquery .= " AND id IN (SELECT DISTINCT job_id FROM output WHERE job_id >= " . $sid;
   else
	$jquery .= " id IN (SELECT DISTINCT job_id FROM output WHERE job_id >= " . $sid;

   // Get the Skip Error Job List
   if ($main_queue){
	$skipfile = $para["volo_thumb_path"] . "/errskip.jid";
	$skipjid  = "";
	if(file_exists($skipfile)){
		$skipjid = file_get_contents($skipfile);
		$skipjid = preg_replace("/[\n]/", "", $skipjid);
	}
	if ($skipjid != "") $jquery .= " AND job_id NOT IN (" . $skipjid . ")";
   }

   if ($jpriority != "") $jquery .= " AND " . $jpriority;
   if ($jsplit != "") $jquery .= " AND " . $jsplit;
   if ($jstatus == "")
	$jquery .= " ) ORDER BY ID DESC;";
   else
	$jquery .= " AND " . $jstatus . " ) ORDER BY ID DESC;";
  }

  $result = pg_exec($link, $jquery);
  $numrows = pg_numrows($result);
  if ($numrows == "") $numrows = 0;

  # $result = pg_exec($link, "SELECT * FROM JOB WHERE (date_trunc('day',job.ctime) >= '" . $jdate . "' AND date_trunc('day',job.ctime) <= '" . $jdate . "') AND id IN (SELECT DISTINCT job_id FROM output WHERE " . $jpriority . " AND stage<2) ORDER BY ID DESC;");
  #$result = pg_exec($link, "SELECT * FROM JOB WHERE id IN (SELECT DISTINCT job_id FROM output WHERE job_id>1900 AND stage<2) ORDER BY ID DESC;");
  #$numrows = pg_numrows($result);
  echo '<table border="1" bgcolor="white" width="auto">';
  echo "<p><b>[ Encode Queue ] :: </b>Encode Date [" . $para['time_zone'] . " " . $edate . "] Number of Jobs [" . $numrows . "]";
  // echo "Number of Batch Job [$numrows]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Time [" . $para['time_zone'] . " " . $curtime]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='./xml_calls.html' target=_blank style='text-decoration:none'>VISA Request/Response Log</a></p>";
  echo "</p>";
  ?>
  <tr>
   <th>JOB ID</th>
   <th>SOURCE VIDEO</th>
   <th>SOURCE / DESTINATION PATH</th>
   <th>JOB ENCODE TIME</th>
  </tr>
  <?php
 
   // Loop on rows in the result set.
 
   for($ri = 0; $ri < $numrows; $ri++) {
     $row = pg_fetch_array($result, $ri);
     // Check Thumbnail
     $thumb_file = substr($row["ctime"],0,4) . substr($row["ctime"],5,2) . substr($row["ctime"],8,2) . "/" . $row["id"] . ".jpg";
     if ( !file_exists($para["volo_thumb_path"] . "/" . $thumb_file) ){
          $thumb_file = "201601/" . $row["id"] . ".jpg";
          if ( !file_exists($para["volo_thumb_path"] . "/" . $thumb_file) ){
               $thumb_file = "no-thumbnail.png";
          }
     }


     // Display Output in Queue A/B
     echo "<tr>\n";
     echo "<td align='middle'>", $row["id"], "&nbsp;</td>
     <td><img src='./thumbnail/", $thumb_file, "' height='80px'><br />Video Length: ", round($row["source_duration"]/60, 2,PHP_ROUND_HALF_UP), " (mins)&nbsp;</td>
     <td colspan='1'>&nbsp;<a href='./sourceinfo.php?JID=", $row["id"], "' target=_blank style='text-decoration:none'>&nbsp;", $row["source"], "&nbsp;</a><br /><br />&nbsp;&nbsp;", $row["destination"], "&nbsp;</td>
     <td>";
     echo "&nbsp;Submit Time : " . substr($row["ctime"],0, 19);

     // Get Overall Encode Time
     $job = pg_exec($link, "SELECT id, length FROM OUTPUT WHERE job_id=" . $row["id"] . " ORDER BY id LIMIT 1;");
     $outid  = pg_fetch_array($job);
     pg_free_result($job);
     $job = pg_exec($link, "SELECT min(trunksplit_time), max(trunkready_time) FROM SPLIT_OUTPUT WHERE output_id=" . $outid["id"] . ";");
     $trunkresult = pg_fetch_array($job);
     pg_free_result($job);
     if ($trunkresult["min"] > "")
         $job = pg_exec($link, "SELECT min(stime), max(mux_ltime) FROM OUTPUT WHERE job_id=" . $row["id"] . ";");
     else
         $job = pg_exec($link, "SELECT min(stime), max(ltime) FROM OUTPUT WHERE job_id=" . $row["id"] . ";");
     $outresult  = pg_fetch_array($job);
     pg_free_result($job);
/*
     $firstTime= strtotime($row["ctime"]);
     $lastTime = strtotime($outresult["min"]);
     if ($firstTime == $lastTime){
	$outresult["min"] = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
     }
*/

     if ($outid["length"] > 0)
	 $ratio = $outid["length"];
     else
	 $ratio = $row["source_duration"];

     $firstTime= strtotime(substr($row["ctime"],0,19));
     $lastTime = strtotime(substr($outresult["min"],0,19));
     $timeDiff = ' (' . round(($lastTime-$firstTime)/60, 2, PHP_ROUND_HALF_UP) . 'm)';
     echo "<br />&nbsp;Queue Time : " . substr($row["ctime"],11,8) . "-" . substr($outresult["min"],11,8) . $timeDiff;

     if ($trunkresult["min"] > ""){
         $firstTime=$minTime=strtotime(substr($trunkresult["min"],0, 19));
         $stTime   = substr($trunkresult["min"],0, 19);
         $lastTime = strtotime(substr($trunkresult["max"],0,19));
         $timeDiff = ' (' . round(($lastTime-$firstTime)/60, 2, PHP_ROUND_HALF_UP) . 'm)';
         echo "<br />&nbsp;Split Time : " . substr($trunkresult["min"],11,8) . "-" . substr($trunkresult["max"],11,8) . $timeDiff;
	 if ($ratio > 0){
	     $timeDiff = ' [' . round(($lastTime-$firstTime) / $ratio, 2, PHP_ROUND_HALF_UP) . ']';
	     echo $timeDiff;
	 }
     } else {
         $minTime=strtotime(substr($outresult["min"],0, 19));
         $stTime   = substr($outresult["min"],0, 19);
     }
     $firstTime= strtotime(substr($outresult["min"],0, 19));
     $endTime  = substr($outresult["max"],0, 19);
     $maxTime  = strtotime(substr($outresult["max"],0, 19));
     $lastTime = strtotime(substr($outresult["max"],0, 19));
     $timeDiff = ' (' . round(($lastTime-$firstTime)/60, 2, PHP_ROUND_HALF_UP) . 'm)';
     echo "<br />&nbsp;Encode/Mux Time : " . substr($outresult["min"],11,8)  . "-" . substr($outresult["max"],11,8) . $timeDiff;
     if ($ratio > 0){
	$timeDiff = ' [' . round(($lastTime-$firstTime) / $ratio, 2, PHP_ROUND_HALF_UP) . ']';
	echo $timeDiff;
     }

     echo "</tr>";
     echo "<tr><td></td>";
     echo "<td colspan='3'>";
     echo "<table border='0'>";
     echo "<tr><th>OUTPUT<br />ID</th><th>FILENAME</th><th></th><th>VOLO PROFILE</th><th></th><th>STATUS</th><th>ENCODE START / END</th><th></th><th>RATIO</th><th>AUDIO MUX</th><th></th><th>VOLO NODE</th><th></th><th>ACTION/<br />MODE</th></tr>";

     $proc = $retry = $cancel = false;
     $waiting = 0;
     $job = pg_exec($link, "SELECT * FROM OUTPUT WHERE JOB_ID = " . $row["id"] . " ORDER BY ID;");
     $numjob = pg_numrows($job);
     for($rj = 0; $rj < $numjob; $rj++) {
        $jrow = pg_fetch_array($job, $rj);
	$o_retry = false;

        echo "<tr><td align='middle'>", $jrow["id"], "</td>
        <td><a href='./outputinfo.php?OID=", $jrow["id"], "' target=_blank style='text-decoration:none'>", $jrow["filename"], "</a></td><td></td>
        <td>", $jrow["profile"], "</td><td></td>
        <td align='middle'>";

        if ($jrow["c_cancel"] == "t"){
                $status = "<font color=blue>CANCEL</font>";
		$cancel = $o_retry = true;
	} else if ($jrow["stage"] < 0){
                $status = "<a href='./viewlog.php?JID=" . $jrow["job_id"] . "&OID=" . $jrow["id"] . "&MODE=" . $jrow["split_encode"] . "' target=_blank style='text-decoration:none'><font color=red>FAIL</font></a>";
		$retry = $o_retry = true;
        } else if ($jrow["stage"] == 0){
                $status = "<font color=green>WAITING</font>";
		$proc = true;
		$waiting++;
        } else if ($jrow["stage"] == 1){
		if ($jrow["split_encode"] == 't')
			$status = "<font color=orange>";
		else if ($jrow["progress_mode"] == 1)
			$status = "<font color=orange>1Pass<br />";
		else
			$status = "<font color=orange>2Pass<br />";
                $status .= "Progress(" . $jrow["progress"] . "%)<br />[" . $jrow["progress_fps"] . "fps]</font>";
		$proc = true;
        } else if ($jrow["stage"] == 2){
                $status = "<a href='./viewlog.php?JID=" . $jrow["job_id"] . "&OID=" . $jrow["id"] . "&MODE=" . $jrow["split_encode"] . "' target=_blank style='text-decoration:none'><font color=blue>DONE</font><br />[" . $jrow["progress_fps"] . "fps]";
        } else {
                $status = "<a href='./viewlog.php?JID=" . $jrow["job_id"] . "&OID=" . $jrow["id"] . "&MODE=" . $jrow["split_encode"] . "' target=_blank style='text-decoration:none'><font color=blue>CANCEL</font>";
		$cancel = true;
	}
        echo $status, "</td>";
        echo "<td colspan='2'>" . substr($jrow["stime"],11,8) . "-" . substr($jrow["ltime"],11,8);
	$firstTime= strtotime(substr($jrow["stime"],0, 19));
        $lastTime = strtotime(substr($jrow["ltime"],0, 19));
        $timeDiff = '(' . round(($lastTime-$firstTime)/60, 2, PHP_ROUND_HALF_UP) . 'm)';
	if ($jrow["length"] > 0)
		$ratio = $jrow["length"];
	else
		$ratio = $row["source_duration"];
	if ($ratio > 0)
		$ratio = round(($lastTime-$firstTime) / $ratio, 2, PHP_ROUND_HALF_UP);
        echo "&nbsp;&nbsp;" . $timeDiff . "</td><td align='middle'>" . $ratio . "</td>";

	if ($jrow["split_encode"] == 't'){
            echo "<td colspan='1'>" . substr($jrow["merge_stime"],11,8) . "-" . substr($jrow["mux_ltime"],11, 8);
	    $firstTime= strtotime(substr($jrow["merge_stime"],0, 19));
            $lastTime = strtotime(substr($jrow["mux_ltime"],0, 19));
            $timeDiff = '(' . round(($lastTime-$firstTime)/60, 2, PHP_ROUND_HALF_UP) . 'm)';
            echo "&nbsp;&nbsp;" . $timeDiff . "</td>";
	} else {
            echo "<td colspan='1' align='middle'>N/A</td>";
	}

        $node = pg_exec($link, "SELECT name, uuid FROM WORKER WHERE ID = " . $jrow["worker_id"] . ";");
        $nrow = pg_fetch_array($node);
        pg_free_result($node);
	if ($jrow["split_encode"] == 't'){
		echo "<td></td><td align='middle'>" . $nrow["uuid"] . "</td>";
	} else {
		$nodename = explode(".", $nrow["name"]);
		echo "<td></td><td align='middle'>" . $nodename[0] . " " . $nrow["uuid"] . "</td>";
	}
	if ($o_retry){
         	echo "<td></td><td align='middle'><a href='./retry.php?OID=" . $jrow["id"] . "' target=_blank style='text-decoration:none'><font color=red>RETRY</font></a><br />";
	} else {
		echo "<td></td><td align='middle'>";
	}
	if ($jrow["split_encode"] == 't')
	    echo "<a href='./split_status.php?OID=" . $jrow["id"] . "' target=_blank style='text-decoration:none'>SPLIT</a>";
	else
	    echo "NORMAL";
        echo "</td></tr>";
	// echo "<tr><td colspan='9'>Custom Parameter :: Container[" . $jrow["container"] . "] Duration[" . $jrow["length"] . "] Video Bitrate[" . $jrow["videobit"] . "] Audio Bitrate[" . $jrow["audiobit"] . "] Resolution[" . $jrow["resolu"] . "] DeInterlace[" . $jrow["deint"] . "] Pass Mode[" . $jrow["pmode"] . "]</td>";
	echo "<tr><td></td><td colspan='10'>Encode Message :: " . $jrow["message"] . "</td></tr>";
     }
     pg_free_result($job);

     if ($proc){
        if ($waiting == $numjob) {
        echo "<tr><td></td><td colspan='12'>";
        echo '<form id="setpriority" method="get" action="./set_priority.php" target="_blank">';
        echo '<br />Set Job Priority : <input type="number" name="PRIORITY" value="' . $jrow["priority"] . '">';
        echo '<input type="hidden" name="JID" value="' . $row["id"] . '">';
        echo '&nbsp;&nbsp;<input type="submit" value="Send">';
        echo '</form></td></tr>';
	}
	echo "<tr><td colspan='6'><br />JOB ACTION :: <a href='./cancel.php?JID=", $row["id"], "' target=_blank style='text-decoration:none'>CANCEL ENCODE</a>&nbsp;</td><td colspan='7'><br />Job Priority [" . $jrow["priority"] . "]</td></tr>";
     } else if ($retry || $cancel){
	if ($main_queue){
	  echo "<tr><td colspan='6'><br />JOB ACTION :: <a href='./retry_job.php?JID=", $row["id"], "' target=_blank style='text-decoration:none'>[RETRY JOB]</a>&nbsp;&nbsp;";
	  echo "<a href='./clear_job.php?JID=", $row["id"], "' target=_blank style='text-decoration:none'>[CLEAR JOB]</a>&nbsp;</td><td colspan='7'><br />Job Priority [" . $jrow["priority"] . "]</td></tr>";
	} else {
	  echo "<tr><td colspan='6'><br />JOB ACTION :: <a href='./retry_job.php?JID=", $row["id"], "' target=_blank style='text-decoration:none'>[RETRY JOB]</a>&nbsp;</td><td colspan='7'><br />Job Priority [" . $jrow["priority"] . "]</td></tr>";
	}
     } else {
        $job = pg_exec($link, "SELECT count(*) FROM SPLIT_OUTPUT WHERE OUTPUT_ID = " . $jrow["id"] . ";");
        $numjob = pg_fetch_array($job);
        pg_free_result($job);
        if ($jrow["length"] > 0)
                $source_duration = $jrow["length"];
        else
                $source_duration = $row["source_duration"];
        $ratio = round(($maxTime-$minTime) / $source_duration, 2, PHP_ROUND_HALF_UP);

	$timeDiff = '<br />Overall Throughput :: Source Duration (' . round($source_duration/60, 2,PHP_ROUND_HALF_UP) . ' m)';
	echo "<tr><td colspan='14'>" . $timeDiff . ' Encode/Mux Time [ ' . $stTime . '-' . substr($endTime, 11, 8) . ' (' . round(($maxTime-$minTime)/60, 2, PHP_ROUND_HALF_UP) . ' m) ] Ratio [' . $ratio . "] Job Priority [" . $jrow["priority"] . "]<br />";
        echo "</td></tr>";
     }
     echo "<tr><td colspan='13'>&nbsp;</td></tr>";
     echo "</table></tr>";
   }
   pg_free_result($result);
   pg_close($link);
   if ($numrows < 1) echo "<tr><td colspan='13'>JOB QUEUE EMPTY....</td></tr>";
  ?>
  </table>
  </td></tr></table>
  <p><p>
  </div>
  </body>
</html>
