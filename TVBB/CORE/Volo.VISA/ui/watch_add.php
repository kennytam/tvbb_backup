<?php

$dirPath='/home/Video.Source';
$destPath='/home/Video.Output';
$profilePath='/home/Video.Profile/';
$watchFolder='/VOLO_JOB_SUBMIT';


$httpRequest = readInfo("ar9");

if ($httpRequest == 'save as')
{

	$msg = folderCreate($dirPath,$profilePath,$destPath,$watchFolder);
	echo json_encode(array("success"=>$msg));
	insertProfile();
	insertInfo();
	insertMatrix($dirPath);
}

if($httpRequest == 'update')
{
	update($dirPath,$profilePath);
	echo json_encode(array("success"=>"Records updated successfully!!"));
}

if ($httpRequest == 'erases')
{
	erases($dirPath,$profilePath);
	echo json_encode(array("success"=>"Records deleted successfully!!"));
}



function folderCreate($dirPath,$profilePath,$destPath,$watchFolder)
{
	$fdlStatus = folderChecking($dirPath);	
	$outputFdl = readInfo("ar10");
	$folderName = readInfo("ar1");
	$encodeFlag = readinfo("ar8");
	$encodeName = "NOENCODE";
	if ($fdlStatus == 1)
	{
		$msg = "Audio Profile was existed\n";
		exit(1);
	}
	else
	{
		mkdir("$dirPath$folderName$watchFolder",0777,true);
		$fdlName = explode("/",$folderName);
		mkdir("$profilePath$fdlName[2]",0777,true);
		exec("echo $profilePath$fdlName[2] > ok.txt");
		mkdir("$destPath$outputFdl",0777,true);
		exec("echo $destPath$outputFdl > $profilePath$fdlName[2]/DESTINATION");
		

		if ($encodeFlag == "true")
		touch("$profilePath$fdlName[2]/$encodeName");	

		$status = exec("echo $?");
		if ($status == 0)
		{
			xmlCreate($profilePath);
			$msg = "New Audio Profile is created successfully\n";
		}

		exec("sudo chown -R volo:volo $dirPath");	
		exec("sudo chmod -R 777 $dirPath");	
		exec("sudo chown -R volo:volo $profilePath");	
		exec("sudo chmod -R 777 $profilePath");	
	}
	
	return $msg;
}

function folderChecking($dirPath)
{
	$folderName = readInfo("ar1");
	$fdlStatus = exec(" if [ -d $dirPath$folderName ]; then echo 1; else echo 0; fi ");
	return $fdlStatus;	
}

function  xmlCreate($dirPath)
{		
	$folderName = readInfo("ar1");
	$aCodec = readInfo("ar2");
	$sRate = readInfo("ar3");
	$aAdjus = readInfo("ar4");
	$iLine = readInfo("ar5");
	$audioType = readAudioType();
	$audioTrack = readTrack();
	$numOfOutTracks = sizeof($audioTrack);
	$fdlName = explode("/",$folderName);

	$xmlRules = fopen("$dirPath$fdlName[2]/MAP.INFO",'w');
	fwrite($xmlRules,"<audioMapping name=\"ExtremeMixdown\">\r\n");
	fwrite($xmlRules,"\t<buses channels=\"$numOfOutTracks\">\r\n");
	if ($aAdjus == "true")
	{
		if ($iLine == "zero")
		fwrite($xmlRules,"\t\t<channelMap adjustment=\"ebur128\">\r\n");
		else if ($iLine > 0)
		fwrite($xmlRules,"\t\t<channelMap adjustment=\"ebur128\" volume=\"+$iLine".''."dB\">\r\n");
		else if ($iLine < 0)
		fwrite($xmlRules,"\t\t<channelMap adjustment=\"ebur128\" volume=\"$iLine".''."dB\">\r\n");
	}
	else if ($aAdjus == "false")
	{
		if ($iLine == "zero")
		fwrite($xmlRules,"\t\t<channelMap>\r\n");
		else if ($iLine > 0)
		fwrite($xmlRules,"\t\t<channelMap volume=\"+$iLine".''."dB\">\r\n");
		else if ($iLine < 0)
		fwrite($xmlRules,"\t\t<channelMap volume=\"$iLine".''."dB\">\r\n");
	}
	for ($k=0;$k<$numOfOutTracks;$k++)
	{
		$index = $k +1;	
		$cnt = 0;
		fwrite($xmlRules,"\t\t\t<ch track=\"$index\" codec=\"$aCodec\" scope=\"$audioType[$k]\" samplingRate=\"$sRate\">\r\n");
		fwrite($xmlRules,"\t\t\t\t<mapRule>\r\n");
		for ($r=0;$r<16;$r++)
		{
			$trackNum = $r + 1;
			if($audioTrack[$k][$r] == "true")
			{
				fwrite($xmlRules,"\t\t\t\t\t<in track=\"$trackNum\"/>\r\n");
				$cnt++;
			}	
		}
		if ($cnt == 0)
		fwrite($xmlRules,"\t\t\t\t\t<in track=\"silent\"/>\r\n");
		fwrite($xmlRules,"\t\t\t\t</mapRule>\r\n");
		fwrite($xmlRules,"\t\t\t</ch>\r\n");
	}


	fwrite($xmlRules,"\t\t</channelMap>\r\n");
	fwrite($xmlRules,"\t</buses>\r\n");
	fwrite($xmlRules,"</audioMapping>\r\n");
	fclose($xmlRules);
}

function readInfo($str)
{
	$folderName = $_POST["$str"];
	return $folderName;
}

function readTrack()
{
	$tracks = $_POST["ar7"];
	return $tracks;
}


function readAudioType()
{
	$audioType = $_POST["ar6"];
	return $audioType;
}


function connectTodb()
{
	$conn = pg_connect("host=localhost port=5432 dbname=visadb user=visa password=1234");
	
	if ($conn)
	return $conn;
	else
	exit(1);
}

function insertProfile()
{
	$conn = connectTodb();
	$profile_name = readInfo("ar1");
        $id = pg_query($conn,"select id from audio_profile where id = (select max(id) from audio_profile);");
        $currentId = pg_fetch_object($id);
	$insertId = $currentId->id;
        if ($insertId)
        $insertId += 1;
        else
        $insertId = 1;
        $insertSte = "insert into audio_profile values($insertId,'$profile_name',now(),now());";
        $insertRes = pg_query($conn,$insertSte);
        pg_close($conn);
}

function updateProfile($oldId,$oldCreateTime)
{
	$conn = connectTodb();
        $profile_name = readInfo("ar1");
        $insertId = $oldId;
        $insertSte = "insert into audio_profile values($insertId,'$profile_name','$oldCreateTime',now());";
        $insertRes = pg_query($conn,$insertSte);
        pg_close($conn);
}

function insertInfo()
{
	$conn = connectTodb();
        $id = pg_query($conn,"select id from audio_profile where id = (select max(id) from audio_profile);");
	$currentId = pg_fetch_object($id);
	$insertId = $currentId->id;
	$profile_name = readInfo("ar1");
	$audio_codec = readInfo("ar2");
	$sampling_rate = readInfo("ar3");
	$auto_adjust = readInfo("ar4");
	$db_value = readInfo("ar5");
	if ($db_value == "zero")
	$db_value = 0;
	$audio_type = readAudioType();
	$audio_remapping = readInfo("ar8");	
	$insertSte = "insert into audio_profile_info values($insertId,'$profile_name','$audio_codec','$sampling_rate',$db_value,$audio_remapping,$auto_adjust);";	
	$insertRes = pg_query($conn,$insertSte);
	pg_close($conn);
}

function updateInfo($oldInfo)
{
        $conn = connectTodb();
        $insertId = $oldInfo;
        $profile_name = readInfo("ar1");
        $audio_codec = readInfo("ar2");
        $sampling_rate = readInfo("ar3");
        $auto_adjust = readInfo("ar4");
        $db_value = readInfo("ar5");
        if ($db_value == "zero")
        $db_value = 0;
        $audio_type = readAudioType();
        $audio_remapping = readInfo("ar8");
        $insertSte = "insert into audio_profile_info values($insertId,'$profile_name','$audio_codec','$sampling_rate',$db_value,$audio_remapping,$auto_adjust);";
        $insertRes = pg_query($conn,$insertSte);
        pg_close($conn);
}


function insertMatrix($dirPath)
{
	$conn = connectTodb();
	$id = pg_query($conn,"select id from audio_profile where id = (select max(id) from audio_profile);");
        $currentId = pg_fetch_object($id);
        $insertId = $currentId->id;
	$profile_name = readInfo("ar1");
	$audioTrack = readTrack();
	$audioType = readAudioType();
	$numOfOutTracks = sizeof($audioTrack);
	$outTrack = 0;
	for ($i=0;$i<$numOfOutTracks;$i++)
	{
		$audioTrack1 = $audioTrack[$i][0];
		$audioTrack2 = $audioTrack[$i][1];
		$audioTrack3 = $audioTrack[$i][2];
		$audioTrack4 = $audioTrack[$i][3];
		$audioTrack5 = $audioTrack[$i][4];
		$audioTrack6 = $audioTrack[$i][5];
		$audioTrack7 = $audioTrack[$i][6];
		$audioTrack8 = $audioTrack[$i][7];
		$audioTrack9 = $audioTrack[$i][8];
		$audioTrack10 = $audioTrack[$i][9];
		$audioTrack11 = $audioTrack[$i][10];
		$audioTrack12 = $audioTrack[$i][11];
		$audioTrack13 = $audioTrack[$i][12];
		$audioTrack14 = $audioTrack[$i][13];
		$audioTrack15 = $audioTrack[$i][14];
		$audioTrack16 = $audioTrack[$i][15];
		$audioTypes = $audioType[$i];
		$outTrack += 1;

		$insertSte = "insert into audio_profile_matrix values(
		$insertId,
		'$profile_name',
		$outTrack,
		$audioTrack1,
		$audioTrack2,
		$audioTrack3,
		$audioTrack4,
		$audioTrack5,
		$audioTrack6,
		$audioTrack7,
		$audioTrack8,
		$audioTrack9,
		$audioTrack10,
		$audioTrack11,
		$audioTrack12,
		$audioTrack13,
		$audioTrack14,
		$audioTrack15,
		$audioTrack16,
		'$audioTypes'
		);";
		$res = pg_query($conn,$insertSte);
	}
	pg_close($conn);
}


function updateMatrix($oldId)
{
        $conn = connectTodb();
        $insertId = $oldId;
        $profile_name = readInfo("ar1");
        $audioTrack = readTrack();
        $audioType = readAudioType();
        $numOfOutTracks = sizeof($audioTrack);
        $outTrack = 0;
        for ($i=0;$i<$numOfOutTracks;$i++)
        {
                $audioTrack1 = $audioTrack[$i][0];
                $audioTrack2 = $audioTrack[$i][1];
                $audioTrack3 = $audioTrack[$i][2];
                $audioTrack4 = $audioTrack[$i][3];
                $audioTrack5 = $audioTrack[$i][4];
                $audioTrack6 = $audioTrack[$i][5];
                $audioTrack7 = $audioTrack[$i][6];
                $audioTrack8 = $audioTrack[$i][7];
                $audioTrack9 = $audioTrack[$i][8];
                $audioTrack10 = $audioTrack[$i][9];
                $audioTrack11 = $audioTrack[$i][10];
                $audioTrack12 = $audioTrack[$i][11];
                $audioTrack13 = $audioTrack[$i][12];
                $audioTrack14 = $audioTrack[$i][13];
                $audioTrack15 = $audioTrack[$i][14];
                $audioTrack16 = $audioTrack[$i][15];
                $audioTypes = $audioType[$i];
                $outTrack += 1;

                $insertSte = "insert into audio_profile_matrix values(
                $insertId,
                '$profile_name',
                $outTrack,
                $audioTrack1,
                $audioTrack2,
                $audioTrack3,
                $audioTrack4,
                $audioTrack5,
                $audioTrack6,
                $audioTrack7,
                $audioTrack8,
                $audioTrack9,
                $audioTrack10,
                $audioTrack11,
                $audioTrack12,
                $audioTrack13,
                $audioTrack14,
                $audioTrack15,
                $audioTrack16,
                '$audioTypes'
                );";
                $res = pg_query($conn,$insertSte);
        }
	pg_close($conn);
}


function update($dirPath,$profilePath)
{
	$profileName = readInfo("ar1");	
	$conn = connectTodb();
	$id = pg_query($conn,"select * from audio_profile where profile_name = '$profileName';");
        $currentId = pg_fetch_object($id);
        $oldId = $currentId->id;
	$oldCreateTime = $currentId->profile_create_date;
	$resAP = pg_query($conn,"delete from audio_profile where profile_name = '$profileName';");
	$resAPI = pg_query($conn,"delete from audio_profile_info where profile_name = '$profileName';");
	$resAPM = pg_query($conn,"delete from audio_profile_matrix where profile_name = '$profileName';");	
	updateProfile($oldId,$oldCreateTime);
        updateInfo($oldId);
        updateMatrix($oldId);

	$dirPathStatus = exec(" if [ -d $dirPath$profileName ]; then echo 1; else echo 0; fi ");
	$profilePathStatus = exec(" if [ -d $profilePath$profileName ]; then echo 1; else echo 0; fi ");
	if ($dirPathStatus)
	exec("rm -rf $dirPath$profileName");
	if ($profilePathStatus)
	exec("rm -rf $profilePath$profileName");
	$msg = folderCreate($dirPath,$profilePath);
	pg_close($conn);
}

function erases($dirPath,$profilePath)
{
	$profileName = readInfo("ar1");
        $conn = connectTodb();
        $id = pg_query($conn,"select * from audio_profile where profile_name = '$profileName';");
        $currentId = pg_fetch_object($id);
        $oldId = $currentId->id;
        $oldCreateTime = $currentId->profile_create_date;
        $resAP = pg_query($conn,"delete from audio_profile where profile_name = '$profileName';");
        $resAPI = pg_query($conn,"delete from audio_profile_info where profile_name = '$profileName';");
        $resAPM = pg_query($conn,"delete from audio_profile_matrix where profile_name = '$profileName';");
	
	$dirPathStatus = exec(" if [ -d $dirPath$profileName ]; then echo 1; else echo 0; fi ");
        $profilePathStatus = exec(" if [ -d $profilePath$profileName ]; then echo 1; else echo 0; fi ");
        if ($dirPathStatus)
        exec("rm -rf $dirPath$profileName");
        if ($profilePathStatus)
        exec("rm -rf $profilePath$profileName");
        pg_close($conn);


}
?>
