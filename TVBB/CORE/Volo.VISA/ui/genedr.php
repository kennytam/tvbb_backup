<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
## Encode Delivery Report Generation                                                                   ##
## USAGE: genedr.php [date in yyyy-mm-dd (optional)                                                    ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());

if (isset($argv[1]))
	$jdate = htmlspecialchars($argv[1]);
else
	$jdate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
 
$link = pg_Connect("host=localhost dbname=visadb user=visa password=1234");
$result = pg_exec($link, "SELECT * FROM JOB WHERE (date_trunc('day',job.ctime) >= '" . $jdate . "' AND date_trunc('day',job.ctime) <= '" . $jdate . "') ORDER BY ID DESC;");
$numrows = pg_numrows($result);
echo 'Volo VISA Admin Console Reporter v1.4.8' . "\n";
echo "\n";
echo '[ VISA Volo Encoder Farm  - Encode Job Delivery Report ]' . "\n";
echo "\n";
echo 'Encode Job Date : [' . $para['time_zone'] . ' ' . $jdate . ']         Number of Batch Job : [' . $numrows . ']' . "\n";
echo "\n";

// Loop on rows in the result set.
for($ri = 0; $ri < $numrows; $ri++) {
     $row = pg_fetch_array($result, $ri);
     echo 'JOB ID : [' . $row["id"] . ']  SOURCE VIDEO : [' . $row["source"] . ']  LENGTH : [' . round($row["source_duration"]/60, 2,PHP_ROUND_HALF_UP) . 'mins]  DESTINATION : [' . $row["destination"] . ']  JOB SUBMIT TIME : [' . substr($row["ctime"],0, 19) . ']' . "\n";
     echo '[ENCODE ID] [OUTPUT FILE NAME]                      [VOLO PROFILE]         [ENCODE START TIME - END TIME]                     [RATIO]  [VOLO NODE ENCODE]    [STATUS]       [MESSAGE]';
     echo "\n";

     $job = pg_exec($link, "SELECT * FROM OUTPUT WHERE JOB_ID = " . $row["id"] . " ORDER BY ID;");
     $numjob = pg_numrows($job);
     for($rj = 0; $rj < $numjob; $rj++) {
	$proc = false;
        $jrow = pg_fetch_array($job, $rj);
        echo ' ' . str_pad($jrow["id"], 11);
	echo str_pad($jrow["filename"], 40);
	echo str_pad($jrow["profile"], 20);
        echo substr($jrow["stime"],0, 19) . ' - ';
        echo substr($jrow["ltime"],0, 19);
	$firstTime= strtotime(substr($jrow["stime"],0, 19));
	$lastTime = strtotime(substr($jrow["ltime"],0, 19));
	$timeDiff = str_pad('(' . round(($lastTime-$firstTime)/60, 2) . 'm)', 12);
        if ($jrow["length"] > 0)
                $ratio = $jrow["length"];
        else
                $ratio = $row["source_duration"];
        if ($ratio > 0)
                $ratio = str_pad('[' . round(($lastTime-$firstTime) / $ratio, 2, PHP_ROUND_HALF_UP) . ']', 10);
	echo ' ' . $timeDiff . $ratio;

        $node = pg_exec($link, "SELECT uuid FROM WORKER WHERE ID = " . $jrow["worker_id"] . ";");
        $nrow = pg_fetch_array($node);
        echo $nrow["uuid"] . '      ';
        if ($jrow["stage"] < 0)
                $status = 'FAIL';
        else if ($jrow["stage"] == 0)
                $status = 'WAITING';
        else if ($jrow["stage"] == 1)
                $status = 'Progress [' . $jrow["progress"] . '% ' . $jrow["progress_fps"] . 'fps]';
        else if ($jrow["stage"] == 2)
                $status = 'DONE [' . $jrow["progress_fps"] . 'fps]';
        else
                $status = 'CANCEL';
        echo str_pad($status, 16);
        echo $jrow["message"];
        echo "\n";
     }
     echo "\n";
   }
   pg_close($link);
   if ($numrows < 1) echo 'NO ENCODE JOB....' . "\n";

?>
