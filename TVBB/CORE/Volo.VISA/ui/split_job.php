<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
?>
<html>
  <head>
   <title>Volo.VISA</title>
  </head>
  <body bgcolor="grey">
 
  <p><b>Volo VISA Split Admin Console v1.4.8 [Node : <?php echo $para['volo_node'];?>]</b></p>
  <table border="0" bgcolor="white"><tr><td>
  <?php
  if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
  if (isset($_GET["date"]))
        $jdate = htmlspecialchars($_GET["date"]);
  else
        $jdate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
  $curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
 
  $config = include('../scheduler/config/database.php');
  $connstr = "host=" . $config['db_server'] .  " user=". $config['db_user'] .  " password=". $config['db_pass'] .  " dbname=".$config['db_name'];
  $link = pg_connect($connstr)
	or die('DB Connection Error : ' . pg_last_error());

  $result = pg_exec($link, "SELECT * FROM JOB WHERE (date_trunc('day',job.ctime) >= '" . $jdate . "' AND date_trunc('day',job.ctime) <= '" . $jdate . "') AND id IN (SELECT DISTINCT job_id FROM output WHERE split_encode=true AND stage=2) ORDER BY ID ASC;");
  $numrows = pg_numrows($result);
  echo "<hr><p><b>[ Split Encode Job List ]</b></p>";
  echo "<p>Volo Encode Batch Date [" . $para['time_zone'] . " " . $jdate . "]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
  // echo "Number of Batch Job [$numrows]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Time [" . $para['time_zone'] . " " . $curtime]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='./xml_calls.html' target=_blank style='text-decoration:none'>VISA Request/Response Log</a></p>";
  echo "Number of Split Encode Job [$numrows]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System Time [" . $para['time_zone'] . " " . $curtime . "]</p>";
  ?>
 
  <table border="1" bgcolor="white">
  <tr>
   <th>JOB ID</th>
   <th>SOURCE VIDEO</th>
   <th>SOURCE LENGTH</th>
   <th>DESTINATION PATH</th>
   <th>SUBMIT TIME</th>
  </tr>
  <?php
 
   // Loop on rows in the result set.
 
   for($ri = 0; $ri < $numrows; $ri++) {
     echo "<tr>\n";
     $row = pg_fetch_array($result, $ri);
     echo " <td align='middle'>", $row["id"], "&nbsp;</td>
     <td><img src='./thumbnail/", $row["id"], ".jpg' height='80px'><br /><a href='./sourceinfo.php?JID=", $row["id"], "' target=_blank style='text-decoration:none'>&nbsp;", $row["source"], "&nbsp;</a></td>
     <td align='middle'>&nbsp;", round($row["source_duration"]/60, 2,PHP_ROUND_HALF_UP), " (mins)&nbsp;</td>
     <td>&nbsp;", $row["destination"], "&nbsp;</td>
     <td>&nbsp;", substr($row["ctime"],0, 19), "&nbsp;</td>
     </tr>";
     echo "<tr><td></td>";
     echo "<td colspan='4'>";
     echo "<table border='0'>";
     echo "<tr><th>ENCODE ID</th><th>OUTPUT FILE NAME</th><th></th><th>VOLO PROFILE</th><th></th><th>STATUS</th><th>ENCODE START</th><th>ENCODE END</th><th></th><th></th><th>VOLO NODE</th><th></th><th>ENCODE MODE</th></tr>";
     $job = pg_exec($link, "SELECT * FROM OUTPUT WHERE JOB_ID = " . $row["id"] . " ORDER BY ID;");
     $numjob = pg_numrows($job);
     $proc = $retry = false;
     $waiting = 0;
     for($rj = 0; $rj < $numjob; $rj++) {
        $jrow = pg_fetch_array($job, $rj);
        if ($rj == 0){
                $stTime = $minTime = strtotime(substr($jrow["stime"],0, 19));
                $maxTime = strtotime(substr($jrow["ltime"],0, 19));
        } else {
                $curTime = strtotime(substr($jrow["stime"],0, 19));
                if ($curTime < $minTime) $minTime = $curTime;
                $curTime = strtotime(substr($jrow["ltime"],0, 19));
                if ($curTime > $maxTime) $maxTime = $curTime;
        }
        echo "<tr><td align='middle'>", $jrow["id"], "</td>
        <td>", $jrow["filename"], "</td><td></td>
        <td>", $jrow["profile"], "</td><td></td>
        <td align='middle'>";
        if ($jrow["stage"] < 0){
                $status = "<a href='./viewlog.php?OID=" . $jrow["id"] . "' target=_blank style='text-decoration:none'><font color=red>FAIL</font></a>";
		// $retry = true;
        } else if ($jrow["stage"] == 0){
                $status = "<font color=green>WAITING</font>";
		$proc = true;
		$waiting++;
        } else if ($jrow["stage"] == 1){
		if ($jrow["split_encode"] == 't')
			$status = "<font color=orange>";
		else if ($jrow["progress_mode"] == 1)
			$status = "<font color=orange>1Pass<br />";
		else
			$status = "<font color=orange>2Pass<br />";
                $status .= "Progress (" . $jrow["progress"] . "%)<br />[" . $jrow["progress_fps"] . "fps]</font>";
		$proc = true;
        } else if ($jrow["stage"] == 2){
                $status = "<font color=blue>DONE</font><br />[" . $jrow["progress_fps"] . "fps]";
        } else {
                $status = "<font color=blue>CANCEL</font>";
	}
        echo $status, "</td>";
        echo "<td>" . substr($jrow["stime"],0, 19) . "</td>";
        echo "<td>" . substr($jrow["ltime"],0, 19) . "</td>";
	$firstTime= strtotime(substr($jrow["stime"],0, 19));
        $lastTime = strtotime(substr($jrow["ltime"],0, 19));
        $timeDiff = '(' . round(($lastTime-$firstTime)/60, 2, PHP_ROUND_HALF_UP) . 'm)<br />';
	if ($jrow["length"] > 0)
		$ratio = $jrow["length"];
	else
		$ratio = $row["source_duration"];
	if ($ratio > 0)
		$ratio = '[' . round(($lastTime-$firstTime) / $ratio, 2, PHP_ROUND_HALF_UP) . ']';
        echo "<td>" . $timeDiff . $ratio . "</td>";

        $node = pg_exec($link, "SELECT uuid FROM WORKER WHERE ID = " . $jrow["worker_id"] . ";");
        $nrow = pg_fetch_array($node);
        echo "<td></td><td align='middle'>" . $nrow["uuid"] . "</td>";
	// if ($retry){
        // 	echo "<td></td><td align='middle'><a href='./retry.php?OID=" . $jrow["id"] . "' target=_blank style='text-decoration:none'><font color=red>RETRY</font></a></td>";
	// } else {
		if ($jrow["split_encode"] == 't')
			echo "<td></td><td align='middle'>SPLIT</td>";
		else
			echo "<td></td><td align='middle'>NORMAL</td>";
	// }
        echo "</tr>";
	// echo "<tr><td colspan='9'>Custom Parameter :: Container[" . $jrow["container"] . "] Duration[" . $jrow["length"] . "] Video Bitrate[" . $jrow["videobit"] . "] Audio Bitrate[" . $jrow["audiobit"] . "] Resolution[" . $jrow["resolu"] . "] DeInterlace[" . $jrow["deint"] . "] Pass Mode[" . $jrow["pmode"] . "]</td>";
	echo "<tr><td></td><td colspan='6'>Encode Message :: " . $jrow["message"] . "</td></tr>";
     }
     if ($proc){
        if ($waiting == $numjob) {
        echo "<tr><td></td><td colspan='12'>";
        echo '<form id="setpriority" method="get" action="./set_priority.php" target="_blank">';
        echo '<br />Set Job Priority : <input type="number" name="PRIORITY" value="' . $jrow["priority"] . '">';
        echo '<input type="hidden" name="JID" value="' . $row["id"] . '">';
        echo '&nbsp;&nbsp;<input type="submit" value="Send">';
        echo '</form></td></tr>';
	}
	echo "<tr><td colspan='13'><br />JOB ACTION :: <a href='./cancel.php?JID=", $row["id"], "' target=_blank style='text-decoration:none'>CANCEL ENCODE</a>&nbsp;</td></tr>";
     } else if ($retry){
	echo "<tr><td colspan='13'><br />JOB ACTION :: <a href='./retry_job.php?JID=", $row["id"], "' target=_blank style='text-decoration:none'>RETRY WHOLE JOB</a>&nbsp;</td></tr>";
     } else {
        if ($jrow["length"] > 0)
                $ratio = $jrow["length"];
        else
                $ratio = $row["source_duration"];
        $ratio = '[' . round(($maxTime-$stTime) / $ratio, 2, PHP_ROUND_HALF_UP) . ']';
        $timeDiff = '<br />Overall Encode Job Throughput :: Time (' . round(($maxTime-$minTime)/60, 2, PHP_ROUND_HALF_UP) . ' m) Ratio ' . $ratio . '';
 
        pg_free_result($job);
        $job = pg_exec($link, "SELECT count(*) FROM SPLIT_OUTPUT WHERE OUTPUT_ID = " . $jrow["id"] . ";");
        $numjob = pg_fetch_array($job);
        echo "<tr><td colspan='13'>" . $timeDiff . " Split Trunk [" . $numjob[0] . "] Job Priority [" . $jrow["priority"] . "]</td></tr>";
     }
	echo "<tr><td colspan='13'>&nbsp;</td></tr>";
     echo "</table></tr>";
   }
   pg_close($link);
   if ($numrows < 1) echo "<tr><td colspan='13'>NO ENCODE JOB....</td></tr>";
  ?>
  </table>
  </td></tr></table>
  <p><p>
  </body>
</html>

