<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
?>
<!DOCTYPE html>
<html>
  <head>
   <title>Volo.VISA</title>
   <link rel="stylesheet" href="./template/styles.css">
  <script src="jquery/3.2.1/jquery.min.js"></script>
  </head>
<style>
body{
background-color: rgba(65,65,65,1);
font-size:13px;
font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif
}
.btn{
//border:none;
height:30px;
width:100px;
}
.edit_title{
font-size:25px;
color:white;
left:7px;
top:7px;
position:relative;
}
.sl{
width:460px;
height:25px;
border: none;
margin-top:10px;
}
.container{
border: #999997 solid 2px;
position: relative;
width: 1100px;
height: 760px;
top: 10px;
left: 120px;
}
.le-ft{
position: absolute;
margin-left: 10px;
}
#le-ft-de
{
position:relative;
top:30px;
}
.profile_name{
display:inline-block;
border: 1px solid rgba(232, 230, 230, 1); 
height: 35px;
width: 250px;
font-size:1em;
color: rgba(127, 204, 247,1);
background-color: rgba(65,65,65,1);
}
::-webkit-input-placeholder{
color: rgba(232, 230, 230, .5);
}
.dest_name{
display:inline-block;
border: 1px solid rgba(232, 230, 230, 1); 
height: 35px;
width: 250px;
font-size:1em;
color: rgba(127,204,247,1);
background-color: rgba(65,65,65,1);
}
#title{
font-size:1.5em;
position: relative;
color:white;
display:inline-block;
width:200px;
}
#dest{
font-size:1.5em;
position: relative;
color:white;
display:inline-block;
width:200px;
}
.btn{
margin-top:30px;
}
</style>

  <body>
  <div id='header'>
 <?php
   $curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
   echo "<p><b>VISA Admin Console v1.4.8 [Node : " . $para['volo_node'] . "] [System Time : " . $para['time_zone'] . " " . $curtime ."]</b></p>";
 ?>
  </div>
  <div id='cssmenu'>
  <ul>
   <li><a href='./index_admin.php'><span>Job Queue</span></a></li>
   <li><a href='./job_filter.php'><span>Job Filter</span></a></li>
   <!--<li><a href='./job_submit.php'><span>Job Submit</span></a></li>-->
<?php
 if (file_exists("./channels/index.php"))
   echo "<li><a href='./channels/index.php'><span>CH Extract</span></a></li>";
 if (file_exists("./trp/index.php"))
   echo "<li><a href='./trp/index.php'><span>TS Extract</span></a></li>";
?>
   <li><a href='./worker_status.php'><span>Encoder Status</span></a></li>
   <li><a href='./logs_status.php'><span>System Log</span></a></li>
   <li class='active xdcambt'><a href='./xdcam.php'><span>XDCAM</span></a>
	  <div class="xdcambt-content">
      		<a class="xdcambt-content-a" href="xdcam.php">XDCAM_Profile</a>
      		<a class="xdcambt-content-a" href="xdcam_edit.php">Audio Profile Edit</a>
		<a class="xdcambt-content-a" href="nasman.php">NAS Management</a>
    	  </div>
    </li>
                <li class="imx50bt"><a href='./imx50.php'><span>IMX50</span></a>
                    <div class="imx50bt-content">
                        <a class="imx50bt-content-a" href="imx50.php">IMX50_Profile</a>
                        <a class="imx50bt-content-a" href="imx50_edit.php">Audio Profile Edit</a>
                    </div>
                </li>

   <li class='last'><a href='./visa_status.php'><span>System Control</span></a></li>
  </ul>
  </div>
<div class="container">
	<div class="edit_title">NAS management</div>
	<div class="le-ft" id="le-ft-de">
		<h3 id="dest">IP Address:</h3>
		<span><input class="profile_name" id="ipaddr" type="text" placeholder="xxx.xxx.xxx.xxx"></span>
  	</div>
	<div class="le-ft" id="le-ft-de">
		<h3 id="dest">Folder Name:</h3>
		<span><input class="dest_name" id="tfolder" type="text" placeholder="SharedFolder"></span>
	</div>
	<div class="le-ft" id="le-ft-de">
		<h3 id="dest">User Name:</h3>
		<span><input class="dest_name" id="username" type="text" placeholder="TVB-USER"></span>
	</div>
	<div class="le-ft" id="le-ft-de">
		<h3 id="dest">Password:</h3>
		<span><input class="dest_name" id="password" type="text" placeholder="Password"></span>
	</div>	
	<div class="le-ft" id="le-ft-de">
		<h3 id="dest">Dest Folder Name:</h3>
		<span><input class="dest_name" id="mfolder" type="text" placeholder="MountFolder"></span>
	</div>
	<div>
		<select class="le-ft sl" id="le-ft-de">
		<option value="tt">Click to select</option>
			<?php
				$database = "/opt/Volo.VISA/client/helper/mount.csv";
                		$file = fopen($database,"r");
               		 	while(! feof($file))
                		{
                        		$eachLine = fgets($file);
                        		$dataArr = trim($eachLine);
					if ($dataArr == null)
					continue;
					else
					{
						$tempLine = explode('|',$dataArr);
						echo "<option value='$tempLine[0]' id='$tempLine[8]'>$tempLine[0]</option>";
					}
					
                		}
                		fclose($file);
			?>
		</select>
	</div>
	<button class="le-ft btn" id="le-ft-de" onclick="addNas()">submit</button>
	<button class="le-ft btn" id="le-ft-de" onclick="delNas()">delete</button>
	<button class="le-ft btn cancel" id="le-ft-de">reset</button>
</div>
<script>
$(".cancel").click(function(){
	$("#ipaddr").val("");
        $("#tfolder").val("");
        $("#username").val("");
        $("#password").val("");
        $("#mfolder").val("");
        location.reload();
});
</script>
<script>
function addNas()
{
        var ipaddr = $("#ipaddr").val();
        var tfolder = $("#tfolder").val();
        var username = $("#username").val();
        var password = $("#password").val();
        var mfolder = $("#mfolder").val();
	uuid = generateUUID();
        var cmd = "/home/Video.Source/" + tfolder + "|" + "/home/Video.Output/" + tfolder + "|" +
		 "sudo mount -t cifs //" + ipaddr + "/" + tfolder + " /home/Video.Source/" + mfolder + " -o username=" + username + 
		 ",password=" + password + ",domain=my_domain,sec=ntlmssp,iocharset=utf8,rw,file_mode=0777,dir_mode=0777|" + 
		 "sudo mount -t cifs //" + ipaddr + "/" + tfolder + " /home/Video.Output/" + mfolder + " -o username=" + username + 
		 ",password=" + password + ",domain=my_domain,sec=ntlmssp,iocharset=utf8,rw,file_mode=0777,dir_mode=0777|" + 
		 ipaddr + "|" + tfolder + "|" + username + "|" + mfolder + "|" + uuid;
	var action = "add";
        arr = {ar1:cmd,ar2:action};
        
        $.ajax({
        url: "nas.php",
        type: "POST",
        dataType: "json",
        data:arr,
        success: function(Jdata)
                {
                        alert(Jdata);
			location.reload();
                },
        error: function()
                {
                        alert("ERROR!!!");
                }
});
        
}
</script>
<script>
function delNas()
{
	var action = "delete";
	var deleteUUid = getUUID();
        arr = {ar2:action,ar3:deleteUUid};
        
        $.ajax({
        url: "nas.php",
        type: "POST",
        dataType: "json",
        data:arr,
        success: function(Jdata)
                {
			alert(Jdata);
			location.reload();
                },
        error: function()
                {
                        alert("ERROR!!!");
                }
});

        
}
</script>
<script>
$(document).ready(function(){
$( ".sl" ).change(function(event) {

	var aa = $(this).find('option:selected').attr('value');
	if (aa == "tt")
	{
		$("#ipaddr").val("");
        	$("#tfolder").val("");
        	$("#username").val("");
        	$("#password").val("");
        	$("#mfolder").val("");
		return;
	}
	var uuid = $(this).find('option:selected').attr('id');
	var action = "query";
	arr = {ar2:action,ar3:uuid};
	
	$.ajax({
        url: "nas.php",
        type: "POST",
        dataType: "json",
        data:arr,
        success: function(Jdata)
                {
			var dataStr = Jdata.split("|");
			$("#ipaddr").val(dataStr[4]);
			$("#tfolder").val(dataStr[5]);
			$("#username").val(dataStr[6]);
			$("#mfolder").val(dataStr[7]);
                },
        error: function()
                {
                        alert("ERROR!!!");
                }
	});

});
});
</script>
<script>
function getUUID()
{
	var uuid = $(".sl").find('option:selected').attr('id');
	return uuid;
}
</script>
<script>
function generateUUID(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
    return (c=='x' ? r : (r&0x7|0x8)).toString(16);
    });
    return uuid;
};
</script>
