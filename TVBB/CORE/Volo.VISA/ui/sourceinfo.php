<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
if (isset($_GET["date"]))
        $jdate = htmlspecialchars($_GET["date"]);
else
        $jdate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
?>
<html>
  <head>
   <title>Volo.VISA</title>
  </head>
  <body bgcolor="grey">
<?php
echo "<p><b>[ Source Media Status ]</b>&nbsp;&nbsp;&nbsp;&nbsp;System Time - " . $para['time_zone'] . " " . $curtime . "</p><hr>";
echo '<table border="0" bgcolor="white" width="100%"><tr><td>';
if (isset($_GET["JID"])){
	$JID = htmlspecialchars($_GET["JID"]);
} else {
	echo "VISA Reponse ::<br />ERROR REQUEST";
	exit();
}

$link = pg_Connect("host=localhost dbname=visadb user=visa password=1234");
$src  = pg_exec($link, "SELECT ctime, source_info FROM job WHERE id=" . $JID . ";");
$nrow = pg_fetch_array($src);
// Check Thumbnail
$thumb_file = substr($nrow["ctime"],0,4) . substr($nrow["ctime"],5,2) . substr($nrow["ctime"],8,2) . "/" . $JID . ".jpg";
if ( !file_exists($para["volo_thumb_path"] . "/" . $thumb_file) ){
          $thumb_file = "201601/" . $JID . ".jpg";
          if ( !file_exists($para["volo_thumb_path"] . "/" . $thumb_file) ){
               $thumb_file = "no-thumbnail.png";
          }
}
echo "<img src='./thumbnail/", $thumb_file, "'><br /><hr>";
echo '[ Output Video Information by Mediainfo Tool ]<br />';
echo '<pre>', htmlentities($nrow["source_info"]), '</pre>';
echo "<br />";
pg_close($link);
?>
  </td></tr></table>
  <p><p>
  </body>
</html>
