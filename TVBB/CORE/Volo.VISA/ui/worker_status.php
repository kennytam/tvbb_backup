<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');

if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
if (isset($_GET["date"]))
        $jdate = htmlspecialchars($_GET["date"]);
else
        $jdate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

$config = include('../scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .  " user=". $config['db_user'] .  " password=". $config['db_pass'] .  " dbname=".$config['db_name'];
$link = pg_connect($connstr)
	or die('DB Connection Error : ' . pg_last_error());
 
?>
<html>
  <head>
   <title>Volo.VISA</title>
   <meta http-equiv="refresh" content="60">
   <link rel="stylesheet" href="./template/styles.css">
  </head>
  <body style="background:rgba(65,65,65,1)">
  <div id='header'>
<?php
  echo "<p><b>VISA Admin Console v1.4.8 [Node : " . $para['volo_node'] . "] [System Time : " . $para['time_zone'] . " " . $curtime ."]</b></p>";
?>
  </div>
  <div id='cssmenu'>
  <ul>
   <li><a href='./index_admin.php'><span>Job Queue</span></a></li>
   <li><a href='./job_filter.php'><span>Job Filter</span></a></li>
   <!--<li><a href='./job_submit.php'><span>Job Submit</span></a></li>-->
<?php
 if (file_exists("./channels/index.php"))
   echo "<li><a href='./channels/index.php'><span>CH Extract</span></a></li>";
 if (file_exists("./trp/index.php"))
   echo "<li><a href='./trp/index.php'><span>TS Extract</span></a></li>";
?>
   <li class='active'><a href='./worker_status.php'><span>Encoder Status</span></a></li>
   <li><a href='./logs_status.php'><span>System Log</span></a></li>
   <li class='xdcambt'><a href='./xdcam.php'><span>XDCAM</span></a>
          <div class="xdcambt-content">
		<a class="xdcambt-content-a" href="xdcam.php">XDCAM_Profile</a>
                <a class="xdcambt-content-a" href="xdcam_edit.php">Audio Profile Edit</a>
		<a class="xdcambt-content-a" href="nasman.php">NAS Management</a>
          </div>
    </li>
                <li class="imx50bt"><a href='./imx50.php'><span>IMX50</span></a>
                    <div class="imx50bt-content">
                        <a class="imx50bt-content-a" href="imx50.php">IMX50_Profile</a>
                        <a class="imx50bt-content-a" href="imx50_edit.php">Audio Profile Edit</a>
                    </div>
                </li>

   <li class='last'><a href='./visa_status.php'><span>System Control</span></a></li>
  </ul>
  </div>
  <div id='joblist'>
  <table border="0" bgcolor="white"><tr><td>
<?php
//  echo '<table border="0" bgcolor="white" width="100%">';
//  echo "<tr><td colspan=6><b>[ VISA Scheduler / Splitter Status ]</b></td></tr>";
//  echo "<tr><th>VOLO ID</th><th>HOSTNAME / IP</th><th>VISA STATUS<br >(CPU Load)</th><th>LAST UP TIME</th><th></th><th></th><th></th></tr>";
//  // Scheduler and Splitter
//  $work = pg_exec($link, "SELECT * FROM worker WHERE id<30 ORDER BY uuid;");
//  $numwork = pg_numrows($work);
//  for($rj = 1; $rj <= $numwork; $rj++) {
//     $row = pg_fetch_array($work);
//     echo "<tr><td align='middle'>" . $row["id"] . "</td><td align='left'>" . $row["name"] . " [" . $row["uuid"] . "]</td>";
//
//     // Read Status from STATUS file
//     $fileList = explode("\n", shell_exec("ls " . $para["status_path"] . "/" . $row["name"] . ".STATUS"));
//     $status = "<font color=red>DOWN</font>";
//     $uptime = "<font color=red>---------- --:--:--</font>";
//     $cpuload= 0;
//     foreach($fileList as $StatusFile){
//	if (trim($StatusFile) == "") continue;
//	$nodename = substr(pathinfo($StatusFile, PATHINFO_BASENAME), 0, -7);
//	$schdfile = file_get_contents($StatusFile);
//	$schdline = explode("\n", $schdfile);
//	foreach($schdline as $statusline){
//         if (trim($statusline) == "") continue;
//            $schdstatus = explode(",", $statusline);
//	    if (substr($schdstatus[0], -7) == "SERVICE"){
//                if ($schdstatus[2] == "UP") $status = "<font color=green>UP</font>";
//                if ( (time() - strtotime($schdstatus[1])) > 70 )
//                    $uptime = "<font color=red>" . $schdstatus[1] . "</font>";
//                else
//                    $uptime = $schdstatus[1];
//            }
//	}
//	$schdfile = $para["status_path"] . "/VISA.CPU." . $row["name"] . ".log";
//	$cpuload  = file_get_contents($schdfile) * 1;
//     }
//
//     $status .= " (" . $cpuload . "%)";
//     echo "<td align='middle'>" . $status . "</td>";
//     echo "<td align='middle'>" . $uptime . "</td>";
//     echo "<td align='middle'></td></tr>";
//  }
//  pg_free_result($work);

  // Worker
  echo "<tr><td colspan=6><br /></td></tr>";
  $work = pg_exec($link, "SELECT * FROM worker WHERE id>30 ORDER BY uuid;");
  $numwork = pg_numrows($work);
  echo "<tr><td colspan=6><b>[ Volo Encoder Status ]</b></td></tr>";
  if ($numwork < 1){
    echo "<tr><th>NO VOLO ENCODER....</th></tr>";
  } else {
    echo "<tr><th>VOLO ID</th><th>HOSTNAME / IP</th><th>VISA STATUS<br >(CPU Load)</th><th>LAST UP TIME</th><th>VISA UNIT<br />[cur / max]</th><th>ENCODE JOB<br />[Mode/JID/OID/SID/Unit/Prior/Pass(%)/FPS]</th><th>ENCODE JOB<br />[Start Time]</th></tr>";
   for($rj = 1; $rj <= $numwork; $rj++) {
     $row = pg_fetch_array($work);
     echo "<tr><td align='middle'>" . $row["id"] . "</td><td align='left'>" . $row["name"] . " [" . $row["uuid"] . "]</td>";

     $schdfile = $para["status_path"] . "/VISA.CPU." . $row["name"] . ".log";
     $cpuload  = file_get_contents($schdfile) * 1;
     if ($row["mode"] == 'S'){
	$status = "<font color=blue>SUSPEND</font>";
     } else if ($row["dead"] == 't'){
	$status = "<font color=red>DOWN</font>";
     } else {
        $status = "<font color=green>UP</font> (" . $cpuload . "%)";
     }
     echo "<td align='middle'>" . $status . "</td>";
     echo "<td align='middle'>" . substr($row["heartbeat"], 0, 19) . "</td>";
     echo "<td align='middle'>" . $row["unit"] . " / " . $row["worker_unit"] . "</td>";

     // Get Normal Encode Job
     $jobquery = pg_exec($link, "SELECT id, job_id, encode_unit, priority, progress, progress_fps, progress_mode, stime, ltime FROM OUTPUT WHERE worker_id=" . $row["id"] . " AND stage=1 AND split_encode=false ORDER BY id;");
     $jobcount=0;
     while ($jobdtl = pg_fetch_array($jobquery)){
      $jobcount++;
      $emode = "NORMAL / " . $jobdtl["job_id"] . " / " . $jobdtl["id"] . " / - / " . $jobdtl["encode_unit"] . " / " . $jobdtl["priority"] . " / " . $jobdtl["progress_mode"] . "P(" . $jobdtl["progress"] . "%) / " . $jobdtl["progress_fps"];
      $firstTime= strtotime(substr($jobdtl["stime"],0, 19));
      $lastTime = strtotime(substr($jobdtl["ltime"],0, 19));
      $timeDiff = '(' . (int) (($lastTime-$firstTime)/60) . 'm)';
      if ($jobcount ==1){
	echo "<td align='left'>" . $emode . "</td>";
	echo "<td>" . substr($jobdtl["stime"], 0, 19) . " " . $timeDiff . "</td></tr>";
      } else {
	echo "<tr><td colspan='5'></td><td align='left'>" . $emode . "</td>";
	echo "<td>" . substr($jobdtl["stime"], 0, 19) . " " . $timeDiff . "</td></tr>";
      }
     }
     pg_free_result($jobquery);
 
     // Get Split Encode Job
     $jobquery = pg_exec($link, "SELECT SP.id, SP.job_id, SP.output_id, SP.encode_unit, SP.progress, SP.progress_fps, SP.progress_mode, SP.stime, SP.ltime FROM SPLIT_OUTPUT SP WHERE SP.worker_id=" . $row["id"] . " AND SP.stage=1 ORDER BY SP.id;");
     while ($jobdtl = pg_fetch_array($jobquery)){
      $jobcount++;
      $emode = " SPLIT / " . $jobdtl["job_id"] . " / " . $jobdtl["output_id"] . " / " . $jobdtl["id"] . " / " . $jobdtl["encode_unit"] . " / - / " . $jobdtl["progress_mode"] . "P(" . $jobdtl["progress"] . "%) / " . $jobdtl["progress_fps"];
      $firstTime= strtotime(substr($jobdtl["stime"],0, 19));
      $lastTime = strtotime(substr($jobdtl["ltime"],0, 19));
      $timeDiff = '(' . (int) (($lastTime-$firstTime)/60) . 'm)';
      if ($jobcount ==1){
	echo "<td align='left'>" . $emode . "</td>";
	echo "<td>" . substr($jobdtl["stime"], 0, 19) . " " . $timeDiff . "</td></tr>";
      } else {
	echo "<tr><td colspan='5'></td><td align='left'>" . $emode . "</td>";
	echo "<td>" . substr($jobdtl["stime"], 0, 19) . " " . $timeDiff . "</td></tr>";
      }
     }
     pg_free_result($jobquery);

     if ($jobcount == 0) echo "</tr>";
   }
  }
  pg_free_result($work);

  pg_close($link);
?>
  </table>
  <p><br /><p>
  </td></tr></table>
  </div>
  <p><p>
  </body>
</html>
