<?php
	$cpuStr = file_get_contents("/opt/Volo.VISA/VISA_Control/VISA.CPU.CTS-HKG-VOLO-STAG.cfg");
	$cpuTmp = explode("\n", $cpuStr);
	$cpuStr = $cpuTmp[0];
	$cpuInfos = explode("||", $cpuStr);

	echo json_encode(array("success"=>true, "data"=>array("cpus"=>$cpuInfos[0], "cores"=>$cpuInfos[1], "virtuals"=>$cpuInfos[2])));
?>
