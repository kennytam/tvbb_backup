<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if (isset($_GET["JID"])){
	$JID = htmlspecialchars($_GET["JID"]);
} else {
	echo "VISA Reponse ::<br />ERROR REQUEST";
	exit();
}

// Mark Split Encode Jobs Cancel
$config = include('../scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .  " user=". $config['db_user'] .  " password=". $config['db_pass'] .  " dbname=".$config['db_name'];
$link = pg_connect($connstr)
        or die('DB Connection Error : ' . pg_last_error());

$updsql = pg_exec($link, "INSERT INTO job_handler (job_id,output_id,c_cancel) VALUES (" . $JID . ",0,true);");
pg_free_result($updsql);
pg_close($link);

// Mark Output Job Cancel via API
$xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<VISA>\n<Request Command=\"JobControl\">\n";
$xml .= "<JobID>" . $JID . "</JobID>";
$xml .= "<Operation>Abort</Operation></Request></VISA>";

$post_data = array(
    "xml" => $xml,
);

$stream_options = array(
    'http' => array(
       'header'  => "Content-type: application/xml; charset=utf-8\r\n",
       'method'  => 'POST',
       'content' => $xml,
    ),
);

$context  = stream_context_create($stream_options);
$response = file_get_contents($para['schd_api'], null, $context);

echo "VISA Reponse ::<br />";
echo '<pre>', htmlentities($response), '</pre>';
exit(0);
?>
