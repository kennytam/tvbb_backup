<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if (function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
    @date_default_timezone_set(@date_default_timezone_get());
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Volo.VISA</title>
        <link rel="stylesheet" href="./template/styles.css">
        <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script-->
        <script src="jquery/3.2.1/jquery.min.js"></script>
    </head>


    <style>
        .add,.delete,.update,.erases,.saveas{
            position:absolute;
            height:35px;
            width:100px;
            left:28px;
            top:525px;
            color:white;
            background-color: rgba(153,153,151,1);
        }
        .add,.delete{
            display:none;
        }
        .update{
            top:135px;
        }
        .erases{
            top:180px;
        }
        .saveas{
            top:225px;
        }
        .delete:hover,.add:hover,.update:hover,.erases:hover,.saveas:hover{
            cursor:pointer;
            background-color: rgba(65,65,65,1);
            transition: .5s;
        }
        .delete{
            top:570px;
        }
        #addetail,#dedetail,#updatedetail,#erasesdetail,#saveasdetail{
            position: relative;
            margin:0 auto;
            text-align:center;
            top:10px;
            padding:0;
        }
        .ac{
            position:absolute;
            top:209px;
            left: 618px;
            width:150px;
            height:28px;
            //display:none;
        }
        .sr{
            //display:none;
            position:absolute;
            top:250px;
            left: 618px;
            width:150px;
            height:28px;
        }
        .dropbtn {
            background-color: rgba(65,65,65,1);
            cursor: pointer;
            width: 145px;
            height:28px;
            border:1px solid white;
            color:white;
            position:absolute;
        }
        .display,.display_sr{
            width:145px;
            height:28px;
            display:inline-block;
            position:absolute;
            top:-14px;
            left:5px;
            padding-top:6px;
        }

        .display::after{
            content: "...";
            display: inline-block;
            background-color: rgba(153,153,151,1);
            width: 30px;
            height:28px;
            position:absolute;
            padding:0px;
            right:5px;
            text-align:center;
            top: 0px;
            right: 0px;
            bottom: 0px;
            border-right:1px solid white;
            border-top:1px solid white;
            border-bottom:1px solid white;
        }
        .display_sr::after{
            content: "...";
            display: inline-block;
            background-color: rgba(153,153,151,1);
            width: 30px;
            height:28px;
            position:absolute;
            padding:0px;
            right:5px;
            text-align:center;
            top: 0px;
            right: 0px;
            bottom: 0px;
            border-right:1px solid white;
            border-top:1px solid white;
            border-bottom:1px solid white;
        }

        .dropdown {
            position: relative;
        }

        .dropdown-content {
            display: none;
            position: relative;
            z-index: 1;
            width: 150px; 
        }

        .dropdown-content .option{
            position: relative;
            color: white;
            height: 28px;
            width:146px;
            padding-left: 3px;
            background-color:rgba(153,153,151,1);
            text-align: left;
            top: 30px;
            border-bottom: white 1px solid;
            border-left: white 1px solid;
            border-right: white 1px solid;
        }

        .dropdown-content .option:hover {background-color: black;}

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .switch {
            position: relative;
            display: inline-block;
            width: 34px;
            height: 17px;
        }

        .switch input {display:none;}

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            top:2px;
            height: 13px;
            width: 13px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(18px);
            -ms-transform: translateX(26px);
            transform: translateX(14px);
        }



        body{
            background-color: rgba(65,65,65,1);
            font-size:13px;
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif
        }

        input[type=range] {
            -webkit-appearance: none;
            width: 300px;
        }

        input[type=range]::-webkit-slider-runnable-track {
            width: 300px;
            height: 2px;
            background: rgba(199, 199, 199, 1);
            border-radius: 50%;
        }

        input[type=range]::-webkit-slider-thumb {
            -webkit-appearance: none;
            border: none;
            height: 20px;
            width: 20px;
            background: rgba(153,153,151,.6);
            margin-top: -8px;
            -webkit-clip-path:polygon(0% 0%, 100% 0%, 50% 100%);
        }

        input[type=range]:focus {
            outline: none;
        }

        input[type=range]:focus::-webkit-slider-runnable-track {
            background: #ccc;
        }


        .show {display:block;}
        .container{
            border: #999997 solid 2px;
            position: relative;
            width: 1100px;
            height: 760px;
            top: 10px;
            left: 120px;
        }
        .le-ft{
            position: absolute;
            margin-left: 10px;
        }
        #le-ft-de
        {
            position:relative;
            top:50px;
        }
        #info{
            top: 180px;
        }
        #le-ft-lb{
            color:white;
            text-align:left;
        }
        .le-ft-in{
            margin-left: 100px;
        }
        #user{
            display:none;
            left: 150px;
            position: absolute;
            color: #999997;
        }
        #create_time{
            display:none;
            left: 150px;
            position: absolute;
            color: #999997;
            width:210px;
        }
        #le-ft-cr{
            top: 100px;
        }
        .md-dl{
            top: 100px;
            position: absolute;
            left: 350px;
        }
        #create{
            display:none;
            color:white;
            position: absolute;
            width: 100px;
        }
        #create_time_label{
            color:white;
            position: absolute;
            width: 100px;
        }
        #modify{
            display:none;
            color:white;
            position: absolute;
            width: 100px;
            top: 30px;
        }
        #modify_time{
            display:none;
            left: 150px;
            color:#999997;
            position: absolute;
            width: 210px;
            top: 30px;
        }
        .matrix{
            position: absolute;
            top: 410px;
            width: 100%;
        }
        #content{
            border:1px solid white;
            padding: 8px;
            margin: 3px;
            width: 280px;
            font-size:1em;
            color:rgba(232, 230, 230, 1);
        }
        .profile_name{
            display:inline-block;
            border: 1px solid rgba(232, 230, 230, 1); 
            height: 35px;
            width: 250px;
            font-size:1em;
            color: rgba(127, 204, 247,1);
            background-color: rgba(65,65,65,1);
        }
        /*kenny start,20180312*/
        .source_done_folder{
            display:inline-block;
            border: 1px solid rgba(232, 230, 230, 1); 
            height: 35px;
            width: 250px;
            font-size:1em;
            color: rgba(127, 204, 247,1);
            background-color: rgba(65,65,65,1);
        }
        .source_error_folder{
            display:inline-block;
            border: 1px solid rgba(232, 230, 230, 1); 
            height: 35px;
            width: 250px;
            font-size:1em;
            color: rgba(127, 204, 247,1);
            background-color: rgba(65,65,65,1);
        }
        .switch_watch_folder{
            display:inline-block;
            border: 1px solid rgba(232, 230, 230, 1); 
            height: 35px;
            width: 250px;
            font-size:1em;
            color: rgba(127, 204, 247,1);
            background-color: rgba(65,65,65,1);
        }
        /*kenny end,20180312*/
        ::-webkit-input-placeholder{
            color: pink;
        }
        .dest_name{
            display:inline-block;
            border: 1px solid rgba(232, 230, 230, 1);
            height: 35px;
            width: 250px;
            font-size:1em;
            color: rgba(127,204,247,1);
            background-color: rgba(65,65,65,1);
        }
        .profile_name_msg{
            display:none;
        }
        .dest_name_msg{
            display:none;
        }
        #title{
            font-size:1.5em;
            position: relative;
            color:white;
            display:inline-block;
            width:200px;
        }
        #dest{
            font-size:1.5em;
            position: relative;
            color:white;
            display:inline-block;
            width:200px;
        }
        #muxing{
            top:380px;
            color:white;
            font-size:20px;
        }
        #matrix_msg{
            top:382px;
            color:red;
            left: 165px;
            display:none;
        }
        #audio_muxing{
            padding: 0px;
            border-spacing: 0px;
            width:100%;
            height:100%;
        }
        #audio_muxing2{
            padding: 0px;
            width:1000px;
            height:1000px;
            border-spacing: 0px;
            width:100%;
        }
        .thead{
            height:35px;
            background-color: rgba(153,153,151,1);
            padding: 8 8 8 5;
            border-right: solid 1px white;
            border-bottom: solid 1px white;
            text-align:center;
            color: rgba(65,65,65,1);
        }
        #out1{
            border-top-left-radius: 5px;
        }
        #th15{
            border-top-right-radius: 5px;
        }
        .trrow:nth-child(odd){
            background-color: rgba(232,230,230,1); 
        }
        .trrow:nth-child(even){
            background-color: rgba(153,153,151,1); 
        }
        .audioType{
            position:relative;
            font-size:15px;
        }
        .audioTypetd{
            position:relative;
            text-align:center;
            width:50px;
            cursor:pointer;
        }
        td{
            height:30px;
            width:55px;
            border-left: none;
            border-right: solid 1px white;
            border-bottom: solid 1px white;
            color: rgba(33, 61, 77,1);
            position:relative;
            text-align:center;
        }
        .audio_lv{
            position:absolute;
            height:100px;
            width: 500px;
            top: 201px;
            left: 12px;
        }
        .audio_bg{
            position: relative;
            display: inline-block;
            width: 25px;
            height: 25px;
        }
        .audio_bg:checked ~ {
            display:none;
        }

        .audio_in{
            display:none;
        }
        .audio_fl{
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .audio_in:checked + .audio_fl{
            background-color: rgba(153,153,151,1); 
            clip-path: polygon(72% 0, 100% 16%, 48% 100%, 0 81%, 15% 54%, 34% 64%);
        }
        .audio_in:checked ~ #filter{
            background-color:white;
            height:25px;
            width:25px;
        }
        .audio_auto,.video-encoding-text{
            width: 200px;
            top: 2px;
            position: absolute;
            left: 40px;
            color:white;
        }
        #lv{
            width:500px;
            position:absolute;
            top:40px;
            left:3px;
        }
        #lv1,#lv2,#lv3{
            color:white;
            margin-right:103px;
        }
        #line{
            top:32px;
            position:absolute;
            width:315px;
        }
        #line1{
            top:32px;
            position:absolute;
        }
        #disp{
            position:absolute;
            height:50px;
            width:100px;
            left: 350px;
            font-size:3em;
            color:white;
        }
        .desc{
            position: absolute;
            right: 50px;
            top: 80px;
        }
        .db_level{
            position: absolute;
            right: 47px;
            top: 123px;
            //border:grey 1px solid;
            height:208px;
            width:550px;
            //font-size:16px;
        }
        .db_level_groups{
            color:white;
            position: absolute;
            //border:grey 1px solid;
            height:171px;
            width:115px;
            display:inline-block;
            margin:20px;
            //padding:5px;
        }
        #db_level_gp1{
            right:300px;
        }
        #db_level_gp2{
            right:265px;
        }
        #db_level_gp3{
            right:130px;
        }
        #db_level_gp4{
            right:-5px;
        }
        .db_level_output{
            text-transform:uppercase;
            width:21px;
            height:10px;
            margin-left:5px;
            margin-top:15px;
            text-align:center;
        }
        #desc_title{
            color:white;
            position:relative;
            top:-6px;
        }
        #desc_body{
            top: 2px;
            position:relative;

        }
        .ac-code{
            position:absolute;
            width: 100px;
            display:none;
        }
        .sa-ra{
            position:absolute;
            width: 100px;
            top: 43px;
            display:none;
        }
        .trrow.msg{
            background-color: red;
        }
        .profile{
            color: red;
            position:relative;
            left: 25px;
            display:inline-block;
        }
        .profileName{
            border:1px solid red;
        }
        .video-encoding{
            width:25px;
            height:25px;
            position:absolute;
            top:157px;
            left:12px;
        }
    </style>

    <body>
        <div id='header'>
            <?php
            $curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
            echo "<p><b>VISA Admin Console v1.4.8 [Node : " . $para['volo_node'] . "] [System Time : " . $para['time_zone'] . " " . $curtime . "]</b></p>";
            ?>
        </div>
        <div id='cssmenu'>
            <ul>
                <li><a href='./index_admin.php'><span>Job Queue</span></a></li>
                <li><a href='./job_filter.php'><span>Job Filter</span></a></li>
                <!--<li><a href='./job_submit.php'><span>Job Submit</span></a></li>-->
                <?php
                if (file_exists("./channels/index_admin.php"))
                    echo "<li><a href='./channels/index_admin.php'><span>CH Extract</span></a></li>";
                if (file_exists("./trp/index_admin.php"))
                    echo "<li><a href='./trp/index_admin.php'><span>TS Extract</span></a></li>";
                ?>
                <li><a href='./worker_status.php'><span>Encoder Status</span></a></li>
                <li><a href='./logs_status.php'><span>System Log</span></a></li>
                <li class='xdcambt'><a href='./xdcam.php'><span>XDCAM</span></a>
                    <div class="xdcambt-content">
                        <a class="xdcambt-content-a" href="xdcam.php">XDCAM Profile</a>
                        <a class="xdcambt-content-a" href="xdcam_edit.php">Audio Profile Edit</a>
                        <a class="xdcambt-content-a" href="nasman.php">NAS MANAGEMENT</a>
                    </div>
                </li>
                <li class="active imx50bt"><a href='./imx50.php'><span>IMX50</span></a>
                    <div class="imx50bt-content">
                        <a class="imx50bt-content-a" href="imx50.php">IMX50_Profile</a>
                        <a class="imx50bt-content-a" href="imx50_edit.php">Audio Profile Edit</a>
                    </div>
                </li>

                <li class='last'><a href='./visa_status.php'><span>System Control</span></a></li>
            </ul>
        </div>
        <?php

        function getConnect() {
            $conn = pg_connect("host=localhost port=5432 dbname=visadb user=visa password=1234");
            return $conn;
        }
        ?>
        <?php
        $profileName = $_GET['profileName'];
        $conn = getConnect();
        $res = pg_query($conn, "select * from audio_profile_info inner join audio_profile on audio_profile_info.id = audio_profile.id where audio_profile_info.profile_name = '$profileName';");
        $row = pg_fetch_object($res);
        $create_date = date_create($row->profile_create_date);
        $modified_date = date_create($row->profile_modified_date);
        $Id = $row->id;
        $Name = $row->full_path;
        $audio_codec = $row->audio_codec;
        $sampling_rate = $row->sampling_rate;
        $db_value = $row->db_value;
        $audio_remapping_only = $row->audio_remapping_only;
        $auto_level_adjustment = $row->auto_level_adjustment;
        $destName = $row->dest_path;
        //kenny start,20180312
        $src_done_path = $row->job_done_source_path;
        $src_err_path = $row->job_done_wrong_path;
        $switch_watch_folder = $row->switch_watch_folder;
        //kenny end,20180312
        pg_close($conn);
        ?>
        <div class="add"><p id="addetail">Add</p></div>
        <div class="delete"><p id="dedetail">Delete</div>
        <div class="update monitoring"><p id="updatedetail">Update</p></div>
        <div class="erases"><p id="erasesdetail">Erases</p></div>
        <div class="saveas"><p id="saveasdetail">Save as</p></div>
        <div class="container">
            <div class="le-ft">
                <h3 id="title">Watch Folder Name</h3>
                <span><input class="profile_name" type="text" value="<?php echo $Name; ?>"></span>
                <span class="profile_name_msg">* This field must be inputed.</span>
                
                <!--kenny start,20180312-->
                <h3 id="title">&nbsp;&nbsp;&nbsp;Source Done Path</h3>
                <span><input class="source_done_folder" type="text" value="<?php echo $src_done_path; ?>"></span>
                <!--kenny end-->
                
            </div>
            <div class="le-ft" id="le-ft-de">
                <h3 id="dest">Output Destination</h3>
                <span><input class="dest_name" type="text" value="<?php echo $destName; ?>"></span>
                <span class="dest_name_msg">* This field must be inputed.</span>
                
                <!--kenny start,20180312-->
                <h3 id="title">&nbsp;&nbsp;&nbsp;Source Error Path</h3>
                <span><input class="source_error_folder" type="text" value="<?php echo $src_err_path; ?>"></span>
                <!--kenny end-->
                
            </div>
            
            <!--kenny start 20180315, switch folder-->
            <div class="le-ft" id="le-ft-cr">
                <h3 id="dest">Switch Watch Folder</h3>
                <span><input class="switch_watch_folder" type="text" value="<?php echo $switch_watch_folder; ?>"></span>
                <span class="dest_name_msg">* This field must be inputed.</span>
                
            </div>
            <!--kenny end 20180315-->
            
            <div class="le-ft" id="le-ft-cr">
                <span id="create">Created By</span>
                <span id="user">Volo</span>

            </div>
            <!--div class="md-dl" id="le-ft-md">
                    <span id="create_time_label">Creation Date</span>
                    <span id="create_time"><?php echo date_format($create_date, 'Y-m-d H:i:s'); ?></span>
                    <span id="modify">Last Modified</span>
                    <span id="modify_time"><?php echo date_format($modified_date, 'Y-m-d H:i:s'); ?></span>
            </div-->
            <!--div class="desc">
                    <div id="desc_title">Description</div>	
                    <div id="desc_body">
                    <p id="content">High quality MPEG4-AVC encoding for Set top
                            <br>box IPTV service. To be userd in three diferent
                            <br>bit rate setting all at near constant bitrate
                    </p>
                </div>	
            </div>-->
                <?php
                    $conn = getConnect();
                    $audioTypeSet = pg_query($conn, "select * from audio_profile_matrix where id = $Id");
                    $db_array = array();
                    $idx = 0;
                    while ($audios = pg_fetch_object($audioTypeSet)) {
//                        $db_array[$idx++] = $audios['out_track_dbvalue'];
                        $db_array[$idx++] =  $audios->out_track_dbvalue;
                    }
                
                ?>
            <div class="db_level">
                <div class="db_level_groups" id="db_level_gp1">
                    <div class="db_track1" id="db_level_gp1_out1">Track 01:<input class="db_level_output" id="db_level_out1" type="text" maxlength="3" value="<?php echo $db_array[0]; ?>"> <span id="db_level_label1">dB</span></div>
                </div>
            </div>
            <div>
                <form action="d.php" method="POST">
                    <div class="le-ft" id="info">
                        <div class="ac-code">
                            <span id="le-ft-lb" for="fdName">Audio CODEC</span>
                        </div>
                        <br>
                        <div class="sa-ra">
                            <label id="le-ft-lb" for="sRate">Sampling Rate</label>
                        </div>
                        <br>
                        <!--div>
                                <label  id="le-ft-lb" for="aCodec">Audio BitRate</label>
                        </div-->
                    </div>

                    <div class="ac">
                        <div class="dropdown">
                            <div class="dropbtn" onclick="showf()"><p class="display"</p></div>
                            <div class="dropdown-content" onclick="hidef()">
                                <div class="option" id="op1"><?php echo $audio_codec?></div>
                                <div class="option" id="op2"><?php if($audio_codec=="PCM16") echo "PCM24"; if($audio_codec=="PCM24") echo "PCM16";?></div>
                            </div>
                        </div>
                    </div>

                    <div class="sr">
                        <div class="dropdown">
                            <div class="dropbtn" onclick="showf()"><p class="display_sr"</p></div>
                            <div class="dropdown-content" onclick="hidef()">
                                <div class="option" id="sr1">4</div>
                                <div class="option" id="sr2">8</div>
                            </div>
                        </div>
                    </div>

                    <div class="video-encoding">
                        <label class="audio_bg">
                            <?php
                            if ($audio_remapping_only == 't')
                                echo "<input class='audio_in' id='videoEncode' type='checkbox' checked>";
                            else
                                echo "<input class='audio_in' id='videoEncode' type='checkbox'>";
                            ?>
                            <div class="audio_fl"></div>
                            <div id="filter"></div>
                        </label>
                        <div class="video-encoding-text">Audio Remapping Only</div>
                    </div>




                    <div class="audio_lv">
                        <label class="audio_bg">
                                                        <?php
                                if ($auto_level_adjustment == 't')
                                        echo "<input class='audio_in' id='autoLevel' onclick='tt()' type='checkbox' checked>";
                                else
                                        echo "<input class='audio_in' id='autoLevel' onclick='tt()' type='checkbox'>";
                            ?>

                            <div class="audio_fl"></div>
                            <div id="filter"></div>
                        </label>
                        <div class="audio_auto">Auto Level Adjustment</div>
                        <div class="hiden">
                            <div id="lv">
                                <span id="lv1">-10dB</span>
                                <span id="lv2">0dB</span>
                                <span id="lv3">+10dB</span>
                            </div>
                            <div id="line1">
                                <input id="line" name="hello" type="range" max="10" min="-10" oninput="dbValue()">
                            </div>
                            <div id="disp"a>0dB
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="le-ft" id="muxing" onclick="tt()">AUDIO MUXING</div>
                        <div class="le-ft" id="matrix_msg">* The following channel must be mapped.</div>
                        <div>

                            <div class="matrix">
                                <table id="audio_muxing">
                                    <thead>
                                        <tr>
                                            <th class="thead" id="out1">Output Channels</th>
                                            <th class="thead" id="th0">Input Ch1</th>
                                            <th class="thead" id="th1">Input Ch2</th>
                                            <th class="thead" id="th2">Input Ch3</th>
                                            <th class="thead" id="th3">Input Ch4</th>
                                            <th class="thead" id="th4">Input Ch5</th>
                                            <th class="thead" id="th5">Input Ch6</th>
                                            <th class="thead" id="th6">Input Ch7</th>
                                            <th class="thead" id="th7">Input Ch8</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $conn = getConnect();
                                        $maxOutput = pg_query($conn, "select * from audio_profile_matrix where out_track = (select max(out_track) from audio_profile_matrix where id = $Id ) and id = $Id;");
                                        $row2 = pg_fetch_object($maxOutput);
                                        $audioTypeSet = pg_query($conn, "select * from audio_profile_matrix where id = $Id");
                                        $index = 0;
                                        $trrowId = 0;
                                        $inTrackName;
                                        $i = 0;
                                        $trackNumber = 0;
                                        while ($audios = pg_fetch_object($audioTypeSet)) {
                                            $index = $i + 1;
                                            $trrowId = $i + 100;
                                            echo '<tr class="trrow" id="trrow' . $trrowId . '">';
                                            echo "<td>$index</td>";
                                            for ($k = 0; $k < 8; $k++) {
                                                $trackNumber = $k + 1;
                                                $inTrackName = "in_track$trackNumber";
                                                echo '<td id="td' . $trrowId . $k . '">
                                        <label class="switch">';
                                                if ($audios->$inTrackName == 't')
                                                    echo '<input type="checkbox" class="sw" id="ch' . $trrowId . $k . '" checked>';
                                                else
                                                    echo '<input type="checkbox" class="sw" id="ch' . $trrowId . $k . '">';
                                                echo '<div class="slider"></div>
                                        </label>
                                        </td>';
                                            }
                                            echo '</tr>';
                                            $i++;
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            </form>
                        </div>
                    </div>
                    </body>
                    <script>
                        function dbValue()
                        {
                            var x = document.getElementById("line").value
                            if (x > 0)
                            {
                                document.getElementById("disp").innerHTML = "+" + x + "dB";
                                setToAll(x);
                            } else
                            {
                                document.getElementById("disp").innerHTML = x + "dB";
                                setToAll(x);
                            }
                        }
                    </script>
                    <script>
                        function hidef() {
                            $(".option").hide();
                        }
                        function showf() {
                            $(".option").show();
                        }
                        var ac = $("#op1").text();
                        $(".display").text(ac);
                        $(document).ready(function () {
                            $(".option").click(function (event) {
                                var msg = $(event.target).text();
                                var id = event.target.id;
                                if (id == "op1" || id == "op2")
                                    $(".display").text(msg);
                            });
                        });

                        var sa = $("#sr1").text();
                        $(".display_sr").text(sa);
                        $(document).ready(function () {
                            $(".option").click(function (event) {
                                var msg = $(event.target).text();
                                var id = event.target.id;
                                if (id == "sr1" || id == "sr2")
                                {
                                    $(".display_sr").text(msg);
                                    removeMatrix();
                                    for (var rows=1;rows!=msg;rows++)
                                        addMatrix();
                                }
                            });
                        });



                    </script>
                    <script>
                        function tt()
                        {
                            var sta = $("#autoLevel").is(":checked");
                            if (sta == true)
                            {
                                $(".hiden").hide();
                                $("#line").val(0);
                                $("#disp").text("0dB");
                                reSetToAuto();
                            }


                            if (sta == false)
                            {
                                $(".hiden").show();
                                reSetToDefault();
                            }

                        }
                    </script>
                    <script>
                        function contentSubmit(httpPar)
                        {
			    var profileType = "IMX50";
                            var pName = $(".profile_name").val();
                            var dPlay1 = $(".display").text();
                            //var dPlay2 = $(".display_sr").text();
                            var dPlay2 = "48KHz";
                            var aAdjus = $("#autoLevel").is(":checked");
                            var iLine = $("#line").val();
                            if (iLine == 0)
                                iLine = "zero";
                            var audioTrackCnt = parseInt($("#audio_muxing tr:last td:first").text());
                            var audioType = new Array();
                            for (var i = 0; i < audioTrackCnt; i++)
                            {
                                audioType[i] = $("#typetd" + i).text();
                            }
                            var audiodBvalue = new Array();
                            for (var i = 0; i < audioTrackCnt; i++)
                            {
                                audiodBvalue[i] = $("#db_level_out" + (i + 1)).val();
                            }
                            var audioTrack = new Array();
                            for (var k = 0; k < audioTrackCnt; k++)
                            {
                                var cellId = 100;
                                audioTrack[k] = new Array();
                                for (var j = 0; j < 16; j++)
                                {
                                    cellId = 100 + k;
                                    audioTrack[k][j] = $("#ch" + cellId + j).is(":checked");
                                }
                            }
                            var encodeStatus = $("#videoEncode").is(":checked");
                            var httpRequest = httpPar.toLowerCase();
                            var destPath = $(".dest_name").val();
                            //kenny start,20180312
                            var soure_done_folder = $(".source_done_folder").val().trim();
                            var soure_error_folder = $(".source_error_folder").val().trim();
                            var switch_watch_folder = $(".switch_watch_folder").val().trim();
                            arr = {ar1: pName, ar2: dPlay1, ar3: dPlay2, ar4: aAdjus, ar5: iLine, ar6: audioType, ar7: audioTrack, ar8: encodeStatus, ar9: httpRequest, ar10: destPath, ar11: audiodBvalue, ar12: profileType,ar13:soure_done_folder,ar14:soure_error_folder,ar15:switch_watch_folder};
//                            arr = {ar1: pName, ar2: dPlay1, ar3: dPlay2, ar4: aAdjus, ar5: iLine, ar6: audioType, ar7: audioTrack, ar8: encodeStatus, ar9: httpRequest, ar10: destPath, ar11: audiodBvalue, ar12: profileType};
                            //kenny end,20180312
                            $.ajax({
                                type: "POST",
                                url: "imx50_manage.php",
                                data: arr,
                                dataType: "json",
                                success: function (data)
                                {
                                    if (data["success"])
                                        alert(data["success"])
                                    else
                                        alert(data["err"]);
                                    window.location = "imx50_edit.php";
                                },
                                error: function ()
                                {
                                    alert("Failed to request submition");
                                }
                            })

                        }
                    </script>
                    <script>
                        function addMatrix(){
                            var rowNumber = parseInt($("#audio_muxing tr:last td:first").text());
                            var index = rowNumber + 1;
                            rowId = rowNumber + 100;
                            if (rowNumber < 8)
                            {
                                var html = '<tr class="trrow" id="trrow' + rowId + '">';
                                html += '<td>' + index + '</td>';
                                for (var i = 0; i < 8; i++)
                                {
                                    html += '<td id="td' + rowId + i + '">';
                                    html += '<label class="switch">';
                                    html += '<input type="checkbox" class="sw" id="ch' + rowId + i + '">';
                                    html += '<div class="slider"></div>';
                                    html += '</label>';
                                    html += '</td>';
                                }
                                html += '</tr>';
                                var containerHeight = parseInt($(".container").css("height")) + 32;
                                var btnTop = containerHeight - 40;
                                $(".container").css("height", containerHeight);
                                $(".btn").css("top", btnTop);
                                $("#audio_muxing tr:last").after(html);
                                $(".db_track" + (rowNumber + 1)).css("display", "inline-block");
                            }
                        }
                    </script>
                    <script>
                        function removeMatrix()
                        {
                            var rowNumber = $("#audio_muxing tr:last td:first").text();
                            var i = rowNumber;
                                for(i;i!=1;i--)
                                {
                                        $("#audio_muxing tr:last").remove();
                                        var containerHeight = parseInt($(".container").css("height")) - 32;
                                        $(".container").css("height", containerHeight);
                                }
                        }
                    </script>
                    <script>
                        $(document).on("click",".sw",function(event) {
                        var id = event.target.id;       
                        var position = id[4];
                        var eachCol;
                        for (var i=0;i<8;i++)
                        {
                                eachCol = "ch10" + position + i;
                                //alert(eachCol);
                                if (eachCol == id)
                                ;
                                else
                                $('#'+eachCol).prop('checked',false);
                        }
                         });
                         
                    </script>

                    <script>
                        $(document).ready(function () {
                            $(".erases").click(function () {
                                var profileNameReady = checkProfileName();
                                var matrixReady = checkMatrix();
                                var httpRequest = $(this).text();
                                if (profileNameReady == 1 && matrixReady == 1)
                                    contentSubmit(httpRequest);

                            });
                        });
                    </script>
                    <script>
                        $(document).ready(function () {
                        });
                    </script>
                    <script>
                        $(document).ready(function () {
                            var oldName = $(".profile_name").val();
                            $(".update").click(function () {
                                var profileNameReady = checkProfileName();
                                var matrixReady = checkMatrix();
                                var httpRequest = $(this).text();
                                var newName = $(".profile_name").val();
                                var nameStd;
                                if (oldName == newName)
                                    nameStd = 1;
                                else
                                    nameStd = 0;
                                if (profileNameReady == 1 && matrixReady == 1 && nameStd == 1)
                                    contentSubmit(httpRequest);
                                else if (profileNameReady == 1 && matrixReady == 0 && nameStd == 1)
                                    ;
                                else
                                    alert("Please Click 'Save as' button to save as new audio profile");
                            });
                        });
                    </script>
                    <script>
                        $(document).ready(function () {
                            var oldName = $(".profile_name").val();
                            $(".saveas").click(function () {
                                var newName = $(".profile_name").val();
                                var nameStd;
                                if (oldName == newName)
                                    nameStd = 0;
                                else
                                    nameStd = 1;
                                var profileNameReady = checkProfileName();
                                var matrixReady = checkMatrix();
                                var httpRequest = $(this).text();
                                var destPathReady = checkDestPath();
                                if (profileNameReady == 1 && matrixReady == 1 && destPathReady == 1 && nameStd == 1)
                                    contentSubmit(httpRequest);
                                else if (profileNameReady == 1 && matrixReady == 1 && destPathReady == 1 && nameStd == 0)
                                    alert("The profile name should not be same with the original profile name");

                            });
                        });
                    </script>
                    <script>
                        function checkProfileName()
                        {

                            $(".profile_name_msg").removeClass("profile");
                            $(".profile_name").removeClass("profileName");
                            var regx = /^[\/]+[0-9a-zA-Z]+(\/|\_*|\-*[0-9a-zA-Z]*)*[0-9a-zA-Z]*$/;
                            var pName = $(".profile_name").val();
                            if (pName == "")
                            {
                                $(".profile_name_msg").addClass("profile");
                                $(".profile_name").addClass("profileName");
                                return 0;
                            } else if (regx.test(pName))
                            {
                                return 1;
                            } else
                            {
                                $(".profile_name_msg").addClass("profile");
                                $(".profile_name_msg").text("This field allow symbols - and _ only");
                                $(".profile_name").addClass("profileName");
                                return 0;
                            }
                        }
                    </script>
                    <script>
                        function checkDestPath()
                        {
                            $(".dest_name_msg").removeClass("profile");
                            $(".dest_name").removeClass("profileName");
                            var regx = /^[\/]+[0-9a-zA-Z]+(\/|\_*|\-*[0-9a-zA-Z]*)*[0-9a-zA-Z]*$/;
                            var pName = $(".dest_name").val();
                            if (pName == "")
                            {
                                $(".dest_name_msg").addClass("profile");
                                $(".dest_name").addClass("profileName");
                                return 0;
                            } else if (regx.test(pName))
                            {
                                return 1;
                            } else
                            {
                                $(".dest_name_msg").addClass("profile");
                                $(".dest_name_msg").text("This field allow symbols / - _ only");
                                $(".dest_name").addClass("profileName");
                                return 0;
                            }

                        }
                    </script>
                    <script>
                        function checkMatrix()
                        {
                            var status = 0;
                            var statusTwo = 0;
			    var statusThree = 0;
                            var id;
                            var flag = 0;
                            var sta;
                            
                            $(".trrow").removeClass("msg");
                            $("#matrix_msg").css("display","none");
                            var lastRow = $("#audio_muxing tr:last td:first").text();
                            for (var i=1;i<=lastRow;i++)
                                {
                                        flag = i - 1;   
                                        statusTwo = 0;
                                        for (var k=0;k<8;k++)
                                        {
                                                id = "#ch10" + flag + k;
                                                sta = $(id).is(":checked");
                                                if (sta == true)
                                                {
                                                        statusTwo++; 
                                                }
                                        }       
                                        if (statusTwo == 0)
                                        {
                                                //status = 0;             
						
                                                $("#matrix_msg").css("display","inline-block");
                                                $("#trrow10"+flag).addClass("msg");     
						//return status;
                                        }
					else
					{
						statusThree++;
					}
                                }
			    if (statusThree == 4 || statusThree == 8)
				status = 1;
			    else
				status = 0; 
                                
			    
                            return status;
                        }
                    </script>
                    <script>
                        function reSetToAuto()
                        {
                            var i = 1;
                            for (i; i < 9; i++)
                            {
                                var message = $("#db_level_out" + i).css('display');
                                if (message == "inline-block")
                                {
                                    $("#db_level_out" + i).val("A");
                                    $("#db_level_label" + i).css("display", "none");
                                }
                            }
                        }

                        function reSetToDefault()
                        {
                            var i = 1;
                            for (i; i < 9; i++)
                            {
                                var message = $("#db_level_out" + i).css('display');
                                if (message == "inline-block")
                                {
                                    $("#db_level_out" + i).val("0");
                                    $("#db_level_label" + i).css("display", "inline-block");
                                }
                            }
                        }

                        function setToAll(x)
                        {
                            var i = 1;
                            for (i; i < 9; i++)
                            {
                                var message = $("#db_level_out" + i).css('display');
                                if (message == "inline-block")
                                {
                                    if (x > 0)
                                    {
                                        $("#db_level_out" + i).val("+" + x);
                                    } else
                                    {
                                        $("#db_level_out" + i).val(x);
                                    }
                                }

                            }

                        }

                        $(document).on("change", ".db_level_output", function (event) {
                            var id = event.target.id;
                            var value = $("#" + id).val().toUpperCase();
                            var regx = /^[A]$|^[0]$|^\+[0-9]$|^\+[0-9]*$|^\-[0-9]$|^\-[0-9]*$/;
                            if (regx.test(value))
                            {
                                $(".hiden").show();
                                if ($("#" + id).val() != "0")
                                {
                                    $("#disp").text("Customize");
                                    $("#autoLevel").prop("checked", false);
                                }
                                if ($("#" + id).val() != "A")
                                    $("#db_level_label" + id[id.length - 1]).css("display", "inline-block");
                                if ($("#" + id).val() == "A")
                                    $("#db_level_label" + id[id.length - 1]).css("display", "none");
                            } else
                            {
                                alert("This field can only be \"A\" or 0 and range from -10 to +10");
                                $("#" + id).val("A");
                                $("#db_level_label" + id[id.length - 1]).css("display", "none");
                                $(".hiden").show();
                                if ($("#" + id).val() != "0")
                                {
                                    $("#disp").text("Customize");
                                    $("#autoLevel").prop("checked", false);
                                }
                            }
                        });
                    </script>
		    <script>
                        $(document).ready(function(){
                                if($("#autoLevel").is(':checked'))
                                tt();
                        });
                    </script>
		    <script>
                        $(document).ready(function(){
                                var rowNumber = $("#audio_muxing tr:last td:first").text();
				if (rowNumber == 8)
				//alert(rowNumber);
				$(".display_sr").text(rowNumber);
                                
                        });
                    </script>

                    </html>

