<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if (isset($_GET["source"])){
	$SRC = trim(htmlspecialchars($_GET["source"]));
	$SRCNAME = exec('/bin/basename ' . $SRC);
	$pos = strrpos($SRCNAME, ".");
	$SRCNAME = substr($SRCNAME, 0, $pos);
	if ($pos < 1){
		echo "VISA Reponse ::<br />ERROR REQUEST";
		exit();
	}
} else {
	echo "VISA Reponse ::<br />ERROR REQUEST";
	exit();
}

if (isset($_GET["dest"])){
	$DEST = trim(htmlspecialchars($_GET["dest"]));
} else {
	echo "VISA Reponse ::<br />ERROR REQUEST";
	exit();
}

if (isset($_GET["profile"])){
	$profile = htmlspecialchars($_GET["profile"]);
} else {
	echo "VISA Reponse ::<br />ERROR REQUEST";
	exit();
}

// Custom Parameters
$container=$resolu=$vdbit=$adbit=$deint=$pmode="";
if (substr($profile,0,3) != "SET"){
  if (isset($_GET["container"])){
	$container = strtolower(htmlspecialchars($_GET["container"]));
  }
  if (isset($_GET["resolu"])){
	$resolu = strtolower(htmlspecialchars($_GET["resolu"]));
  }
  if (isset($_GET["vdbit"])){
	$vdbit = strtolower(htmlspecialchars($_GET["vdbit"]));
  }
  if (isset($_GET["adbit"])){
	$adbit = strtolower(htmlspecialchars($_GET["adbit"]));
  }
  if (isset($_GET["deint"])){
	$deint = strtolower(htmlspecialchars($_GET["deint"]));
  }
  if (isset($_GET["pmode"])){
	$pmode = strtolower(htmlspecialchars($_GET["pmode"]));
  }
}

if (isset($_GET["hls"])){
	$HLS = htmlspecialchars($_GET["hls"]);
} else {
	$HLS = "N";
}

if (isset($_GET["priority"])){
	$PRIORITY = htmlspecialchars($_GET["priority"]);
} else {
	$PRIORITY = 40;
}

if (isset($_GET["split"])){
	$SPLIT = htmlspecialchars($_GET["split"]);
} else {
	$SPLIT = "N";
}

$xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<VISA>\n<Request Command=\"JobSubmit\">\n";
$xml .= "<SourceUri>" . $SRC . "</SourceUri>\n";
$xml .= "<OutputPath>" . $DEST . "</OutputPath>\n";
$xml .= "<EncodePriority>" . $PRIORITY . "</EncodePriority>\n";
if ($SPLIT == "Y")
	$xml .= "<SplitEncode>true</SplitEncode>\n";
else
	$xml .= "<SplitEncode>false</SplitEncode>\n";
include_once('./template/JobDetail.tmpl');
$xml .= $PROF;
$xml .= "</Request>\n";
$xml .= "</VISA>";

// DEBUG
#echo '<pre>', htmlentities($xml), '</pre>';
#exit();

$post_data = array(
    "xml" => $xml,
);

$stream_options = array(
    'http' => array(
       'header'  => "Content-type: application/xml; charset=utf-8\r\n",
       'method'  => 'POST',
       'content' => $xml,
    ),
);

$context  = stream_context_create($stream_options);
$response = file_get_contents($para['schd_api'], null, $context);

echo "VISA Reponse ::<br />";
echo '<pre>', htmlentities($response), '</pre>';

// Write the Workflow Job File
$xml = new SimpleXMLElement( $response );
$SRCPATH=$para["source_path"];
if ( strcasecmp($xml->Response->Status, "OK") == 0 ) {
	$JOBPATH    = $para["volo_job_path"];
	$jobInfo    = pathinfo($SRC);
	$fullPath   = $jobInfo['dirname'];
	$filePrefix = $jobInfo['filename'];
	$SRCNAME    = $jobInfo['basename'];
	$SUBPATH    = substr($fullPath, strspn($SRCPATH, $fullPath)+1);
	$SRCFILE    = $SRC;
	$OUTPUT     = $DEST;
	$jobID      = $xml->Response->JobID[0];
	$source_duration = 0;
	$Plan       = "ALL";
	$JobPost    = $HLS;

	// 1977|/media/adhoc/oscar/source|op0101.txt|op0101.mxf|/media/adhoc/oscar/output/|PROMO|op0101|3565|{PUBLISH JOB DETAIL [/media/adhoc/oscar/source  /media/adhoc/oscar/source/op0101.mxf /media/adhoc/oscar/output/ OSCAR PROMO op0101 3565]}
        file_put_contents($JOBPATH . '/' . $jobID . '.job', $jobID . '|' . $fullPath . '|' . $filePrefix . '.txt|' . $SRCNAME . '|' . $OUTPUT . '|' . $JobPost  . '|' . $filePrefix . '|' . $source_duration . '|{PUBLISH DETAIL[' . $fullPath . ' ' . $SUBPATH . ' ' . $SRCFILE . ' ' . $OUTPUT . ' ' . $Plan . ' ' . $JobPost  . ' ' . $filePrefix . ' ' . $source_duration . ']}' . "\n");
	chmod($JOBPATH . '/' . $jobID . '.job', 0666);
}

?>
