<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
if (isset($_GET["JID"])){
	$JID = htmlspecialchars($_GET["JID"]);
} else {
	echo "VISA Reponse ::<br />ERROR REQUEST";
	exit(1);
}

$config = include('../scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .  " user=". $config['db_user'] .  " password=". $config['db_pass'] .  " dbname=".$config['db_name'];    
$link = pg_connect($connstr)
        or die('DB Connection Error : ' . pg_last_error());

// Get Job ID and Encode Mode
$jsql = pg_exec($link, "SELECT job_id, split_encode FROM output WHERE job_id=" . $JID . " LIMIT 1;");
$nrow = pg_fetch_array($jsql);
$JID = $nrow["job_id"];
pg_free_result($jsql);

if ($nrow["split_encode"] == "t"){
	// Update Job Handler
	$updsql = pg_exec($link, "INSERT INTO job_handler (job_id,output_id,c_retry) VALUES (" . $JID . ",0,true);");
	pg_free_result($updsql);
} else {
	// Reset OUTPUT JOB
	$updsql = pg_exec($link, "UPDATE output SET c_cancel=false,stage=0,worker_id=0,err=0,progress=0,progress_fps=0,progress_mode='1',stime=now(),ltime=now(),message='[STATUS] Encode Job Resubmitted' WHERE job_id=" . $JID . ";");
	pg_free_result($updsql);
	// Update Job Handler
	$updsql = pg_exec($link, "INSERT INTO job_handler (job_id,output_id,c_retry,mode) VALUES (" . $JID . ",0,true,'D');");
	pg_free_result($updsql);
}

echo '<pre>Encode Job [' . $JID . '] Resubmitted</pre>';
echo "<br />";
pg_close($link);
exit(0);
?>
