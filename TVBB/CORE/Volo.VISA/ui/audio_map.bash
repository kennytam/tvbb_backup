#!/bin/bash
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
# Volo Split Encoder :: Scripts to Split Video Source
# "USAGE: encode_audio.sh [JOB ID] [OUTPUT ID] [SOURCE FILE FULL PATH] [DESTINATION FILE FULL PATH] [AUDIO MAPPING]"
#
 
cd /opt/Volo.VISA/control/split_encode
JID=$1
OID=$2
SOURCE=$3
DEST=$4
AUDIOMAP=$5
 
VOLO_PROBE=/opt/volo/bin/vprobe
JOB_PATH=/dev/shm/Volo.VISA/splitjobs/SP_${JID}/muxaudio/${OID}
JOB_LOG='/opt/Volo.VISA/logs/VISA.AudioMUX'.`hostname`.`date +%Y%m%d`.'log'
ERRSTR=""
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Mux Audio Job Submit :: JID[${1}] OID[${2}] SOURCE[${3}] DEST[${4}] AUDIOMAP[${5}] >> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Mux Audio Command :: ./encode_audio.sh ${1} ${2} ${3} ${4} ${5} >> ${JOB_LOG}
 
# Source Not Exist -- Skip Checking Source, Check in Retry Level
#if [ ! -f "${SOURCE}" ]
#then
#	echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Mux Audio Job Error :: JOB[${OID}] SOURCE[${SOURCE}] Not Found >> ${JOB_LOG}
#	ERRSTR="[ERROR] Mux Audio Error :: Source Video Not Found"
#fi
 
# Create Encode Working Path
if [ -d "${JOB_PATH}" ]; then
	mv ${JOB_PATH} ${JOB_PATH}.`date +"%Y%m%d%H%M%S"`
fi
mkdir -p ${JOB_PATH}

# Mark Job ERROR
if [ ! -z "${ERRSTR}" ]; then
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

# Create Encode Destination Path (with directory 755 and file 664)
umask ug=rwx,o=rx
OUTPATH=`dirname ${DEST}`
if [ -d "${OUTPATH}" ]; then
	rm -rf ${DEST}
else
	mkdir -p ${OUTPATH}
fi

echo Mux Audio Job Start :: `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.start
echo Source Audio Information :: >> ${JOB_PATH}/job.start
ls -lh --full-time ${SOURCE} >> ${JOB_PATH}/job.start 2>&1
${VOLO_PROBE} -i ${SOURCE} >> ${JOB_PATH}/job.start 2>&1
echo "" >> ${JOB_PATH}/job.start

# Prepare mux audio
TEMPOUT=${SOURCE}.FINAL.mp4

## Set Audio Mapping Retry
#for(( s=1; s<4; s++ ))
#do
#	php /opt/volo/script/mono_2_stereo_prepare.php ${SOURCE} ${JOB_PATH} ${AUDIOMAP} >> ${JOB_PATH}/audioparse.log 2>&1
#	if [ -f "${JOB_PATH}/merge_filter.log" ]; then
#		php /opt/volo/script/mono_2_stereo.php ${SOURCE} ${TEMPOUT} ${JOB_PATH} >> ${JOB_PATH}/audioparse.log 2>&1
#	fi
#	if [ -f "${TEMPOUT}" ]; then
#		# Move Temp File to Output
#		echo Mux Audio Job End :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
#		echo Move Final Audio File :: mv -f ${TEMPOUT} ${DEST} >> ${JOB_PATH}/job.start
#		ls -lh --full-time ${TEMPOUT} >> ${JOB_PATH}/job.start 2>&1
#		mv -f ${TEMPOUT} ${DEST} >> ${JOB_PATH}/job.start 2>&1
#		echo Destination Audio Information :: >> ${JOB_PATH}/job.start
#		ls -lh --full-time ${DEST} >> ${JOB_PATH}/job.start 2>&1
#		${VOLO_PROBE} -i ${DEST} >> ${JOB_PATH}/job.start 2>&1
#		echo "" >> ${JOB_PATH}/job.start
#		echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.done
#		exit 0
#	fi
#	if [ ${s} -lt 3 ]; then
#		echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Mux Audio Retry Job :: JID[${JID}] OID[${OID}] SOURCE[${SOURCE}] DEST[${DEST}] RETRY[${s}] >> ${JOB_LOG}
#                echo Split Encode Retry [$s] :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/audioparse.log
#                echo Split Encode Retry [$s] :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
#                sleep $((s*5))
#		echo Retry Source Information :: >> ${JOB_PATH}/job.start
#		ls -lh --full-time ${SOURCE} >> ${JOB_PATH}/job.start 2>&1
#                ${VOLO_PROBE} -i ${SOURCE} >> ${JOB_PATH}/job.start 2>&1
#		echo "" >> ${JOB_PATH}/job.start
#        fi
#done

#kenny, new mapping

# Prepare mux audio
TEMPOUT=${SOURCE}.FINAL.mov

# Set Audio Mapping Retry
for(( s=1; s<4; s++ ))
do
	php /opt/volo/script/audio_map.php -in ${SOURCE} -out ${TEMPOUT} -map_rule ${AUDIOMAP} >> ${JOB_PATH}/audioparse.log 2>&1 >> ${JOB_PATH}/audioparse.log 2>&1

	if [ -f "${TEMPOUT}" ]; then
		# Move Temp File to Output
		echo Mux Audio Job End :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
		echo Move Final Audio File :: mv -f ${TEMPOUT} ${DEST} >> ${JOB_PATH}/job.start
		ls -lh --full-time ${TEMPOUT} >> ${JOB_PATH}/job.start 2>&1
		mv -f ${TEMPOUT} ${DEST} >> ${JOB_PATH}/job.start 2>&1
		echo Destination Audio Information :: >> ${JOB_PATH}/job.start
		ls -lh --full-time ${DEST} >> ${JOB_PATH}/job.start 2>&1
		${VOLO_PROBE} -i ${DEST} >> ${JOB_PATH}/job.start 2>&1
		echo "" >> ${JOB_PATH}/job.start
		echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.done
		exit 0
	fi
	if [ ${s} -lt 3 ]; then
		echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Mux Audio Retry Job :: JID[${JID}] OID[${OID}] SOURCE[${SOURCE}] DEST[${DEST}] RETRY[${s}] >> ${JOB_LOG}
                echo Split Encode Retry [$s] :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/audioparse.log
                echo Split Encode Retry [$s] :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
                sleep $((s*5))
		echo Retry Source Information :: >> ${JOB_PATH}/job.start
		ls -lh --full-time ${SOURCE} >> ${JOB_PATH}/job.start 2>&1
                ${VOLO_PROBE} -i ${SOURCE} >> ${JOB_PATH}/job.start 2>&1
		echo "" >> ${JOB_PATH}/job.start
        fi
done



echo Mux Audio Job Fail :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
echo [ERROR] Mux Audio Error :: Fail to Mux Output Audio > ${JOB_PATH}/job.error
exit 1
