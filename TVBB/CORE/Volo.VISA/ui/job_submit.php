<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
?>
<html>
  <head>
   <title>Volo.VISA</title>
   <link rel="stylesheet" href="./template/styles.css">
  </head>
  <body style="background:rgba(65,65,65,1)">
  <div id='header'>
 <?php
   $curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
   echo "<p><b>VISA Admin Console v1.4.8 [Node : " . $para['volo_node'] . "] [System Time : " . $para['time_zone'] . " " . $curtime ."]</b></p>";
 ?>
  </div>
  <div id='cssmenu'>
  <ul>
   <li><a href='./index.php'><span>Job Queue</span></a></li>
   <li><a href='./job_filter.php'><span>Job Filter</span></a></li>
   <li class='active'><a href='./job_submit.php'><span>Job Submit</span></a></li>
<?php
 if (file_exists("./channels/index.php"))
   echo "<li><a href='./channels/index.php'><span>CH Extract</span></a></li>";
 if (file_exists("./trp/index.php"))
   echo "<li><a href='./trp/index.php'><span>TS Extract</span></a></li>";
?>
   <li><a href='./worker_status.php'><span>Encoder Status</span></a></li>
   <li><a href='./logs_status.php'><span>System Log</span></a></li>
   <li><a href='./xdcam.php'><span>XDCAM</span></a></li>
   <li class='last'><a href='./visa_status.php'><span>System Control</span></a></li>
  </ul>
  </div>
  <div id='joblist'>
<?php
  include_once('./template/JobSubmit.tmpl');
?>
  </div>
  <p><p>
  </body>
</html>
