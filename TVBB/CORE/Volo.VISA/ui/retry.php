<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################

if (isset($_GET["OID"])){
	$OID = htmlspecialchars($_GET["OID"]);
} else {
	echo "VISA Reponse ::<br />ERROR REQUEST";
	exit(1);
}

$para   = include('../scheduler/config/scheduler.php');
$config = include('../scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .  " user=". $config['db_user'] .  " password=". $config['db_pass'] .  " dbname=".$config['db_name'];    
$link = pg_connect($connstr)
        or die('DB Connection Error : ' . pg_last_error());

// Get Job ID and Encode Mode
$jsql = pg_exec($link, "SELECT job_id, split_encode FROM output WHERE id=" . $OID . " LIMIT 1;");
$nrow = pg_fetch_array($jsql);
$JID = $nrow["job_id"];
pg_free_result($jsql);

if ($nrow["split_encode"] == "t"){
   $jobPath = $para['volo_splitjob_work'] . "/SP_" . $JID . "/split";
   if (file_exists($jobPath)){
	// Update Job Handler
	$updsql = pg_exec($link, "INSERT INTO job_handler (job_id,output_id,c_retry) VALUES (" . $JID . "," . $OID . ",true);");
	pg_free_result($updsql);
   } else {
	echo '<pre>ERROR :: Split Encode Output Job [' . $OID . '] Resubmit Failed, Split Job Purged.</pre>';
	echo "<br />";
	pg_close($link);
	exit(1);
   }
} else {
	$updsql = pg_exec($link, "UPDATE output SET c_cancel=false,err=0,stage=0,worker_id=0,progress=0,stime=now(), ltime=now(),message='[STATUS] Encode Output Job Resubmitted' WHERE id=" . $OID . ";");
	pg_free_result($updsql);
	$updsql = pg_exec($link, "INSERT INTO job_handler (job_id,output_id,c_retry,mode) VALUES (" . $JID . "," . $OID . ",true,'D');");
	pg_free_result($updsql);
}

echo '<pre>Encode Output Job [' . $OID . '] Resubmitted</pre>';
echo "<br />";

// Reset Job File
pg_close($link);
exit(0);
?>
