<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');

function parseLog($logPath, $logType, $logText){
	$LOGLIST = shell_exec("ls " . $logPath);
	$LOGPATH = explode("\n", $LOGLIST);
	foreach($LOGPATH as $LOGLIST){
		if (trim($LOGLIST) == "") continue;
		file_put_contents($logText, "#" . basename($LOGLIST) . "\n", FILE_APPEND);
		$cmd = 'grep "' . $logType . '" ' . $LOGLIST . ' | tail -50 >> ' . $logText;
		exec($cmd);
	}
}

function displayLog($logFile){
  echo '<table border="0" bgcolor="white" width="100%">';
  echo "<tr><th align='left' width='25%'>Log File Name</th><th align='left' width='5%'>Type</th><th align='left' width='70%'>Log Messages</th></tr>";
  $preLog = "";
  if(file_exists($logFile)){
   $fch = fopen($logFile, "r");
   while(is_resource($fch) && !feof($fch)){
        $logline = fgets($fch);
        $logline = preg_replace("/[\n]/", "", $logline);
        if ( (trim($logline) != "") && (substr($logline, 0, 1) == "#") ){
		$logname = substr($logline, 1);
	}
        if ( (trim($logline) != "") && (substr($logline, 0, 1) != "#") ){
		echo "<tr>";
		if ($preLog !=  $logname){
			$preLog = $logname;
			echo "<td width='25%'>" . $logname . "</td>";
		} else {
			echo "<td></td>";
		}
		echo "<td width='5%'>" . substr($logline, 0, 7) . "</td>";
		echo "<td width='70%'>" . substr($logline, 8) . "</td>";
		echo "</tr>";
        }
   }
   fclose($fch);
  }
  echo "</table>";
  return true;
}

if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

$config = include('../scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .  " user=". $config['db_user'] .  " password=". $config['db_pass'] .  " dbname=".$config['db_name'];
$link = pg_connect($connstr)
        or die('DB Connection Error : ' . pg_last_error());

?>
<html>
  <head>
   <title>Volo.VISA</title>
   <meta http-equiv="refresh" content="60">
   <link rel="stylesheet" href="./template/styles.css">
  </head>
  <body style="background:rgba(65,65,65,1)">
  <div id='header'>
<?php
  echo "<p><b>VISA Admin Console v1.4.8 [Node : " . $para['volo_node'] . "] [System Time : " . $para['time_zone'] . " " . $curtime ."]</b></p>";
?>
  </div>
  <div id='cssmenu'>
  <ul>
   <li><a href='./index_admin.php'><span>Job Queue</span></a></li>
   <li><a href='./job_filter.php'><span>Job Filter</span></a></li>
   <!--<li><a href='./job_submit.php'><span>Job Submit</span></a></li>-->
<?php
 if (file_exists("./channels/index.php"))
   echo "<li><a href='./channels/index.php'><span>CH Extract</span></a></li>";
 if (file_exists("./trp/index.php"))
   echo "<li><a href='./trp/index.php'><span>TS Extract</span></a></li>";
?>
   <li><a href='./worker_status.php'><span>Encoder Status</span></a></li>
   <li class='active'><a href='./logs_status.php'><span>System Log</span></a></li>
   <li class='xdcambt'><a href='./xdcam.php'><span>XDCAM</span></a>
          <div class="xdcambt-content">
		<a class="xdcambt-content-a" href="xdcam.php">XDCAM_Profile</a>
                <a class="xdcambt-content-a" href="xdcam_edit.php">Audio Profile Edit</a>
		<a class="xdcambt-content-a" href="nasman.php">NAS Management</a>
          </div>
    </li>
                <li class="imx50bt"><a href='./imx50.php'><span>IMX50</span></a>
                    <div class="imx50bt-content">
                        <a class="imx50bt-content-a" href="imx50.php">IMX50_Profile</a>
                        <a class="imx50bt-content-a" href="imx50_edit.php">Audio Profile Edit</a>
                    </div>
                </li>

   <li class='last'><a href='./visa_status.php'><span>System Control</span></a></li>
  </ul>
  </div>
  <div id='joblist'>
  <table border="0" bgcolor="white"><tr><td>
<?php
  // Consolidate Logs
  $logdate = date("Ymd", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

  // Watch Folder Job Publishing
  file_put_contents($para["logs_path"] . "/VOLOPUBS.txt", "");
  parseLog($para["logs_path"] . "/VISA.SCHD.PollSource.*." . $logdate . ".log", "ERROR", $para["logs_path"] . "/VOLOPUBS.txt");
  parseLog($para["logs_path"] . "/VISA.SCHD.PollSource.*." . $logdate . ".log", "WARNS", $para["logs_path"] . "/VOLOPUBS.txt");

  // Watch Folder Job Post Processing
  file_put_contents($para["logs_path"] . "/VOLOPOST.txt", "");
  parseLog($para["logs_path"] . "/VISA.SCHD.ProcQueue.*." . $logdate . ".log", "ERROR", $para["logs_path"] . "/VOLOPOST.txt");
  parseLog($para["logs_path"] . "/VISA.SCHD.ProcQueue.*." . $logdate . ".log", "WARNS", $para["logs_path"] . "/VOLOPOST.txt");

  // Split Encode Processing
  file_put_contents($para["logs_path"] . "/VOLOSPLIT.txt", "");
  parseLog($para["logs_path"] . "/VISA.SPLIT.*Ctrl.*." . $logdate . ".log", "ERROR", $para["logs_path"] . "/VOLOSPLIT.txt");
  parseLog($para["logs_path"] . "/VISA.SPLIT.*Ctrl*." . $logdate . ".log", "WARNS", $para["logs_path"] . "/VOLOSPLIT.txt");

  // Encode Worker Processing
  file_put_contents($para["logs_path"] . "/VOLOWORK.txt", "");
  parseLog($para["logs_path"] . "/VISA.WORKER.WorkerCtrl.*." . $logdate . ".log", "ERROR", $para["logs_path"] . "/VOLOWORK.txt");
  parseLog($para["logs_path"] . "/VISA.WORKER.Poll*." . $logdate . ".log", "ERROR", $para["logs_path"] . "/VOLOWORK.txt");
  parseLog($para["logs_path"] . "/VISA.WORKER.WorkerCtrl.*." . $logdate . ".log", "WARNS", $para["logs_path"] . "/VOLOWORK.txt");
  parseLog($para["logs_path"] . "/VISA.WORKER.Poll*." . $logdate . ".log", "WARNS", $para["logs_path"] . "/VOLOWORK.txt");

  // Volo Farm Error
  file_put_contents($para["logs_path"] . "/VOLOERROR.txt", "");
  parseLog($para["logs_path"] . "/VISA.*Monitor*.*." . $logdate . ".log", "ERROR", $para["logs_path"] . "/VOLOERROR.txt");
  parseLog($para["logs_path"] . "/VISA.*Housekeep.*." . $logdate . ".log", "ERROR", $para["logs_path"] . "/VOLOERROR.txt");
  file_put_contents($para["logs_path"] . "/VOLOWARNS.txt", "");
  parseLog($para["logs_path"] . "/VISA.*Monitor*.*." . $logdate . ".log", "WARNS", $para["logs_path"] . "/VOLOWARNS.txt");
  parseLog($para["logs_path"] . "/VISA.*Housekeep.*." . $logdate . ".log", "WARNS", $para["logs_path"] . "/VOLOWARNS.txt");

  // Job Publishing Audit
  $jdate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
  $jdate = date('Y-m-d',strtotime("-1 day", strtotime($jdate)));
  $jobquery = pg_exec($link, "SELECT * FROM job_handler WHERE date_trunc('day',job_handler.stime) >= '" . $jdate . "' ORDER BY id DESC LIMIT 100;");
  $logstart = true;
  file_put_contents($para["logs_path"] . "/VOLOJOBS.txt", "");
  while ($jobdtl = pg_fetch_array($jobquery)){
	$logstr = "";
	if ($jobdtl["c_insert"] == 't'){
		$logstr .= "[INFOS][" . substr($jobdtl["stime"], 0, 19) . "][jobaudit] ";
		$logstr .= "VISA.Publisher Job Submit :: JOBID[" . $jobdtl["job_id"] . "] SOURCE[" . $jobdtl["source_file"] . "]" . "\n";
	} else if ($jobdtl["c_cancel"] == 't'){
		$logstr .= "[WARNS][" . substr($jobdtl["stime"], 0, 19) . "][jobaudit] ";
		$logstr .= "VISA.Publisher Job Cancel :: JOBID[" . $jobdtl["job_id"] . "] OUTID[" . $jobdtl["output_id"] . "]" . "\n";
	} else {
		$logstr .= "[WARNS][" . substr($jobdtl["stime"], 0, 19) . "][jobaudit] ";
		$logstr .= "VISA.Publisher Job Retry  :: JOBID[" . $jobdtl["job_id"] . "] OUTID[" . $jobdtl["output_id"] . "]" . "\n";
	}
	if ($logstart){
		$logstart = false;
		file_put_contents($para["logs_path"] . "/VOLOJOBS.txt", "#VISA.SCHD.JobAudit" . "\n", FILE_APPEND);
	}
	file_put_contents($para["logs_path"] . "/VOLOJOBS.txt", $logstr, FILE_APPEND);
  }
  pg_free_result($jobquery);

  // Display Logs
  echo "<p><b>[ Watch Folder Job Publishing ]</b></p>";
  displayLog($para["logs_path"] . "/VOLOPUBS.txt");
  echo "<hr><p><b>[ Encode Job Post Processing ]</b></p>";
  displayLog($para["logs_path"] . "/VOLOPOST.txt");
  echo "<hr><p><b>[ Encode Job Audit ]</b></p>";
  displayLog($para["logs_path"] . "/VOLOJOBS.txt");
  echo "<hr><p><b>[ Split Encode Processing ]</b></p>";
  displayLog($para["logs_path"] . "/VOLOSPLIT.txt");
  echo "<hr><p><b>[ Encode Worker Processing ]</b></p>";
  displayLog($para["logs_path"] . "/VOLOWORK.txt");
  echo "<hr><p><b>[ Volo Farm Monitor Error ]</b></p>";
  displayLog($para["logs_path"] . "/VOLOERROR.txt");
  echo "<hr><p><b>[ Volo Farm Monitor Warning ]</b></p>";
  displayLog($para["logs_path"] . "/VOLOWARNS.txt");

  pg_close($link);
?>
  <p><br /><br /><p>
  </td></tr></table>
  </div>
  <p><p>
  </body>
</html>
