<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curDate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
?>
<html>
  <head>
   <title>Volo.VISA</title>
   <link rel="stylesheet" href="./template/styles.css">
  </head>
  <body style="background:rgba(65,65,65,1)">
  <div id='header'>
 <?php
   $curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
   echo "<p><b>VISA Admin Console v1.4.8 [Node : " . $para['volo_node'] . "] [System Time : " . $para['time_zone'] . " " . $curtime ."]</b></p>";
 ?>
  </div>
  <div id='cssmenu'>
  <ul>
   <li><a href='./index_user.php'><span>Job Queue</span></a></li>
   <li class='active'><a href='./job_filter_user.php'><span>Job Filter</span></a></li>
   <!--<li><a href='./job_submit.php'><span>Job Submit</span></a></li>-->
  </ul>
  </div>
  <div id='joblist'>
  <table border="0" bgcolor="white"><tr><td>
  <table border="0" bgcolor="white">
  <p><b>[ Encode Job Listing Filter ]</b></p>
  <form id="subjob" method="get" action="./index_user.php">
  <tr>
  <td>Encode Job ID :</td><td colspan=3><input type="number" name="jobid" value="0"></td>
  <tr/>
  <tr><td><br /></td></tr>
  <tr>
  <td>Job Submit Date :</td><td colspan=3>From&nbsp;&nbsp;<input type="date" name="sdate" size="12" value="<?php echo $curDate?>">&nbsp;&nbsp;To&nbsp;&nbsp;<input type="date" name="edate" size="12" value="<?php echo $curDate?>"></td>
  <tr/>
  <tr><td><br /></td></tr>
  <tr>
  <td>Source Video Full Name :</td><td colspan="2"><input type="text" name="sname" size="50" value="*"></td>
  <td align='right'>Destination Path :</td><td colspan="2"><input type="text" name="dest" size="50" value="*"></td>
  <tr/>
  <tr><td><br /></td></tr>
  <tr><td>Encode Status :</td>
  <td colspan=3><input type="radio" name="status" value="ALL" checked>ALL
  &nbsp;&nbsp;<input type="radio" name="status" value="WAIT">Waiting
  &nbsp;&nbsp;<input type="radio" name="status" value="QUEUE">Queueing
  &nbsp;&nbsp;<input type="radio" name="status" value="RUN">Encoding
  &nbsp;&nbsp;<input type="radio" name="status" value="DONE">Completed
  &nbsp;&nbsp;<input type="radio" name="status" value="FAIL">Failed
  &nbsp;&nbsp;<input type="radio" name="status" value="CANCEL">Cancelled
  <td></td>
  </tr>
  <tr><td><br /></td></tr>
  <tr><td>Priority :</td>
  <td colspan=3>From&nbsp;&nbsp;<input type="number" name="sprior" value="10">&nbsp;&nbsp;To&nbsp;&nbsp;<input type="number" name="eprior" value="100"></td>
  </tr>
  <tr><td><br /></td></tr>
  <tr><td colspan='7'><input type="submit" value="Submit">&nbsp;&nbsp;<input type="reset" value="Reset"></td></tr>
  </form>
  </table>
  </div>
  <p><p>
  </body>
</html>
