<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');

if (isset($_GET["JID"])){
	$JID = htmlspecialchars($_GET["JID"]);
} else {
	echo "VISA Reponse ::<br />ERROR REQUEST";
	exit(1);
}

$skipfile = $para["volo_thumb_path"] . "/errskip.jid";
$skipjid  = "";
if(file_exists($skipfile)){
	$skipjid = file_get_contents($skipfile);
	$skipjid = preg_replace("/[\n]/", "", $skipjid);
}
if ($skipjid != "") $skipjid .= ",";
$skipjid .= $JID;
file_put_contents($skipfile, $skipjid);

echo '<pre>Encode Job [' . $JID . '] Clear from Main Queue</pre>';
echo "<br />";
exit(0);
?>
