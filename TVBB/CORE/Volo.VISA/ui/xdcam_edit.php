<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
?>
<!DOCTYPE html>
<html>
  <head>
   <title>Volo.VISA</title>
   <link rel="stylesheet" href="./template/styles.css">
<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script-->
<script src="jquery/3.2.1/jquery.min.js"></script>
</head>


<style>
body{
background-color: rgba(65,65,65,1);
font-size:13px;
font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif
}
.container{
border: #999997 solid 2px;
position: relative;
width: 1100px;
min-height: 500px;
top: 10px;
left: 120px;
color:white;
}
.edit_title{
font-size:25px;
left:7px;
top:7px;
position:relative;
}
.search{
height:45px;
background:rgba(153, 153, 151,1);
position:relative;
top:60px;
border-top-left-radius: 5px;
border-top-right-radius: 5px;
border-bottom: solid 1px white;
}
.paging{
height:45px;
background:rgba(153, 153, 151,1);
position:relative;
top:60px;
border-bottom-left-radius: 5px;
border-bottom-right-radius: 5px;
}
.edit_icon{
position:absolute;
left:15px;
top:6px;
}
.profile_search{
position:absolute;
right:12px;
height:25px;
top:7px;
}
#searching
{
height:25px;
width:170px;
border: 1px solid rgba(235,235,234,1);
border-radius: 3px;
font-size:15px;
background-color:rgba(235,235,234,1);
}
.record_zone{
position:relative;
top:60px;
}
.table_header{
border-right:1px solid white;
border-bottom:1px solid white;
height:40px;
background:rgba(153,153,151,1);
color:rgba(65,65,65,1);
}
table{
border-spacing: 0px;
width:100%;
height:100%;
text-align:left;
font-size:15px;
}
#field1,#field6,#field7{
width:20%;
}
.recordField{
height:25px;
border-right:1px solid white;
border-bottom:1px solid white;
height:40px;
}
.recordRow{
font-size:13px;
color:rgba(33,61,102,1);
}
.recordRow:nth-child(odd){
background-color: rgba(232,230,230,1);
}
.recordRow:nth-child(even){
background-color: rgba(199,199,199,1);
}
.recordRow:hover{
background-color: rgba(153,153,151,1);
cursor:pointer;
}
.withoutRecord{
background-color: rgba(199,199,199,1);
height:40px;
font-size:13px;
color:rgba(33,61,102,1);
}
.search_res{
min-height:35px;
width:175px;
background-color:rgba(235,235,234,1);
position:absolute;
right:12px;
top:130px;
border-radius:5px;
z-index:1;
display:none;
font-size:1em;
color:black;
}
.search_res_row:hover{
background:rgba(153, 153, 151,1);
font-size:1em;
color:black;
}


</style>

<body>
  <div id='header'>
 <?php
   $curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
   echo "<p><b>VISA Admin Console v1.4.8 [Node : " . $para['volo_node'] . "] [System Time : " . $para['time_zone'] . " " . $curtime ."]</b></p>";
 ?>
  </div>
  <div id='cssmenu'>
  <ul>
   <li><a href='./index_admin.php'><span>Job Queue</span></a></li>
   <li><a href='./job_filter.php'><span>Job Filter</span></a></li>
   <!--<li><a href='./job_submit.php'><span>Job Submit</span></a></li>-->
<?php
 if (file_exists("./channels/index_admin.php"))
   echo "<li><a href='./channels/:index_admin.php'><span>CH Extract</span></a></li>";
 if (file_exists("./trp/index_admin.php"))
   echo "<li><a href='./trp/index_admin.php'><span>TS Extract</span></a></li>";
?>
   <li><a href='./worker_status.php'><span>Encoder Status</span></a></li>
   <li><a href='./logs_status.php'><span>System Log</span></a></li>
   <li class='active xdcambt'><a href='./xdcam.php'><span>XDCAM</span></a>
          <div class="xdcambt-content">
                <a class="xdcambt-content-a" href="xdcam.php">XDCAM_Profile</a>
                <a class="xdcambt-content-a" href="xdcam_edit.php">Audio Profile Edit</a>
		<a class="xdcambt-content-a" href="nasman.php">NAS Management</a>
          </div>
    </li>
                <li class="imx50bt"><a href='./imx50.php'><span>IMX50</span></a>
                    <div class="imx50bt-content">
                        <a class="imx50bt-content-a" href="imx50.php">IMX50_Profile</a>
                        <a class="imx50bt-content-a" href="imx50_edit.php">Audio Profile Edit</a>
                    </div>
                </li>

   <li class='last'><a href='./visa_status.php'><span>System Control</span></a></li>
  </ul>
  </div>
<?php
function getConnect()
{
	$conn = pg_connect("host=localhost port=5432 dbname=visadb user=visa password=1234");
	return $conn;
}
?>
	<div class="container">
		<form action="profile_load.php" method="GET">
			<div class="edit_title">Edit Available Audio Profiles</div>
			<div class="search">
				<span class="edit_icon"><img src="png/edit.png" height="32" width="32"></span>
				<span class="profile_search"><input id="searching" type="text" autocomplete="off" placeholder="  Search"></span>
			</div>
				<div class="search_res"></div>
			<div class="record_zone">
			<table>
				<thead>
					<tr>
						<th class="table_header" id="field1">&nbsp Name</th>
						<th class="table_header" id="field2">&nbsp CODEC</th>
						<th class="table_header" id="field4">&nbsp Sampling Rate</th>
						<th class="table_header" id="field5">&nbsp Description</th>
						<th class="table_header" id="field6">&nbsp Creation Date</th>
						<th class="table_header" id="field7">&nbsp Last Edit Date</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$conn = getConnect();
						$res = pg_query($conn,"select * from audio_profile_info inner join audio_profile on  audio_profile_info.id = audio_profile.id and audio_profile_info.profile_type = 'XDCAM' order by audio_profile_info.profile_name asc;");
						$count = 1;
								while($row = pg_fetch_object($res))
								{
								$create_date = date_create($row->profile_create_date);
								$modified_date = date_create($row->profile_modified_date);
								echo "<tr class='recordRow' id='recordId$count'>
								  	<td class='recordField' id='fieldId_1-$count'>$row->profile_name</td>
								  	<td class='recordField' id='fieldId_2-$count'>&nbsp $row->audio_codec</td>
								  	<td class='recordField' id='fieldId_3-$count'>&nbsp $row->sampling_rate</td>
								  	<td class='recordField' id='fieldId_4-$count'>&nbsp $row->description</td>
								  	<td class='recordField' id='fieldId_5-$count'>&nbsp ".date_format($create_date, 'Y-m-d H:i:s')."</td>
								  	<td class='recordField' id='fieldId_6-$count'>&nbsp".date_format($modified_date, 'Y-m-d H:i:s')."</td>
							     	      </tr>";
								$count++;
								}
							if ($count == 1)
							echo "<tr class='withoutRecord'><td colspan='7'>&nbsp No audio profiles were found!</td></tr>";
						pg_close($conn);
					?>
					
				</tbody>
			</table>
			</div>
			<div class="paging">
                        </div>
			<input type="text" name="profileName" class="profileName">
			<input type="submit" class="formSub">
		</form>
	</div>
</body>
<script>
 $(document).on("click",".recordRow",function(event) {
var id = event.target.id;
var rowNumber =  id.split('-')[1];
var profileName = $("#fieldId_1-"+String(rowNumber)).text();
$(".profileName").val(profileName);
$(".formSub").trigger("click");
    });
</script>
<script>
$(document).on("input","#searching",function(){
        $(".search_res_row").remove();
        var ab = $("#searching").val();
        $(".search_res").css("display","block");
        var arr = {ar1:ab};
        $.ajax({
            type: "POST",
            url: "searching.php",
            data: arr,
            dataType: "json",
            success: function(data)
                {
                        showContent(data);               
                },
            error:function()
                {
			;
                }
        });

});
</script>
<script>
        function showContent(data)
        {
                var html = "";  
                if (data.length != 0)           
                for (var i=0;i<data.length;i++)
                {
                        html += "<div class='search_res_row'>" + data[i] + "</div>";
                }
                else
                        html += "<div class='search_res_row'>No records found</div>";
                $(".search_res").append(html);
                
        }
</script>
<script>
	$(document).on("click",".search_res_row",function(){
	var profileName = $(this).text();
	$("#searching").val($(this).text());
        $(".search_res").css("display","none");
	$(".recordRow").remove();
	var arr = {ar2:profileName};
	$.ajax({
            type: "POST",
            url: "searching_two.php",
            data: arr,
            dataType: "json",
            success: function(data)
                {
			if (data[3] == null)
			data[3] = "";
			var create_date =  data[4].split('.')[0];
			var modified_date=  data[5].split('.')[0];
			var html = "";
			html = "<tr class='recordRow' id='recordId" + data[6] + "'>";
                        html += "<td class='recordField' id='fieldId_1-" + data[6] + "'>" + data[0] + "</td>";
                        html += "<td class='recordField' id='fieldId_2-" + data[6] + "'>&nbsp " + data[1] + "</td>";
                        html += "<td class='recordField' id='fieldId_3-" + data[6] + "'>&nbsp " + data[2] + "</td>";
                        html += "<td class='recordField' id='fieldId_4-" + data[6] + "'>&nbsp " + data[3] + "</td>";
                        html += "<td class='recordField' id='fieldId_5-" + data[6] + "'>&nbsp " + create_date + "</td>";
                        html += "<td class='recordField' id='fieldId_6-" + data[6] + "'>&nbsp " + modified_date + "</td>";
                        html += "</tr>";
			$("tbody").append(html);
                },
            error:function()
                {
                        alert("Page Error?");
                }
        });


});
</script>
<script>
	$(document).ready(function(){
	var ab = parseInt($(".container").css("height"));	
	ab += 100;
	$(".container").css("height",ab);
});
</script>
</html>
