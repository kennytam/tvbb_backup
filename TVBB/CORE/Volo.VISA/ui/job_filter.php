<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curDate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
?>
<html>
  <head>
   <title>Volo.VISA</title>
   <link rel="stylesheet" href="./template/styles.css">
  </head>
  <body style="background:rgba(65,65,65,1)">
  <div id='header'>
 <?php
   $curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
   echo "<p><b>VISA Admin Console v1.4.8 [Node : " . $para['volo_node'] . "] [System Time : " . $para['time_zone'] . " " . $curtime ."]</b></p>";
 ?>
  </div>
  <div id='cssmenu'>
  <ul>
   <li><a href='./index_admin.php'><span>Job Queue</span></a></li>
   <li class='active'><a href='./job_filter.php'><span>Job Filter</span></a></li>
   <!--<li><a href='./job_submit.php'><span>Job Submit</span></a></li>-->
<?php
 if (file_exists("./channels/index.php"))
   echo "<li><a href='./channels/index.php'><span>CH Extract</span></a></li>";
 if (file_exists("./trp/index.php"))
   echo "<li><a href='./trp/index.php'><span>TS Extract</span></a></li>";
?>
   <li><a href='./worker_status.php'><span>Encoder Status</span></a></li>
   <li><a href='./logs_status.php'><span>System Log</span></a></li>
   <li class='xdcambt'><a href='./xdcam.php'><span>XDCAM</span></a>
          <div class="xdcambt-content">
		<a class="xdcambt-content-a" href="xdcam.php">XDCAM_Profile</a>
                <a class="xdcambt-content-a" href="xdcam_edit.php">Audio Profile Edit</a>
		<a class="xdcambt-content-a" href="nasman.php">NAS Management</a>
          </div>
    </li>
                <li class="imx50bt"><a href='./imx50.php'><span>IMX50</span></a>
                    <div class="imx50bt-content">
                        <a class="imx50bt-content-a" href="imx50.php">IMX50_Profile</a>
                        <a class="imx50bt-content-a" href="imx50_edit.php">Audio Profile Edit</a>
                    </div>
                </li>

   <li class='last'><a href='./visa_status.php'><span>System Control</span></a></li>
  </ul>
  </div>
  <div id='joblist'>
  <table border="0" bgcolor="white"><tr><td>
  <table border="0" bgcolor="white">
  <p><b>[ Encode Job Listing Filter ]</b></p>
  <form id="subjob" method="get" action="./index_admin.php">
  <tr>
  <td>Encode Job ID :</td><td colspan=3><input type="number" name="jobid" value="0"></td>
  <tr/>
  <tr><td><br /></td></tr>
  <tr>
  <td>Job Submit Date :</td><td colspan=3>From&nbsp;&nbsp;<input type="date" name="sdate" size="12" value="<?php echo $curDate?>">&nbsp;&nbsp;To&nbsp;&nbsp;<input type="date" name="edate" size="12" value="<?php echo $curDate?>"></td>
  <tr/>
  <tr><td><br /></td></tr>
  <tr>
  <td>Source Video Full Name :</td><td colspan="2"><input type="text" name="sname" size="50" value="*"></td>
  <td align='right'>Destination Path :</td><td colspan="2"><input type="text" name="dest" size="50" value="*"></td>
  <tr/>
  <tr><td><br /></td></tr>
  <tr><td>Encode Status :</td>
  <td colspan=3><input type="radio" name="status" value="ALL" checked>ALL
  &nbsp;&nbsp;<input type="radio" name="status" value="WAIT">Waiting
  &nbsp;&nbsp;<input type="radio" name="status" value="QUEUE">Queueing
  &nbsp;&nbsp;<input type="radio" name="status" value="RUN">Encoding
  &nbsp;&nbsp;<input type="radio" name="status" value="DONE">Completed
  &nbsp;&nbsp;<input type="radio" name="status" value="FAIL">Failed
  &nbsp;&nbsp;<input type="radio" name="status" value="CANCEL">Cancelled
  <td></td>
  </tr>
  <tr><td><br /></td></tr>
  <tr><td>Priority :</td>
  <td colspan=3>From&nbsp;&nbsp;<input type="number" name="sprior" value="10">&nbsp;&nbsp;To&nbsp;&nbsp;<input type="number" name="eprior" value="100"></td>
  </tr>
  <tr><td><br /></td></tr>
  <tr><td colspan='7'><input type="submit" value="Submit">&nbsp;&nbsp;<input type="reset" value="Reset"></td></tr>
  </form>
  </table>
  </div>
  <p><p>
  </body>
</html>
