<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');

	while(connection_status() == CONNECTION_NORMAL) {
		$cpuUsageData = file_get_contents("/opt/Volo.VISA/VISA_Control/VISA.CPU.CTS-HKG-VOLO-STAG.log");
		$results = explode(" ", $cpuUsageData);
		if($results[count($results)-1] == "")
                        unset($results[count($results)-1]);

		if(count($results) > 0) {
			$result = array('success'=>true, 'data'=>$results);
			print "Event: update" . PHP_EOL;
			print "data: " . json_encode($result) . PHP_EOL . PHP_EOL;
			@ob_end_flush();
			flush();
		}
		
                sleep(1);	
	}

	pclose($handle);
?>
