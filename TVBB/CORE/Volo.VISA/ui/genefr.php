<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
## Encode Failure Report Generation                                                                    ##
## USAGE: geneer.php [date in yyyy-mm-dd (optional)                                                    ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());

if (isset($argv[1]))
	$jdate = htmlspecialchars($argv[1]);
else
	$jdate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
 
$link = pg_Connect("host=localhost dbname=visadb user=visa password=1234");
$result = pg_exec($link, "SELECT * FROM JOB WHERE (date_trunc('day',job.ctime) >= '" . $jdate . "' AND date_trunc('day',job.ctime) <= '" . $jdate . "') ORDER BY ID DESC;");
$numrows = pg_numrows($result);
echo 'Volo VISA Admin Console Reporter v1.4.8' . "\n";
echo "\n";
echo '[ VISA Volo Encoder Farm  - Encode Job Failure Report ]' . "       ";
echo 'Encode Job Date : [' . $para['time_zone'] . ' ' . $jdate . ']' . "\n";;

// Loop on rows in the result set.
for($ri = 0; $ri < $numrows; $ri++) {
     $row = pg_fetch_array($result, $ri);

     $job = pg_exec($link, "SELECT * FROM OUTPUT WHERE JOB_ID = " . $row["id"] . " AND STAGE < 0 ORDER BY ID;");
     $numjob = pg_numrows($job);
     if ($numjob > 0){
	echo "\n";
	echo 'JOB ID : [' . $row["id"] . ']  SOURCE VIDEO : [' . $row["source"] . ']  LENGTH : [' . round($row["source_duration"]/60, 2,PHP_ROUND_HALF_UP) . 'mins]  DESTINATION : [' . $row["destination"] . ']  JOB SUBMIT TIME : [' . substr($row["ctime"],0, 19) . ']' . "\n";
	echo '[ENCODE ID] [OUTPUT FILE NAME]                      [VOLO PROFILE]         [ENCODE START TIME - END TIME]          [VOLO NODE ENCODE]    [MESSAGE]';
	echo "\n";
     }
     for($rj = 0; $rj < $numjob; $rj++) {
	$proc = false;
        $jrow = pg_fetch_array($job, $rj);
        echo '   ' . str_pad($jrow["id"], 11);
	echo str_pad($jrow["filename"], 40);
	echo str_pad($jrow["profile"], 20);
        echo substr($jrow["stime"],0, 19) . ' - ';
        echo substr($jrow["ltime"],0, 19);
        $node = pg_exec($link, "SELECT uuid FROM WORKER WHERE ID = " . $jrow["worker_id"] . ";");
        $nrow = pg_fetch_array($node);
        echo " " . $nrow["uuid"] . '      ';
        echo $jrow["message"];
     }
   }
   pg_close($link);
   if ($numrows < 1) echo 'NO ENCODE JOB....' . "\n";

?>
