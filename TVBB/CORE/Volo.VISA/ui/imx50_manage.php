<?php

$sourcePath = '/home/Video.Source';
$outputPath = '/home/Video.Output';
$profilePath = '/home/Video.Profile/';
$watchFolder = '/VOLO_JOB_SUBMIT';
$watchFolder_switch = '/VOLO_JOB_SUBMIT_AUTO_SWITCH';
$basicPath = '/home/Video.Profile/';

$httpRequest = readRequest("ar9");

if ($httpRequest == 'save as') {
    folderCreate($sourcePath, $profilePath, $outputPath, $watchFolder, $watchFolder_switch);
    echo json_encode(array("success" => "Records insert successfully!!"));
}

if ($httpRequest == 'update') {
    update($sourcePath, $profilePath, $outputPath, $watchFolder, $watchFolder_switch);
    echo json_encode(array("success" => "Records updated successfully!!"));
}

if ($httpRequest == 'erases') {
    erases($sourcePath, $profilePath, $watchFolder, $watchFolder_switch);
    echo json_encode(array("success" => "Profile is deleted successfully!!"));
}

function connectTodb() {
    $conn = pg_connect("host=localhost port=5432 dbname=visadb user=visa password=1234");
    if ($conn)
        return $conn;
    else {
        errMsg("Failed to connect with database");
        exit(-100);
    }
}

function errMsg($str) {
    $msg = $str;
    echo json_encode(array("err" => $msg));
}

function readRequest($str) {
    $requestInfo = $_POST["$str"];
    return $requestInfo;
}

function pathCheck($sourcePath, $outputPath, $profilePath) {
    $status = exec(" if [ -d $sourcePath ]; then echo 1; else echo -90; fi ");
    if ($status != 1) {
        errMsg("$sourcePath is not existed err:$status");
        exit(-90);
    }

    $status = exec(" if [ -d $outputPath ]; then echo 1; else echo -91; fi ");
    if ($status != 1) {
        errMsg("$outputPath is not existed err:$status");
        exit(-91);
    }

    $status = exec(" if [ -d $profilePath ]; then echo 1; else echo -92; fi ");
    if ($status != 1) {
        errMsg("$profilePath is not existed err:$status");
        exit(-92);
    }

    $fullProfilePath = readRequest("ar1");
    $srcNasPathArr = explode('/', $fullProfilePath);
    $srcNasPath = "/" . $srcNasPathArr[1];

    $status = exec(" if [ -d $sourcePath$srcNasPath ]; then echo 1; else echo -95; fi ");
    if ($status != 1) {
        errMsg("$sourcePath$srcNasPath is not existed err:$status");
        exit(-95);
    }

    $fullProfilePath = readRequest("ar10");
    $destNasPathArr = explode('/', $fullProfilePath);
    $destNasPath = "/" . $destNasPathArr[1];

    $status = exec(" if [ -d $outputPath$destNasPath ]; then echo 1; else echo -96; fi");
    if ($status != 1) {
        errMsg("$outputPath$destNasPath is not existed err:$status");
        exit(-96);
    }

    //kenny start,20180312
    $fullProfilePath = readRequest("ar13");
    if ($fullProfilePath != '') {

        $srcNasPathArr = explode('/', $fullProfilePath);
        $srcNasPath = "/" . $srcNasPathArr[1];

        $status = exec(" if [ -d $sourcePath$srcNasPath ]; then echo 1; else echo -195; fi ");
        if ($status != 1) {
            errMsg("$sourcePath$srcNasPath is not existed err:$status");
            exit(-195);
        }
    }

    $fullProfilePath = readRequest("ar14");
    if ($fullProfilePath != '') {

        $srcNasPathArr = explode('/', $fullProfilePath);
        $srcNasPath = "/" . $srcNasPathArr[1];

        $status = exec(" if [ -d $sourcePath$srcNasPath ]; then echo 1; else echo -196; fi ");
        if ($status != 1) {
            errMsg("$sourcePath$srcNasPath is not existed err:$status");
            exit(-196);
        }
    }

    $fullProfilePath = readRequest("ar15");
    if ($fullProfilePath != '') {

        $srcNasPathArr = explode('/', $fullProfilePath);
        $srcNasPath = "/" . $srcNasPathArr[1];

        $status = exec(" if [ -d $sourcePath$srcNasPath ]; then echo 1; else echo -197; fi ");
        if ($status != 1) {
            errMsg("$sourcePath$srcNasPath is not existed err:$status");
            exit(-197);
        }
    }
    //kenny end,20180312
}

function profileCheck($sourcePath, $outputPath, $profilePath) {
    $fullProfileName = readRequest("ar1");
    $profileNameArr = pathinfo($fullProfileName);
    $lastName = $profileNameArr['basename'];
    /* $status = exec(" if [ -d $sourcePath$fullProfileName ]; then echo -93; else echo 1; fi ");
      if ($status == -93)
      {
      errMsg("The source directory: $sourcePath$fullProfileName is existed err:$status");
      exit(-93);
      } */

    $status = exec(" if [ -d $profilePath$lastName ]; then echo -94; else echo 1; fi ");
    if ($status == -94) {
        errMsg("The profile: $profilePath$lastName is existed err:$status");
        exit(-94);
    }
}

function insertDB() {
    $conn = connectTodb();
    $id = pg_query($conn, "select nextval('profile_seq')");
    $fullProfileName = pathinfo(readRequest("ar1"));
    $profile_name = $fullProfileName['basename'];
    $currentId = pg_fetch_row($id);
    $insertRes = insertProfile($conn, $currentId, $profile_name);
    if ($insertRes != 1) {
        errMsg("Insert data to audio profile is failed with error code:$insertRes");
        exit(-101);
    }

    $insertRes = insertInfo($conn, $currentId, $profile_name);
    if ($insertRes != 1) {
        errMsg("Insert data to audio profile info is failed err:.'$insertRes'");
        exit(-102);
    }

    $insertRes = insertMatrix($conn, $currentId, $profile_name);
    if ($insertRes != 1) {
        errMsg("Insert data to audio profile matrix at row $i is failed err:.'$insertRes'");
        exit(-103);
    }

    pg_close($conn);
}

function insertProfile($conn, $insertId, $profile_name) {
    $insertArr = array("id" => "$insertId[0]", "profile_name" => $profile_name);
    $insertRes = pg_insert($conn, 'audio_profile', $insertArr);
    return $insertRes;
}

function insertInfo($conn, $insertId, $profile_name) {
    global $basicPath;
    $fullProfileName = readRequest("ar1");
    $audioCodec = readRequest("ar2");
    $samplingRate = readRequest("ar3");
    $autoAdjustment = readRequest("ar4");
    $dbValue = readRequest("ar5");
    $profileType = readRequest("ar12");
    if ($dbValue == "zero")
        $dbValue = 0;
    $audioRemapping = readRequest("ar8");
    $destPath = readRequest("ar10");
    //kenny start, 20180312
    $source_done_path = readRequest("ar13");
    $source_error_path = readRequest("ar14");
    $switch_watch_folder = readRequest("ar15");
    //kenny end, 20180312
    $insertArr = array(
        "id" => "$insertId[0]",
        "profile_name" => "$profile_name",
        "audio_codec" => "$audioCodec",
        "sampling_rate" => "$samplingRate",
        "db_value" => "$dbValue",
        "audio_remapping_only" => "$audioRemapping",
        "auto_level_adjustment" => "$autoAdjustment",
        "basic_path" => "$basicPath",
        "dest_path" => "$destPath",
        "full_path" => "$fullProfileName",
        //kenny start, 20180312
//				"profile_type"=>"$profileType"
        "profile_type" => "$profileType",
        "job_done_source_path" => "$source_done_path",
        "job_done_wrong_path" => "$source_error_path",
        "switch_watch_folder" => "$switch_watch_folder"
            //kenny end, 20180312
    );
    $insertRes = pg_insert($conn, 'audio_profile_info', $insertArr);
    return $insertRes;
}

function insertMatrix($conn, $insertId, $profile_name) {
    $audioTrack = readRequest("ar7");
    $audioType = readRequest("ar6");
    $audiodBvalue = readRequest("ar11");
    $totalTracks = sizeof($audioTrack);
    $outputTrack = 0;
    for ($i = 0; $i < $totalTracks; $i++) {
        $insertArr = array(
            "id" => "$insertId[0]",
            "profile_name" => "$profile_name",
            "out_track" => $outputTrack,
            "in_track1" => $audioTrack[$i][0],
            "in_track2" => $audioTrack[$i][1],
            "in_track3" => $audioTrack[$i][2],
            "in_track4" => $audioTrack[$i][3],
            "in_track5" => $audioTrack[$i][4],
            "in_track6" => $audioTrack[$i][5],
            "in_track7" => $audioTrack[$i][6],
            "in_track8" => $audioTrack[$i][7],
            "in_track9" => $audioTrack[$i][8],
            "in_track10" => $audioTrack[$i][9],
            "in_track11" => $audioTrack[$i][10],
            "in_track12" => $audioTrack[$i][11],
            "in_track13" => $audioTrack[$i][12],
            "in_track14" => $audioTrack[$i][13],
            "in_track15" => $audioTrack[$i][14],
            "in_track16" => $audioTrack[$i][15],
            "audio_type" => $audioType[0],
            "out_track_dbvalue" => $audiodBvalue[$i]
        );
        $insertRes = pg_insert($conn, 'audio_profile_matrix', $insertArr);
        if ($insertRes != 1) {
            errMsg("Insert data to audio profile matrix at row $i is failed err:.'$insertRes'");
            exit(-103);
        }
        $outputTrack = $outputTrack + 1;
    }

    return $insertRes;
}

function folderCreate($sourcePath, $profilePath, $outputPath, $watchFolder, $watchFolder_switch) {
    pathCheck($sourcePath, $outputPath, $profilePath);
    profileCheck($sourcePath, $outputPath, $profilePath);
    insertDB();
    $destName = readRequest("ar10");
    $fullProfileName = readRequest("ar1");
    //kenny start, 20180312
    $source_done_path = readRequest("ar13");
    $source_error_path = readRequest("ar14");
    $switch_watch_folder = readRequest("ar15");
    //kenny end, 20180312
    $profileNameArr = pathinfo($fullProfileName);
    $lastName = $profileNameArr['basename'];
    $encodeFlag = readRequest("ar8");
    $encodeName = "/NOENCODE";
    $IMX50 = "/IMX50";
    umask(0);
    mkdir("$sourcePath$fullProfileName$watchFolder", 0777, true);
    mkdir("$profilePath$lastName", 0777, true);
    mkdir("$outputPath$destName", 0777, true);

    //kenny start, 20180312
    if ($source_done_path != '') {
        mkdir("$sourcePath/$source_done_path", 0777, true);
        exec(" echo $sourcePath/$source_done_path > $profilePath$lastName/SRCDONEPATH");
    }
    if ($source_error_path != '') {
        mkdir("$sourcePath/$source_error_path", 0777, true);
        exec(" echo $sourcePath/$source_error_path > $profilePath$lastName/SRCERRPATH");
    }
    if ($switch_watch_folder != '') {
        mkdir("$sourcePath$fullProfileName$watchFolder_switch", 0777, true);
        exec(" echo $sourcePath/$switch_watch_folder > $profilePath$lastName/SRCSWITCHPATH");
    }else{
        rmdir("$sourcePath$fullProfileName$watchFolder_switch");
    }
    //kenny end, 20180312

    exec(" echo $outputPath$destName > $profilePath$lastName/DESTINATION");
    touch("$profilePath$lastName$IMX50");
    if ($encodeFlag == "true")
        touch("$profilePath$lastName$encodeName");
    matrixCreate($profilePath);
}

function matrixCreate($profilePath) {
    $fullProfileName = readRequest("ar1");
    $profileNameArr = pathinfo($fullProfileName);
    $lastName = $profileNameArr['basename'];
    $aCodec = readRequest("ar2");
    $sRate = readRequest("ar3");
    $aAdjus = readRequest("ar4");
    $iLine = readRequest("ar5");
    $audioType = readRequest("ar6");
    $audioTrack = readRequest("ar7");
    $audiodBvalue = readRequest("ar11");
    $numOfOutTracks = sizeof($audioTrack);
    $xmlRules = fopen("$profilePath$lastName/MAP.INFO", 'w');
    fwrite($xmlRules, "<audioMapping name=\"ExtremeMixdown\">\r\n");
    fwrite($xmlRules, "\t<buses channels=\"$numOfOutTracks\">\r\n");
    fwrite($xmlRules, "\t\t<channelMap>\r\n");
    /* if ($aAdjus == "true")
      {
      //if ($iLine == "zero")
      fwrite($xmlRules,"\t\t<channelMap>\r\n");
      //else if ($iLine > 0)
      fwrite($xmlRules,"\t\t<channelMap adjustment=\"ebur128\" volume=\"+$iLine".''."dB\">\r\n");
      //else if ($iLine < 0)
      fwrite($xmlRules,"\t\t<channelMap adjustment=\"ebur128\" volume=\"$iLine".''."dB\">\r\n");
      }
      else if ($aAdjus == "false")
      {
      if ($iLine == "zero")
      fwrite($xmlRules,"\t\t<channelMap>\r\n");
      else if ($iLine > 0)
      fwrite($xmlRules,"\t\t<channelMap volume=\"+$iLine".''."dB\">\r\n");
      else if ($iLine < 0)
      fwrite($xmlRules,"\t\t<channelMap volume=\"$iLine".''."dB\">\r\n");
      } */
    //for ($k=0;$k<$numOfOutTracks;$k++)
    //{
    $index = 1;
    $cnt = 0;
    if ($audiodBvalue[0] == "0")
        fwrite($xmlRules, "\t\t\t<ch track=\"$index\" codec=\"$aCodec\" scope=\"$numOfOutTracks\" samplingRate=\"$sRate\">\r\n");
    else if ($audiodBvalue[0] == "A")
        fwrite($xmlRules, "\t\t\t<ch track=\"$index\" codec=\"$aCodec\" scope=\"$numOfOutTracks\" samplingRate=\"$sRate\" adjustment=\"ebur128\">\r\n");
    //if ($audiodBvalue[0] != "0" or $audiodBvalue[0] != "A")
    else
        fwrite($xmlRules, "\t\t\t<ch track=\"$index\" codec=\"$aCodec\" scope=\"$numOfOutTracks\" samplingRate=\"$sRate\" volume=\"$audiodBvalue[0]dB\">\r\n");
    fwrite($xmlRules, "\t\t\t\t<mapRule>\r\n");
    for ($k = 0; $k < $numOfOutTracks; $k++) {
        //fwrite($xmlRules,"\t\t\t\t<mapRule>\r\n");
        for ($r = 0; $r < 16; $r++) {
            $trackNum = $r + 1;
            if ($audioTrack[$k][$r] == "true") {
                fwrite($xmlRules, "\t\t\t\t\t<in track=\"1\" selector=\"$trackNum\"/>\r\n");
                $cnt++;
            }
        }
        if ($cnt == 0)
            fwrite($xmlRules, "\t\t\t\t\t<in track=\"silent\"/>\r\n");
        //fwrite($xmlRules,"\t\t\t\t</mapRule>\r\n");
        //fwrite($xmlRules,"\t\t\t</ch>\r\n");
    }
    fwrite($xmlRules, "\t\t\t\t</mapRule>\r\n");
    fwrite($xmlRules, "\t\t\t</ch>\r\n");

    fwrite($xmlRules, "\t\t</channelMap>\r\n");
    fwrite($xmlRules, "\t</buses>\r\n");
    fwrite($xmlRules, "</audioMapping>\r\n");
    fclose($xmlRules);
}

function erases($sourcePath, $profilePath, $watchFolder, $watchFolder_switch) {
    $fullProfileName = readRequest("ar1");
    $profileNameArr = pathinfo($fullProfileName);
    $lastName = $profileNameArr['basename'];
    $conn = connectTodb();
    $PgRes = pg_query($conn, "select * from audio_profile where profile_name = '$lastName';");
    $resAP = pg_query($conn, "delete from audio_profile where profile_name = '$lastName';");
    $resAPI = pg_query($conn, "delete from audio_profile_info where profile_name = '$lastName';");
    $resAPM = pg_query($conn, "delete from audio_profile_matrix where profile_name = '$lastName';");

    $sourcePathStatus = exec(" if [ -d $sourcePath$fullProfileName$watchFolder ]; then echo 1; else echo -110; fi ");
    $sourceSwitchPathStatus = exec(" if [ -d $sourcePath$fullProfileName$watchFolder_switch ]; then echo 1; else echo -1100; fi ");
    $profilePathStatus = exec(" if [ -d $profilePath$lastName ]; then echo 1; else echo -111; fi ");
    if ($sourcePathStatus) {
        exec("rm -rf $sourcePath$fullProfileName$watchFolder");
    } else {
        errMsg("The path: $sourcePath$fullProfileName$watchFolder is not existed err:-110");
        exit(-110);
    }
    if ($sourceSwitchPathStatus) {
        exec("rm -rf $sourcePath$fullProfileName$watchFolder_switch");
    } else {
        errMsg("The path: $sourcePath$fullProfileName$watchFolder_switch is not existed err:-1100");
        exit(-1100);
    }
    if ($profilePathStatus)
        exec("rm -rf $profilePath$lastName");
    else {
        errMsg("The path: $profilePathStatus is not existed err:-111");
        exit(-111);
    }

    pg_close($conn);
}

function erasesUpdate($sourcePath, $profilePath, $watchFolder, $watchFolder_switch) {
    $fullProfileName = readRequest("ar1");
    $profileNameArr = pathinfo($fullProfileName);
    $lastName = $profileNameArr['basename'];
    $conn = connectTodb();
    $PgRes = pg_query($conn, "select * from audio_profile where profile_name = '$lastName';");
    $resAP = pg_query($conn, "delete from audio_profile where profile_name = '$lastName';");
    $resAPI = pg_query($conn, "delete from audio_profile_info where profile_name = '$lastName';");
    $resAPM = pg_query($conn, "delete from audio_profile_matrix where profile_name = '$lastName';");

    //$sourcePathStatus = exec(" if [ -d $sourcePath$fullProfileName ]; then echo 1; else echo -110; fi ");
    $profilePathStatus = exec(" if [ -d $profilePath$lastName ]; then echo 1; else echo -111; fi ");
    /* if ($sourcePathStatus)
      {
      exec("rm -rf $sourcePath$fullProfileName");
      }
      else
      {
      errMsg("The path: $sourcePath$fullProfileName is not existed err:-110");
      exit(-110);
      } */
    if ($profilePathStatus)
        exec("rm -rf $profilePath$lastName");
    else {
        errMsg("The path: $profilePathStatus is not existed err:-111");
        exit(-111);
    }

    pg_close($conn);
}

function update($sourcePath, $profilePath, $outputPath, $watchFolder, $watchFolder_switch) {
    $conn = connectTodb();
    $fullProfileName = readRequest("ar1");
    $profileNameArr = pathinfo($fullProfileName);
    $lastName = $profileNameArr['basename'];
    $id = pg_query($conn, "select * from audio_profile where profile_name = '$lastName';");
    $currentId = pg_fetch_object($id);
    $oldId = $currentId->id;
    $oldCreateTime = $currentId->profile_create_date;
    erasesUpdate($sourcePath, $profilePath, $watchFolder, $watchFolder_switch);
    folderCreate($sourcePath, $profilePath, $outputPath, $watchFolder, $watchFolder_switch);
    pg_close($conn);
}

?>

