<?php

//define("VOLOBIN", "/opt/volo/bin/volo-1.4.1");
define('VOLOBIN', '/home/kenny/NewWatchmen/ffmpeg-3.3/volo-1.4.2/Stage/build_ffmpeg/ffmpeg');
define('FFSILENTSRC', ' -f lavfi -i anullsrc=r=48000:channel_layout=mono ');
define('EBUR', 'loudnorm=I=-23:LRA=1');

//ffmpeg-3.3-64bit-static/ffmpeg -i a1565-8ch_xdcam.mxf 
//-map 0:v -vcodec mpeg2video -s 1920x1080 -pix_fmt yuv422p 
//-minrate 50M -maxrate 50M -flags +ildct+ilme -top 1 -b:v 50M 
//-map 0:a -af loudnorm=I=-23:LRA=1 -acodec pcm_s24le -ar 48000 -ac 1 -y out.mxf

class audioDetails {

    public $sampleRate;
    public $codec;
    public $channels;
    public $bitrate;
    public $selector;
    public $target;
    public $streamId;
    public $name;
    public $src = array();
    public $srcNum;
    public $audioAction;

}

$audioIn = array();
$audioOut = array();
$FFCMD = "";

function get_codec_name($name) {
    $codec = "";
    switch ($name) {
        case 'pcm24':
            $codec = "pcm_s24le";
            break;
        case 'pcm16':
            $codec = "pcm_s16le";
            break;
        case 'pcm32':
            $codec = "pcm_s32le";
            break;
        case 'aac':
            $codec = "libfdk_aac";
            break;
        default :
            $codec = "";
            break;
    }
    return $codec;
}

function get_channel_num($name) {
    $channelsNum = 0;
    switch ($name) {
        case 'mono':
            $channelsNum = 1;
            break;
        case 'stereo':
            $channelsNum = 2;
            break;
        default :
            $channelsNum = 0;
            break;
    }
    return $channelsNum;
}

function get_sample_rate($name) {
    $sampleRate = str_replace("khz", "000", $name);
    return is_numeric($sampleRate) ? intval($sampleRate) : 0;
}

function get_bitrate($name) {
    $sampleRate = str_replace("kbps", "000", $name);
    return is_numeric($sampleRate) ? $sampleRate : 0;
}

function read_xml($xml_file) {
    $xml = simplexml_load_file($xml_file) or die("Read XML ERROR: Cannot create object");
//    return strtolower($xml);
    return $xml;
}

function get_ini_val($ch) {
    $details = new audioDetails();
    $details->codec = get_codec_name(strtolower($ch['codec']));
    $details->channels = get_channel_num($ch['scope']);
    $details->sampleRate = get_sample_rate(strtolower($ch['samplingRate']));
    $details->bitrate = get_bitrate(strtolower($ch['bitRate']));
    $details->name = intval($ch['name']);
    $details->streamId = $details->name - 1;
    $details->srcNum = 0; //assume only one input src now
    return $details;
}

function empty_audio_src(){
    $details = new audioDetails();
    $details->srcNum = 1;
    $details->streamId = 0;
    return $details;
}

function parse_xml_data($xml) {

    global $audioOut;
    global $audioIn;

    foreach ($xml->children() as $child) {

        if (strcmp($child->getName(), "source") == 0 && (intval($child['channels']) > 0 )) {
            foreach ($child->children()->children() as $ch) {
                $audioIn[] = get_ini_val($ch);
            }
        }

        if (strcmp($child->getName(), "buses") == 0 && strcmp(strtolower($child->children()->getName()), "channelmap") == 0) {
            //if audio source information empty
//            if (count($audioIn) < intval($child['channels'])) {
//                fwrite(STDERR, "[WARN] output source information not match with input source, use null source\n");
//                $i = count($audioIn);
//                for ($i; $i < 9; $i++) {
//                    $details = new audioDetails();
//                    $details->srcNum = 0;
//                    $details->name = $i;
//                    $details->streamId = $i - 1;
//                    $audioIn[] = $details;
//                }
//            }

            foreach ($child->children()->children() as $ch) {
                $details = get_ini_val($ch);

                if (strcmp(strtolower($ch->children()->getName()), "maprule") != 0) {
                    fwrite(STDERR, "[WARN] no map rule,use silent source\n");
                    if (count($details->src) == 0)
                        $details->src[] = (array)(empty_audio_src()); //0 is silent
                    $audioOut[] = $details;
                    continue;
                }

                $mapRule = $ch->children();

                foreach ($mapRule->children() as $in) {
                    $name = (string) $in['name'];
                    if (strcmp(strtolower($name), "silent") == 0) {
                        $details->src[] = (array)(empty_audio_src());
                    } else if (is_numeric($name)) {
                        foreach ($audioIn as $audioIn_obj) {
                            if ($audioIn_obj->name == intval($name)) {
                                $details->src[] = (array) $audioIn_obj;
                            }
                        }
                    }
                }

//                $details->audioAction = strcmp(strtolower($mapRule[0]['action']), "") == 0 ? "redirect" : strtolower($mapRule[0]['action']); //dafault redirect

                $num_src = count($details->src);
                if ($num_src == 0)
                    $details->src[] = (array)(empty_audio_src()); //0 is silent
                
                if ($num_src == 1)
                    $details->audioAction = "redirect";
                else if ($num_src > 1)
                    $details->audioAction = "mix";
                $audioOut[] = $details;
            }
        }
    }
}

function parse_audio_details_to_output($out) {
    $outStr = "";

    if (!is_null($out['codec']))
        $outStr .= ' -c:a:' . $out['streamId'] . ' ' . $out['codec'];

    if ($out['sampleRate'] != 0)
        $outStr .= ' -ar:a:' . $out['streamId'] . ' ' . $out['sampleRate'];

    if ($out['channels'] != 0)
        $outStr .= ' -ac:a:' . $out['streamId'] . ' ' . $out['channels'];

    if ($out['bitrate'] != 0)
        $outStr .= ' -ab:a:' . $out['streamId'] . ' ' . str_replace("000", "k", $out['bitrate']);

    return $outStr;
}

function parse_to_ffmpeg_cmd_ini_output_simple($out) {
    $output = "";

    $src = $out['src'];
    $output .= $src[0] == 0 ? ' -map 1:a:0 ' : ' -map ' . $src[0]['srcNum'] . ':a:' . $src[0]['streamId'];
    $output .= parse_audio_details_to_output($out);


    return $output;
}

function parse_to_ffmpeg_cmd_ini_output_complex($out) {
    $output = "";

    $output .= ' -map [aout' . $out['name'] . '] ';
    $output .= parse_audio_details_to_output($out);

    return $output;
}

function parse_to_ffmpeg_cmd_ini_filtercomplex($out) {
    $filter_complex_str = "";

    $src = $out['src'];
    global $args;

    if (count($src) == 1) {
        $filter_complex_str .= '[' . $src[0]['srcNum'] . ':a:' . $src[0]['streamId'] . ']';
        if ($args['ebur128'])
            $filter_complex_str .= 'loudnorm=I=-23:LRA=1';
        $filter_complex_str .= '[aout' . $out['name'] . ']';
    }else if (count($src) == 2) {
        $filter_complex_str .= ' [' . $src[0]['srcNum'] . ':a:' . $src[0]['streamId'] . ']';
        $filter_complex_str .= '[' . $src[1]['srcNum'] . ':a:' . $src[1]['streamId'] . ']';
        $filter_complex_str .= 'amerge=inputs=2';
        if ($args['ebur128'])
            $filter_complex_str .= ',loudnorm=I=-23:LRA=1';
        $filter_complex_str .= '[aout' . $out['name'] . '] ';
    } else {
        fwrite(STDERR, '[ERR]filter_complex not support, input src over 2' . PHP_EOL);
    }

    return $filter_complex_str;
}

function parse_to_ffmpeg_cmd() {
    global $audioOut;
    global $FFCMD;
    global $args;

    $FFGlobalObj = "";
    $FFSilentSrc = "";
    $FFFilterComplex = "";
    $FFOutput = "";

    foreach ($audioOut as $tmp) {
        $out = (array) $tmp;

        if ($args['ebur128']) { //ebur128 must use filter_complex
            $FFFilterComplex .=  strcmp($FFFilterComplex,"") == 0 ? parse_to_ffmpeg_cmd_ini_filtercomplex($out): ','.parse_to_ffmpeg_cmd_ini_filtercomplex($out);
            $FFOutput .= parse_to_ffmpeg_cmd_ini_output_complex($out);
        } else {

            if (strcmp($out['audioAction'], "mix") == 0) {
                $FFFilterComplex .= parse_to_ffmpeg_cmd_ini_filtercomplex($out);
                $FFOutput .= parse_to_ffmpeg_cmd_ini_output_complex($out);
            }

            if (strcmp($out['audioAction'], "redirect") == 0) {
                $FFOutput .= parse_to_ffmpeg_cmd_ini_output_simple($out);
            }
        }
        if ($out['src'][0]['srcNum'] == 1)//1 is empty audio src
            $FFSilentSrc = FFSILENTSRC;
    }

    if (strcmp($FFFilterComplex, "") != 0)
        $FFFilterComplex = '-filter_complex "' . $FFFilterComplex.'"';

    //generate cmd
    $FFCMD .= VOLOBIN . $FFGlobalObj . ' -i ' . $args['videoSrc'];
    $FFCMD .= strcmp($FFSilentSrc, "") != 0 ? $FFSilentSrc : '';
    $FFCMD .= $args['videoControlCopy'] ? ' -map 0:v:0 -vcodec copy ' : ' -vn ';
    $FFCMD .= $FFFilterComplex;
    $FFCMD .= $FFOutput;
    $FFCMD .= strcmp($FFSilentSrc, "") != 0 ? ' -shortest ' : '';
    $FFCMD .= ' -y ' . $args['videoOut'];
}

function get_args($argv) {

    $args = array();

    for ($i = 1; $i < count($argv); $i++) {
        switch ($argv[$i]) {
            case "-map_rule":
                $args['mapFile'] = $argv[++$i];
                break;

            case "-in":
                $args['videoSrc'] = $argv[++$i];
                break;

            case "-out":
                $args['videoOut'] = $argv[++$i];
                break;

            case "-video_control_copy":
                $args['videoControlCopy'] = intVal($argv[++$i]);
                break;

            case "-ebur128":
                $args['ebur128'] = true;
                break;

            case "--help":
                echo 'Usage: ' . PHP_EOL . $argv[0] . ' -map_rule {mapping rule xml} -in {input video file} -out {output video file} -video_control_copy {0|1}' . PHP_EOL;
                exit(0);
                break;

            default:
                break;
        }
    }

    return $args;
}

//pre-check
function pre_check($args) {
    $flag = false;
    $errorMsg = "";

    if (empty($args['mapFile'])) {
        $flag = true;
        $errorMsg .= '[ERR]Mapping rule is empty' . PHP_EOL;
    }

    if (empty($args['videoSrc'])) {
        $flag = true;
        $errorMsg .= '[ERR]Video input is empty' . PHP_EOL;
    }

    if (empty($args['videoOut'])) {
        $flag = true;
        $errorMsg .= '[ERR]Video output is empty' . PHP_EOL;
    }

    if ($flag) {
        fwrite(STDERR, $errorMsg);
        echo 'Usage: ' . PHP_EOL . $args[0] . ' -map_rule {mapping rule xml} -in {input video file} -out {output video file} -video_control_copy {0|1}' . PHP_EOL;
        exit(0);
    }
}

//start
$args = get_args((array) $argv);
pre_check($args);

parse_xml_data(read_xml($args['mapFile'])); //read maping matrix
parse_to_ffmpeg_cmd(); //create cmd

echo $FFCMD . PHP_EOL;
system($FFCMD);
?>
