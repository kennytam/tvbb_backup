$(document).ready(function() {
    var errorHandler = function(event, id, fileName, reason) {
        qq.log("id: " + id + ", fileName: " + fileName + ", reason: " + reason);
    };

    uploader = new qq.FineUploader({
        element: $('#addBtn')[0],
        autoUpload: false,
        uploadButtonText: "<font color=\"#000000\">Upload Media</a>",
        request: {
            endpoint: "/cgi-bin/upload.cgi"
        },
	validation: {
	    allowedExtensions: ['mov', 'mp4', 'rmvb', 'avi', 'wmv', '3gp', 'flv', 'ts', 'mkv', 'mpg', 'mpeg', 'ps', 'mxf'],
	},
        callbacks: {
            onError: errorHandler
        }
    });

    $('#uploadAllBtn').click(function() {
        uploader.uploadStoredFiles();
    });
});
