<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');

if (isset($_GET["OID"])){
        $OID = htmlspecialchars($_GET["OID"]);
} else {
        echo "VISA Reponse ::<br />ERROR REQUEST";
        exit();
}

if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
if (isset($_GET["date"]))
        $jdate = htmlspecialchars($_GET["date"]);
else
        $jdate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

$config = include('../scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .  " user=". $config['db_user'] .  " password=". $config['db_pass'] .  " dbname=".$config['db_name'];
$link = pg_connect($connstr)
	or die('DB Connection Error : ' . pg_last_error());
 
?>
<html>
  <head>
   <title>Volo.VISA</title>
   <meta http-equiv="refresh" content="60">
  </head>
  <body style="background:rgba(65,65,65,1)">
 
<?php
  $job = pg_exec($link, "SELECT * FROM split_output WHERE output_id=" . $OID . " ORDER BY ID;");
  $numjob = pg_numrows($job);
  echo "<p><b>[ Split Trunk Encode Status ]</b>&nbsp;&nbsp;&nbsp;&nbsp;System Time - " . $para['time_zone'] . " " . $curtime . "</p><hr>";

  if ($numjob < 1){
    echo '<table border="0" bgcolor="white" width="100%">';
    echo "<tr><td width='3%'></td><td>NO SPLIT OUTPUT RECORD....</td></tr>";
  } else {
   for($rj = 1; $rj <= $numjob; $rj++) {
     $row = pg_fetch_array($job);
     if ($rj == 1){
	echo '<table border="0" bgcolor="white" width="100%"><tr><td>';
	echo "<p>Job ID [" . $row['job_id'] . "]&nbsp;&nbsp;Output ID [" . $row['output_id'] . "]&nbsp;&nbsp;Split Trunk Total [" . $numjob . "]<br />";
	echo "Trunk Source Path - " . $row['split_srcpath'] . "<br />Trunk Destination Path - " . $row['split_destpath'] . "</p><hr>";
	echo "</td></tr></table>";
	echo '<table border="0" bgcolor="white" width="100%">';
	echo "<tr><th>SPLIT ID</th><th>SOURCE TRUNK</th><th>READY TIME</th><th>DEST TRUNK</th><th>STATUS</th><th>ENCODE START / END</th><th>SPEED</th><th>VOLO NODE</th><th>ENCODE MESSAGE</th></tr>";
	$strunk=substr($row["trunksplit_time"], 0, 19);
	$ltrunk=substr($row["trunkready_time"], 0, 19);
	$stime=substr($row["stime"], 0, 19);
	$ltime=substr($row["ltime"], 0, 19);
     }
     echo "<tr><td align='middle'>" . $row["id"] . "</td><td align='middle'>" . $row["split_srcfile"] . "</td><td>" . substr($row["trunkready_time"], 0, 19) . "</td><td align='middle'>" . $row["split_destfile"] . "</td>";

     if ($row["stime"] < $stime) $stime = substr($row["stime"], 0, 19);
     if ($row["ltime"] > $ltime) $ltime = substr($row["ltime"], 0, 19);
     if ($row["trunksplit_time"] < $strunk) $strunk= substr($row["trunksplit_time"], 0, 19);
     if ($row["trunkready_time"] > $ltrunk) $ltrunk= substr($row["trunkready_time"], 0, 19);

     if ($row["stage"] < 0){
	$status = "<a href='./viewlog.php?JID=" . $row["job_id"] . "&OID=" . $row["output_id"] . "&SID=" . $row["id"] . "&MODE=t' target=_blank style='text-decoration:none'><font color=red>FAIL</font></a>";
     } else if ($row["stage"] == 0){
        $status = "<font color=green>WAITING</font>";
     } else if ($row["stage"] == 1){
        $status = "<font color=orange>";
        if ($row["progress_mode"] == 1)
		$status = "<font color=orange>1P-";
        else
		$status = "<font color=orange>2P-";
	$status .= "(" . $row["progress"] . "%)<br />[" . $row["progress_fps"] . "fps]</font>";
     } else if ($row["stage"] == 3){
	$status  = "<a href='./viewlog.php?JID=" . $row["job_id"] . "&OID=" . $row["output_id"] . "&SID=" . $row["id"] . "&MODE=t' target=_blank style='text-decoration:none'><font color=blue>CANCEL</font></a>";
     } else {
	$status  = "<a href='./viewlog.php?JID=" . $row["job_id"] . "&OID=" . $row["output_id"] . "&SID=" . $row["id"] . "&MODE=t' target=_blank style='text-decoration:none'>";
        $status .= "<font color=blue>DONE</font><br />[" . $row["progress_fps"] . "fps]</a>";
     }
     echo "<td align='middle'>" . $status . "</td>";

     echo "<td>" . substr($row["stime"], 0, 19) . "-" . substr($row["ltime"], 11, 8) . "</td>";
     $firstTime= strtotime(substr($row["stime"],0, 19));
     $lastTime = strtotime(substr($row["ltime"],0, 19));
     $timeDiff = '(' . ($lastTime-$firstTime) . 's)';
     echo "<td align='middle'>" . $timeDiff . "</td>";
 
     $node = pg_exec($link, "SELECT uuid FROM WORKER WHERE ID = " . $row["worker_id"] . ";");
     $nrow = pg_fetch_array($node);
     pg_free_result($node);
     echo "<td align='middle'>" . $nrow["uuid"] . "</td>";
     echo "<td>" . $row["message"] . "</td>" . "</tr>";
   }
   echo "<td colspan='11'><hr></td></tr>";
   $firstTime= strtotime($strunk);
   $lastTime = strtotime($ltrunk);
   $timeDiff = '(' . ($lastTime-$firstTime) . 's~';
   $timeMin  = round(($lastTime-$firstTime)/60, 1,PHP_ROUND_HALF_UP) . "m)";
   echo "<tr><td></td><td colspan='8'>Overall Throughput :: Trunk Creation [" . $strunk . "-" . substr($ltrunk,11,8) . " " . $timeDiff . $timeMin . "]&nbsp;&nbsp;";
   $firstTime= strtotime($stime);
   $lastTime = strtotime($ltime);
   $timeDiff = '(' . ($lastTime-$firstTime) . 's~';
   $timeMin  = round(($lastTime-$firstTime)/60, 1,PHP_ROUND_HALF_UP) . "m)";
   echo "Trunk Encode [" . $stime . "-" . substr($ltime,11,8) . " " . $timeDiff . $timeMin . "]</td></tr>";
  }
  pg_free_result($job);
  pg_close($link);
?>
  </table>
  <p><p>
  </body>
</html>
