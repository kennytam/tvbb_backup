<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if (isset($_GET["MDIA"])){
        $Media = htmlspecialchars($_GET["MDIA"]);
} else {
        echo "VISA Reponse ::<br />ERROR REQUEST";
        exit();
}

$xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
$xml .= "<VISA>\n<Request Command=\"MediaInfo\">\n";
$xml .= "<SourceUri>" . $Media . "</SourceUri>\n";
$xml .= "</Request></VISA>";

$post_data = array(
    "xml" => $xml,
);

$stream_options = array(
    'http' => array(
       'header'  => "Content-type: application/xml; charset=utf-8\r\n",
       'method'  => 'POST',
       'content' => $xml,
    ),
);

$context  = stream_context_create($stream_options);
$response = file_get_contents($para['schd_api'], null, $context);

echo "VISA Reponse ::<br />";
echo '<pre>', htmlentities($response), '</pre>';
?>
