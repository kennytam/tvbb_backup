<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
if (isset($_GET["date"]))
        $jdate = htmlspecialchars($_GET["date"]);
else
        $jdate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));


function dispEncodeLog($LOGLIST, $LOGNAME){
 global $NOLOG;
 $LOGPATH=explode("\n", $LOGLIST);
 foreach($LOGPATH as $LOGLIST){
  if (trim($LOGLIST) == "") continue;
  $NOLOG = false;
  echo "[ Volo Encode Log Path - " . $LOGLIST . " ]<br />";

  // encode.sh Process Log
  $LOGFILE = $LOGLIST . "/job.start";
  if (file_exists($LOGFILE)){
     echo "<pre style='white-space:pre-wrap; word-wrap:break-word;'><font size=+1>Encode Control Log - job.start ::</font><br />";
     $linecount=0;
     if ($handle = fopen($LOGFILE, "r")){
	while(!feof($handle)){
		$line = fgets($handle);
		echo htmlentities($line);
	}
	fclose($handle);
    }
    echo '<br /><hr></pre>';
  }
  // vologui.php Process Log
  $LOGFILE = $LOGLIST . "/encode.log";
  if (file_exists($LOGFILE)){
     echo "<pre style='white-space:pre-wrap; word-wrap:break-word;'><font size=+1>Encode Process Log - encode.log ::</font><br />";
     $linecount=0;
     if ($handle = fopen($LOGFILE, "r")){
	while(!feof($handle)){
		$line = fgets($handle);
		echo htmlentities($line);
	}
	fclose($handle);
    }
    echo '<br /><hr></pre>';
  }

  // Volo Encode Log
  $LOGFILE = $LOGLIST . "/" . $LOGNAME;
  if (file_exists($LOGFILE)){
     echo "<pre style='white-space:pre-wrap; word-wrap:break-word;'><font size=+1>Encode Detail Log - " . $LOGNAME . " ::</font><br />";
     $linecount=0;
     if ($handle = fopen($LOGFILE, "r")){
	while(!feof($handle)){
		$line = fgets($handle);
		if (substr($line,8,6) == "frame="){
			$linecount++;
			if ($linecount < 20)
				echo htmlentities($line);
			else if ($linecount < 21)
				echo htmlentities("........\n");
		} else {
			echo htmlentities($line);
		}
	}
	fclose($handle);
    }
    echo '<br /><hr></pre>';
  }
 }
} // End Function

function dispSplitLog($LOGLIST){
 global $NOLOG, $OID;
 $LOGPATH=explode("\n", $LOGLIST);
 foreach($LOGPATH as $LOGLIST){
  if (trim($LOGLIST) == "") continue;
  $NOLOG = false;
  echo "[ Volo Splitter Log Path - " . $LOGLIST . " ]<br />";

  // Split Process Log
  $LOGFILE = $LOGLIST . "/split/encode.log";
  if (file_exists($LOGFILE)){
     echo "<pre style='white-space:pre-wrap; word-wrap:break-word;'><font size=+1>Split Process Log - ./split/encode.log ::</font><br />";
     $linecount=0;
     if ($handle = fopen($LOGFILE, "r")){
	while(!feof($handle)){
		$line = fgets($handle);
		echo htmlentities($line);
	}
	fclose($handle);
    }
    echo '<br /><hr></pre>';
  }

  // Split Trunk Log
  $LOGFILE = $LOGLIST . "/split/" . "encode_*.log";
  $SPLIST  = shell_exec("ls " . $LOGFILE);
  $SPPATH  = explode("\n", $SPLIST);
  foreach($SPPATH as $SPLOG){
   if (trim($SPLOG) == "") continue;
   if (file_exists($SPLOG)){
     echo "<pre style='white-space:pre-wrap; word-wrap:break-word;'><font size=+1>Split Detail Log - ./split/" .  pathinfo($SPLOG, PATHINFO_BASENAME ) . " ::</font><br />";
     $linecount=0;
     if ($handle = fopen($SPLOG, "r")){
	while(!feof($handle)){
		$line = fgets($handle);
		if ( (substr($line,8,5) == "size=") || (substr($line,8,6) == "frame=") ){
			$linecount++;
			if ($linecount < 20)
				echo htmlentities($line);
			else if ($linecount < 21)
				echo htmlentities("........\n");
		} else {
			echo htmlentities($line);
		}
	}
	fclose($handle);
    }
    echo '<br /><hr></pre>';
   }
  }

  // Merge Process Log
  $LOGFILE = $LOGLIST . "/merge/" . $OID . "/encode.log";
  if (file_exists($LOGFILE)){
     echo "<pre style='white-space:pre-wrap; word-wrap:break-word;'><font size=+1>Video/Audio Merge Process Log - ./merge/" . $OID . "/encode.log ::</font><br />";
     $linecount=0;
     if ($handle = fopen($LOGFILE, "r")){
	while(!feof($handle)){
		$line = fgets($handle);
		echo htmlentities($line);
	}
	fclose($handle);
    }
    echo '<br /><hr></pre>';
  }

  // Audio Remap Log
  $LOGFILE = $LOGLIST . "/muxaudio/" . $OID . "/audioparse.log";
  if (file_exists($LOGFILE)){
     echo "<pre style='white-space:pre-wrap; word-wrap:break-word;'><font size=+1>Audio Remap Process Log - ./muxaudio/" .  $OID . "/audioparse.log ::</font><br />";
     $linecount=0;
     if ($handle = fopen($LOGFILE, "r")){
	while(!feof($handle)){
		$line = fgets($handle);
		if ( (substr($line,8,5) == "size=") || (substr($line,8,6) == "frame=") ){
			$linecount++;
			if ($linecount < 20)
				echo htmlentities($line);
			else if ($linecount < 21)
				echo htmlentities("........\n");
		} else {
			echo htmlentities($line);
		}
	}
	fclose($handle);
    }
    echo '<br /><hr></pre>';
  }

  // Mux Audio Compare Log
  $LOGFILE = $LOGLIST . "/muxaudio/" . $OID . "/job.start";
  if (file_exists($LOGFILE)){
     echo "<pre style='white-space:pre-wrap; word-wrap:break-word;'><font size=+1>Audio Remap Compare Log - ./muxaudio/" . $OID . "/job.start ::</font><br />";
     $linecount=0;
     if ($handle = fopen($LOGFILE, "r")){
	while(!feof($handle)){
		$line = fgets($handle);
		echo htmlentities($line);
	}
	fclose($handle);
    }
    echo '<br /><hr></pre>';
  }

 } // End Loop Job Path
} // End Function

?>
<html>
  <head>
   <title>Volo.VISA</title>
  </head>
  <body bgcolor="grey">
<?php
echo "<p><b>[ Encode Output Log Status ]</b>&nbsp;&nbsp;&nbsp;&nbsp;System Time - " . $para['time_zone'] . " " . $curtime . "</p><hr>";
echo '<table border="0" bgcolor="white" width="100%"><tr><td>';
if (isset($_GET["JID"])){
	$JID = htmlspecialchars($_GET["JID"]);
} else {
	echo "VISA Reponse ::<br />ERROR REQUEST";
	exit();
}

if (isset($_GET["OID"])){
	$OID = htmlspecialchars($_GET["OID"]);
} else {
	echo "VISA Reponse ::<br />ERROR REQUEST";
	exit();
}

if (isset($_GET["MODE"])){
	$SPLIT = htmlspecialchars($_GET["MODE"]);
} else {
	$SPLIT = "f";
}

if (isset($_GET["SID"])){
	$SID = htmlspecialchars($_GET["SID"]);
} else {
	$SID = 0;
}

$NOLOG   = true;
// Split Trunk Encode Logs
if ($SID > 0){
    // /opt/Volo.VISA/splitjobs/SP_9578.done/merge/37855/F_79.CTS-HKG-VOL02-STAG.20160610160033.done
    $LOGPATH = $para["volo_splitjob_done"] . "/SP_" . $JID . "*.*/merge/" . $OID . "/F_" . $SID . "*";
    $LOGLIST = shell_exec("ls -d " . $LOGPATH);
    dispEncodeLog($LOGLIST, "SPLIT.F_" . $OID . "_" . $SID . "_e.log");
// Normal Encode Logs
} else if ($SPLIT == "f"){
    $LOGPATH = $para["volo_job_done"] . "/NE_" . $JID . "/F_" . $OID . ".*";
    $LOGLIST = shell_exec("ls -d " . $LOGPATH);
    dispEncodeLog($LOGLIST, "SCHEDULAR.F_" . $OID . "_e.log");
} else {
// Split Encode Logs
    $LOGPATH = $para["volo_splitjob_done"] . "/SP_" . $JID . "*.*";
    $LOGLIST = shell_exec("ls -d " . $LOGPATH);
    dispSplitLog($LOGLIST);
}

// End Log Display
echo '<br />';
  if ($NOLOG){
	echo "VISA Reponse ::<br />ERROR REQUEST, ";
	echo "Encode Logs Not Ready or Purged ......<br /><br />";
  }
?>
  </td></tr></table>
  <p><p>
  </body>
</html>
