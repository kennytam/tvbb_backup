<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
 
if (isset($_GET["ID"])){
        $NID = htmlspecialchars($_GET["ID"]);
} else {
        echo "VISA Reponse ::<br />ERROR REQUEST";
        exit(1);
}
if (isset($_GET["MODE"])){
        $MODE = htmlspecialchars($_GET["MODE"]);
} else {
        $MODE = "";
}

if ( $MODE == 'A' )
	$state='Active';
else if ( $MODE == 'S' )
	$state='Suspend';
else {
        echo "VISA Reponse ::<br />ERROR REQUEST";
        exit(1);
}
 
$config = include('../scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .  " user=". $config['db_user'] .  " password=". $config['db_pass'] .  " dbname=".$config['db_name'];    
$link = pg_connect($connstr)
        or die('DB Connection Error : ' . pg_last_error());
$src  = pg_exec($link, "UPDATE worker SET mode='" . $MODE . "' WHERE id=" . $NID . ";");
$nrow = pg_fetch_array($src);
pg_close($link);
echo '<pre>Encoder [' . $NID . '] Mode Set to ' . $state . ' (' . $MODE . ')</pre>';
echo "<br />";
exit(0); 
?>
