<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.4.8 Build Tue Jan 17 16:17:46 HKT 2017 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
$para = include('../scheduler/config/scheduler.php');

if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

// Reset Platform Control
$vmesg = "";
if (isset($_GET["MODE"])){
        $vmode = htmlspecialchars($_GET["MODE"]);
        $rpass = htmlspecialchars($_GET["PASS"]);
	if ($rpass == $para['volo_pass']){
	  if ($vmode == "ON"){
		$vmesg = "<font color='red'>[INFOS] VISA Platform Set to Enable  ........</font>";
		$cmd   = "(cp " . $para["control_path"] . "/scheduler.ctl.UP " . $para["control_path"] . "/scheduler.ctl";
		$cmd  .= ";cp " . $para["control_path"] . "/splitter.ctl.UP "  . $para["control_path"] . "/splitter.ctl";
		$cmd  .= ";cp " . $para["control_path"] . "/worker.ctl.UP "    . $para["control_path"] . "/worker.ctl)";
	  } else if ($vmode == "OFF"){
		$vmesg = "<font color='red'>[INFOS] VISA Platform Set to Disable ........</font>";
		$cmd   = "(cp " . $para["control_path"] . "/scheduler.ctl.DOWN " . $para["control_path"] . "/scheduler.ctl";
		$cmd  .= ";cp " . $para["control_path"] . "/splitter.ctl.DOWN "  . $para["control_path"] . "/splitter.ctl";
		$cmd  .= ";cp " . $para["control_path"] . "/worker.ctl.DOWN "    . $para["control_path"] . "/worker.ctl)";
	  }
	}
}

$config = include('../scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .  " user=". $config['db_user'] .  " password=". $config['db_pass'] .  " dbname=".$config['db_name'];
$link = pg_connect($connstr)
	or die('DB Connection Error : ' . pg_last_error());
 
?>
<html>
  <head>
   <title>Volo.VISA</title>
   <?php if ($vmesg == "") echo '<meta http-equiv="refresh" content="60">'; ?>
   <link rel="stylesheet" href="./template/styles.css">
  </head>
  <body style="background:rgba(65,65,65,1)">
  <div id='header'>
<?php
  echo "<p><b>VISA Admin Console v1.4.8 [Node : " . $para['volo_node'] . "] [System Time : " . $para['time_zone'] . " " . $curtime ."]</b></p>";
?>
  </div>
  <div id='cssmenu'>
  <ul>
   <li><a href='./index_admin.php'><span>Job Queue</span></a></li>
   <li><a href='./job_filter.php'><span>Job Filter</span></a></li>
   <!--<li><a href='./job_submit.php'><span>Job Submit</span></a></li>-->
<?php
 if (file_exists("./channels/index.php"))
   echo "<li><a href='./channels/index.php'><span>CH Extract</span></a></li>";
 if (file_exists("./trp/index.php"))
   echo "<li><a href='./trp/index.php'><span>TS Extract</span></a></li>";
?>
   <li><a href='./worker_status.php'><span>Encoder Status</span></a></li>
   <li><a href='./logs_status.php'><span>System Log</span></a></li>
   <li class='xdcambt'><a href='./xdcam.php'><span>XDCAM</span></a>
          <div class="xdcambt-content">
                <a class="xdcambt-content-a" href="xdcam.php">XDCAM_Profile</a>
                <a class="xdcambt-content-a" href="xdcam_edit.php">Audio Profile Edit</a>
                <a class="xdcambt-content-a" href="nasman.php">NAS Management</a>
          </div>	
   </li>
                <li class="imx50bt"><a href='./imx50.php'><span>IMX50</span></a>
                    <div class="imx50bt-content">
                        <a class="imx50bt-content-a" href="imx50.php">IMX50_Profile</a>
                        <a class="imx50bt-content-a" href="imx50_edit.php">Audio Profile Edit</a>
                    </div>
                </li>

   <li class='active'><a href='./visa_status.php'><span>System Control</span></a></li>
  </ul>
  </div>
  <div id='joblist'>
  <table border="0" bgcolor="white"><tr><td>
<?php
  if ($vmesg != ""){
	echo "<p><br /><b>" . $vmesg . "</b></p></br /></table></div>";
	exec($cmd);
	exit();
  }

  echo "<p><b>[ VISA System Control ]</b>";
  echo "</p>";
  echo '<form id="visacontrol" method="get" action="./visa_status.php">';
  echo '<input type="radio" name="MODE" value="ON" checked>Enable VISA Platform';
  echo '&nbsp;&nbsp;<input type="radio" name="MODE" value="OFF">Disable VISA Platform';
  echo "<br /></br>";
  echo '&nbsp;&nbsp;System Control Password - <input type="password" name="PASS">';
  echo "<br /></br>";
  echo '<input type="submit" value="Submit">&nbsp;&nbsp;<input type="reset" value="Reset">';
  echo "</form>";
  echo "<hr>";
  echo '<table border="0" width="100%" bgcolor="white">';

  // Encoder Stats
  echo "<tr><td colspan='2'><b>[ Encode Throughput ]</b></td><td></td></tr>";
  echo "<tr><th>ENCODE SUBMIT DATE</th><th>JOB TOTAL</th><th>WIP</th><th>DONE</th><th>CANCEL</th><th>FAIL</th></tr>";

  $jdate  = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
  for ($i=1; $i<=7; $i++){
	$jobttl[0] = $jobwip[0] = $joberr[0] = $jobcnx[0] = 0;
	$jquery = "date_trunc('day',job.ctime) = '" . $jdate . "'";
	$job    = pg_exec($link, "SELECT COUNT(*) FROM job WHERE " . $jquery . ";");
	$jobttl = pg_fetch_array($job);
	pg_free_result($job);
	if ($jobttl[0] > 0){
		$jquery = "date_trunc('day',job.ctime) = '" . $jdate . "'";
		$job    = pg_exec($link, "SELECT COUNT(*) FROM job WHERE " . $jquery . " AND id IN (SELECT DISTINCT job_id FROM OUTPUT WHERE stage IN (0,1) AND c_cancel=false) AND " . $jquery . ";");
		$jobwip = pg_fetch_array($job);
		pg_free_result($job);
		$jquery = "date_trunc('day',job.ctime) = '" . $jdate . "'";
		$job    = pg_exec($link, "SELECT COUNT(*) FROM job WHERE " . $jquery . " AND id IN (SELECT DISTINCT job_id FROM OUTPUT WHERE c_cancel=true) AND " . $jquery . ";");
		$jobcnx = pg_fetch_array($job);
		pg_free_result($job);
		$jquery = "date_trunc('day',job.ctime) = '" . $jdate . "'";
		$job    = pg_exec($link, "SELECT COUNT(*) FROM job WHERE " . $jquery . " AND id IN (SELECT DISTINCT job_id FROM OUTPUT WHERE stage<0 AND c_cancel=false) AND id NOT IN (SELECT DISTINCT job_id FROM OUTPUT WHERE c_cancel=true) AND " . $jquery . ";");
		$joberr = pg_fetch_array($job);
		pg_free_result($job);
	}
	$jobdone = $jobttl[0] - $jobwip[0] - $joberr[0] - $jobcnx[0];
	if ($jobdone < 0) $jobdone = 0;
	echo "<tr><td align='middle'>" . $jdate . "</td><td align='middle'>" . $jobttl[0] . "</td><td align='middle'>" . $jobwip[0] . "</td><td align='middle'>" . $jobdone . "</td><td align='middle'>" . $jobcnx[0] . "</td><td align='middle'>" . $joberr[0] . "</td></tr>";
	$jdate = date('Y-m-d',strtotime("-1 day", strtotime($jdate)));
  }
  echo "<tr><td></td></tr>";

  // Scheduler / Splitter Status
  echo "<tr><td colspan='2'><b>[ Scheduler / Splitter Status ]</b></td><td></td></tr>";
  echo "<tr><th>SYSTEM SERVICE</th><th></th><th>MONITOR STATUS / </th><th align='left'>TIME</th></tr>";
  $fileList = explode("\n", shell_exec("ls " . $para["status_path"] . "/*.STATUS"));
  $numwork  = 0;
  foreach($fileList as $StatusFile){
    if (trim($StatusFile) == "") continue;
    $numwork++;
    $nodename = substr(pathinfo($StatusFile, PATHINFO_BASENAME), 0, -7);
    $schdfile = file_get_contents($StatusFile);
    $schdline = explode("\n", $schdfile);
    foreach($schdline as $statusline){
	if (trim($statusline) == "") continue;
	    $schdstatus = explode(",", $statusline);
	    echo "<tr><td colspan='2' align='left'>" . $nodename . " - " . $schdstatus[0] . "</td>";
	    if ( (time() - strtotime($schdstatus[1])) > 70 ){
		echo "<td colspan='2' align='middle'><font color=red>DOWN</font>&nbsp;&nbsp;";
		echo "<font color=red>" . $schdstatus[1] . "</font></td>";
	    } else {
	    	if ($schdstatus[2] == "UP")
		    $status = "<font color=green>UP</font>";
	    	else
		    $status = "<font color=red>DOWN</font>";
	    	echo "<td colspan='2' align='middle'>" . $status . "&nbsp;&nbsp;";
		echo $schdstatus[1] . "</td>";
	    }
	    echo "</tr>";
    }
  }
  if ($numwork == 0){
	    $curtime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	    echo "<tr><td colspan='2' align='left'>VISA Platform - NAS Control Path</td>";
	    echo "<td colspan='2' align='middle'><font color=red>DOWN</font>&nbsp;&nbsp;";
	    echo "<font color=red>" . $curtime . "</font></td>";
	    echo "</tr>";
  }
  echo "<tr><td></td></tr>";
  echo "<tr><td colspan='2'><b>[ Volo Encoder Status ]</b></td><td></td></tr>";
  $work = pg_exec($link, "SELECT * FROM worker WHERE id>30 ORDER BY id;");
  $numwork = pg_numrows($work);
  if ($numwork < 1){
    echo "<tr><th>NO VOLO ENCODER....</th></tr>";
  } else {
    echo "<tr><th>VOLO ID</th><th>HOSTNAME / IP</th><th>MONITOR STATUS / </th><th align='left'>TIME</th><th>VISA STATUS (CPU Load)</th><th>LAST UP TIME</th><th>VISA UNIT [cur/max]</th><th>ACTION</th></tr>";
   for($rj = 1; $rj <= $numwork; $rj++) {
     $row = pg_fetch_array($work);
     echo "<tr><td align='middle'>" . $row["id"] . "</td><td align='middle'>" . $row["name"] . " [" . $row["uuid"] . "]</td>";

     // Check Monitor Status
     $StatusFile = $para["status_path"] . "/" . $row["name"] . ".STATUS.W";
     $statusline = file_get_contents($StatusFile);
     $nodestatus = explode(",", $statusline);
     $StatusFile = $para["status_path"] . "/VISA.CPU." . $row["name"] . ".log";
     $cpuload    = file_get_contents($StatusFile) * 1;
     if ( (time() - strtotime($nodestatus[1])) > 70 ){
		echo "<td colspan='2' align='middle'><font color=red>DOWN</font>&nbsp;&nbsp;";
		echo "<font color=red>" . $nodestatus[1] . "</font></td>";
     } else {
		if (substr($nodestatus[2],0,2) == 'UP'){
			$status = "<font color=green>UP</font>";
		} else {
			$status = "<font color=red>DOWN</font>";
		}
		echo "<td colspan='2' align='middle'>" . $status . "&nbsp;&nbsp;";
		echo $nodestatus[1] . "</td>";
     }
     if ($row["mode"] == 'S'){
	$status = "<font color=blue>SUSPEND</font>";
     } else if ($row["dead"] == 't'){
	$status = "<font color=red>DOWN</font>";
     } else {
        $status = "<font color=green>UP</font> (" . $cpuload . "%)";
     }
     echo "<td align='middle'>" . $status . "</td>";
     echo "<td align='middle'>" . substr($row["heartbeat"], 0, 19) . "</td>";
     echo "<td align='middle'>" . $row["unit"] . " / " . $row["worker_unit"] . "</td>";
     if (substr($nodestatus[2],0,2) == 'UP')
     if ($row["mode"] == 'A')
     	echo "<td align='middle'><a href='./set_encoder.php?ID=" . $row["id"] . "&MODE=S' target=_blank style='text-decoration:none'>DISABLE</a></td>";
     else
     	echo "<td align='middle'><a href='./set_encoder.php?ID=" . $row["id"] . "&MODE=A' target=_blank style='text-decoration:none'>ENABLE</a></td>";
   }
  }
  pg_free_result($work);

  pg_close($link);
?>
  </tr></td></table>
  <p><br /><br /></p>
  </table>
  </div>
  <p></p>
  </table>
  </body>
</html>
