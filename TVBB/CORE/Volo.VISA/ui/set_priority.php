<?php
#########################################################################################################
## Volo VISA Management Interface UI v1.3.8 Build Tue Aug 30 16:10:23 HKT 2016 (DaoLab)                ##
##                                                                                                     ##
#########################################################################################################
 
if (isset($_GET["JID"])){
        $JID = htmlspecialchars($_GET["JID"]);
} else {
        echo "VISA Reponse ::<br />ERROR REQUEST";
        exit(1);
}
if ( (isset($_GET["PRIORITY"])) && ($_GET["PRIORITY"] > 0) ){
        $PRIORITY = htmlspecialchars($_GET["PRIORITY"]);
} else {
        echo "VISA Reponse ::<br />ERROR REQUEST";
        exit(1);
}
 
 
$config = include('../scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .  " user=". $config['db_user'] .  " password=". $config['db_pass'] .  " dbname=".$config['db_name'];    
$link = pg_connect($connstr)
        or die('DB Connection Error : ' . pg_last_error());
$src  = pg_exec($link, "UPDATE output SET priority=" . $PRIORITY . " WHERE job_id=" . $JID . ";");
$nrow = pg_fetch_array($src);
pg_close($link);
echo '<pre>Job [' . $JID . '] Priority Set to ' . $PRIORITY . '</pre>';
echo "<br />";
exit(0); 
?>
