#!/bin/sh

if [ $# -eq 0 ]
  then
    echo "USAGE: VISA.Call.sh [command.xml]"
    exit
fi

/usr/bin/curl -X POST -d @$1 http://127.0.0.1/scheduler/api.php
