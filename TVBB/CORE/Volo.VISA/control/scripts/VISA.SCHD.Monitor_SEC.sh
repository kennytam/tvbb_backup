#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Scheduler Monitor and Housekeeping Script
#
cd /opt/Volo.VISA/control/scripts
CTLFILE=/opt/Volo.VISA/control/scheduler.ctl
CTLPATH=/opt/Volo.VISA/VISA_Control
IN1PATH=/media/volo_source_A/VISA_Control
IN2PATH=/media/volo_source_B/VISA_Control
OU1PATH=/media/volo_output_A/VISA_Control
OU2PATH=/media/volo_output_B/VISA_Control
SP1PATH=/media/volo_spool_A/VISA_Control
SP2PATH=/media/volo_spool_B/VISA_Control

PBFILE=/opt/Volo.VISA/control/scripts/VISA.PUBS.Monitor.sh
HSFILE=/opt/Volo.VISA/control/scripts/VISA.SCHD.Housekeep.sh
WKFILE=/opt/Volo.VISA/control/scripts/VISA.PERIOD.Start.sh
 
# Infinite Loop
while true
do
	LOGFILE='/opt/Volo.VISA/logs/VISA.SCHD.ProcMonitor'.`hostname`.`date +%Y%m%d`.'log'
	LOGPURGE='/opt/Volo.VISA/logs/VISA.SCHD.Housekeep'.`hostname`.`date +%Y%m%d`.'log'
	CMD=U

        # Verify Package/Source/Spool/Output Mount Status
        if [ -f "${CTLPATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${CTLPATH}/VISA.CONTROL
           echo SECONDARY SCHEDULER PACKAGE NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP > ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
           echo SECONDARY SCHEDULER PACKAGE NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN > ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi
 
        if [ -f "${IN1PATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${IN1PATH}/VISA.CONTROL
           echo SECONDARY SCHEDULER SOURCE NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
           echo SECONDARY SCHEDULER SOURCE NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi

        if [ -f "${IN2PATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${IN2PATH}/VISA.CONTROL
           echo SECONDARY SCHEDULER SOURCE NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
           echo SECONDARY SCHEDULER SOURCE NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi
 
        if [ -f "${OU1PATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${OU1PATH}/VISA.CONTROL
           echo SECONDARY SCHEDULER OUTPUT NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
           echo SECONDARY SCHEDULER OUTPUT NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi
 
        if [ -f "${OU2PATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${OU2PATH}/VISA.CONTROL
           echo SECONDARY SCHEDULER OUTPUT NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
           echo SECONDARY SCHEDULER OUTPUT NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi
 
        if [ -f "${SP1PATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${SP1PATH}/VISA.CONTROL
           echo SECONDARY SCHEDULER SPOOL NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
           echo SECONDARY SCHEDULER SPOOL NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi
 
        if [ -f "${SP2PATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${SP2PATH}/VISA.CONTROL
           echo SECONDARY SCHEDULER SPOOL NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
           echo SECONDARY SCHEDULER SPOOL NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi
 
        # Scheduler Services Check
        jobStr=`ps -Alf | grep "/usr/pgsql-9.4" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
        if [ -z "${jobStr}" ]; then
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD PGSQL DB Not Running >> ${LOGFILE}
           echo SECONDARY SCHEDULER PGSQL DB,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        else
           echo SECONDARY SCHEDULER PGSQL DB,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        fi
 
        jobStr=`ps -Alf | grep "/usr/sbin/nginx" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
        if [ -z "${jobStr}" ]; then
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD NGINX Not Running >> ${LOGFILE}
           echo SECONDARY SCHEDULER NGINX,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        else
           echo SECONDARY SCHEDULER NGINX,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        fi
 
        jobStr=`ps -Alf | grep "/etc/php-fpm.conf" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
        if [ -z "${jobStr}" ]; then
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD PHP-FPM Not Running >> ${LOGFILE}
           echo SECONDARY SCHEDULER PHP-FPM,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        else
           echo SECONDARY SCHEDULER PHP-FPM,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        fi
 
	if [ "${CMD}" == "D" ]; then
	   echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Platform NAS Not Ready, Force Platform Down >> ${LOGFILE}
	   cp /opt/Volo.VISA/control/scheduler.ctl.DOWN /opt/Volo.VISA/control/scheduler.ctl
	   cp /opt/Volo.VISA/control/splitter.ctl.DOWN /opt/Volo.VISA/control/splitter.ctl
	   cp /opt/Volo.VISA/control/worker.ctl.DOWN /opt/Volo.VISA/control/worker.ctl
	fi

	# Read Control File (U - UP, D - FORCE DOWN)
	if [ "${CMD}" == "U" ] && [ -f "${CTLFILE}" ]; then
	  while read line
	  do
		if [[ ${line:0:1} != "#" ]]; then
			CMD=${line}
		fi
	  done < ${CTLFILE}
	fi
 
        # Monitor Status
        if [ "${CMD}" == "D" ]; then
           echo SECONDARY SCHEDULER SERVICE,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
        else
           echo SECONDARY SCHEDULER SERVICE,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        fi
 
	# Housekeeping Job
	NOW=`date +%H%M`
	if [ "${CMD}" == "U" ] && [[ "${NOW}" == "0500" ]]; then
		echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Housekeeping Start >> ${LOGFILE}
		${HSFILE} >> ${LOGPURGE} 2>&1 &
	fi

	# Sleep
	sleep 60
done
