#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Split Encode - Split Engine Control Script
#

while true; do
  LOGFILE='/opt/Volo.VISA/logs/VISA.SPLIT.EngineCtrl_SEC'.`hostname`.`date +%Y%m%d`.'log'

  # Ensure Running One Instance Split Engine - TOP PRIORITY JOB ONLY
  jobStr=`ps -Alf | grep "splitengine.php" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
  if [ -z "${jobStr}" ]; then
	cd /opt/Volo.VISA/control/split_encode
	/usr/bin/php ./splitengine.php 2 1 >> ${LOGFILE} 2>&1 &
        PID=$!
        wait $PID
  fi
  sleep 5

done
exit 0

