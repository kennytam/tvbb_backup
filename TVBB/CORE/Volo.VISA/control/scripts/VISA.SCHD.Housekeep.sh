#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Scheduler Housekeeping Script
#

# Housekeep Volo Logs
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Housekeeping Logs
cd /opt/Volo.VISA/logs/
find ./Z_archive/ -type f -mtime +14 -name "*.gz" -exec rm -rf {} \;
find . -type f -name '*.log' -mmin +$((60*23)) -exec gzip {} \;
mv *.gz ./Z_archive/ 2>/dev/null

# Housekeep Job Publish
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Housekeeping Job Publish
cd /opt/Volo.VISA/jobs/
find ./Z_archive/ -type f -mtime +14 -name "*.gz" -exec rm -rf {} \;
find . -type f -name '*.done' -mmin +$((60*23)) -exec gzip {} \;
find . -type f -name '*.error' -mmin +$((60*23)) -exec gzip {} \;
find . -type f -name '*.encode' -mmin +$((60*23)) -exec gzip {} \;
mv *.gz ./Z_archive/ 2>/dev/null

# Housekeep Jobs Results
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Housekeeping Normal Encode Job Result
cd /home/daolab/VISA.Archive/results/
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Purge Normal Encode Job Archive
find . -type f -mtime +3 -name "*.tgz" -exec rm -rf {} \;
cd /opt/Volo.VISA/results/
#find . -type f -name '*.ts' -o -name '*.mov' -o -name '*.zip' -o -name '*.mp4' -o -name '*.temp' -o -name 'volo_high*' -o -type d -name '*.hls' -exec rm -rf {} \;
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Archive, Purge Normal Encode Job Result
find . -maxdepth 1 -type d -name 'NE_*' -mmin +$((60*23)) -exec tar -zcf /home/daolab/VISA.Archive/results/{}.tgz --remove-files {} \;

# Housekeep Split Jobs Results
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Housekeeping Split Encode Job Result
cd /home/daolab/VISA.Archive/splitjobs/
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Purge Split Encode Job Archive
find . -type f -mtime +3 -name "*.tgz" -exec rm -rf {} \;
cd /opt/Volo.VISA/splitjobs/
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Archive, Purge Split Encode Job Result
find . -maxdepth 1 -type d -name 'SP_*' -mmin +$((60*23)) -exec tar -zcf /home/daolab/VISA.Archive/splitjobs/{}.tgz --remove-files {} \;

# Housekeep Split Jobs Unknown Results
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Housekeeping Split Encode Job Unknown Result
cd /opt/Volo.VISA/splitjobs/SPO_Unknown/
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Purge Unknown Split Encode Job Archive
find ./Z_archive/ -type f -mtime +3 -name "*.tgz" -exec rm -rf {} \;
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Archive, Purge Unknown Split Encode Job Result
find . -maxdepth 1 -type d -name 'F_*' -mmin +$((60*23)) -exec tar -zcf ./Z_archive/{}.tgz --remove-files {} \;

# Housekeep Batch Jobs
#echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Housekeeping Batch Job Files
#cd /opt/Volo.VISA/batch/
#find . -type f -mmin +$((60*23)) -exec gzip {} \;
#mv *.gz ./Z_archive/ 2>/dev/null
#find ./Z_archive/ -type f -mtime +30 -name "*.gz" -exec rm -rf {} \;

# Housekeep Encoded Source Video
#echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Housekeeping Encoded Source Video
#find /media/volo_source_A/tvblegacy/ -type f -name '*.bak' -mmin +$((60*23)) -exec rm -rf {} \;

# Generate Encode Delivery Report
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] Generate Encode Delivery Report
#rptdt=$(date --date yesterday "+%Y-%m-%d")
#/usr/bin/php /opt/Volo.VISA/control/jobproc/edr_bypath.php /media/volo_source_A ${rptdt} ${rptdt} > /opt/Volo.VISA/ui/edr/edrpath-source_A-${rptdt}.txt
#/usr/bin/php /opt/Volo.VISA/control/jobproc/edr_bypath.php /media/volo_source_B ${rptdt} ${rptdt} > /opt/Volo.VISA/ui/edr/edrpath-source_B-${rptdt}.txt

exit 0
