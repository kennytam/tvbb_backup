#!/bin/sh
# Volo Scheduler Client Job Publishing Script (Monitoring Job Text File)
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
# ${PUBLISH} ${dir}${infile} ${OUTPATH} ${PLAN}
if [ $# -lt 1 ]
  then
    echo "USAGE: VISA.jobpublish.sh [SOURCE VIDEO FULL PATH] [DESTINATION FULL PATH]"
    exit 1
fi
SRCPATH=$1
JOBPATH=$1/Publish
OUTPATH=$2
PLAN=ALL
PUBPATH='/opt/Volo.VISA/client'
PUBLISH='/usr/bin/php ./publish.php'
LOGFILE='/opt/Volo.VISA/logs/VISA.JobTxtPublish'.`hostname`.`date +%Y%m%d`.'log'

if [ ! -d "${JOBPATH}" ]; then
	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Error :: Job Monitor Path Not Found [${JOBPATH}]
	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Error :: Job Monitor Path Not Found [${JOBPATH}] >> ${LOGFILE}
        exit 1
fi
 
# Check any Job entry
cd ${JOBPATH}
JobFile=`ls -1tr *.txt 2>/dev/null`
if [ -z "${JobFile}" ];then
	exit 0
fi
FILE_IN=`cat ${JobFile}`

mv -f ${JobFile} ./Done/${JobFile}.done

if [ ! -f "${SRCPATH}/${FILE_IN}" ]; then
	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Skip Job :: Source Video Not Found JOB[${JobFile}] SOURCE[${SRCPATH}/${FILE_IN}]
	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Skip Job :: Source Video Not Found JOB[${JobFile}] SOURCE[${SRCPATH}/${FILE_IN}] >> ${LOGFILE}
else
	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Submit Job :: JOB[${JobFile}] SOURCE[${SRCPATH}/${FILE_IN}] PLAN[${PLAN}]
	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Submit Job :: JOB[${JobFile}] SOURCE[${SRCPATH}/${FILE_IN}] PLAN[${PLAN}] >> ${LOGFILE}
	cd ${PUBPATH}
	eval "${PUBLISH} ${SRCPATH}/${FILE_IN} ${OUTPATH} ${PLAN}" >> ${LOGFILE} 2>&1
fi

