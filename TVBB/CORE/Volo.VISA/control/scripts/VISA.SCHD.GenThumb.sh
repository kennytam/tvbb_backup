#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Scheduler - Source Thumbnail Generation Script
#
SRC=$1
JOBID=$2
STSS=$3
OUTPATH=$4

cd /opt/Volo.VISA/control/scripts
if [ ! -d "${OUTPATH}" ]; then
	mkdir -p ${OUTPATH}
fi
/opt/volo/bin/volo-1.4.1 -ss $STSS -i $SRC -vf scale=320:-1 -vcodec mjpeg -vframes 1 -f rawvideo -y $OUTPATH/$JOBID.jpg > /dev/null 2>&1
exit 0
