#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Encode Worker - Worker Monitor and Housekeeping Script
#

cd /opt/Volo.VISA/control/scripts
CTLFILE=/opt/Volo.VISA/control/worker.ctl
CTLPATH=/opt/Volo.VISA/VISA_Control
SRCPATH=/home/Video.Source/VISA_Control
OUTPATH=/home/Video.Output/VISA_Control
HSFILE=/opt/Volo.VISA/control/scripts/VISA.WORKER.Housekeep.sh
WKFILE=/opt/Volo.VISA/control/scripts/VISA.WORKER.Start.sh
 
# Infinite Loop
while true
do
	LOGFILE='/opt/Volo.VISA/logs/VISA.WORKER.ProcMonitor'.`hostname`.`date +%Y%m%d`.'log'
	CMD=U

        # Verify Package/Source/Spool/Output Mount Status
        if [ ! -f "${SRCPATH}/VISA.CONTROL" ]; then
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER SOURCE NAS Not Ready >> ${LOGFILE}
           CMD=D
        fi

        if [ ! -f "${CTLPATH}/VISA.CONTROL" ]; then
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER CTL NAS Not Ready >> ${LOGFILE}
           CMD=D
        fi

        if [ ! -f "${OUTPATH}/VISA.CONTROL" ]; then
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER OUTPUT NAS Not Ready >> ${LOGFILE}
           CMD=D
        fi

        # Monitor Status
        if [ "${CMD}" == "D" ]; then
           echo WORKER,`date +"%Y-%m-%d %H:%M:%S"`,DOWN > ${CTLPATH}/`hostname`.STATUS.W
        else
           echo WORKER,`date +"%Y-%m-%d %H:%M:%S"`,UP > ${CTLPATH}/`hostname`.STATUS.W
           # Read Control File (U - UP, D - FORCE DOWN)
           if [ -f "${CTLFILE}" ]; then
             while read line
             do
                if [[ ${line:0:1} != "#" ]]; then
                        CMD=${line}
                fi
             done < ${CTLFILE}
           fi
        fi

	# Force DOWN if Worker Running
	RUNNING=0
	jobStr=`ps -Alf | grep "worker.php" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
	if [ -z "${jobStr}" ]; then
	   echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Worker Not Running >> ${LOGFILE}
	   RUNNING=-1
	else
	   if [ "${CMD}" == "D" ]; then
		jobs=( ${jobStr} )
		if [ ${#jobs[@]} -gt 0 ]; then
			echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Worker Force Stop >> ${LOGFILE}
			kill -9 ${jobs[3]}
	   		RUNNING=-1
		fi
	   else
	   	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Worker Running >> ${LOGFILE}
	   fi
        fi
	# Start Worker if UP mode 
	if [ "${CMD}" == "U" ] && [ ${RUNNING} -lt 0 ]; then
		echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Worker Force Start >> ${LOGFILE}
		${WKFILE} >> ${LOGFILE} 2>&1 &
	fi
	
	# Housekeeping Job
	NOW=`date +%H%M`
	if [ "${CMD}" == "U" ] && [ "${NOW}" == "0400" ]; then
		echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Housekeeping Start >> ${LOGFILE}
		${HSFILE} >> ${LOGFILE} 2>&1
		echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Housekeeping End >> ${LOGFILE}
	fi
	
	#mount nas
	sudo python /opt/Volo.VISA/client/helper/mount.py 

	sleep 60
done
exit 0
