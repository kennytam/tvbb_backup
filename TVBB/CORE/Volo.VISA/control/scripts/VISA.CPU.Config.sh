#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Encode - CPU Info Script
#

cd /opt/Volo.VISA/control/scripts
CPUFILE='/opt/Volo.VISA/VISA_Control/VISA.CPU'.`hostname`.'cfg'
cpus=`grep 'physical id' /proc/cpuinfo | sort | uniq | wc -l`
cores=`grep 'cpu cores' /proc/cpuinfo | tail -1 | sed 's/[^0-9]//g'`
virtuals=`grep ^processor /proc/cpuinfo | wc -l | sed 's/[^0-9]//g'`
 
echo "$cpus||$cores||$virtuals" > ${CPUFILE}
exit 0
