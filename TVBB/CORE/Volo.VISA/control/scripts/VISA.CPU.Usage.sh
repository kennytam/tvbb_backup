#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Encode - CPU Usage Calculation Script
#

cd /opt/Volo.VISA/control/scripts
CPUFILE='/opt/Volo.VISA/VISA_Control/VISA.CPU'.`hostname`.'cfg'
LOG='/opt/Volo.VISA/VISA_Control/VISA.CPU'.`hostname`.'log'
cpuConfig=`cat ${CPUFILE}`
cpuInfos=( `echo $cpuConfig | sed -e 's/[||\/]/ /g'` )
 
CPUS=${cpuInfos[0]}
CORES=${cpuInfos[1]}
THREADS=${cpuInfos[2]}
 
# Set the size of the returning data
let SIZE=$CPUS*$CORES
let THREADSPERCPU=$THREADS/$SIZE
let WEIGHT=$THREADS/$SIZE
 
PREVTOTALS=(`for (( i=0; i<$SIZE; i++ )); do echo -n "0 "; done`)
PREVIDLES=(`for (( i=0; i<$SIZE; i++ )); do echo -n "0 "; done`)
 
while true; do
        TOTALS=()
        IDLES=()
        USAGES=()
 
        for (( idx=0; idx<$SIZE; idx++ )); do
                for (( t=0; t<$THREADSPERCPU; t++ )); do
                        let c=$SIZE*$t+$idx
                        cmd="cat /proc/stat | grep 'cpu${c} '"
                        CPU=(`eval $cmd`)
                        unset CPU[0]
 
                        let IDLES[$idx]=${IDLES[$idx]}+${CPU[4]}/$WEIGHT
 
                        TOTAL=0
                        for VALUE in "${CPU[@]}"; do
                                let "TOTAL=$TOTAL+$VALUE"
                        done
 
                        let TOTALS[$idx]=${TOTALS[$idx]}+$TOTAL/$WEIGHT
                done
 
                #echo "CPU $idx: TOTAL = ${TOTALS[$idx]}, IDLE = ${IDLES[$idx]}"
 
                # Calculate the CPU usage
                let "DIFF_IDLE=${IDLES[$idx]}-${PREVIDLES[$idx]}"
                let "DIFF_TOTAL=${TOTALS[$idx]}-${PREVTOTALS[$idx]}"
                let "DIFF_USAGE=(1000*($DIFF_TOTAL-$DIFF_IDLE)/$DIFF_TOTAL+5)/10"
                #echo "Usage = $DIFF_USAGE"
                if [ $DIFF_USAGE -ge 0 ]; then
                        USAGES[$idx]=$DIFF_USAGE
                else
                        USAGES[$idx]=0
                fi
        done
 
        # Remember the total and idle CPU times for the next check.
        PREVTOTALS=(`for (( i=0; i<$SIZE; i++ )); do echo -n "${TOTALS[$i]} "; done`)
        PREVIDLES=(`for (( i=0; i<$SIZE; i++ )); do echo -n "${IDLES[$i]} "; done`)
 
        echo -n "`for (( i=0; i<$SIZE; i++ )); do echo -n "${USAGES[$i]} "; done`" > $LOG
        sleep 1
done
exit 0
