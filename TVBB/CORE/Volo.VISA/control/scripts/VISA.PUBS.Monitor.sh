#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Scheduler Job Publishing Script (Poll Source Video Path)
#
while true; do
  LOGFILE1='/opt/Volo.VISA/logs/VISA.SCHD.PollSource'.`hostname`.`date +%Y%m%d`.'log'
  LOGFILE2='/opt/Volo.VISA/logs/VISA.SCHD.ProcQueue'.`hostname`.`date +%Y%m%d`.'log'

  # Monitor /media/volo_source_A
  jobStr=`ps -Alf | grep "pollsource.php" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
  if [ -z "${jobStr}" ]; then
	cd /opt/Volo.VISA/client
	/usr/bin/php /opt/Volo.VISA/control/jobproc/pollsource.php /home/Video.Source /home/Video.Output ALL >> ${LOGFILE1} 2>&1 &
        PID=$!
        wait $PID
  fi

  # Process Job Status Queue in /opt/Volo.VISA/jobs
  jobStr=`ps -Alf | grep "procqueue.php" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
  if [ -z "${jobStr}" ]; then
	cd /opt/Volo.VISA/control/jobproc
	/usr/bin/php ./procqueue.php >> ${LOGFILE2} 2>&1 &
        PID=$!
        wait $PID
  fi

  sleep 5
done
