#!/bin/sh
#
# Volo.Worker :: Scripts to Poll Source Videos 
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
#
cd /opt/Volo.VISA/client
INPATH=/media/volo_source
PUBLISH='./VISA.publish.sh'
 
# inotifywait
# -mr : monitor recursive
# --timefmt : Set a time format string as accepted by strftime(3)
# --format : Output in a user-specified format 
# -e close_write,modify,create,moved_to,moved_from --excludei /*.filepart/ \
inotifywait -mr --timefmt '%d/%m/%y %H:%M' --format '%T %w %f' \
    -e close_write,moved_to,moved_from ${INPATH} | while read date time dir file; do
 
    if [ -f "${dir}${file}" ]; then
        SUBDIR=`echo ${dir%/} | sed 's,'${INPATH}',,g'`
        PLAN=${SUBDIR}"/"
        if [[ "${PLAN}" == "${INPATH}" ]]; then
                PLAN=ALL
                SUBDIR=""
                OUTPATH=/media/volo_output
        else
                PLAN=ALL
                #DISABLE# PLAN=${SUBDIR}
                OUTPATH=/media/volo_output/
        fi
 
        infile=`echo ${file} | tr " " "_"`
        if [[ "${file}" != "${infile}" ]]; then
                `mv -f "${dir}${file}" ${dir}${infile}`
        fi
 
        echo "[INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA Publish Job Trigger :: FILE[${dir}${infile}]"
        eval "${PUBLISH} ${dir} ${infile} ${OUTPATH}${SUBDIR} ${PLAN}" &
    fi
done
 
