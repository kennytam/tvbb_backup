#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Split Encode - Worker Encode Status Update Script
#

cd /opt/Volo.VISA/client/
# Ensure Running One Instance
jobStr=`ps -Alf | grep "pollVodJobs.php" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
if [ -z "${jobStr}" ]; then
	LOGPATH='/opt/Volo.VISA/logs/VISA.WORKER.PollJob.'`hostname`
	/usr/bin/php ./helper/pollVodJobs.php >> ${LOGPATH}.`date +%Y%m%d`.'log' 2>&1 &
fi

jobStr=`ps -Alf | grep "pollSPVodJobs.php" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
if [ -z "${jobStr}" ]; then
	LOGPATH='/opt/Volo.VISA/logs/VISA.WORKER.PollSPJob.'`hostname`
	/usr/bin/php ./helper/pollSPVodJobs.php >> ${LOGPATH}.`date +%Y%m%d`.'log' 2>&1 &
fi

exit 0
