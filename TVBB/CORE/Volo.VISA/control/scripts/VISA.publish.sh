#!/bin/sh
# Volo Scheduler Client Job Publishing Script
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
# ${PUBLISH} ${dir}${infile} ${OUTPATH} ${PLAN}
if [ $# -lt 4 ]
  then
    echo "USAGE: VISA.publish.sh [SOURCE FILE FULL PATH] [SOURCE FILE NAME] [OUTPUT FULL PATH] [VISA JOB PLAN XML]"
    exit
fi
FILE_IN=$1$2
OUTPATH=$3
PLAN=$4
PUBLISH='/usr/bin/php ./publish.php'
LOGFILE='/opt/Volo.VISA/logs/VISA.JobPublish'.`hostname`.`date +%Y%m%d`.'log'
ERRLOG='/opt/Volo.VISA/logs/VISA.JobPublish'.`hostname`.`date +%Y%m%d`.'error.log'
 
# Exclude Source Not Found
if [ ! -f "${FILE_IN}" ]; then
        echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Skip Job :: File Not Found [${FILE_IN}]
        echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Skip Job :: File Not Found [${FILE_IN}] >> ${ERRLOG}
        exit
fi
 
# Exclude Dot file [ie .xxx]
if [[ ${2:0:1} == "." ]]; then
        echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Skip Job :: Unsupported File [${FILE_IN}]
        echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Skip Job :: Unsupported File [${FILE_IN}] >> ${ERRLOG}
        exit
fi
 
# Check File Modify Timestamp
LASTMOD=`stat -c %Y ${FILE_IN}`
sleep 5
CURMOD=`stat -c %Y ${FILE_IN}`
if [ ${CURMOD} -ne ${LASTMOD} ]; then
        echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Skip Job :: File Uploading [${FILE_IN}]
        echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Skip Job :: File Uploading [${FILE_IN}] >> ${ERRLOG}
        exit
fi
 
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Submit Job :: [${PLAN}] [${FILE_IN}]
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.Publisher Submit Job :: [${PLAN}] [${FILE_IN}] >> ${LOGFILE}
eval "${PUBLISH} ${FILE_IN} ${OUTPATH} ${PLAN}" >> ${LOGFILE} 2>&1

