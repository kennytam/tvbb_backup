#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Split Encode Engine Monitor and Housekeeping Script
#
cd /opt/Volo.VISA/control/scripts/

# VISA Control Path
CTLFILE=/opt/Volo.VISA/control/splitter.ctl
CTLPATH=/opt/Volo.VISA/VISA_Control
 
# NAS IN/OUT Mount Point
IN1PATH=/mnt/spretrans/VISA_Control
#IN2PATH=/media/volo_source_B/VISA_Control
OU1PATH=/mnt/spostrans/VISA_Control
#OU2PATH=/media/volo_output_B/VISA_Control
SP1PATH=/mnt/spretrans/Volo.Spool/VISA_Control
#SP2PATH=/media/volo_spool_B/VISA_Control

# Splitter Scripts
SEFILE=/opt/Volo.VISA/control/scripts/VISA.SPLIT.Engine_SEC.sh
MNFILE=/opt/Volo.VISA/control/scripts/VISA.SPLIT.Manage.sh
ECFILE=/opt/Volo.VISA/control/scripts/VISA.SPLIT.EncodeCtrl.sh
MGFILE=/opt/Volo.VISA/control/scripts/VISA.SPLIT.Merge.sh
ERFILE=/opt/Volo.VISA/control/scripts/VISA.SPLIT.Error.sh
HSFILE=/opt/Volo.VISA/control/scripts/VISA.SPLIT.Housekeep.sh


# Infinite Loop
while true
do
	LOGFILE='/opt/Volo.VISA/logs/VISA.SPLIT.SplitMonitor_SEC'.`hostname`.`date +%Y%m%d`.'log'
	CMD=U

        # Verify Package/Source/Spool/Output Mount Status
        if [ -f "${CTLPATH}/VISA.CONTROL" ]; then
           echo SECONDARY SPLITTER PACKAGE NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP > ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
           echo SECONDARY SPLITTER PACKAGE NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN > ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi
 
        if [ -f "${IN1PATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${IN1PATH}/VISA.CONTROL
           echo SECONDARY SPLITTER SOURCE NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
           echo SECONDARY SPLITTER SOURCE NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi
 
        #if [ -f "${IN2PATH}/VISA.CONTROL" ]; then
        #   echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${IN2PATH}/VISA.CONTROL
        #   echo SECONDARY SPLITTER SOURCE NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        #else
        #   echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
        #   echo SECONDARY SPLITTER SOURCE NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
        #   CMD=D
        #fi
 
        if [ -f "${OU1PATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${OU1PATH}/VISA.CONTROL
           echo SECONDARY SPLITTER OUTPUT NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
           echo SECONDARY SPLITTER OUTPUT NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi
 
        #if [ -f "${OU2PATH}/VISA.CONTROL" ]; then
        #   echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${OU2PATH}/VISA.CONTROL
        #   echo SECONDARY SPLITTER OUTPUT NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        #else
        #   echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
        #   echo SECONDARY SPLITTER OUTPUT NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
        #   CMD=D
        #fi
 
        if [ -f "${SP1PATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${SP1PATH}/VISA.CONTROL
           echo SECONDARY SPLITTER SPOOL NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
           echo SECONDARY SPLITTER SPOOL NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi
 
        #if [ -f "${SP2PATH}/VISA.CONTROL" ]; then
        #   echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${SP2PATH}/VISA.CONTROL
        #   echo SECONDARY SPLITTER SPOOL NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        #else
        #   echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD SOURCE NAS Not Ready >> ${LOGFILE}
        #   echo SECONDARY SPLITTER SPOOL NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
        #   CMD=D
        #fi

	# Read Control File (U - UP, D - FORCE DOWN)
	if [ "${CMD}" == "U" ] && [ -f "${CTLFILE}" ]; then
	  while read line
	  do
		if [[ ${line:0:1} != "#" ]]; then
			CMD=${line}
		fi
	  done < ${CTLFILE}
	fi
 
        # Monitor Status
        if [ "${CMD}" == "D" ]; then
           echo SECONDARY SPLITTER SERVICE,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
        else
           echo SECONDARY SPLITTER SERVICE,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        fi
 
	# Force DOWN if Process Running
	RUNNING=0
	jobStr=`ps -Alf | grep "VISA.SPLIT.Engine_SEC.sh" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
	if [ -z "${jobStr}" ]; then
	   echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Engine Not Running >> ${LOGFILE}
	   RUNNING=-1
	else
	   if [ "${CMD}" == "D" ]; then
		jobs=( ${jobStr} )
		if [ ${#jobs[@]} -gt 0 ]; then
			echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Engine Force Stop >> ${LOGFILE}
			kill -9 ${jobs[3]}
	   		RUNNING=-1
		fi
	   else
	   	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Engine Running >> ${LOGFILE}
	   fi
        fi
	# Force Start if Not Running in UP Mode
	if [ "${CMD}" == "U" ] && [ ${RUNNING} -lt 0 ]; then
		echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Engine Force Start >> ${LOGFILE}
		${SEFILE} >> ${LOGFILE} 2>&1 &
	fi

	# Force DOWN if Process Running
	RUNNING=0
	jobStr=`ps -Alf | grep "VISA.SPLIT.Merge.sh" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
	if [ -z "${jobStr}" ]; then
	   echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Merge Control Not Running >> ${LOGFILE}
	   RUNNING=-1
	else
	   if [ "${CMD}" == "D" ]; then
		jobs=( ${jobStr} )
		if [ ${#jobs[@]} -gt 0 ]; then
			echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Merge Control Force Stop >> ${LOGFILE}
			kill -9 ${jobs[3]}
	   		RUNNING=-1
		fi
	   else
	   	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Merge Control Running >> ${LOGFILE}
	   fi
        fi
	# Force Start if Not Running in UP Mode
	if [ "${CMD}" == "U" ] && [ ${RUNNING} -lt 0 ]; then
		echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Merge Control Force Start >> ${LOGFILE}
		${MGFILE} >> ${LOGFILE} 2>&1 &
	fi

	# END
	sleep 60
done
exit 0
