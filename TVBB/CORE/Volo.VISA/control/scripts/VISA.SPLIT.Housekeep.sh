#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Split Encode - Houseleeping Control Script
#

while true; do
  LOGFILE='/opt/Volo.VISA/logs/VISA.SPLIT.PurgeCtrl'.`hostname`.`date +%Y%m%d`.'log'

  # Housekeeping Job
  jobStr=`ps -Alf | grep "splithousekeep.php" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
  if [ -z "${jobStr}" ]; then
	cd /opt/Volo.VISA/control/split_encode
	/usr/bin/php ./splithousekeep.php >> ${LOGFILE} 2>&1 &
        PID=$!
        wait $PID
  fi

  #kenny, house keeping /home/Video.Output/*/VOLO_TMP
  php /opt/Volo.VISA/client/helper/housekeeping_Video.Output.php

#  sleep 300
    sleep 60
done
exit 0
