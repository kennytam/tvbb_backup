#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Split Encode - Split Engine Control Script
#
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Housekeep Process Start
cd /dev/shm/Volo.VISA/results
find . -maxdepth 1 -type d -name 'F_*' -mmin +$((60*23)) -exec tar -zcf /opt/Volo.VISA/results/Z_archive/{}.tgz --remove-files {} \;

cd /dev/shm/Volo.VISA/jobs
find . -maxdepth 1 -type d -name 'F_*' -mmin +$((60*23)) -exec tar -zcf /opt/Volo.VISA/results/Z_archive/{}.tgz --remove-files {} \;

cd /dev/shm/Volo.VISA/splitjobs
find . -maxdepth 1 -type d -name 'F_*' -mmin +$((60*23)) -exec tar -zcf /opt/Volo.VISA/splitjobs/Z_archive/{}.tgz --remove-files {} \;

# Housekeep Failed TS
cd /tmp
find /tmp/encode/ -type f -mmin +$((60*23)) -exec rm -rf {} \;

# Server Status
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Check Server Status Start
CTLFILE=/opt/Volo.VISA/VISA_Control/`hostname`.SYSTEM.W
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.WORKER Server Status > ${CTLFILE}
echo [INFOS][Disk Usage] >> ${CTLFILE}
df -kh >> ${CTLFILE}
echo [INFOS][Volo Process] >> ${CTLFILE}
ps -def | grep daolab >> ${CTLFILE}

exit 0
