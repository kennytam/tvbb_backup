#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Scheduler Job Publishing Script (Poll Source Video Path)
#
while true; do
  LOGFILE='/opt/Volo.VISA/logs/VISA.SCHD.UnitCtrl'.`hostname`.`date +%Y%m%d`.'log'

  # Reset Worker Unit
  jobStr=`ps -Alf | grep "splitmanage" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
  if [ -z "${jobStr}" ]; then
	cd /opt/Volo.VISA/control/split_encode
	/usr/bin/php ./splitmanage.php >> ${LOGFILE} 2>&1 &
        PID=$!
        wait $PID
  fi

  sleep 5
done
