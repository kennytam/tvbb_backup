#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Split Encode - Split Engine Control Script
#

cd /opt/Volo.VISA/client/
LOGFILE='/opt/Volo.VISA/logs/VISA.SCHD.PdicCtrl'.`hostname`.`date +%Y%m%d`.'log'
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Periodic Process Start >> ${LOGFILE}
/usr/bin/php ./periodic.php >> ${LOGFILE} 2>&1 &
exit 0
