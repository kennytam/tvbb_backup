#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Scheduler Monitor and Housekeeping Script
#
cd /opt/Volo.VISA/control/scripts
CTLFILE=/opt/Volo.VISA/control/scheduler.ctl
CTLPATH=/opt/Volo.VISA/VISA_Control
SRCPATH=/home/Video.Source/VISA_Control
OUTPATH=/home/Video.Output/VISA_Control
PBFILE=/opt/Volo.VISA/control/scripts/VISA.PUBS.Monitor.sh
UNFILE=/opt/Volo.VISA/control/scripts/VISA.CTRL.Monitor.sh
HSFILE=/opt/Volo.VISA/control/scripts/VISA.SCHD.Housekeep.sh
WKFILE=/opt/Volo.VISA/control/scripts/VISA.PERIOD.Start.sh
 
# Infinite Loop
while true
do
	LOGFILE='/opt/Volo.VISA/logs/VISA.SCHD.ProcMonitor'.`hostname`.`date +%Y%m%d`.'log'
        CMD=U

        # Verify Package/Source/Spool/Output Mount Status
        if [ -f "${SRCPATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${SRCPATH}/VISA.CONTROL
           echo PRIMARY SCHEDULER PACKAGE NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP > ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD ${SRCPATH}/VISA.CONTROL Not Ready >> ${LOGFILE}
           echo PRIMARY SCHEDULER PACKAGE NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN > ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi

        if [ -f "${CTLPATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${CTLPATH}/VISA.CONTROL
           echo PRIMARY SCHEDULER SOURCE NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD ${CTLPATH}/VISA.CONTROL Not Ready >> ${LOGFILE}
           echo PRIMARY SCHEDULER SOURCE NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi

        if [ -f "${OUTPATH}/VISA.CONTROL" ]; then
           echo `date +"%Y-%m-%d %H:%M:%S"` `hostname` > ${OUTPATH}/VISA.CONTROL
           echo PRIMARY SCHEDULER OUTPUT NAS,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        else
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD ${OUTPATH}/VISA.CONTROL Not Ready >> ${LOGFILE}
           echo PRIMARY SCHEDULER OUTPUT NAS,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        fi

        # Scheduler Services Check
        jobStr=`ps -Alf | grep "/usr/pgsql-9.4" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
        if [ -z "${jobStr}" ]; then
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD PGSQL DB Not Running >> ${LOGFILE}
           echo PRIMARY SCHEDULER PGSQL DB,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        else
           echo PRIMARY SCHEDULER PGSQL DB,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        fi
 
        jobStr=`ps -Alf | grep "/usr/sbin/nginx" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
        if [ -z "${jobStr}" ]; then
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD NGINX Not Running >> ${LOGFILE}
           echo PRIMARY SCHEDULER NGINX,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        else
           echo PRIMARY SCHEDULER NGINX,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        fi
 
        jobStr=`ps -Alf | grep "/etc/php-fpm.conf" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
        if [ -z "${jobStr}" ]; then
           echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD PHP-FPM Not Running >> ${LOGFILE}
           echo PRIMARY SCHEDULER PHP-FPM,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
           CMD=D
        else
           echo PRIMARY SCHEDULER PHP-FPM,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        fi

        if [ "${CMD}" == "D" ]; then
          echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Services Not Ready, Force Platform Down >> ${LOGFILE}
          cp /opt/Volo.VISA/control/worker.ctl.DOWN /opt/Volo.VISA/control/worker.ctl
          cp /opt/Volo.VISA/control/splitter.ctl.DOWN /opt/Volo.VISA/control/splitter.ctl
          cp /opt/Volo.VISA/control/scheduler.ctl.DOWN /opt/Volo.VISA/control/scheduler.ctl
        fi

	# Read Control File (U - UP, D - FORCE DOWN)
	if [ "${CMD}" == "U" ] && [ -f "${CTLFILE}" ]; then
	  while read line
	  do
		if [[ ${line:0:1} != "#" ]]; then
			CMD=${line}
		fi
	  done < ${CTLFILE}
	fi
 
	# Monitor Status
        if [ "${CMD}" == "D" ]; then
           echo PRIMARY SCHEDULER SERVICE,`date +"%Y-%m-%d %H:%M:%S"`,DOWN >> ${CTLPATH}/`hostname`.STATUS
        else
           echo PRIMARY SCHEDULER SERVICE,`date +"%Y-%m-%d %H:%M:%S"`,UP >> ${CTLPATH}/`hostname`.STATUS
        fi
	
	# Force DOWN if Periodic Running
	RUNNING=0
	jobStr=`ps -Alf | grep "periodic.php" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
	if [ -z "${jobStr}" ]; then
	   echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Periodic Not Running >> ${LOGFILE}
	   RUNNING=-1
	else
	   if [ "${CMD}" == "D" ]; then
		jobs=( ${jobStr} )
		if [ ${#jobs[@]} -gt 0 ]; then
			# Wait ALL Worker DOWN First
			sleep 60
			echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Periodic Force Stop >> ${LOGFILE}
			kill -9 ${jobs[3]}
	   		RUNNING=-1
		fi
	   else
	   	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Periodic Running >> ${LOGFILE}
	   fi
        fi
	# Start Periodic if UP mode 
	if [ "${CMD}" == "U" ] && [ ${RUNNING} -lt 0 ]; then
		echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Periodic Force Start >> ${LOGFILE}
		${WKFILE} >> ${LOGFILE} 2>&1 &
	fi

	# Force DOWN if Job Publish Monitor Running
	RUNNING=0
	jobStr=`ps -Alf | grep "VISA.PUBS.Monitor.sh" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
	if [ -z "${jobStr}" ]; then
	   echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Job Publish Not Running >> ${LOGFILE}
	   RUNNING=-1
	else
	   if [ "${CMD}" == "D" ]; then
		jobs=( ${jobStr} )
		if [ ${#jobs[@]} -gt 0 ]; then
			echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Job Publish Force Stop >> ${LOGFILE}
			kill -9 ${jobs[3]}
	   		RUNNING=-1
		fi
	   else
	   	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Job Publish Running >> ${LOGFILE}
	   fi
        fi
	# Start Job Publish Monitor if UP mode 
	if [ "${CMD}" == "U" ] && [ ${RUNNING} -lt 0 ]; then
		echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Job Publish Force Start >> ${LOGFILE}
		${PBFILE} >> ${LOGFILE} 2>&1 &
	fi

	# Force DOWN if Unit Control Monitor Running
	# ---- Disable in Split Encode Mode
	#RUNNING=0
	#jobStr=`ps -Alf | grep "VISA.CTRL.Monitor.sh" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
	#if [ -z "${jobStr}" ]; then
	#   echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Unit Control Not Running >> ${LOGFILE}
	#   RUNNING=-1
	#else
	#   if [ "${CMD}" == "D" ]; then
	#	jobs=( ${jobStr} )
	#	if [ ${#jobs[@]} -gt 0 ]; then
	#		echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Unit Control Force Stop >> ${LOGFILE}
	#		kill -9 ${jobs[3]}
	#   		RUNNING=-1
	#	fi
	#   else
	#   	echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Unit Control Running >> ${LOGFILE}
	#   fi
        #fi
	# Start Unit Control Monitor if UP mode 
	if [ "${CMD}" == "U" ] && [ ${RUNNING} -lt 0 ]; then
		echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Unit Control Force Start >> ${LOGFILE}
		${UNFILE} >> ${LOGFILE} 2>&1 &
	fi


	# Housekeeping Job
	NOW=`date +%H%M`
	if [ "${CMD}" == "U" ] && [ "${NOW}" == "0400" ]; then
		echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Housekeeping Start >> ${LOGFILE}
		${HSFILE} >> ${LOGFILE} 2>&1
		echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SCHD Housekeeping End >> ${LOGFILE}
	fi

	sleep 60
done
