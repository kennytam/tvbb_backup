#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
# Volo Split Encode - Encoded Trunk Control Script
#

while true; do
  LOGFILE='/opt/Volo.VISA/logs/VISA.SPLIT.EncodeCtrl'.`hostname`.`date +%Y%m%d`.'log'

  # Ensure Running One Instance Split Job Control
  jobStr=`ps -Alf | grep "splitjobctl.php" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
  if [ -z "${jobStr}" ]; then
	cd /opt/Volo.VISA/control/split_encode
	/usr/bin/php ./splitjobctl.php >> ${LOGFILE} 2>&1 &
        PID=$!
        wait $PID
  fi

  sleep 2
done
exit 0
