#!/bin/bash
#
 
cd /opt/Volo.VISA/scheduler/split_encode
SOURCE=$1
NOOFSPLIT=$2
DEST=$3
 
VOLO_BIN=/opt/volo/bin/volo-1.4.1

${VOLO_BIN} -f concat -i <(for((s=0; s<$NOOFSPLIT; s++)) do echo "file ${SOURCE}_$((s+1)).ts"; done) -map 0:v -vcodec copy -y ${DEST}

exit 0
