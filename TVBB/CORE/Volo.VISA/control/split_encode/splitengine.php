<?php

//
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
// [Split Encode - Source Splitting Engine]
//
$PARA = include('/opt/Volo.VISA/scheduler/config/scheduler.php');

// 
// START HERE
// Split Engine Queue Priority and Working Path
if (!(isset($argv[1]) && isset($argv[2])))
    exit(0);
$curSID = htmlspecialchars($argv[1]);
$chkSID = htmlspecialchars($argv[2]);
if ($curSID == "" || $chkSID == "")
    exit(0);



// Disable 2nd Splitter
# if ( $curSID == "2" ) exit ();
// Current Date/Time
if (function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
    @date_default_timezone_set(@date_default_timezone_get());
$procStart = $stTime = $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

// Skip If Merge Process Running
$jobStr = 'ps -Alf | grep "encode_audio.sh" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1';
$cmd = exec($jobStr);
if (substr($cmd, 0, 1) != "") {
    if ($PARA["schd_debug_mode"]) {
        echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitPROC Split Waiting Mux Audio :: [" . $cmd . "]" . "\n";
    }
    exit();
}

// Connect Scheduler DB
$config = include('/opt/Volo.VISA/scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .
        " user=" . $config['db_user'] .
        " password=" . $config['db_pass'] .
        " dbname=" . $config['db_name'];
$link = pg_connect($connstr)
        or die('Could not connect: ' . pg_last_error());

// Lock Current Splitter
$updsql = pg_exec($link, "UPDATE worker SET mode='L' where id=" . $curSID . ";");
pg_free_result($updsql);
$result = pg_exec($link, "SELECT mode FROM worker WHERE id=" . $chkSID . " FOR UPDATE;");
$state = pg_fetch_array($result);
if ($state["mode"] == "L") {
    pg_free_result($result);
    $updsql = pg_exec($link, "UPDATE worker SET mode='S' where id=" . $curSID . ";");
    pg_free_result($updsql);
    pg_close($link);
    exit(0);
}
pg_free_result($result);

// Check Split Waiting Jobs, Control to 20 Jobs
$result = pg_exec($link, "select count(*) from output where stage in (1) and priority < " . $PARA['volo_split_priority'] . ";");
$state = pg_fetch_array($result);
pg_free_result($result);
if ($state[0] < 100)
    $q_priority = " AND priority > 0";
else
    $q_priority = " AND priority >= " . $PARA['volo_split_priority'];

$result = pg_exec($link, "select count(*) from output where stage in (1) and priority >= " . $PARA['volo_split_priority'] . ";");
$state = pg_fetch_array($result);
pg_free_result($result);
if ($state[0] > 160) {
    $updsql = pg_exec($link, "UPDATE worker SET mode='S' where id=" . $curSID . ";");
    pg_free_result($updsql);
    pg_close($link);
    exit(0);
}

// Parse Encode Jobs
if ($PARA["schd_debug_mode"]) {
    $cmd = "SELECT * FROM output WHERE stage=0 AND worker_id=0 AND split_encode=true AND job_id NOT IN (SELECT job_id FROM job_handler WHERE mode='P') " . $q_priority . " ORDER BY priority DESC, id ASC;";
    echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitPROC Split Query Statement :: [" . $cmd . "]" . "\n";
}
$result = pg_exec($link, "SELECT * FROM output WHERE stage=0 AND worker_id=0 AND split_encode=true AND job_id NOT IN (SELECT job_id FROM job_handler WHERE mode='P') " . $q_priority . " ORDER BY priority DESC, id ASC;");
$ttljob = pg_numrows($result);
$curjobid = 0;
$splitflag = $skipflag = false;
$fpsbase = "1.0";
$framerate = "";
for ($rj = 0; $rj < $ttljob; $rj++) {
    $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
    $ojrow = pg_fetch_array($result, $rj);

    // Get Job Source Details
    if ($curjobid <> $ojrow["job_id"]) {
        // Set volo_spool in Round Robin Mode
        if ($ojrow["job_id"] & 1) {
            $SplitTrunkPath = $PARA['volo_split_path_high'];
            $SplitTrunkJob = $PARA['volo_split_workpath_high'];
            $SplitTrunkOut = $PARA['volo_split_outpath_high'];
        } else {
            $SplitTrunkPath = $PARA['volo_split_path_low'];
            $SplitTrunkJob = $PARA['volo_split_workpath_low'];
            $SplitTrunkOut = $PARA['volo_split_outpath_low'];
        }

        // Hack 11APR, Remove Previous Trunks
        $cmd = "rm -rf " . $PARA["volo_split_path_high"] . "/" . $ojrow["job_id"];
        exec("$cmd > /dev/null 2>&1");
        $cmd = "rm -rf " . $PARA["volo_split_path_low"] . "/" . $ojrow["job_id"];
        exec("$cmd > /dev/null 2>&1");
        $cmd = "rm -rf " . $PARA["volo_split_workpath_high"] . "/" . $ojrow["job_id"];
        exec("$cmd > /dev/null 2>&1");
        $cmd = "rm -rf " . $PARA["volo_split_workpath_low"] . "/" . $ojrow["job_id"];
        exec("$cmd > /dev/null 2>&1");
        $cmd = "rm -rf " . $PARA["volo_split_outpath_high"] . "/" . $ojrow["job_id"];
        exec("$cmd > /dev/null 2>&1");
        $cmd = "rm -rf " . $PARA["volo_split_outpath_low"] . "/" . $ojrow["job_id"];
        exec("$cmd > /dev/null 2>&1");

        // Just Process One Split Output Job
        if ($splitflag)
            $skipflag = true;

        $curjobid = $ojrow["job_id"];
        $jobquery = pg_exec($link, "SELECT * FROM job WHERE id=" . $ojrow["job_id"] . ";");
        $jrow = pg_fetch_array($jobquery);
        pg_free_result($jobquery);

        // Verify the fps, if integer eg. 25fps (base=1.0), else (base=1.001)
        $minfo = explode("\n", $jrow["source_info"]);
        for ($ln = 0; $ln < sizeof($minfo); $ln++) {
            if (substr($minfo[$ln], 0, 12) == "Frame rate  ") {
                $frate = explode(":", $minfo[$ln]);
                $framerate = trim(substr($frate[1], 1, 20)) * 1.0;
                if (round($framerate, 0, PHP_ROUND_HALF_DOWN) != $framerate)
                    $fpsbase = "1.001";
                break;
            }
        }

        // Calculate Split Trunk
        $split_trunk = $PARA['volo_split_interval'] * $PARA['volo_split_node'];
        $split_trunk = floor($jrow['source_duration'] / $split_trunk);

        //kenny hack start, because i use a new split method
        if ($jrow['source_duration'] % $PARA['volo_split_interval'] != 0)
            $split_trunk += 1;
        //kenny hack end;
        // Set Encode Trunk
        if ($ojrow["length"] > 0 && $ojrow["length"] < $split_trunk)
            $encode_trunk = $ojrow["length"];
        else
            $encode_trunk = $split_trunk;

        // Just Process One Split Output Job
        if (!$skipflag) {
            // Spawn Split Process
            if ($split_trunk > $PARA['volo_split_minimum']) {
                $splitflag = true;

                // Skip if Split Video Job Running
                $jobStr = 'ps -Alf | grep "encode_splitvideo.sh" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1';
                $cmd = exec($jobStr);
                if (substr($cmd, 0, 1) != "") {
                    if ($PARA["schd_debug_mode"]) {
                        echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitPROC Split Waiting Video :: [" . $cmd . "]" . "\n";
                    }
                    $skipflag = true;
                }

                // Skip if Split Audio Job Running
                $jobStr = 'ps -Alf | grep "encode_splitaudio.sh" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1';
                $cmd = exec($jobStr);
                if (substr($cmd, 0, 1) != "") {
                    if ($PARA["schd_debug_mode"]) {
                        echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitPROC Split Waiting Audio :: [" . $cmd . "]" . "\n";
                    }
                }

                // Check Duplicate Split Job due to Job Publishing Delay - 01DEC
                if (!$skipflag) {
                    $jobquery = pg_exec($link, "SELECT count(*) FROM output WHERE stage=1 AND split_encode=true AND job_id=" . $ojrow["job_id"] . ";");
                    $jobrun = pg_fetch_array($jobquery);
                    pg_free_result($jobquery);

                    if ($jobrun[0] > 0) {
                        echo "[WARNS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPROC Skip Split Source :: JOB ID[" . $ojrow["job_id"] . "]  OUTPUT ID[" . $ojrow["id"] . "]" . "\n";
                    } else {
                        echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPROC Split Source :: JOB ID[" . $ojrow["job_id"] . "] SOURCE FILE[" . $jrow["source"] . "] SOURCE DURATION[" . $jrow['source_duration'] . "] FRAME RATE[" . $framerate . "] FRAME BASE [" . $fpsbase . "] TRUNKS[" . $split_trunk . "]" . " ENCODE TRUNKS[" . $encode_trunk . "]" . "\n";
                        $cmd = $PARA["volo_split_cmd"] . " " . $ojrow["job_id"] . " " . $ojrow["id"] . " " . $jrow["source"] . " " . $split_trunk . " " . $encode_trunk . " " . $SplitTrunkPath . "/SP_" . $ojrow["job_id"] . " SPTS_" . $ojrow["job_id"] . " " . $jrow['source_duration'] . " " . $fpsbase;
                        echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPROC Split Command :: " . $cmd . "\n";
                        exec("$cmd > /dev/null 2>&1 &");
                    }
                } // End SkipFlag
            }
        } // End SplitFlag 
    }

    //kenny, no normal encode
//    if ($split_trunk < ($PARA['volo_split_minimum'] + 1)) {
    if (false) {
        // Force Normal Encode
        $updsql = pg_exec($link, "UPDATE output SET split_encode=false, priority=" . ($ojrow['priority'] + 10) . " WHERE id=" . $ojrow["id"] . ";");
        echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPROC Reset to High Prority Encode Job :: JOB ID[" . $ojrow["job_id"] . "] OUTPUT ID[" . $ojrow["id"] . "] PRIORITY[" . ($ojrow['priority'] + 10) . "]" . "\n";
        pg_free_result($updsql);
    } else {
        if (!$skipflag) {
            // Update Job Output
            $updsql = pg_exec($link, "UPDATE output SET stage=1, worker_id=" . $curSID . ", progress=1, stime=now(), ltime=now(), merge_stime=now(), merge_ltime=now(), mux_stime=now(), mux_ltime=now(), message='[STATUS] Creating Split Encode Trunks' WHERE id=" . $ojrow["id"] . ";");
            pg_free_result($updsql);

            //kenny hack start, add split audio job
            //reduce the job unit for no encode job
            $profile_path = $PARA['volo_encode_profile_path'] . '/' . basename(dirname($jrow["source"]));
            $job_unit = file_exists($profile_path . "/NOENCODE") ? $PARA["volo_noencode_job_unit"] : $ojrow["encode_unit"];

            if (!file_exists($profile_path . "/NOENCODE")) {
//            if (!file_exists(dirname($jrow["source"]) . "/NOENCODE")) {
                // Insert the Split Output Video&Audio Job

                $ii = $encode_trunk + 1;
//                $updsql = pg_exec($link, "INSERT INTO split_output (job_id, output_id, split_srcpath, split_srcfile, split_destpath, split_destfile, trunk_total, trunk_number, encode_unit, worker_id, priority, message) VALUES ('" . $ojrow["job_id"] . "', '" . $ojrow["id"] . "', '" . $SplitTrunkPath . "/SP_" . $ojrow["job_id"] . "', 'SPTS_" . $ojrow["job_id"] . "_" . $ii . ".ts', '" . $SplitTrunkJob . "/SP_" . $ojrow["job_id"] . "', 'SPTS_" . $ojrow["job_id"] . "_" . $ojrow["id"] . "_" . $ii . ".mxf', '" . ($encode_trunk) . "', '" . $ii . "', '" . $job_unit . "', '-1', '" . $ojrow['priority'] . "', '[STATUS] Creating Split Video Trunk');");
                $updsql = pg_exec($link, "INSERT INTO split_output (job_id, output_id, split_srcpath, split_srcfile, split_destpath, split_destfile, trunk_total, trunk_number, encode_unit, worker_id, priority, trunk_type, message) VALUES ('" . $ojrow["job_id"] . "', '" . $ojrow["id"] . "', '" . $SplitTrunkPath . "/SP_" . $ojrow["job_id"] . "', 'SPTS_" . $ojrow["job_id"] . "_audio.mov', '" . $SplitTrunkJob . "/SP_" . $ojrow["job_id"] . "', 'SPTS_" . $ojrow["job_id"] . "_" . $ojrow["id"] . "_audio.mov', '" . $ii . "', '" . $ii . "', '20', '-1', '" . $ojrow['priority'] . "', 'A', '[STATUS] Creating Split Audio Trunk');");
                pg_free_result($updsql);

                for ($ii = 1; $ii <= $encode_trunk; $ii++) {
                    $updsql = pg_exec($link, "INSERT INTO split_output (job_id, output_id, split_srcpath, split_srcfile, split_destpath, split_destfile, trunk_total, trunk_number, encode_unit, worker_id, priority, message) VALUES ('" . $ojrow["job_id"] . "', '" . $ojrow["id"] . "', '" . $SplitTrunkPath . "/SP_" . $ojrow["job_id"] . "', 'SPTS_" . $ojrow["job_id"] . "_" . $ii . ".ts', '" . $SplitTrunkJob . "/SP_" . $ojrow["job_id"] . "', 'SPTS_" . $ojrow["job_id"] . "_" . $ojrow["id"] . "_" . $ii . ".mxf', '" . ($encode_trunk + 1) . "', '" . $ii . "', '" . $job_unit . "', '-1', '" . $ojrow['priority'] . "', '[STATUS] Creating Split Video Trunk');");
                    pg_free_result($updsql);
                }
            } else {
                //only one audio
                $ii = 1;
                $updsql = pg_exec($link, "INSERT INTO split_output (job_id, output_id, split_srcpath, split_srcfile, split_destpath, split_destfile, trunk_total, trunk_number, encode_unit, worker_id, priority, message) VALUES ('" . $ojrow["job_id"] . "', '" . $ojrow["id"] . "', '" . $SplitTrunkPath . "/SP_" . $ojrow["job_id"] . "', 'SPTS_" . $ojrow["job_id"] . "_" . $ii . ".ts', '" . $SplitTrunkJob . "/SP_" . $ojrow["job_id"] . "', 'SPTS_" . $ojrow["job_id"] . "_" . $ojrow["id"] . "_" . $ii . ".mxf', '" . 1 . "', '" . $ii . "', '" . $job_unit . "', '-1', '" . $ojrow['priority'] . "', '[STATUS] Creating Split Audio Trunk');");
                pg_free_result($updsql);
            }
            //kenny hack end
        }
    } // end process split
} // end parse split output 
pg_free_result($result);

// Free Splitter
$updsql = pg_exec($link, "UPDATE worker SET mode='S' where id=" . $curSID . ";");
pg_free_result($updsql);

// END
if ($PARA["schd_debug_mode"]) {
    $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
    echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitPROC Process Done :: TIME[" . $procStart . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime, 0, 19)) - strtotime(substr($procStart, 0, 19)) ) . ")\n";
}
pg_close($link);
exit(0);
?>
