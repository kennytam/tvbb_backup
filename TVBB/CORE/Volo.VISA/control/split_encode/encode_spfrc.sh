#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
# Volo Split Encoder :: Scripts to Split Video Source with Framerate Conversion
# "USAGE: encode_spfrc.sh [JOB ID] [OUTPUT ID] [SOURCE FULL PATH] [NO. OF SPLIT] [NO. OF ENCODE] [SPLIT WORK FULL PATH] [SPLIT FILE PREFIX] [SOURCE DURATION] [DURATION FRAME BASE] [FRAME RATE] [SPLIT INTERVAL]"
# eg. ./encode_spfrc.sh 4077 29419 /media/volo_source/samples/BAD_GUYS/BAD_GUYS_EP01.mpg 57 57 /media/volo_spool/Split_Encode/IN_Trunk/SP_4077 SPTS_4077 3470 1.001 25 60
#
## CMD ::
## /opt/volo/bin/FRC -i /mnt/spretrans/Samples/benchmark_5min.mxf -path /tmp/encode/ -fps 25 -video_th 1 -split 60 -deint 1
## /opt/volo/script/volo -label SPLIT -f /tmp/encode/1.sf -o /tmp/encode/1.ts -workpath /dev/shm/Volo.VISA/splitjobs/F_37 -do CTS_360p.xml -vsync 0 -container ts -audio OFF -stitchable Y -threads 1
#
 
round()
{
echo "$1" | awk '{printf("%d\n",$1 + 0.5)}'
};

divide()
{
echo "$1" "$2" | awk '{printf("%f\n",$1/$2)}'
};

multiply()
{
echo "$1" "$2" | awk '{printf("%f\n",$1*$2)}'
};

cd /opt/Volo.VISA/control/split_encode
JID=$1
OID=$2
SOURCE=$3
NOOFSPLIT=$4
NOOFENCODE=$5
DEST=$6
OUTFILEPREFIX=$7
DURSOURCE=$8
DURBASE=$9
DESTFR=${10}
DURSPLIT=${11}
VTRACK=1
DEINT=1
 
VOLO_BIN=/opt/volo/bin/volo-1.4.1
AUDIO_BIN=/opt/Volo.VISA/control/split_encode/encode_splitaacaudio.sh
VIDEO_BIN=/opt/volo/bin/voloFRC
JOB_PATH=/dev/shm/Volo.VISA/splitjobs/SP_${JID}/split
JOB_LOG='/opt/Volo.VISA/logs/VISA.SplitSource'.`hostname`.`date +%Y%m%d`.'log'
ERRSTR=""

echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Job Submit :: JID[${1}] OID[${2}] SOURCE[${3}] TRUNK[${4}] ENCODE TRUNK[${5}] DEST[${6}] PREFIX[${7}] SOURCE DURATION[${8}] DURATION BASE[${9}] FRAME RATE[${10}] SPLIT SEC[${11}]>> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Command :: ./encode_spfrc.sh ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} ${10} ${11} >> ${JOB_LOG}
 
# Source Not Exist
if [ ! -f "${SOURCE}" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Job Error :: JOB[${JID}] SOURCE[${SOURCE}] Not Found >> ${JOB_LOG}
        ERRSTR=" Split Source Job Error :: Source Video Not Found"
fi

# Create Encode Working Path
if [ ! -d "${JOB_PATH}" ]; then
	mkdir -p ${JOB_PATH}
else
	rm -rf ${JOB_PATH}/*
fi

# Mark Job ERROR
if [ ! -z "${ERRSTR}" ]; then
	echo ${ERRSTR} > ${JOB_PATH}/encode.log
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

EXT=${SOURCE##*.}
if [ "$EXT" == "jpg" ] || [ "$EXT" == "txt" ]; then
	echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Error :: JOB[${JID}] Invalid Source Video Format >> ${JOB_LOG}
	ERRSTR=" Split Source Job Error :: Invalid Source Video Format"
	echo ${ERRSTR} > ${JOB_PATH}/encode.log
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

# Create or Purge Split Working Path
if [ ! -d "${DEST}" ]; then
	mkdir -p ${DEST}
# Disabled rm due to DB delay BUG -- 01DEC2015
#else
#	rm -rf ${DEST}/*
fi

# Split Start
echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.start
echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode_video.start
echo "Split Encode Video :: "`date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode_video.log
echo "Split Encode Start :: "`date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode.log
echo "No. of Encode / Split = ${NOOFENCODE} / ${NOOFSPLIT}" >> ${JOB_PATH}/encode.log
echo "Source Duration       = ${DURSOURCE}" >> ${JOB_PATH}/encode.log
echo "Source Frame Base     = ${DURBASE}" >> ${JOB_PATH}/encode.log
echo "Convert Frame Rate    = ${DESTFR}" >> ${JOB_PATH}/encode.log
echo "Split Trunk Duration  = ${DURSPLIT}" >> ${JOB_PATH}/encode.log

N_THREAD=$(less /proc/cpuinfo | grep processor | wc -l)

# Split Audio in Background
echo [SPLIT AUDIO CMD]:: ${VOLO_BIN} -label AUDIO.F_${JID} -i ${SOURCE} -vn -map 0:a -acodec libfdk_aac -ab 192k -threads ${N_THREAD} -y ${DEST}/${OUTFILEPREFIX}_audio.mov >> ${JOB_PATH}/encode.log
${AUDIO_BIN} ${JID} ${SOURCE} ${DEST}/${OUTFILEPREFIX}_audio.mov ${JOB_PATH} ${N_THREAD} 2>&1 &

# Split Video in Foreground
echo [SPLIT VIDEO CMD]:: ${VIDEO_BIN} -i ${SOURCE} -path ${DEST} -fps ${DESTFR} -video_th ${VTRACK} -split ${DURSPLIT} -deint ${DEINT} >> ${JOB_PATH}/encode.log
${VIDEO_BIN} -i ${SOURCE} -path ${DEST} -fps ${DESTFR} -video_th ${VTRACK} -split ${DURSPLIT} -deint ${DEINT} >> ${JOB_PATH}/encode_video.log 2>&1

# Exit for Split Trunk Error / Cancel
if [ -f "${JOB_PATH}/job.error" ] || [ -f "${JOB_PATH}/job.cancel" ]
then
	exit 1;
fi

# Normal Exit
echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode_video.done
echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.done
exit 0
