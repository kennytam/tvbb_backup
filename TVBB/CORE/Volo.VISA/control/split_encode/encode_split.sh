#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
# Volo Split Encoder :: Scripts to Split Video Source
# "USAGE: encode_split.sh [JOB ID] [OUTPUT ID] [SOURCE FULL PATH] [NO. OF SPLIT] [NO. OF ENCODE] [SPLIT WORK FULL PATH] [SPLIT FILE PREFIX] [SOURCE DURATION] [DURATION FRAME BASE]"
# eg. ./encode_split.sh 4077 29419 /media/volo_source/samples/BAD_GUYS/BAD_GUYS_EP01.mpg 57 57 /media/volo_spool/Split_Encode/IN_Trunk/SP_4077 SPTS_4077 3470 1.001
#
 
round()
{
echo "$1" | awk '{printf("%d\n",$1 + 0.5)}'
};

divide()
{
echo "$1" "$2" | awk '{printf("%f\n",$1/$2)}'
};

multiply()
{
echo "$1" "$2" | awk '{printf("%f\n",$1*$2)}'
};

cd /opt/Volo.VISA/control/split_encode
JID=$1
OID=$2
SOURCE=$3
NOOFSPLIT=$4
NOOFENCODE=$5
DEST=$6
OUTFILEPREFIX=$7
DURSOURCE=$8
DURBASE=$9
 
VOLO_BIN=/opt/volo/bin/volo-1.4.1
AUDIO_BIN=/opt/Volo.VISA/control/split_encode/encode_splitaudio.sh
VIDEO_BIN=/opt/Volo.VISA/control/split_encode/encode_splitvideo.sh
JOB_PATH=/dev/shm/Volo.VISA/splitjobs/SP_${JID}/split
JOB_LOG='/opt/Volo.VISA/logs/VISA.SplitSource'.`hostname`.`date +%Y%m%d`.'log'
ERRSTR=""
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Job Submit :: JID[${1}] OID[${2}] SOURCE[${3}] TRUNK[${4}] ENCODE TRUNK[${5}] DEST[${6}] PREFIX[${7}] SOURCE DURATION[${8}] DURATION BASE[${9}] >> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Command :: ./encode_split.sh ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} >> ${JOB_LOG}
 
# Source Not Exist
if [ ! -f "${SOURCE}" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Job Error :: JOB[${JID}] SOURCE[${SOURCE}] Not Found >> ${JOB_LOG}
        ERRSTR="[ERROR] Split Source Job Error :: Source Video Not Found"
fi
 
# Create Encode Working Path
if [ ! -d "${JOB_PATH}" ]; then
	mkdir -p ${JOB_PATH}
else
	rm -rf ${JOB_PATH}/*
fi

# Mark Job ERROR
if [ ! -z "${ERRSTR}" ]; then
	echo ${ERRSTR} > ${JOB_PATH}/encode.log
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

EXT=${SOURCE##*.}
if [ "$EXT" == "jpg" ] || [ "$EXT" == "txt" ]; then
	echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Error :: JOB[${JID}] Invalid Source Video Format >> ${JOB_LOG}
	ERRSTR="[ERROR] Split Source Job Error :: Invalid Source Video Format"
	echo ${ERRSTR} > ${JOB_PATH}/encode.log
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

# Create or Purge Split Working Path
if [ ! -d "${DEST}" ]; then
	mkdir -p ${DEST}
# Disabled rm due to DB delay BUG -- 01DEC2015
#else
#	rm -rf ${DEST}/*
fi

# Parse the Video Duration by MediaInfo
echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.start
echo "Split Encode Start :: "`date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode.log

# Split with Duration
echo "No. of Encode / Split = ${NOOFENCODE} / ${NOOFSPLIT}" >> ${JOB_PATH}/encode.log
echo "Split Total Duration = ${DURSOURCE}" >> ${JOB_PATH}/encode.log
duration=$(round $(divide ${DURSOURCE} $NOOFSPLIT))
echo "Split Duration = $duration" >> ${JOB_PATH}/encode.log
echo "Frame Base = $DURBASE" >> ${JOB_PATH}/encode.log
duration=`expr "${duration}"\*"${DURBASE}" | bc`
echo "Adjusted Split Duration = $duration" >> ${JOB_PATH}/encode.log

if [ $NOOFENCODE -lt 5 ]
then
	maxproc=$NOOFENCODE
else
	maxproc=5
fi
NN_THREAD=$(less /proc/cpuinfo | grep processor | wc -l)
N_THREAD=1
[ $maxproc -lt $NN_THREAD ] && N_THREAD=$(($NN_THREAD/$maxproc))

maxproc=1
for((s=0; s<=${NOOFSPLIT}; s++))
do
	# Break for No of Encode Trunk
        if [ $s -eq ${NOOFENCODE} ]
	then
		break;
	fi 

	# Break for Split Trunk Error / Cancel
	if [ -f "${JOB_PATH}/job.error" ] || [ -f "${JOB_PATH}/job.cancel" ]
	then
		exit 1;
	fi

        if [ $s -eq 0 ]
        then
                j1="0"
                j2="0"

        else
                # j1="$((duration*s-1))"
		j1=`expr "${duration}"\*"${s}"-"1" | bc`
		j2="1"

        fi

        if [ $((s+1)) -eq ${NOOFSPLIT} ]
        then
                d="0"

        else
                d="$duration"

        fi

	OUTFILE="${OUTFILEPREFIX}_$((s+1)).ts"

#kenny hack start, no need to split

#        if [ $s -eq 0 ]
#        then
#		echo `date +"%Y-%m-%d %H:%M:%S"` [SPLIT AUDIO CMD] :: ${VOLO_BIN} -label AUDIO.F_${JID} -i ${SOURCE} -vn -map 0:a -acodec libfdk_aac -ab 192k -threads ${N_THREAD} -y ${DEST}/${OUTFILEPREFIX}_audio.mov >> ${JOB_PATH}/encode.log
#
#		## ${AUDIO_BIN} ${JID} ${SOURCE} ${DEST}/${OUTFILEPREFIX}_audio.mov ${JOB_PATH} ${N_THREAD} 2>&1 &
#		${AUDIO_BIN} ${JID} ${SOURCE} ${DEST}/${OUTFILEPREFIX}_audio.mov ${JOB_PATH} 12 2>&1 &
#	fi
#	echo `date +"%Y-%m-%d %H:%M:%S"` [SPLIT VIDEO CMD] :: ${VIDEO_BIN} ${JID} $((s+1)) $j1 $j2 $d ${SOURCE} ${DEST}/${OUTFILE} ${JOB_PATH} ${N_THREAD} >> ${JOB_PATH}/encode.log
#
#	maxproc=$((maxproc+1))
#
#	( ${VIDEO_BIN} ${JID} $((s+1)) $j1 $j2 $d ${SOURCE} ${DEST}/${OUTFILE} ${JOB_PATH} ${N_THREAD} 2>&1 ) &
#	PID=$!

        if [ $s -eq 0 ]
        then
            #kenny, create split empty trunk file 
            touch ${DEST}/${OUTFILEPREFIX}_audio.mov
            echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode_audio.done

            #kenny, move config file
            CONFIG_PATH="/home/Video.Profile"
            PROFILE_PATH=${CONFIG_PATH}/$(basename $(dirname ${SOURCE}))/

            if [ -d ${PROFILE_PATH} ]; then
                cp ${PROFILE_PATH}/* ${DEST}
            else
                cp ${CONFIG_PATH}/DEFAULT/* ${DEST}
            fi

            #kenny write info
            echo ${DURSOURCE} > ${DEST}/DURSOURCE
            echo ${DURBASE} > ${DEST}/DURBASE
	fi

	echo `date +"%Y-%m-%d %H:%M:%S"` [SPLIT VIDEO CMD] :: NO NEED TO SPLIT NOW, ONLY CREATE A EMTPY FILE SRC: ${SOURCE} EMPTY FILE: ${DEST}/${OUTFILE} JOB PATH: ${JOB_PATH} >> ${JOB_PATH}/encode.log

	maxproc=$((maxproc+1))
        
        echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode_video_$((s+1)).done
        touch ${DEST}/${OUTFILE} 
        echo ${SOURCE} > ${DEST}/SOURCE.txt  
	PID=$!

#kenny hack end


	# Wait if Split Process > 5
	if [ $maxproc -eq 6 ]
	then
		maxproc=1
		wait $PID
	fi
done

echo "Split Encode End :: "`date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/encode.log
echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.done

exit 0
