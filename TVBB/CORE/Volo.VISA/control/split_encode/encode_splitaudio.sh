#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
# Volo Split Encoder :: Scripts to Split Audio Track from Video Source
# "USAGE: encode_splitaudio.sh [JID] [SOURCE FULL PATH] [SPLIT AUDIO FILE FULL PATH] [JOB FULL PATH] [THREAD CONTROL]"
#
 
cd /opt/Volo.VISA/control/split_encode
JID=$1
SOURCE=$2
DEST=$3
JOB_PATH=$4
N_THREAD=$5

VOLO_BIN=/opt/volo/bin/volo-1.4.1
VOLO_PROBE=/opt/volo/bin/vprobe
WORKFILE=${DEST}_TEMP.mov

echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode_audio.start
echo "Split Audio Start :: "`date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode_audio.log

echo ${VOLO_BIN} -label SPLIT.AUDIO_${JID} -i ${SOURCE} -vn -map 0:a -acodec copy -y ${WORKFILE} >> ${JOB_PATH}/encode_audio.log
${VOLO_BIN} -label SPLIT.AUDIO_${JID} -i ${SOURCE} -vn -map 0:a -acodec copy -y ${WORKFILE} >> ${JOB_PATH}/encode_audio.log 2>&1

if [ -f "${WORKFILE}" ]
then
	mv ${WORKFILE} ${DEST}
	sleep 1
	echo "Split Audio End :: "`date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/encode_audio.log
	echo "Destination Audio Information :: " >> ${JOB_PATH}/encode_audio.log
	ls -lh --full-time ${DEST} >> ${JOB_PATH}/encode_audio.log 2>&1
	${VOLO_PROBE} -i ${DEST} >> ${JOB_PATH}/encode_audio.log 2>&1
	echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode_audio.done
else
	echo "Split Audio Fail :: "`date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/encode_audio.log
	echo "Split Audio Error" > ${JOB_PATH}/encode_audio.error
	echo "Split Audio Error" > ${JOB_PATH}/job.error
fi

exit 0
