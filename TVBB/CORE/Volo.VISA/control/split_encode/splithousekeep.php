<?php
//
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
// [Split Encode Housekeeping Control Engine]
//
$PARA = include('/opt/Volo.VISA/scheduler/config/scheduler.php');
// Input Parameter -- FORCE to skip Run Time Check
if (isset($argv[1]) && $argv[1] == "FORCE")
        $checkHS=false;
else {
        $checkHS=true;
}
$checkHS = false;

// START HERE

// Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$procStart=$stTime=$curTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

// Connect Scheduler DB
$config = include('/opt/Volo.VISA/scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .
                             " user=". $config['db_user'] .
                             " password=". $config['db_pass'] .
                             " dbname=".$config['db_name'];
$link = pg_connect($connstr)
	or die('Could not connect: ' . pg_last_error());

// Stop if Split Job Running
/*
$jobStr='ps -Alf | grep "volo" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1';
$cmd = exec($jobStr);
if (substr($cmd, 0, 1) != ""){
        exit(0);
}
*/

// Run only every hour > 56 minute -- Assume Control Script Sleep in 5 Mins
$runTime = date("H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
if ($checkHS){
        $curTime = explode(":", $runTime);
        if ( ($curTime[1] > "00" && $curTime[1] < "15" ) || ( $curTime[1] > "20" && $curTime[1] < "35" ) || ($curTime[1] > "40" && $curTime[1] < "55" ) ){
		if ($PARA["schd_debug_mode"]){
                    $runTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
                    echo "[DEBUG][" . $runTime . "][" . gethostname() . "] VOLO.SplitPURGE Split Encode Job Housekeep Skip......" . "\n";
		}
                exit(0);
        }
}

// Housekeep Completed Job Handler
$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Split Encode Job Housekeep ........" . "\n";
$jobList=$job=array();
foreach(glob($PARA["volo_splitjob_work"] . "/SP_*") as $folder) {
	if(is_dir($folder)) $jobList[] = basename($folder);
}
foreach($jobList as $job) {
  $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
  $jobid = (int) substr($job, 3) * 1;
  $jobERR = $jobCNX = false;

  // All Output Job Done
  if ($jobid > 0){
	if ($checkHS)
		$jobquery = pg_exec($link, "SELECT count(*) FROM output WHERE stage IN (-1,0,1) AND c_cancel=false AND job_id=" . $jobid . ";");
	else
		$jobquery = pg_exec($link, "SELECT count(*) FROM output WHERE stage IN (0,1) AND c_cancel=false AND job_id=" . $jobid . ";");

	$jobrun = pg_fetch_array($jobquery);
	pg_free_result($jobquery);
  }
  if ($jobid > 0 && $jobrun[0] == 0){
    $jobpath=$PARA["volo_splitjob_work"] . "/" . $job. "/split/job.done";
    if ( !file_exists($jobpath) ){
	$jobpath=$PARA["volo_splitjob_work"] . "/" . $job. "/split/job.error";
    	if ( !file_exists($jobpath) )
		$jobpath=$PARA["volo_splitjob_work"] . "/" . $job. "/split/job.cancel";
    }

    //kenny, change 15 mins to 
//    if ( file_exists($jobpath) && ( time() - filemtime($jobpath) > 900) ) {
    if ( file_exists($jobpath) && ( time() - filemtime($jobpath) > 60) ) {
      $jobquery = pg_exec($link, "SELECT job_id, id, stage FROM output WHERE job_id=" . $jobid . " ORDER BY id ASC;");
      if (pg_numrows($jobquery) > 0){
	while ($jobdtl = pg_fetch_array($jobquery)){
		if ($jobdtl["stage"] < 0){
			$jobERR = true;
			break;
		} else if ($jobdtl["stage"] > 2){
			$jobCNX = true;
			break;
		}
	}
	pg_free_result($jobquery);

	// Housekeep Last Error Split Job
	if ($jobERR)
		$jobpath=$PARA["volo_splitjob_done"] . "/" . $job. ".error";
	else if ($jobCNX)
		$jobpath=$PARA["volo_splitjob_done"] . "/" . $job. ".cancel";
	else
		$jobpath=$PARA["volo_splitjob_done"] . "/" . $job. ".done";

	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Housekeep Job :: JOBID[" . $jobid . "] PATH[" . $jobpath . "]" . "\n";

	if ( is_dir($jobpath) ){
		$cmd="mv -f " . $jobpath . " " . $jobpath . "-retry." . date("YmdHis", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
		exec("$cmd > /dev/null 2>&1");
	}
	$cmd="mv -f " . $PARA["volo_splitjob_work"] . "/" . $job . " " . $jobpath;
	exec("$cmd > /dev/null 2>&1");

	// Housekeep Trunk Files
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Purge Work Trunk :: JOBID[" . $jobid . "]" . "\n";
	$cmd="rm -rf " . $PARA["volo_split_path_high"] . "/" . $job;
	exec("$cmd > /dev/null 2>&1");
	$cmd="rm -rf " . $PARA["volo_split_path_low"] . "/" . $job;
	exec("$cmd > /dev/null 2>&1");
	$cmd="rm -rf " . $PARA["volo_split_workpath_high"] . "/" . $job;
	exec("$cmd > /dev/null 2>&1");
	$cmd="rm -rf " . $PARA["volo_split_workpath_low"] . "/" . $job;
	exec("$cmd > /dev/null 2>&1");
	$cmd="rm -rf " . $PARA["volo_split_outpath_high"] . "/" . $job;
	exec("$cmd > /dev/null 2>&1");
	$cmd="rm -rf " . $PARA["volo_split_outpath_low"] . "/" . $job;
	exec("$cmd > /dev/null 2>&1");
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Purge Work Trunk Done" . "\n";

	$jobquery = pg_exec($link, "SELECT id, output_id FROM split_output WHERE job_id=" . $jobid . " ORDER BY id ASC;");
	while ($jobdtl = pg_fetch_array($jobquery)){
		$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
		// Destination /opt/Volo.VISA/splitjobs/SP_3865/merge/28824/
		if (!is_dir($jobpath . "/merge/" . $jobdtl["output_id"])){
			$cmd="mkdir -p " . $jobpath . "/merge/" . $jobdtl["output_id"];
			echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Create Merge Path :: JOBID[" . $jobid . "] CMD[" . $cmd . "]" . "\n";
	   		exec("$cmd > /dev/null 2>&1");
		}
		if ($PARA["schd_debug_mode"]){
			echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Move Split Result :: OID[" . $jobdtl["output_id"] . "] SID[" . $jobdtl["id"] . "]" . "\n";
		}
		$cmd="mv -f " . $PARA["volo_splitjob_done"] . "/encode/F_" . $jobdtl["id"] . ".* " . $jobpath . "/merge/" . $jobdtl["output_id"] . "/";
		exec("$cmd > /dev/null 2>&1");
	}
      } // End Job Exists
      pg_free_result($jobquery);
    } // Exist Job Done Path
  } // End JobRun
} // End Loop Job Directory

if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Split Encode Folder Housekeep End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
}


// Housekeep ALL Job Done Handler
echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Split Encode Folder Housekeep ........" . "\n";
$jobList=$job=array();
foreach(glob($PARA["volo_splitjob_done"] . "/encode/F_*") as $folder) {
	if(is_dir($folder)) $jobList[] = basename($folder);
}
foreach($jobList as $job) {
 $jobpath=$PARA["volo_splitjob_done"] . "/encode/" . $job. "/job.done";
 if ( !file_exists($jobpath) ){
	$jobpath=$PARA["volo_splitjob_done"] . "/encode/" . $job. "/job.error";
    	if ( !file_exists($jobpath) )
		$jobpath=$PARA["volo_splitjob_done"] . "/encode/" . $job. "/job.cancel";
 }

 if ( file_exists($jobpath) && ( time() - filemtime($jobpath) > 7200) ) {
   // Determine the split ID - F_39530.volo21.encoder.hk1.tvb.com.20160126034813.done
   $jobname = explode(".", $job);
   $spid = substr($jobname[0], 2);
   $jobERR = $jobCNX = false;
   $jobquery = pg_exec($link, "SELECT sp.job_id, sp.output_id, out.stage FROM split_output sp, output out WHERE out.stage NOT IN (-1,0,1) AND sp.id=" . $spid . " AND sp.output_id=out.id;");
   if (pg_numrows($jobquery) > 0){
     while ($jobdtl = pg_fetch_array($jobquery)){
	  if ($jobdtl["stage"] < 0)
		$jobERR = true;
	  else if ($jobdtl["stage"] > 2)
		$jobCNX = true;

	// Housekeep Last Error Split Job
	if ($jobERR)
		$jobpath=$PARA["volo_splitjob_done"] . "/SP_" . $jobdtl["job_id"] . ".error";
	else if ($jobCNX)
		$jobpath=$PARA["volo_splitjob_done"] . "/SP_" . $jobdtl["job_id"] . ".cancel";
	else
		$jobpath=$PARA["volo_splitjob_done"] . "/SP_" . $jobdtl["job_id"] . ".done";

	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Move Split Result :: SPLIT ID[" . $spid . "] PATH[" . $jobpath . "]" . "\n";
	// Destination /opt/Volo.VISA/splitjobs/SP_3865/merge/28824/
	if (!is_dir($jobpath . "/merge/" . $jobdtl["output_id"])){
			$cmd="mkdir -p " . $jobpath . "/merge/" . $jobdtl["output_id"];
			echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Create Merge Path :: JOBID[" . $jobdtl["job_id"] . "] CMD[" . $cmd . "]" . "\n";
	   		exec("$cmd > /dev/null 2>&1");
	}
	$cmd="mv -f " . $PARA["volo_splitjob_done"] . "/encode/" . $job . " " . $jobpath . "/merge/" . $jobdtl["output_id"] . "/";
	exec("$cmd > /dev/null 2>&1");
     } // End while
   } else {
	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Unknown Trunk Result :: TRUNK[" . $job . "]" . "\n";
	$cmd="mv -f " . $PARA["volo_splitjob_done"] . "/encode/" . $job . " " . $PARA["volo_splitjob_done"] . "/SPO_Unknown/";
	exec("$cmd > /dev/null 2>&1");
   }
   pg_free_result($jobquery);
 } // End Exists Enocde.DONE Path
} // End Loop Encode Directory

if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Split Encode Folder Housekeep End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitPURGE Process Done :: TIME[" . $procStart . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($procStart,0,19)) ) . ")\n";
}

// End Process
pg_close($link);
exit(0);
?>
