<?php
//
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
// [Split Encode Error Control Engine]
//
$PARA = include('/opt/Volo.VISA/scheduler/config/scheduler.php');

// 
// START HERE

// Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$procStart=$stTime=$curTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

// Connect Scheduler DB
$config = include('/opt/Volo.VISA/scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .
                             " user=". $config['db_user'] .
                             " password=". $config['db_pass'] .
                             " dbname=".$config['db_name'];
$link = pg_connect($connstr)
	or die('Could not connect: ' . pg_last_error());


// Get Output Info for Normal Encode Jobs
$SRCPATH  = "/opt/Volo.VISA/results/NE_*/F_*.done";
$srcList  = explode("\n", shell_exec("find " . $SRCPATH . " -name '*.minfo' -print"));
foreach($srcList as $job) {
   if (trim($job) == "") continue;
   $jobInfo    = pathinfo($job);
   $fullPath   = $jobInfo['dirname'];
   $outID      = $jobInfo['filename'];
   $outInfo    = pg_escape_string(file_get_contents($job));
   $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
   echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitUNIT Normal Encode Output Mediainfo Update :: OID[" . $outID . "] FILE[" . $job . "]\n";
   $updsql = pg_exec($link, "UPDATE output SET output_info='" . $outInfo . "' WHERE id=" . $outID . ";");
   pg_free_result($updsql);
   $cmd = "mv -f " . $job . " " . $job . ".done";
   exec("$cmd > /dev/null 2>&1 &");
} // End Parse Result Directory
 

// Split Trunk Error Handler
$jobquery = pg_exec($link, "SELECT DISTINCT job_id FROM output WHERE stage=1 AND c_cancel=false AND split_encode=true AND job_id NOT IN (SELECT job_id FROM job_handler WHERE mode='P') ORDER BY job_id ASC;");
while ($jobdtl = pg_fetch_array($jobquery)){
 $curTime   = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
 $splitdone=$spTimeout=false;
 $jstfile=$PARA["volo_splitjob_work"] . "/SP_" . $jobdtl["job_id"] . "/split/job.start";
 $jobfile=$PARA["volo_splitjob_work"] . "/SP_" . $jobdtl["job_id"] . "/split/job.done";
 $errfile=$PARA["volo_splitjob_work"] . "/SP_" . $jobdtl["job_id"] . "/split/job.error";
 $cnxfile=$PARA["volo_splitjob_work"] . "/SP_" . $jobdtl["job_id"] . "/split/job.cancel";

 // Check Split Timeout
 /*
 if(!is_dir($PARA["volo_splitjob_work"] . "/SP_" . $jobdtl["job_id"] . "/split")){
	$spTimeout = true;
 } else if (!file_exists($jstfile)){
	$spTimeout = true;
 } else {
	$splitStart = file_get_contents($jstfile);
	$splitStart = strtotime(substr($splitStart,0, 19));
	if ( (time() - $splitStart) > 1800 ) $spTimeout = true;
 }
 */

 // Split Done
 if (file_exists($jobfile)){
  	$splitdone=true;
 // Split Timeout or Creation Error
 } else if ( $spTimeout || file_exists($errfile)){
	if ( $spTimeout )
		$err_mesg = "[ERROR] Split Encode Trunk Creation Timeout";
	else
		$err_mesg = "[ERROR] Split Encode Trunk Creation Failure";
	echo "[ERROR][" . $curTime . "][" . gethostname() . "] VOLO.SplitERR " . $err_mesg . " :: JID[" . $jobdtl["job_id"] . "]" . "\n";
	// Update All Waiting Split Job Output
    	$updsql = pg_exec($link, "UPDATE split_output SET stage=-1, err=1, ltime=now(), message='" . $err_mesg . "' WHERE stage IN (0) AND job_id=" . $jobdtl["job_id"] . ";");
	pg_free_result($updsql);
	// Cancel All Running Split Job Output
    	$updsql = pg_exec($link, "UPDATE split_output SET c_cancel=true, ltime=now(), message='" . $err_mesg . "' WHERE stage IN (1) AND job_id=" . $jobdtl["job_id"] . ";");
	pg_free_result($updsql);
	// Update All Job Output
    	$updsql = pg_exec($link, "UPDATE output SET stage=-1, err=1, ltime=now(), merge_stime=now(), merge_ltime=now(), mux_stime=now(), mux_ltime=now(), message='" . $err_mesg . "' WHERE stage IN (0,1) AND job_id=" . $jobdtl["job_id"] . ";");
	pg_free_result($updsql);
 // Split Cancel
 } else if (file_exists($cnxfile)){
	$err_mesg = "[STATUS] Split Encode Job Cancelled";
	echo "[WARNS][" . $curTime . "][" . gethostname() . "] VOLO.SplitERR Split Encode Job Cancel :: JID[" . $jobdtl["job_id"] . "]" . "\n";
	// Update All Waiting Split Job Output
    	$updsql = pg_exec($link, "UPDATE split_output SET stage=3, c_cancel=true, ltime=now(), message='" . $err_mesg . "' WHERE stage IN (0) AND job_id=" . $jobdtl["job_id"] . ";");
	pg_free_result($updsql);
	// Update All Running Split Job Output
    	$updsql = pg_exec($link, "UPDATE split_output SET c_cancel=true, ltime=now(), message='" . $err_mesg . "' WHERE stage IN (1) AND job_id=" . $jobdtl["job_id"] . ";");
	pg_free_result($updsql);
	// Update All Job Output
    	$updsql = pg_exec($link, "UPDATE output SET stage=3, c_cancel=true, ltime=now(), merge_stime=now(), merge_ltime=now(), mux_stime=now(), mux_ltime=now(), message='" . $err_mesg . "' WHERE stage IN (0,1) AND job_id=" . $jobdtl["job_id"] . ";");
	pg_free_result($updsql);
 // Check Every Split Trunk File Exist
 } else {
	$split_err = false;
	$outquery = pg_exec($link, "SELECT id, job_id FROM output WHERE job_id=" . $jobdtl["job_id"] . " AND stage=1 ORDER BY id LIMIT 1;");
	$outdtl = pg_fetch_array($outquery);
	pg_free_result($outquery);

	$result = pg_exec($link, "SELECT id, split_srcpath, split_srcfile FROM split_output WHERE output_id=" . $outdtl["id"] . " AND c_cancel=false AND stage=0 ORDER BY id ASC;");
        $ttljob = pg_numrows($result);
        for($rj = 0; $rj < $ttljob; $rj++) {
	   $ojrow = pg_fetch_array($result, $rj);
	   $summaryPos = $rj + 1;
 
	   // Check Audio Trunk Ready
	   if ($summaryPos == 1){
               	$errfile=$PARA["volo_splitjob_work"] . "/SP_" . $outdtl["job_id"] . "/split/encode_audio.error";
               	// $audfile=$PARA["volo_splitjob_work"] . "/SP_" . $outdtl["job_id"] . "/split/encode_audio.done";
               	// $TrunkFile = $ojrow["split_srcpath"] . "/SPTS_" . $outdtl["job_id"] . "_audio.mov";
               	// if ( file_exists($errfile) || ( file_exists($audfile) && !file_exists($TrunkFile) ) ){
               	if ( file_exists($errfile) ){
                       	$split_err = true;
                       	$err_mesg = "[ERROR] Source Audio Split Trunk Creation Failure";
			break;
               	}
	   }
 
           // Check Video Trunk Ready
           $errfile=$PARA["volo_splitjob_work"] . "/SP_" . $outdtl["job_id"] . "/split/encode_video_" . $summaryPos . ".error";
           // $audfile=$PARA["volo_splitjob_work"] . "/SP_" . $outdtl["job_id"] . "/split/encode_video_" . $summaryPos . ".done";
           // $TrunkFile = $ojrow["split_srcpath"] . "/" . $ojrow["split_srcfile"];
           // if ( file_exists($errfile) || ( file_exists($audfile) && !file_exists($TrunkFile) ) ){
           if ( file_exists($errfile) ){
                $split_err = true;
                $err_mesg = "[ERROR] Source Video Split Trunk Creation Failure :: Trunk ID[" . $ojrow["id"] . "]";
                break;
           }
	}
	pg_free_result($result);
	if ($split_err){
		// Update All Split Job Output
    		$updsql = pg_exec($link, "UPDATE split_output SET stage=-1, err=1, ltime=now(), message='" . $err_mesg . "' WHERE stage IN (0) AND job_id=" . $outdtl["job_id"] . ";");
		pg_free_result($updsql);
		// Update All Running Split Job Output
		$updsql = pg_exec($link, "UPDATE split_output SET c_cancel=true, ltime=now(), message='" . $err_mesg . "' WHERE stage IN (1) AND job_id=" . $outdtl["job_id"] . ";");
		pg_free_result($updsql);
		// Update All Job Output
    		$updsql = pg_exec($link, "UPDATE output SET stage=-1, err=1, ltime=now(), merge_stime=now(), merge_ltime=now(), mux_stime=now(), mux_ltime=now(), message='" . $err_mesg . "' WHERE stage IN (0,1) AND job_id=" . $outdtl["job_id"] . ";");
		pg_free_result($updsql);
	}
 } // End Trunk Creation Check

 // Merge, Mux Check in Output Level
 if ($splitdone){
   $outquery = pg_exec($link, "SELECT job_id, id, priority,filename FROM output WHERE job_id=" . $jobdtl["job_id"] . " AND c_cancel=false AND stage=1 ORDER BY id ASC;");
   while ($outdtl = pg_fetch_array($outquery)){
	$split_err = false;
	$err_mesg = "";

	// Merge Error
	$errfile=$PARA["volo_splitjob_work"] . "/SP_" . $outdtl["job_id"] . "/merge/" . $outdtl["id"] . "/job.error";
	if ( file_exists($errfile) ){
		$split_err = true;
                //kenny hack start
                $err_mesg = file_get_contents($errfile);
                //kenny hack end
//		$err_mesg = "[ERROR] Split Encode Video/Audio Trunks Merge Failure";
	}

	// Merge Missing Trunk Error
	$muxfile=$PARA["volo_splitjob_work"] . "/SP_" . $outdtl["job_id"] . "/muxaudio/" . $outdtl["id"] . "/job.start";
	$errfile=$PARA["volo_splitjob_work"] . "/SP_" . $outdtl["job_id"] . "/merge/" . $outdtl["id"] . "/encode.log";
	$merfile=$PARA["volo_splitjob_work"] . "/SP_" . $outdtl["job_id"] . "/merge/" . $outdtl["id"] . "/job.done";
	$chkfile=$PARA["volo_splitjob_work"] . "/SP_" . $outdtl["job_id"] . "/merge/" . $outdtl["id"] . "/trunkcheck.done";
	if ( file_exists($errfile) && file_exists($merfile) && !file_exists($muxfile) ){
	  $cmd='grep "\[error\]" ' . $errfile . ' | wc -l | bc';
	  $errfile = shell_exec($cmd);
	  if ( trim($errfile) != "0" ){
		$split_err = true;
		$err_mesg = "[ERROR] Split Encode Video/Audio Trunks Merge Failure";
	  }
          //kenny hack start, edit the check method
//	  if ( !$split_err && !file_exists($chkfile) ){
//	    // Merged TS and Audio Length Error
//	    if ($outdtl['priority'] < $PARA['volo_split_priority']){
//		$SplitTrunkOut  = $PARA['volo_split_outpath_low'];
//	    } else {
//		$SplitTrunkOut  = $PARA['volo_split_outpath_high'];
//	    }
//	    $result = pg_exec($link, "SELECT split_destpath FROM split_output WHERE output_id=" . $outdtl["id"] . " LIMIT 1;");
//	    $ojrow = pg_fetch_array($result);
//	    pg_free_result($result);
//	    $VFILE = $SplitTrunkOut . "/" . "SP_" . $outdtl["job_id"] . "/" . $outdtl["id"] . "_vmerge_done.ts";
//	    $AFILE = $ojrow["split_destpath"] . "/" . "SPTS_" . $outdtl["job_id"] . "_" . $outdtl["id"] . "_audio.mov";
//	    $VLength = shell_exec("mediainfo --Output=\"General;%Duration%\" $VFILE");
//	    $VLength = round($VLength/1000, 0, PHP_ROUND_HALF_UP);
//	    $ALength = shell_exec("mediainfo --Output=\"General;%Duration%\" $AFILE");
//	    $ALength = round($ALength/1000, 0, PHP_ROUND_HALF_UP);
// 
//	    if ( (($VLength - $ALength) > 10) || (($ALength - $VLength) > 10) ){
//		$split_err = true;
//		$err_mesg = "[ERROR] Split Encode Video/Audio Trunks Duration Not Matched";
//	    }
//
//	    // Check Once Only
//	    echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitERR Check Match Length :: JID[" . $outdtl["job_id"] . "] OID[" . $outdtl["id"] . "] FILE[" . $chkfile . "] V[" . $VLength . "] A[" . $ALength . "]\n";
//	    file_put_contents($chkfile, "Video/Audio Length :: V=" . $VLength . " A=" . $ALength);
//	  }
//	}

        if ( !$split_err && !file_exists($chkfile) ){
	    // Merged TS and Audio Length Error
//	    if ($outdtl['priority'] < $PARA['volo_split_priority']){
//		$SplitTrunkOut  = $PARA['volo_split_outpath_low'];
//	    } else {
//		$SplitTrunkOut  = $PARA['volo_split_outpath_high'];
//	    }
//	    $result = pg_exec($link, "SELECT split_destpath FROM split_output WHERE output_id=" . $outdtl["id"] . " LIMIT 1;");
//	    $ojrow = pg_fetch_array($result);
//	    pg_free_result($result);
//	    $VFILE = $SplitTrunkOut . "/" . "SP_" . $outdtl["job_id"] . "/" . $outdtl["id"] . "_vmerge_done.ts";
//	    $AFILE = $ojrow["split_destpath"] . "/" . "SPTS_" . $outdtl["job_id"] . "_" . $outdtl["id"] . "_audio.mov";
//	    $VLength = shell_exec("mediainfo --Output=\"General;%Duration%\" $VFILE");
//	    $VLength = round($VLength/1000, 0, PHP_ROUND_HALF_UP);
//	    $ALength = shell_exec("mediainfo --Output=\"General;%Duration%\" $AFILE");
//	    $ALength = round($ALength/1000, 0, PHP_ROUND_HALF_UP);
// 
//	    if ( (($VLength - $ALength) > 10) || (($ALength - $VLength) > 10) ){
//		$split_err = true;
//		$err_mesg = "[ERROR] Split Encode Video/Audio Trunks Duration Not Matched";
//	    }

	    // Check Once Only
	    echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitERR Check Match Length :: JID[" . $outdtl["job_id"] . "] OID[" . $outdtl["id"] . "] FILE[" . $chkfile . "] V[" . $VLength . "] A[" . $ALength . "]\n";
	    file_put_contents($chkfile, "Video/Audio Length :: V=" . $VLength . " A=" . $ALength);
	  }
	}
                //kenny hack end
          
	// Mux Error
	$errfile=$PARA["volo_splitjob_work"] . "/SP_" . $outdtl["job_id"] . "/muxaudio/" . $outdtl["id"] . "/job.error";
	if ( file_exists($errfile) ){
		$split_err = true;
		$err_mesg = "[ERROR] Split Encode Audio Mux Failure";
	}

	if ($split_err){
		echo "[ERROR][" . $curTime . "][" . gethostname() . "] VOLO.SplitERR " . $err_mesg . " :: JID[" . $outdtl["job_id"] . "] OID[" . $outdtl["id"] . "]" . "\n";
		// Update Job Output
    		$updsql = pg_exec($link, "UPDATE output SET stage=-1, err=1, ltime=now(), merge_stime=now(), merge_ltime=now(), mux_stime=now(), mux_ltime=now(), message='" . $err_mesg . "' WHERE stage IN (0,1) AND id=" . $outdtl["id"] . ";");
		pg_free_result($updsql);
	}
   } // End Loop Output
   pg_free_result($outquery);
 } // End Merge,Mux Check
} // End Loop JobID
pg_free_result($jobquery);
if ($PARA["schd_debug_mode"]){
        $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
        echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitERR Split Trunk Creation Check End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
}

// Split Trunk Audio/Video Encode Error Handler
$curTime=$stTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$outquery = pg_exec($link, "SELECT job_id, output_id, id, trunk_type FROM split_output WHERE stage<0 AND c_cancel=false AND job_id NOT IN (SELECT job_id FROM job_handler WHERE mode='P') AND job_id IN (SELECT DISTINCT job_id FROM output WHERE stage=1 AND split_encode=true AND c_cancel=false) ORDER BY id ASC;");
while ($outdtl = pg_fetch_array($outquery)){
	if ($outdtl["trunk_type"] == "A")
                $err_mesg = "[ERROR] Split Trunk Audio Encode Failure :: Trunk ID[" . $outdtl["id"] . "]";
	else
                $err_mesg = "[ERROR] Split Trunk Video Encode Failure :: Trunk ID[" . $outdtl["id"] . "]";
	echo "[ERROR][" . $curTime . "][" . gethostname() . "] VOLO.SplitERR " . $err_mesg . " :: JID[" . $outdtl["job_id"] . "] OID[" . $outdtl["output_id"] . "] SID[" . $outdtl["id"] . "]" . "\n";
	// Update Waiting Split Job Output
    	$updsql = pg_exec($link, "UPDATE split_output SET stage=-1, err=1, ltime=now(), message='" . $err_mesg . "' WHERE stage IN (0) AND output_id=" . $outdtl["output_id"] . ";");
	pg_free_result($updsql);
	// Update All Running Split Job Output
	$updsql = pg_exec($link, "UPDATE split_output SET c_cancel=true, ltime=now(), message='" . $err_mesg . "' WHERE stage IN (1) AND output_id=" . $outdtl["output_id"] . ";");
	pg_free_result($updsql);
	// Update Job Output
    	$updsql = pg_exec($link, "UPDATE output SET stage=-1, err=1, ltime=now(), merge_stime=now(), merge_ltime=now(), mux_stime=now(), mux_ltime=now(), message='" . $err_mesg . "' WHERE stage IN (0,1) AND id=" . $outdtl["output_id"] . ";");
	pg_free_result($updsql);

	// Force Split Process Stop
	$errfile=$PARA["volo_splitjob_work"] . "/SP_" . $outdtl["job_id"] . "/split/job.error";
	if (!file_exists($errfile)){
	    $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	    file_put_contents($errfile, $curTime);
	}
} // End Loop jobquery
pg_free_result($outquery);
if ($PARA["schd_debug_mode"]){
        $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
        echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitERR Split Trunk Encode Check End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
}

// End Process
if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitERR Process Done :: TIME[" . $procStart . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($procStart,0,19)) ) . ")\n";
}
pg_close($link);
exit(0);
?>
