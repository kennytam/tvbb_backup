<?php
//
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
// [Split Encode Trunks Merge and Audio Mux Control Engine]
//
$PARA = include('/opt/Volo.VISA/scheduler/config/scheduler.php');

// 
// START HERE

// Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$procStart=$stTime=$curTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

// Connect Scheduler DB
$config = include('/opt/Volo.VISA/scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .
                             " user=". $config['db_user'] .
                             " password=". $config['db_pass'] .
                             " dbname=".$config['db_name'];
$link = pg_connect($connstr)
	or die('Could not connect: ' . pg_last_error());


// Check All Encoded Video/Audio Split Trunks
$jobquery = pg_exec($link, "SELECT job_id, id, profile, encode_mode, priority FROM output WHERE stage=1 AND c_cancel=false AND split_encode=true AND job_id NOT IN (SELECT job_id FROM job_handler WHERE mode='P') ORDER BY priority DESC, id ASC;");
while ($jobdtl = pg_fetch_array($jobquery)){
  if ($jobdtl['priority'] < $PARA['volo_split_priority']){
        $SplitTrunkOut  = $PARA['volo_split_outpath_low'];
  } else {
        $SplitTrunkOut  = $PARA['volo_split_outpath_high'];
  }
 
  // Get Completed Trunks
  $result = pg_exec($link, "SELECT COUNT(*) FROM split_output WHERE stage=2 AND worker_id>0 AND c_cancel=false AND output_id=" . $jobdtl["id"] . ";");
  $ttlDone = pg_fetch_array($result);
  pg_free_result($result);

  $TrunkTotal = $TrunkCount = 0;
  $result = pg_exec($link, "SELECT id, job_id, output_id, split_destpath, split_destfile, trunk_total FROM split_output WHERE output_id=" . $jobdtl["id"] . " ORDER BY id;");
  while ($ojrow = pg_fetch_array($result)){
    $TrunkTotal     = $ojrow["trunk_total"];
    $TrunkDestpath  = $ojrow["split_destpath"];
    if ($ttlDone[0] != $TrunkTotal) break;

    $TrunkPrefix    = $ojrow["split_destpath"] . "/" . $ojrow["split_destfile"];
    if (file_exists($TrunkPrefix) && filesize($TrunkPrefix) > 0) $TrunkCount++;
  }
  pg_free_result($result);

  // Video,Audio Split Encode Not Complete
  if ($ttlDone[0] != $TrunkTotal){
    if ($ttlDone[0] > 0){
     $remainTrunk = $TrunkTotal - $ttlDone[0];
     $progress    = round(($ttlDone[0]/$TrunkTotal), 2, PHP_ROUND_HALF_UP)*100;
     // Update Output Job Status
     $updsql = pg_exec($link, "UPDATE output SET stage=1, progress=" . $progress . ", ltime=now(), merge_stime=now(), merge_ltime=now(), mux_stime=now(), mux_ltime=now(), message='[STATUS] Encoding Trunks (Remain " . $remainTrunk . " of " . $TrunkTotal . ")' WHERE stage=1 AND id=" . $jobdtl["id"] . ";");
     pg_free_result($updsql);
    }
  } else if ($TrunkTotal == $TrunkCount){
	// Check Any Merge Process -- MAX 5 Jobs
    $jobStr='ps -Alf | grep "' . $PARA["volo_merge_cmd"] . '" | grep -v "grep " | grep -v "tail " | grep -v "vi "';
    $RUN = exec($jobStr, $proclist, $status);
    if (sizeof($proclist) < 10){
        // Get Min/Max Encode Time from Split Output
        $updsql = pg_exec($link, "SELECT min(stime), max(ltime) FROM split_output WHERE trunk_type='V' AND output_id=" . $jobdtl["id"] . ";");
        $strow = pg_fetch_array($updsql);
        pg_free_result($updsql);
        $updsql = pg_exec($link, "SELECT max(progress_fps) FROM split_output WHERE trunk_type='V' AND output_id=" . $jobdtl["id"] . ";");
        $strow2 = pg_fetch_array($updsql);
        pg_free_result($updsql);

	// Update Split Output Job
	$updsql = pg_exec($link, "UPDATE output SET stage=1, progress=95, stime='" . $strow["min"] . "', ltime='" . $strow["max"] . "', progress_fps=" . $strow2["max"] . ", merge_stime=now(), merge_ltime=now(), mux_stime=now(), mux_ltime=now(), message='[STATUS] Merge Encoded Video/Audio Trunks' WHERE stage=1 AND id='" . $jobdtl["id"] . "';");
	pg_free_result($updsql);

	$updsql = pg_exec($link, "UPDATE split_output SET stage=4 WHERE stage=2 AND output_id=" . $jobdtl["id"] . ";");
	pg_free_result($updsql);

	// Merge if ALL Encode Trunk Ready (TRUNK FILE :: SPTS_{job id}_{output id}_{trunk no}.ts)
	$TrunkPrefix = $TrunkDestpath . "/" . "SPTS_" . $jobdtl["job_id"] . "_" . $jobdtl["id"];
	$MergeFile   = $SplitTrunkOut . "/" . "SP_" . $jobdtl["job_id"] . "/" . "SP_" . $jobdtl["job_id"] . "_" . $jobdtl["id"] . ".mxf";
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Merge Split Trunk :: JID[" . $jobdtl["job_id"] . "] OID[" . $jobdtl["id"] . "] MERGE TRUNK[" . $TrunkPrefix . "*]" . "\n";
	$cmd = $PARA["volo_merge_cmd"] . " " . $jobdtl["job_id"] . " " . $jobdtl["id"] . " " . $TrunkPrefix . " " . ($ttlDone[0]-1) . " " . $MergeFile;
	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Merge Command :: " . $cmd . "\n";
        exec("$cmd > /dev/null 2>&1 &");

	// Add Merge Output Job
	// $updsql = pg_exec($link, "INSERT INTO extra_output (job_id, output_id, encode_type, encode_unit, priority, stage, message, command) VALUES ('" . $jobdtl["job_id"] . "', '" . $jobdtl["id"] . "', 'M', '1', '99', '0', '[STATUS] Merge Encoded Video/Audio Trunks', '" . $cmd . "');");
	// pg_free_result($updsql);
    }
  } // End Merge Trunk
}
pg_free_result($jobquery);
if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Split Encode Video/Audio Merge End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
}


// Check Merge Trunk Encode Job
$curTime=$stTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$jobquery = pg_exec($link, "SELECT job_id, id, profile, encode_mode, priority FROM output WHERE stage=1 AND c_cancel=false AND split_encode=true AND job_id NOT IN (SELECT job_id FROM job_handler WHERE mode='P') ORDER BY priority DESC, id ASC;");
while ($jobdtl = pg_fetch_array($jobquery)){
  // Check any Merge Trunks Process
  $result = pg_exec($link, "SELECT COUNT(*) FROM split_output WHERE stage=4 AND worker_id>0 AND c_cancel=false AND output_id=" . $jobdtl["id"] . ";");
  $ttlDone = pg_fetch_array($result);
  pg_free_result($result);

  if ($ttlDone[0] > 0){
    // MUX Audio when Merge Job Done -- /dev/shm/Volo.VISA/splitjobs/SP_6613/merge/51648/job.done
    $jobfile=$PARA["volo_splitjob_work"] . "/SP_" . $jobdtl["job_id"] . "/merge/" . $jobdtl["id"] . "/job.done";
    $encfile=$PARA["volo_splitjob_work"] . "/SP_" . $jobdtl["job_id"] . "/merge/" . $jobdtl["id"] . "/encode.log";
    $chkfile=$PARA["volo_splitjob_work"] . "/SP_" . $jobdtl["job_id"] . "/merge/" . $jobdtl["id"] . "/trunk.check";
    $muxfile=$PARA["volo_splitjob_work"] . "/SP_" . $jobdtl["job_id"] . "/muxaudio/" . $jobdtl["id"] . "/job.start";
    if ($jobdtl['priority'] < $PARA['volo_split_priority']){
        $SplitTrunkOut  = $PARA['volo_split_outpath_low'];
    } else {
        $SplitTrunkOut  = $PARA['volo_split_outpath_high'];
    }

    // Check Merge Error, Mark Error in Error Module
    if (file_exists($encfile) && file_exists($jobfile) && !file_exists($muxfile)){
      if (!file_exists($chkfile)){
	$cmd='grep "\[error\]" ' . $encfile . ' | wc -l | bc';
	$errmerge = shell_exec($cmd);
      } else {
	$errmerge = "0";
      }
    } else {
	$errmerge = "1";
    }

    //kenny hack start, no need to check
    // Check Merged TS and Audio Length
//    if (trim($errmerge) == "0" && !file_exists($chkfile)){
//       $result = pg_exec($link, "SELECT split_destpath FROM split_output WHERE output_id=" . $jobdtl["id"] . " LIMIT 1;");
//       $ojrow = pg_fetch_array($result);
//       pg_free_result($result);
//       $VFILE = $SplitTrunkOut . "/" . "SP_" . $jobdtl["job_id"] . "/" . $jobdtl["id"] . "_vmerge_done.ts";
//       $AFILE = $ojrow["split_destpath"] . "/" . "SPTS_" . $jobdtl["job_id"] . "_" . $jobdtl["id"] . "_audio.mov";
//       $VLength = shell_exec("mediainfo --Output=\"General;%Duration%\" $VFILE");
//       $VLength = round($VLength/1000, 0, PHP_ROUND_HALF_UP);
//       $ALength = shell_exec("mediainfo --Output=\"General;%Duration%\" $AFILE");
//       $ALength = round($ALength/1000, 0, PHP_ROUND_HALF_UP);
// 
//       if ( (($VLength - $ALength) > 10) || (($ALength - $VLength) > 10) ){
//	echo "[ERROR][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Video/Audio Length Not Match :: JID[" . $jobdtl["job_id"] . "] OID[" . $jobdtl["id"] . "] VIDEO[" . $VFILE . " LENGTH - " . $VLength . "] AUDIO[" . $AFILE . " LENGTH - " . $ALength . "]\n";
//	$errmerge = "1";
//       } else {
//	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Video/Audio Length :: JID[" . $jobdtl["job_id"] . "] OID[" . $jobdtl["id"] . "] VIDEO[" . $VFILE . " LENGTH - " . $VLength . "] AUDIO[" . $AFILE . " LENGTH - " . $ALength . "]\n";
//        // Check Once Only
//        file_put_contents($chkfile, "Video/Audio Length :: V=" . $VLength . " A=" . $ALength);
//       }
//    }
    //kenny hack end

    // Merge Split Encode Output File
    if ( (trim($errmerge) == "0") && file_exists($jobfile) && !file_exists($muxfile) ){
      $MergeFile = $SplitTrunkOut . "/" . "SP_" . $jobdtl["job_id"] . "/" . "SP_" . $jobdtl["job_id"] . "_" . $jobdtl["id"] . ".mxf";
      
      echo $MergeFile.'\n';
      if ( file_exists($MergeFile) ){
	$oquery = pg_exec($link, "SELECT job.source, job.destination, out.filename FROM job job, output out WHERE job.id=out.job_id AND out.id=" . $jobdtl["id"] . ";");
	$outfile = pg_fetch_array($oquery);
	pg_free_result($oquery);

	$splitStart = file_get_contents($jobfile);
        $splitStart = substr($splitStart,0, 19);
	$sourceExt  = strtolower(substr($outfile['source'], -3));

        //kenny hack start, run this directly
//	if ( $sourceExt != 'mxf' && $sourceExt != 'mov' ){
        if (true){
        //kenny hack end
		if (!is_dir($outfile['destination'])){
			$cmd = "mkdir -p " . $outfile['destination'];
			exec("$cmd > /dev/null 2>&1");
		}

		// Get Output Mediainfo
		$dest_info = shell_exec("mediainfo $MergeFile");
		$dest_info = pg_escape_string($dest_info);

		// Move to Destination
                //kenny hack start, if mergefile is symblic link, get real file
                if(is_link($MergeFile))
                    $MergeFile = readlink($MergeFile);
                //kenny hack end
		$cmd = "mv -f " . $MergeFile . " " . $outfile['destination'] . "/" . $outfile['filename'];
		exec("$cmd > /dev/null 2>&1");

		// Update Job Output
		$updsql = pg_exec($link, "UPDATE output SET stage=2, progress=100, merge_ltime='" . $splitStart . "', mux_stime='" . $splitStart . "', mux_ltime='" . $splitStart . "', output_info='" . $dest_info . "', message='[STATUS] Split Encode Process Completed' WHERE id=" . $jobdtl["id"] . ";");
		pg_free_result($updsql);
		// Update Split Job Output
		$updsql = pg_exec($link, "UPDATE split_output SET stage=5 WHERE stage=2 AND output_id=" . $jobdtl["id"] . ";");
		pg_free_result($updsql);
	} else {
		// Check Any Muxing Process -- MAX 2 Jobs
		$jobStr='ps -Alf | grep "' . $PARA["volo_muxaudio_cmd"] . '" | grep -v "grep " | grep -v "tail " | grep -v "vi "';
		$RUN = exec($jobStr, $proclist, $status);
		if (sizeof($proclist) < 10){
		    // Check Audio Mapping Control File
		    $dirPath = pathinfo($outfile['source'], PATHINFO_DIRNAME);
		    $audioCtl = $audioPARA = $curlist = "";
		    $pathLoop = explode("/", $dirPath);
		    for ($i=1; $i<sizeof($pathLoop); $i++){
			$curlist .= "/" . $pathLoop[$i];
			$audioCtl = $curlist . "/" . $PARA['audio_map_file'];
			//if (file_exists($audioCtl)){
			//	$austart= true;
			//	$aufile = fopen($audioCtl, "r");
			//	if ($aufile){
			//	  while(is_resource($aufile) && ($line = fgets($aufile)) !== false) {
			//		$line = str_replace("\n", "", $line);
			//		$line = str_replace(",", "_", $line);
			//		if ($austart){
			//			$audioPARA = $line;
			//			$austart = false;
			//		} else {
			//			$audioPARA .= "," . $line;
			//		}
			//	  }
			//	  fclose($aufile);
			//	}
			//}
		    }

		    $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
		    echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Mux Output Audio :: JID[" . $jobdtl["job_id"] . "] OID[" . $jobdtl["id"] . "] MUX AUDIO[" . $outfile['destination'] . "/" . $outfile['filename'] . "] AUDIO CTL[" . $audioCtl . "] MAPPING[" . $audioPARA . "]" . "\n";
		    //$cmd = $PARA["volo_muxaudio_cmd"] . " " . $jobdtl["job_id"] . " " . $jobdtl["id"] . " " . $MergeFile . " " . $outfile['destination'] . "/" . $outfile['filename'] . " " . $audioPARA;
		    $cmd = $PARA["volo_muxaudio_cmd"] . " " . $jobdtl["job_id"] . " " . $jobdtl["id"] . " " . $MergeFile . " " . $outfile['destination'] . "/" . $outfile['filename'] . " " . $audioCtl;
		    echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Mux Audio Command :: " . $cmd . "\n";
		    exec("$cmd > /dev/null 2>&1 &");

		    // Add Mux Output Job
		    // $updsql = pg_exec($link, "INSERT INTO extra_output (job_id, output_id, encode_type, encode_unit, priority, stage, message, command) VALUES ('" . $jobdtl["job_id"] . "', '" . $jobdtl["id"] . "', 'U', '1', '99', '0', '[STATUS] Mux Split Encode Output Audio', '" . $cmd . "');");
		    // pg_free_result($updsql);

		    // Get Output Mediainfo
		    $dest_info = shell_exec("mediainfo $MergeFile");
		    $dest_info = pg_escape_string($dest_info);
                    
		    // Update Job Output
		    $updsql = pg_exec($link, "UPDATE output SET stage=1, progress=98, merge_ltime='" . $splitStart . "', mux_stime=now(), mux_ltime=now(), output_info='" . $dest_info . "', message='[STATUS] Mux Split Encode Output Audio' WHERE stage=1 AND id=" . $jobdtl["id"] . ";");
		    pg_free_result($updsql);
		}
	} // End Mux MXF Output
      } else {
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[ERROR][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Merge Encoded Video/Audio Trunks Failed :: JID[" . $jobdtl["job_id"] . "] OID[" . $jobdtl["id"] . "]" . "\n";
	// Update Job Output Error
	$updsql = pg_exec($link, "UPDATE output SET stage=-1, err=1, merge_ltime=now(), mux_stime=now(), mux_ltime=now(), message='[ERROR] Merge Encoded Video/Audio Trunks Failed' WHERE stage=1 AND id=" . $jobdtl["id"] . ";");
	pg_free_result($updsql);

      } // End MUX Audio
  } // Exist Merge.Done File
 } // End Has Merge Process Run
}
pg_free_result($jobquery);
if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Split Encode Audio Mux End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
}


// Process MUX Audio Output Encode Job
$curTime=$stTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$jobquery = pg_exec($link, "SELECT job_id, id FROM output WHERE stage=1 AND c_cancel=false AND split_encode=true AND job_id NOT IN (SELECT job_id FROM job_handler WHERE mode='P') ORDER BY id ASC;");
while ($jobdtl = pg_fetch_array($jobquery)){
  // Check any Mux Trunks Process
  $result = pg_exec($link, "SELECT COUNT(*) FROM split_output WHERE stage=4 AND worker_id>0 AND c_cancel=false AND output_id=" . $jobdtl["id"] . ";");
  $ttlDone = pg_fetch_array($result);
  pg_free_result($result);

  if ($ttlDone[0] > 0){
	// Final Job Done -- /dev/shm/Volo.VISA/splitjobs/SP_6613/muxaudio/51648/job.done
	$jobfile=$PARA["volo_splitjob_work"] . "/SP_" . $jobdtl["job_id"] . "/muxaudio/" . $jobdtl["id"] . "/job.done";
	if ( file_exists($jobfile) ) {
		echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Split Encode Output Job Done :: JID[" . $jobdtl["job_id"] . "] OID[" . $jobdtl["id"] . "]" . "\n";
		$splitStart = file_get_contents($jobfile);
        	$splitStart = substr($splitStart,0, 19);
		// Update Job Output
		$updsql = pg_exec($link, "UPDATE output SET stage=2, progress=100, mux_ltime='" . $splitStart . "', message='[STATUS] Split Encode Process Completed' WHERE id=" . $jobdtl["id"] . ";");
		pg_free_result($updsql);
		// Update Split Job Output
		$updsql = pg_exec($link, "UPDATE split_output SET stage=5 WHERE stage=4 AND output_id=" . $jobdtl["id"] . ";");
		pg_free_result($updsql);
	} // End Job Done
  }
}
pg_free_result($jobquery);
if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Split Encode Complete End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
}


// End Process
if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
        echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitMERGE Process Done :: TIME[" . $procStart . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($procStart,0,19)) ) . ")\n";
}
pg_close($link);
exit(0);
?>