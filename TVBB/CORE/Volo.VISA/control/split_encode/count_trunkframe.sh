#!/bin/sh

PATH=$1
FILE=$2
CNT=$3
 
#########################################
#count total frames from split trunks
#########################################
NB=0

for i in {1..${CNT}};
do Nf=$(/opt/volo/bin/volo-1.4.1 -vsync 0 -i $PATH/${FILE}_${i}.ts -vcodec rawvideo -f nut -y /dev/null 2>&1 | /bin/grep Lb0 | /bin/sed -e "s/.*frame=//g" -e "s/ fps=.*//g" -e "s/ *//g");
NB=$((NB+Nf));
echo "$i: $Nf"
done;
echo $NB
