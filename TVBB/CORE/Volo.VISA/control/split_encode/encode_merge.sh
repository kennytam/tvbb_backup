#!/bin/bash
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
# Volo Split Encoder :: Scripts to Merge Split Output
# "USAGE: encode_merge.sh [JOB ID] [OUTPUT ID] [SPLIT FILE SUFFIX FULL PATH] [NO. OF SPLIT] [DESTINATION FULL PATH]"
# eg ./encode_merge.sh 3761 28473 /media/volo_spool/Split_Encode/JOB_Trunk/SP_3761/SPTS_3761_28473 9 /media/volo_spool/Split_Encode/OUT_Trunk/SP_3761/SP_3761_28473.mp4
#
 
cd /opt/Volo.VISA/control/split_encode
JID=$1
OID=$2
SOURCE=$3
NOOFSPLIT=$4
DEST=$5
 
VOLO_BIN=/opt/volo/bin/volombc-1.0
VOLO_PROBE=/opt/volo/bin/vprobe-1.4.1
JOB_PATH=/dev/shm/Volo.VISA/splitjobs/SP_${JID}/merge/${OID}
JOB_LOG='/opt/Volo.VISA/logs/VISA.SplitMerge'.`hostname`.`date +%Y%m%d`.'log'
ERRSTR=""
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Merge Split Job Submit :: JID[${1}] OID[${2}] SOURCE[${3}] TRUNK[${4}] DEST[${5}] >> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Merge Command :: ./encode_merge.sh ${1} ${2} ${3} ${4} ${5} >> ${JOB_LOG}
 
# Source Not Exist -- Skip Checking Source, Check in Retry Level
#for((s=0; s<$NOOFSPLIT; s++))
#do
#	SPLITTRUNK=${SOURCE}_$((s+1)).ts
#	if [ ! -f "${SPLITTRUNK}" ]
#	then
#		echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Merge Split Job Error :: JOB[${OID}] SOURCE[${SPLITTRUNK}] Not Found >> ${JOB_LOG}
#		ERRSTR="[ERROR] Merge Split Job Error :: Split Video Not Found"
#	fi
#done
 
# Create Encode Working Path
if [ -d "${JOB_PATH}" ]; then
	mv ${JOB_PATH} ${JOB_PATH}.`date +"%Y%m%d%H%M%S"`
fi
mkdir -p ${JOB_PATH}

# Mark Job ERROR
if [ ! -z "${ERRSTR}" ]; then
	echo ${ERRSTR} > ${JOB_PATH}/encode.log
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

# Create Encode Destination Path (with directory 755 and file 664)
OUTPATH=`dirname ${DEST}`
if [ -d "${OUTPATH}" ]; then
	rm -rf ${DEST}
else
	mkdir -p ${OUTPATH}
fi

echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.start
echo Merge Video/Audio Source Trunks - >> ${JOB_PATH}/job.start 2>&1
ls -lh --full-time ${SOURCE}*.ts >> ${JOB_PATH}/job.start 2>&1
ls -lh --full-time ${SOURCE}_audio.mov >> ${JOB_PATH}/job.start 2>&1

N_THREAD=$(less /proc/cpuinfo | grep processor | wc -l)

# Set Merge Retry
for(( s=1; s<3; s++ ))
do
        #kenny hack start, pcm no need -bsf:a aac_adtstoasc
        #newest
        
        if [ ! $((NOOFSPLIT+1)) -eq 1 ]; then
            #use mbc to merge now
            CTIME=$(date +"%F %T.000")
            MERGESTR=""
            START=1
            for i in $(eval echo "{$START..$(($NOOFSPLIT))}"); do MERGESTR+="${SOURCE}_${i}.mxf " ; done

            #redirect output to final place
            OLD_DEST=$DEST
            SRCPATH=$(dirname $(echo $SOURCE | sed -e "s/JOB/IN/g") )
	    SRCFILE_ORI=$(cat ${SRCPATH}/SOURCE.txt)
	    TIMECODE=$(echo -n $(mediainfo ${SRCFILE_ORI} | grep 'Time code of first frame' | tail -1 | sed -e 's/T.*: //g'))
            #kenny start,20180313, fixed the empty timecode
            if [ ${#TIMECODE} -ge 0 ]; then TIMECODE='00:00:00:00';fi
            #kenny end,20180313
            TMP=$(cat ${SRCPATH}/DESTINATION)'/VOLO_TMP/'
            mkdir -p $TMP >> ${JOB_PATH}/encode.log 2>&1
            DEST=${TMP}'/'$(basename ${DEST})

            echo "${DEST} ${OLD_DEST}" >> ${JOB_PATH}/encode.log 2>&1

            #kenny, support IMX50,20180122
            echo $MERGESTR >> ${JOB_PATH}/encode.log

            if [ -f ${SRCPATH}/IMX50 ]; then
                echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`] VISA.SPLIT Video / Audio Merge Command :: ${VOLO_BIN} -label MERGE.F_${JID}_${OID} -i - -i ${SOURCE}_audio.mov -target imx50 -vcodec copy -acodec copy -metadata creation_time="${CTIME}" -timecode ${TIMECODE} -f mxf -y ${DEST} >> ${JOB_PATH}/encode.log 2>&1 
                cat $MERGESTR | ${VOLO_BIN} -label MERGE.F_${JID}_${OID} -i - -i ${SOURCE}_audio.mov -target imx50 -vcodec copy -acodec copy -metadata creation_time="${CTIME}" -timecode ${TIMECODE} -f mxf -y ${DEST} >> ${JOB_PATH}/encode.log 2>&1            
            else
                echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`] VISA.SPLIT Video / Audio Merge Command :: ${VOLO_BIN} -label MERGE.F_${JID}_${OID} -i - -i ${SOURCE}_audio.mov -target xdcamhd422 -vcodec copy -metadata creation_time="${CTIME}" -timecode ${TIMECODE} -f mxf -an -y ${DEST} -acodec copy -newaudio -map_audio_channel 1:0:0:0:1:0 -acodec copy -newaudio -map_audio_channel 1:1:0:0:2:0 -acodec copy -newaudio -map_audio_channel 1:2:0:0:3:0 -acodec copy -newaudio -map_audio_channel 1:3:0:0:4:0 -acodec copy -newaudio -map_audio_channel 1:4:0:0:5:0 -acodec copy -newaudio -map_audio_channel 1:5:0:0:6:0 -acodec copy -newaudio -map_audio_channel 1:6:0:0:7:0 -acodec copy -newaudio -map_audio_channel 1:7:0:0:8:0 >> ${JOB_PATH}/encode.log 2>&1 
                cat $MERGESTR | ${VOLO_BIN} -label MERGE.F_${JID}_${OID} -i - -i ${SOURCE}_audio.mov -target xdcamhd422 -vcodec copy -metadata creation_time="${CTIME}" -timecode ${TIMECODE} -f mxf -an -y ${DEST} -acodec copy -newaudio -map_audio_channel 1:0:0:0:1:0 -acodec copy -newaudio -map_audio_channel 1:1:0:0:2:0 -acodec copy -newaudio -map_audio_channel 1:2:0:0:3:0 -acodec copy -newaudio -map_audio_channel 1:3:0:0:4:0 -acodec copy -newaudio -map_audio_channel 1:4:0:0:5:0 -acodec copy -newaudio -map_audio_channel 1:5:0:0:6:0 -acodec copy -newaudio -map_audio_channel 1:6:0:0:7:0 -acodec copy -newaudio -map_audio_channel 1:7:0:0:8:0 >> ${JOB_PATH}/encode.log 2>&1            
            fi

            #kenny end, support IMX50,20180122

            ln -s ${DEST} ${OLD_DEST}

        else            
            echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`] VISA.SPLIT Video / Audio Merge Command :: NO NEED MERGE MODE, mv ${SOURCE}_1.mxf ${DEST} >> ${JOB_PATH}/encode.log 2>&1
            mv ${SOURCE}_1.mxf ${DEST} >> ${JOB_PATH}/encode.log 2>&1
        fi

        #kenny hack end;
        if [ -f ${DEST} ] 
        then
            #kenny start,20180313, move file to done space if setting exists
            SRCPATH=$(dirname $(echo $SOURCE | sed -e "s/JOB/IN/g") )
            SRCFILE_ORI=$(cat ${SRCPATH}/SOURCE.txt)
            if [ -f ${SRCPATH}/SRCDONEPATH ]; then mv ${SRCFILE_ORI} $(cat ${SRCPATH}/SRCDONEPATH)/ ; fi
            #kenny end,20180313
		echo Merge Video/Audio Output - >> ${JOB_PATH}/job.start 2>&1
                ls -lh --full-time ${DEST} >> ${JOB_PATH}/job.start 2>&1
		${VOLO_PROBE} -i ${DEST} >> ${JOB_PATH}/job.start 2>&1
		break
	else
		if [ ${s} -lt 2 ]; then
                	echo [WARNS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Video / Audio Merge Retry Job :: JID[${JID}] OID[${OID}] SOURCE[Video:${OUTPATH}/${OID}_vmerge.ts Audio:${SOURCE}_audio.mov] DEST[${DEST}] RETRY[${s}] >> ${JOB_LOG}
                	echo Split Video / Audio Merge Retry [$s] :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/job.start
                	echo Split Video / Audio Merge Retry [$s] :: `date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/encode.log
			sleep 10
			echo Merge Video TS Info ---- >> ${JOB_PATH}/job.start
			echo Merge Audio Info ---- >> ${JOB_PATH}/job.start
                	ls -lh --full-time ${SOURCE}_audio.mov >> ${JOB_PATH}/job.start 2>&1
			${VOLO_PROBE} -i ${SOURCE}_audio.mov >> ${JOB_PATH}/job.start 2>&1
		fi
        fi
done

# Housekeep Temp TS
if [ ! -f "${DEST}" ] || [ $(wc -c< ${DEST}) -lt 1000 ]; then
    #kenny start,20180313, move file to error space if setting exists
    if [ -f ${SRCPATH}/SRCERRPATH ]; then mv ${SRCFILE_ORI} $(cat ${SRCPATH}/SRCERRPATH)/ ;fi
    #kenny end,20180313

	echo [ERROR] Merge Video Error :: Fail to Merge Split Output Video, Not A Standard Input/Output > ${JOB_PATH}/job.error
	exit 1
fi

echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.done
# Sleep for NAS Sync
sleep 3
exit 0

