<?php
//
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
// [Split Encode Encode Time Reset Engine]
//
$startJOB = 190;
$PARA = include('/opt/Volo.VISA/scheduler/config/scheduler.php');

// 
// START HERE

// Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());

// Connect Scheduler DB
$config = include('/opt/Volo.VISA/scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .
                             " user=". $config['db_user'] .
                             " password=". $config['db_pass'] .
                             " dbname=".$config['db_name'];
$link = pg_connect($connstr)
	or die('Could not connect: ' . pg_last_error());


// Reset Output Start Time from Split Ouput First Encoding Trunk
$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$result = pg_exec($link, "SELECT job_id, id, progress_fps, stime FROM output WHERE job_id>=" . $startJOB . " AND split_encode=true AND stage=2 ORDER BY job_id,id ASC;");
$ttljob = pg_numrows($result);
for($rj = 0; $rj < $ttljob; $rj++) {
      $ojrow = pg_fetch_array($result, $rj);
      $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
      echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitHNDL Check Output Start Time :: JID[" . $ojrow["job_id"] . "] OID[" . $ojrow["id"] . "] TIME[" . $ojrow["stime"] . "] FPS[" . $ojrow["progress_fps"] . "]" . "\n";

      // Get First Encode Time
      $tquery = pg_exec($link, "SELECT min(stime) FROM split_output WHERE trunk_type='V' AND output_id=" . $ojrow["id"] . ";");
      $trow   = pg_fetch_array($tquery);
      pg_free_result($tquery);

      // Get Max FPS
      $tquery = pg_exec($link, "SELECT max(progress_fps) FROM split_output WHERE trunk_type='V' AND output_id=" . $ojrow["id"] . ";");
      $trow2  = pg_fetch_array($tquery);
      pg_free_result($tquery);

      if ( $trow["min"] != "" && $trow["min"] != $ojrow["stime"] ){
	echo "[WARNS][" . $curTime . "][" . gethostname() . "] VOLO.SplitHNDL Reset Output Start Time :: JID[" . $ojrow["job_id"] . "] OID[" . $ojrow["id"] . "] TIME[" . $ojrow["stime"] . "] -> [" . $trow["min"] . "]" . "\n";
	$updsql = pg_exec($link, "UPDATE output SET stime='" . $trow["min"] . "' WHERE id=" . $ojrow["id"] . ";");
	pg_free_result($updsql);
      }
      if ( $trow2["max"] > $ojrow["progress_fps"] ){
	echo "[WARNS][" . $curTime . "][" . gethostname() . "] VOLO.SplitHNDL Reset Output FPS :: JID[" . $ojrow["job_id"] . "] OID[" . $ojrow["id"] . "] FPS[" . $ojrow["progress_fps"] . "] -> [" . $trow2["max"] . "]" . "\n";
	$updsql = pg_exec($link, "UPDATE output SET progress_fps='" . $trow2["max"] . "' WHERE id=" . $ojrow["id"] . ";");
	pg_free_result($updsql);
      }
}
pg_free_result($result);

// END
pg_close($link);
exit(0);
?>
