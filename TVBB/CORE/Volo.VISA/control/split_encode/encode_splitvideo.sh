#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
# Volo Split Encoder :: Scripts to Split Video Track from Video Source
# "USAGE: encode_splitvideo.sh [JID] [TRUNK NO] [START ENCODE TIME] [END ENCODE TIME] [DURATION] [SOURCE FULL PATH] [SPLIT AUDIO FILE FULL PATH] [JOB FULL PATH] [THREAD CONTROL]"
# eg. ./encode_splitvideo.sh 4077 1 0 0 61.061 /media/volo_source/samples/BAD_GUYS/BAD_GUYS_EP01.mpg /media/volo_spool/Split_Encode/IN_Trunk/SP_4077/SPTS_4077_1.ts /dev/shm/Volo.VISA/splitjobs/SP_4077/split 4
#
 
cd /opt/Volo.VISA/control/split_encode
JID=$1
TSNO=$2
STTIME=$3
EDTIME=$4
DURATION=$5
SOURCE=$6
DEST=$7
JOB_PATH=$8
N_THREAD=$9


if [ "${STTIME}" == "0" ]
then
	STTIME=""
else
	STTIME="-ss ${STTIME}"
fi
if [ "${EDTIME}" == "0" ]
then
	EDTIME=""
else
	EDTIME="-ss ${EDTIME}"
fi
if [ "${DURATION}" == "0" ]
then
	DURATION=""
else
	DURATION="-t ${DURATION}"
fi

VOLO_BIN=/opt/volo/bin/volo-1.4.1
VOLO_PROBE=/opt/volo/bin/vprobe
WORKFILE=${DEST}_TEMP.ts

echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode_video_${TSNO}.start
echo "Split Video Start :: "`date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode_video_${TSNO}.log

# NO FRC - 25fps
echo ${VOLO_BIN} -label SPLIT.F_${JID}_${TSNO} -vsync 0 ${STTIME} -i ${SOURCE} -vf yadif=0 -pix_fmt yuv420p -map 0:v -vcodec volo -qp 0 -an -preset ultrafast ${EDTIME} ${DURATION} -threads ${N_THREAD} -y ${WORKFILE} >> ${JOB_PATH}/encode_video_${TSNO}.log
${VOLO_BIN} -label SPLIT.F_${JID}_${TSNO} -vsync 0 ${STTIME} -i ${SOURCE} -vf yadif=0 -pix_fmt yuv420p -map 0:v -vcodec volo -qp 0 -an -preset ultrafast ${EDTIME} ${DURATION} -threads ${N_THREAD} -y ${WORKFILE} >> ${JOB_PATH}/encode_video_${TSNO}.log 2>&1

# FRC - 25fps
#echo ${VOLO_BIN} -label SPLIT.F_${JID}_${TSNO} -vsync 0 ${STTIME} -i ${SOURCE} -vf yadif=0 -pix_fmt yuv420p -map 0:v -vcodec volo -qp 0 -an -preset ultrafast ${EDTIME} ${DURATION} -x264opts force-cfr=1:fps=25 -threads ${N_THREAD} -y ${WORKFILE} >> ${JOB_PATH}/encode_video_${TSNO}.log
#${VOLO_BIN} -label SPLIT.F_${JID}_${TSNO} -vsync 0 ${STTIME} -i ${SOURCE} -vf yadif=0 -pix_fmt yuv420p -map 0:v -vcodec volo -qp 0 -an -preset ultrafast ${EDTIME} ${DURATION} -x264opts force-cfr=1:fps=25 -threads ${N_THREAD} -y ${WORKFILE} >> ${JOB_PATH}/encode_video_${TSNO}.log 2>&1

if [ -f "${WORKFILE}" ]
then
	mv ${WORKFILE} ${DEST}
	sleep 1
        echo "Split Video End :: "`date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/encode_video_${TSNO}.log
        echo "Destination Video Information :: " >> ${JOB_PATH}/encode_video_${TSNO}.log
        ls -lh --full-time ${DEST} >> ${JOB_PATH}/encode_video_${TSNO}.log 2>&1
        ${VOLO_PROBE} -i ${DEST} >> ${JOB_PATH}/encode_video_${TSNO}.log 2>&1
	echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/encode_video_${TSNO}.done
else
        echo "Split Video Fail :: "`date +"%Y-%m-%d %H:%M:%S"` >> ${JOB_PATH}/encode_video_${TSNO}.log
	echo "Split Video Error" > ${JOB_PATH}/encode_video_${TSNO}.error
	echo "Split Video Error :: Trunk["${TSNO}"]" > ${JOB_PATH}/job.error
fi

exit 0
