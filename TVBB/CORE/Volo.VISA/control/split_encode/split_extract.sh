#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
# Volo Split Encoder :: Scripts to Split Encode Video Source with Framerate Conversion
#
 
cd /opt/Volo.VISA/control/split_encode
JID=$1
SOURCE=$2
DEST=$3
DESTFR=$4
VTRACK=1
DEINT=1
DURSPLIT=60
PROFILE=360p.xml
 
FRC_BIN=/opt/Volo.VISA/control/split_encode/encode_spfrc.sh
VIDEO_ENC=/opt/Volo.VISA/client/helper/splitencode.sh
AUDIO_ENC=/opt/Volo.VISA/client/helper/splitencode_audio.sh
MERGE_ENC=/opt/Volo.VISA/control/split_encode/encode_merge.sh
JOB_PATH=/tmp/encode/SP_${JID}
DEV_PATH=/dev/shm/Volo.VISA/splitjobs/F_${JID}

echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Encode Job Submit :: JID[${1}] SOURCE[${2}] DESTINATION PATH[${3}] OUTPUT FRAME RATE[${4}]
 
# Source Not Exist
if [ ! -f "${SOURCE}" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Job Error :: JOB[${JID}] SOURCE[${SOURCE}] Not Found 
fi

# Create Encode Working Path
#if [ ! -d "${JOB_PATH}" ]; then
#	mkdir -p ${JOB_PATH}
#else
#	rm -rf ${JOB_PATH}/*
#fi

# Create or Purge Split Working Path
#if [ ! -d "${DEST}" ]; then
#	mkdir -p ${DEST}
#fi

# Check Source Info
DURSOURCE=`mediainfo --Inform="Video;%Duration%" ${SOURCE}`
DURFR=`mediainfo --Inform="Video;%FrameRate%" ${SOURCE}`
#resolution=`mediainfo --Inform="Video;%Width%x%Height%" $1`

# Split Start
echo "Split Encode Start :: "`date +"%Y-%m-%d %H:%M:%S"`
echo "Source Duration       = ${DURSOURCE}"
echo "Source Frame Rate     = ${DURFR}"
echo "Destination Path      = ${DEST}"
echo "Convert Frame Rate    = ${DESTFR}"
echo "Split Trunk Duration  = ${DURSPLIT}"

# Split Video/Audio
echo [SPLIT AUDIO CMD]:: ${FRC_BIN} ${JID} ${JID} -i ${SOURCE} 0 0 ${DEST} SPTS ${DURSOURCE} 1.0 ${DESTFR} ${DURSPLIT}

# Encode Audio
echo [ENCODE AUDIO CMD]:: ${AUDIO_ENC} ${JID} ${JID} ${JID}${JID} ${DEST}/SPTS_audio.mov ${DEST}/SPOUT_audio.mov ${PROFILE} 1

# Encode Video
TSCOUNT=0
for m in "${DEST}"/*.sf
do
  let TSCOUNT+=1
  echo [ENCODE VIDEO CMD]:: ${VIDEO_ENC} ${JID} ${JID} ${JID}${JID} ${m} ${DEST}/SPOUT_${TSCOUNT}.ts ${PROFILE} 1
done

# Merge TS
echo [MERGE TS CMD]:: ${MERGE_ENC} ${JID} ${JID} ${JOB_PATH}/SPOUT ${TSCOUNT} ${DEST}/"${SOURCE%.*}"_OUT.mp4

# Normal Exit
echo "Split Encode End :: "`date +"%Y-%m-%d %H:%M:%S"`
exit 0
