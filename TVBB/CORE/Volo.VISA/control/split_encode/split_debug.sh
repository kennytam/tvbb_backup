#!/bin/sh
#
# Volo Split Encoder :: Scripts to Split Video Source
# VISA v1.4 Build Sat Sep  5 02:01:31 HKT 2015 (DaoLab)
#
# "USAGE: encode_split.sh [JOB ID] [OUTPUT ID] [SOURCE FULL PATH] [NO. OF SPLIT] [SPLIT WORK FULL PATH] [SPLIT FILE PREFIX]"
#
 
round()
{
echo "$1" | awk '{printf("%d\n",$1 + 0.5)}'
};

divide()
{
echo "$1" "$2" | awk '{printf("%f\n",$1/$2)}'
};

multiply()
{
echo "$1" "$2" | awk '{printf("%f\n",$1*$2)}'
};

cd /opt/Volo.VISA/scheduler/split_encode
JID=$1
OID=$2
SOURCE=$3
NOOFSPLIT=$4
DEST=$5
OUTFILEPREFIX=$6
 
VOLO_BIN=volo
JOB_LABEL=SP_${JID}_${OID}

# Parse the Video Duration by MediaInfo
totalDuration=`mediainfo --Output="Video;%Duration%" ${SOURCE}`

# Split with Duration
totalDurationInSec=$(round $(divide $totalDuration 1000))
echo "Split Total Duration = $totalDurationInSec"
duration=$(round $(divide $totalDurationInSec $NOOFSPLIT))
echo "Split Duration = $duration"

for((s=0; s<$NOOFSPLIT; s++))
do
        if [ $s -eq 0 ]
        then
                j1=""
                j2=""

        else
                j1="-ss $((duration*s-1))"
		j2="-ss 1"

        fi

        if [ $((s+1)) -eq $NOOFSPLIT ]
        then
                d=""

        else
                d="-t $duration"

        fi

	OUTFILE="${OUTFILEPREFIX}_$((s+1)).ts"

        echo "#$((s+1)) Job: Jump = $j1, Duration = $duration, OUTPUT = $OUTFILE"
	echo ${VOLO_BIN} -label SPLIT.F_${JID}_$((s+1)) $j1 -i ${SOURCE} -map 0:v -vcodec copy -map 0:a -acodec libfdk_aac -ab 192k -aac_no_padding 1 $j2 $d -y ${DEST}/${OUTFILE}
done
exit 0
