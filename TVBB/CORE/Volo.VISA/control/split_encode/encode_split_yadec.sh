#!/bin/sh
#
# Volo Split Encoder :: Scripts to Split Video Source
# VISA v1.4 Build Sat Sep  5 02:01:31 HKT 2015 (DaoLab)
#
# "USAGE: encode_split.sh [JOB ID] [OUTPUT ID] [SOURCE FULL PATH] [NO. OF SPLIT] [SPLIT WORK FULL PATH] [SPLIT FILE PREFIX]"
#
 
round()
{
echo "$1" | awk '{printf("%d\n",$1 + 0.5)}'
};

divide()
{
echo "$1" "$2" | awk '{printf("%f\n",$1/$2)}'
};

multiply()
{
echo "$1" "$2" | awk '{printf("%f\n",$1*$2)}'
};

cd /opt/Volo.VISA/scheduler/split_encode
JID=$1
OID=$2
SOURCE=$3
NOOFSPLIT=$4
DEST=$5
OUTFILEPREFIX=$6
 
VOLO_BIN=/opt/volo/bin/volo-1.4.1-split
SPLIT_BIN=/opt/volo/bin/yadec
#VOLO_BIN=/opt/volo/bin/volo-1.4
#VOLO_BIN=/opt/volo/script/volo
JOB_LABEL=SP_${JID}_${OID}
JOB_PATH=/dev/shm/Volo.VISA/splitjobs/${JOB_LABEL}/split
JOB_LOG='/opt/Volo.VISA/logs/VISA.SplitSource'.`hostname`.`date +%Y%m%d`.'log'
ERRSTR=""
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Job Submit :: JID[${1}] OID[${2}] SOURCE[${3}] TRUNK[${4}] DEST[${5}] PREFIX[${6}] >> ${JOB_LOG}
echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Command :: ./encode_split.sh ${1} ${2} ${3} ${4} ${5} ${6} >> ${JOB_LOG}
 
# Source Not Exist
if [ ! -f "${SOURCE}" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Job Error :: JOB[${OID}] SOURCE[${SOURCE}] Not Found >> ${JOB_LOG}
        ERRSTR="[ERROR] Split Source Job Error :: Source Video Not Found"
fi
 
# Create Encode Working Path
mkdir -p ${JOB_PATH}

# Mark Job ERROR
if [ ! -z "${ERRSTR}" ]; then
	echo ${ERRSTR} > ${JOB_PATH}/encode.log
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

EXT=${SOURCE##*.}
if [ "$EXT" == "jpg" ] || [ "$EXT" == "txt" ]; then
	echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Error :: JOB[${OID}] Invalid Source Video Format >> ${JOB_LOG}
	ERRSTR="[ERROR] Split Source Job Error :: Invalid Source Video Format"
	echo ${ERRSTR} > ${JOB_PATH}/encode.log
	echo ${ERRSTR} > ${JOB_PATH}/job.error
	exit 1
fi

# Create or Purge Split Working Path
if [ ! -d "${DEST}" ]; then
	mkdir -p ${DEST}
else
	rm -rf ${DEST}/*
fi

# Parse the Video Duration by MediaInfo
echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.start
totalDuration=`mediainfo --Output="Video;%Duration%" ${SOURCE}`
fps=`mediainfo --Inform="Video;%FrameRate%" ${SOURCE}`

# Split with Duration
totalDurationInSec=$(round $(divide $totalDuration 1000))
echo "Split Total Duration = $totalDurationInSec" > ${JOB_PATH}/encode.log
duration=$(round $(divide $totalDurationInSec $NOOFSPLIT))
frames=$(round $(divide $(multiply $fps $totalDurationInSec) $NOOFSPLIT))
frames=$(round $(multiply $frames 1.1))
#echo "Split Duration = $duration" >> ${JOB_PATH}/encode.log
echo "Split Frames = $frames" #>> ${JOB_PATH}/encode.log

#SOURCEWOEXT=`echo ${SOURCE%.*}`
#OUTFILEPREFIX=`basename ${SOURCEWOEXT}`_${JID}_${OID}

for((s=0; s<$NOOFSPLIT; s++))
do
        if [ $s -eq 0 ]
        then
                j1=""
                j2=""

        else
                j1="-ss $((duration*s-1))"
		j2="-ss 1"

        fi

        if [ $((s+1)) -eq $NOOFSPLIT ]
        then
                d=""

        else
                d="-t $duration"

        fi

	start=$((frames*s))
	end=$((start+frames))

	OUTFILE="${OUTFILEPREFIX}_$((s+1)).ts"

        echo "#$((s+1)) Job: Jump = $j1, Duration = $duration, OUTPUT = $OUTFILE" >> ${JOB_PATH}/encode.log
	#echo [SPLIT CMD]:: ${VOLO_BIN} -label SPLIT.F_${JID}_$((s+1)) $j1 -i ${SOURCE} -map 0:v -vcodec copy -map 0:a -acodec libfdk_aac -ab 192k -aac_no_padding 1 $j2 $d -threads 12 -y ${DEST}/${OUTFILE} >> ${JOB_PATH}/encode.log
	#${VOLO_BIN} -label SPLIT.F_${JID}_$((s+1)) $j1 -i ${SOURCE} -map 0:v -vcodec copy -map 0:a -acodec libfdk_aac -ab 192k -aac_no_padding 1 $j2 $d -threads 12 -y ${DEST}/${OUTFILE} >> ${JOB_PATH}/encode.log 2>&1
	echo [SPLIT CMD]:: ${SPLIT_BIN} -i ${SOURCE} -s $start -e $end | ${VOLO_BIN} -f rawvideo -pix_fmt yuv422p -s 1920x1080 -r ${fps} -i - -f mpegts -pix_fmt yuv420p -vcodec volo -an -threads 12 -y ${DEST}/${OUTFILE} >> ${JOB_PATH}/encode.log 2>&1
	${SPLIT_BIN} -i ${SOURCE} -s $start -e $end | ${VOLO_BIN} -f rawvideo -pix_fmt yuv422p -s 1920x1080 -r ${fps} -i - -f mpegts -pix_fmt yuv420p -vcodec volo -an -threads 12 -y ${DEST}/${OUTFILE} >> ${JOB_PATH}/encode.log 2>&1
done
echo `date +"%Y-%m-%d %H:%M:%S"` > ${JOB_PATH}/job.done

exit 0

