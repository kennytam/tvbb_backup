<?php
//
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
// [Split Encode Progress Control Engine]
//
$PARA = include('/opt/Volo.VISA/scheduler/config/scheduler.php');

// 
// START HERE

// Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$procStart=$stTime=$curTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

// Connect Scheduler DB
$config = include('/opt/Volo.VISA/scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .
                             " user=". $config['db_user'] .
                             " password=". $config['db_pass'] .
                             " dbname=".$config['db_name'];
$link = pg_connect($connstr)
	or die('Could not connect: ' . pg_last_error());


// Split Trunk Status Check
$jobquery = pg_exec($link, "SELECT job_id, id FROM output WHERE stage=1 AND c_cancel=false AND split_encode=true AND job_id NOT IN (SELECT job_id FROM job_handler WHERE mode='P') ORDER BY id ASC;");
while ($jobdtl = pg_fetch_array($jobquery)){
  $result = pg_exec($link, "SELECT job_id, output_id, id, trunk_number, trunk_type, split_srcpath, split_srcfile FROM split_output WHERE stage=0 AND worker_id<0 AND c_cancel=false AND output_id=" . $jobdtl["id"] . " ORDER BY id ASC;");
  // Check Split Video/Audio Trunk Ready
  $ttljob = pg_numrows($result);
  for($rj = 1; $rj <= $ttljob; $rj++) {
      $ojrow = pg_fetch_array($result);

      if ($ojrow["trunk_type"] == "A")
		$TrunkLog = $PARA["volo_splitjob_work"] . "/SP_" . $ojrow["job_id"] . "/split/encode_audio.done";
      else
		$TrunkLog = $PARA["volo_splitjob_work"] . "/SP_" . $ojrow["job_id"] . "/split/encode_video_" . $ojrow["trunk_number"] . ".done";
      $TrunkFile = $ojrow["split_srcpath"] . "/" . $ojrow["split_srcfile"];

      
      //kenny hack start, i cannot start a job
//      if (file_exists($TrunkLog) && file_exists($TrunkFile) && filesize($TrunkFile) > 0){
      if (file_exists($TrunkLog) && file_exists($TrunkFile)){
          //kenny hack end
        $splitStart = file_get_contents($TrunkLog);
        $splitStart = substr($splitStart,0, 19);
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitCTRL Split Trunk Ready :: JID[" . $ojrow["job_id"] . "] OID[" . $ojrow["output_id"] . "] SID[" . $ojrow["id"] . "] TRUNK[" . $TrunkFile . "] DONE[" . $TrunkLog . "] READY[" . $splitStart . "]" . "\n";
	// Update Split Job Output
    	$updsql = pg_exec($link, "UPDATE split_output SET stage=0, worker_id=0, progress=0, trunkready_time='" . $splitStart . "', ltime=now(), message='[STATUS] Split Trunk Ready' WHERE stage=0 AND id=" . $ojrow["id"] . ";");
	pg_free_result($updsql);
      } else {
	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitCTRL Split Trunk Not Ready :: JID[" . $ojrow["job_id"] . "] OID[" . $ojrow["output_id"] . "] SID[" . $ojrow["id"] . "] TRUNK[" . $TrunkFile . "] CHECK[" . $TrunkLog . "]" . "\n";
      } // end Trunk Log
 
  } // End Loop
  pg_free_result($result);
}
pg_free_result($jobquery);
if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitCTRL Split Trunk Ready Check End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
}


// Split Trunk Encode Status Check
$curTime=$stTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$jobquery = pg_exec($link, "SELECT job_id, id, stime, progress_fps, progress_mode, progress FROM output WHERE stage=1 AND c_cancel=false AND split_encode=true AND job_id NOT IN (SELECT job_id FROM job_handler WHERE mode='P') ORDER BY id ASC;");
while ($jobdtl = pg_fetch_array($jobquery)){
  // Check any Trunk Ready Waiting Output Jobs
  if ($jobdtl["progress"] == 1){
	$result = pg_exec($link, "SELECT count(*) FROM split_output WHERE stage=0 AND worker_id=0 AND c_cancel=false AND output_id=" . $jobdtl["id"] . ";");
	$ttljob = pg_fetch_array($result);
	pg_free_result($result);

	if ($ttljob[0] > 0){
		$updsql = pg_exec($link, "UPDATE output SET progress=2, message='[STATUS] Split Encode Trunks Ready, Waiting Encode' WHERE id=" . $jobdtl["id"] . ";");
		pg_free_result($updsql);
	}
  }

  // Check Trunk Encode Status
  $minTime = substr($jobdtl["stime"], 0, 19);
  $maxFPS  = 0;
  $pmode   = 1;
  $result = pg_exec($link, "SELECT id, job_id, output_id, stime, ltime, trunk_type, progress_fps, progress_mode FROM split_output WHERE stage=1 AND worker_id>0 AND c_cancel=false AND output_id=" . $jobdtl["id"] . " ORDER BY id ASC;");
  $ttljob = pg_numrows($result);
  for($rj = 0; $rj < $ttljob; $rj++) {
	$ojrow = pg_fetch_array($result);

	// Split Encode Timeout Error - 10mins
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	$jobDiff = strtotime($curTime) - strtotime(substr($ojrow["ltime"],0, 19));
	if ($jobDiff > $PARA['volo_timeout']){
	  if ($ojrow["trunk_type"] == "V")
		$errTYPE = "Video";
	  else
		$errTYPE = "Audio";
	  echo "[ERROR][" . $curTime . "][" . gethostname() . "] VOLO.SplitCTRL Split Trunk " . $errTYPE . " Encode Timeout Error :: JID[" . $ojrow["job_id"] . "] OID[" . $ojrow["output_id"] . "] SID[" . $ojrow["id"] . "]" . "\n";
	  // Update Split Job Output
    	  $updsql = pg_exec($link, "UPDATE split_output SET stage=-1, err=1, ltime=now(), message='[ERROR] Split Trunk " . $errTYPE . " Encode Timeout' WHERE stage IN (0,1) AND id=" . $ojrow["id"] . ";");
	  pg_free_result($updsql);
	} else {
	  if ($ojrow["trunk_type"] == "V"){
		if ($minTime > substr($ojrow["stime"], 0, 19)) $minTime = substr($ojrow["stime"], 0, 19);
		if ($maxFPS < $ojrow["progress_fps"]) $maxFPS = $ojrow["progress_fps"];
		$pmode = $ojrow["progress_mode"];
	  }
	} // End Error Check
  } // end for parse split output 
  pg_free_result($result);

  if ($ttljob > 0){
	// echo "UPDATE OID[" . $jobdtl["id"] . "] END ==> " . $minTime . "/" . $maxFPS . "/" . $pmode . "\n";
	// Update Split Job Output
	if ($minTime > substr($jobdtl["stime"], 0, 19)) $minTime = $jobdtl["stime"];
	$updsql = pg_exec($link, "UPDATE output SET progress_fps=" . $maxFPS . ", progress_mode='" . $pmode . "', stime='" . $minTime . "', ltime=now(), merge_stime=now(), merge_ltime=now(), mux_stime=now(), mux_ltime=now() WHERE id=" . $jobdtl["id"] . ";");
	pg_free_result($updsql);
  }
}
pg_free_result($jobquery);
if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitCTRL Split Trunk Encode Check End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
}
 

// End Process
if ($PARA["schd_debug_mode"]){
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitCTRL Process Done :: TIME[" . $procStart . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($procStart,0,19)) ) . ")\n";
}
pg_close($link);
exit(0);
?>
