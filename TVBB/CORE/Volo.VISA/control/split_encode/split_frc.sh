#!/bin/sh
#
# VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
#
# Volo Split Encoder :: Scripts to Split Encode Video Source with Framerate Conversion
#
 
cd /opt/Volo.VISA/control/split_encode
SOURCE=$1
DEST=$2
DESTFR=$3
JID=10
VTRACK=1
DEINT=1
DURSPLIT=30
PROFILE=tvb_device_4k_h265
 
FRC_BIN=/opt/Volo.VISA/control/split_encode/encode_spfrc.sh
VIDEO_ENC=/opt/Volo.VISA/client/helper/splitencode.sh
AUDIO_ENC=/opt/Volo.VISA/client/helper/splitencode_audio.sh
MERGE_ENC=/opt/Volo.VISA/control/split_encode/encode_merge.sh
DESTMP4=`basename ${SOURCE} ".${SOURCE##*.}"`
JOB_PATH=/media/volo_spool_B/FRC_Encode/SP_${DESTMP4}_${3}fps

echo [INFOS][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Encode Job Submit :: SOURCE[${1}] DESTINATION PATH[${2}] OUTPUT FRAME RATE[${3}]
 
# Source Not Exist
if [ ! -f "${SOURCE}" ]
then
        echo [ERROR][`date +"%Y-%m-%d %H:%M:%S"`][`hostname`] VISA.SPLIT Split Source Job Error :: SOURCE[${SOURCE}] Not Found 
	exit 1
fi
 
# Create Encode Working Path
if [ ! -d "${JOB_PATH}" ]; then
       mkdir -p ${JOB_PATH}
else
       rm -rf ${JOB_PATH}/*
fi
 
# Create or Purge Split Working Path
if [ ! -d "${DEST}" ]; then
       mkdir -p ${DEST}
fi

# Clean DEV Path
DEV_PATH=/dev/shm/Volo.VISA/splitjobs/SP_${JID}
if [ -d "${DEV_PATH}" ]; then
       rm -rf ${DEV_PATH}
fi
DEV_PATH=/dev/shm/Volo.VISA/splitjobs/F_${JID}${JID}
if [ -d "${DEV_PATH}" ]; then
       rm -rf ${DEV_PATH}
fi

# Check Source Info
DURSOURCE=`mediainfo --Inform="Video;%Duration%" ${SOURCE}`
DURFR=`mediainfo --Inform="Video;%FrameRate%" ${SOURCE}`
#resolution=`mediainfo --Inform="Video;%Width%x%Height%" $1`
if [ "${DURSOURCE}" == "" ] || [ "${DURSOURCE}" == " " ]; then
	DURSOURCE=0
	DURFR=0
fi
 
# Split Start
echo "Split Encode Start :: "`date +"%Y-%m-%d %H:%M:%S"`
echo "Source Duration       = ${DURSOURCE}"
echo "Source Frame Rate     = ${DURFR}"
echo "Destination Path      = ${DEST}"
echo "Convert Frame Rate    = ${DESTFR}"
echo "Split Trunk Duration  = ${DURSPLIT}"
 
# Split Video/Audio
echo [SPLIT VIDEO/AUDIO CMD]:: ${FRC_BIN} ${JID} ${JID} ${SOURCE} 0 0 ${JOB_PATH} SPOUT ${DURSOURCE} 1.0 ${DESTFR} ${DURSPLIT}
${FRC_BIN} ${JID} ${JID} ${SOURCE} 0 0 ${JOB_PATH} SPOUT ${DURSOURCE} 1.0 ${DESTFR} ${DURSPLIT}
mv /dev/shm/Volo.VISA/splitjobs/SP_${JID} ${JOB_PATH}/split.done

# Encode Audio
#echo [ENCODE AUDIO CMD]:: ${AUDIO_ENC} ${JID} ${JID} ${JID}${JID} ${JOB_PATH}/SPTS_audio.mov ${JOB_PATH}/SPOUT_audio.mov ${PROFILE} 1
#${AUDIO_ENC} ${JID} ${JID} ${JID}${JID} ${JOB_PATH}/SPTS_audio.mov ${JOB_PATH}/SPOUT_audio.mov ${PROFILE} 1 &
 
# Encode Video
TSCOUNT=0
ENCPARA="-vsync 0 -container ts -audio OFF -stitchable Y "
for m in "${JOB_PATH}"/*.sf
do
  let TSCOUNT+=1
  TSFILE=`basename ${m} ".${m##*.}"`
  echo ${ENCPARA} > /dev/shm/Volo.VISA/splitjobs/${JID}_${JID}_${JID}${JID}.para
  echo [ENCODE VIDEO CMD]:: ${VIDEO_ENC} ${JID} ${JID} ${JID}${JID} ${m} ${JOB_PATH}/SPOUT_${TSFILE}.ts ${PROFILE} 1
  ${VIDEO_ENC} ${JID} ${JID} ${JID}${JID} ${m} ${JOB_PATH}/SPOUT_${TSFILE}.ts ${PROFILE} 1
  mv /dev/shm/Volo.VISA/splitjobs/F_${JID}${JID} ${JOB_PATH}/TS${TSFILE}_encode.done
  rm -rf ${m}
done
 
# Merge TS
FINAL_OUT=${DESTMP4}_${DESTFR}fps_ID10.mp4
echo [MERGE TS CMD]:: ${MERGE_ENC} ${JID} ${JID} ${JOB_PATH}/SPOUT ${TSCOUNT} ${JOB_PATH}/${FINAL_OUT}
${MERGE_ENC} ${JID} ${JID} ${JOB_PATH}/SPOUT ${TSCOUNT} ${JOB_PATH}/${FINAL_OUT}
mv /dev/shm/Volo.VISA/splitjobs/SP_${JID} ${JOB_PATH}/merge.done
 
# Move Final Output
if [ -f "${JOB_PATH}/${FINAL_OUT}" ]
then
	mv ${JOB_PATH}/${FINAL_OUT} ${DEST}/
fi

# Normal Exit
echo "Split Encode End :: "`date +"%Y-%m-%d %H:%M:%S"`

# Housekeep
mv ${JOB_PATH} ${JOB_PATH}.`hostname`.`date +%Y%m%d`.done

exit 0
