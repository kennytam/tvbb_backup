<?php
//
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
// [Split Encode Worker Unit Control Engine]
//
$PARA = include('/opt/Volo.VISA/scheduler/config/scheduler.php');

// Scan through the Video Source Path
function getDIRContents($dir, $ext)
{
global $PARA;
  $handle = opendir($dir);
  if ( !$handle ) return array();
  $contents = array();
  while ( $entry = readdir($handle) )
  {
    if ( $entry=='.' || $entry=='..' || substr($entry, 0, 1)=='.' || $entry==$PARA['encode_control_file'] || $entry==$PARA['audio_map_file'] ) continue;
 
    $entry = $dir.DIRECTORY_SEPARATOR.$entry;
    if ( pathinfo($entry, PATHINFO_EXTENSION)==$PARA['encode_err_pname'] ) continue;
 
    if ( is_file($entry) )
    {
      if (($ext == "" || pathinfo($entry, PATHINFO_EXTENSION) == $ext) && is_writable($entry))
        $contents[] = $entry;
    }
    // One Level Only
    // else if ( is_dir($entry) )
    // {
    //   $contents = array_merge($contents, getDIRContents($entry, $ext));
    // }
  }
  closedir($handle);
  return $contents;
}
 
// 
// START HERE

// Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$procStart=$stTime=$curTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

// Connect Scheduler DB
$config = include('/opt/Volo.VISA/scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .
                             " user=". $config['db_user'] .
                             " password=". $config['db_pass'] .
                             " dbname=".$config['db_name'];
$link = pg_connect($connstr)
	or die('Could not connect: ' . pg_last_error());


// Get Output Info for Normal Encode Jobs
$SRCPATH  = $PARA['volo_job_done'] . "/NE_*/F_*.done";
$srcList  = explode("\n", shell_exec("find " . $SRCPATH . " -name '*.minfo' -print"));
foreach($srcList as $job) {
   if (trim($job) == "") continue;
   $jobInfo    = pathinfo($job);
   $fullPath   = $jobInfo['dirname'];
   $outID      = $jobInfo['filename'];
   $outInfo    = pg_escape_string(file_get_contents($job));
   $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
   echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitUNIT Normal Encode Output Info Update :: OID[" . $outID . "] FILE[" . $job . "]\n";
   $updsql = pg_exec($link, "UPDATE output SET output_info='" . $outInfo . "' WHERE id=" . $outID . ";");
   pg_free_result($updsql);
   $cmd = "mv -f " . $job . " " . $job . ".done";
   exec("$cmd > /dev/null 2>&1 &");
} // End Parse Result Directory


// Reset Unprocess Pending Split Job -- DB ERROR??
$result = pg_exec($link, "SELECT job_id,output_id,id,stage,progress,worker_id,err,c_cancel FROM split_output WHERE stage=2 AND worker_id<1 AND err=0 ORDER BY job_id,output_id,id ASC;");
$ttljob = pg_numrows($result);
for($rj = 0; $rj < $ttljob; $rj++) {
      $ojrow = pg_fetch_array($result, $rj);

      echo "[WARNS][" . $curTime . "][" . gethostname() . "] VOLO.SplitUNIT Reset Split Trunks Status :: JID[" . $ojrow["job_id"] . "] OID[" . $ojrow["output_id"] . "] SID[" . $ojrow["id"] . "] STAGE[" . $ojrow["stage"] . "] PROGRESS[" . $ojrow["progress"] . "] WORKER[" . $ojrow["worker_id"] . "] ERRNO[" . $ojrow["err"] . "] CANCEL[" . $ojrow["c_cancel"] . "]" . "\n";
}
pg_free_result($result);

if ($ttljob > 0){
  // Update Split Job Output
  $updsql = pg_exec($link, "UPDATE split_output SET stage=0, worker_id=0, progress=0, ltime=now(), message='[STATUS] Split Trunk Status Reset' WHERE stage=2 AND worker_id<1 AND err=0;");
  pg_free_result($updsql);
}
if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitUNIT Reset Split Trunks Status End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
}


// Manage Job Handler (GEN THUMBNAIL/CANCEL/RETRY)
$curTime=$stTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$result = pg_exec($link, "SELECT * FROM job_handler WHERE mode='P' ORDER BY id ASC;");
while ($job = pg_fetch_array($result)) {
	// Purge Encoded Split Trunks and Job Done File
	if ($job["c_retry"] == 't'){
		echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitUNIT Retry Job :: JID[" . $job["job_id"] . "] OID[" . $job["output_id"] . "]" . "\n";

		$retryfile = "-retry." . date("YmdHis", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
		// Retry a Output Job
		if ($job["output_id"] > 0){
		    // Reset Split Work Path
		    $jobFile = $PARA['volo_splitjob_work'] . "/SP_" . $job["job_id"] . "/muxaudio/" . $job["output_id"];
		    if ( file_exists($jobFile) ){
		    	exec("mv -f " . $jobFile . " " . $jobFile . $retryfile);
		    }
		    $jobFile = $PARA['volo_splitjob_work'] . "/SP_" . $job["job_id"] . "/merge/" . $job["output_id"];
		    if ( file_exists($jobFile) ){
		    	exec("mv -f " . $jobFile . " " . $jobFile . $retryfile);
		    }

		    // Update Output Job
		    $updsql = pg_exec($link, "UPDATE split_output SET c_cancel=false,err=0,stage=0,worker_id='-1',progress=0,progress_fps=0,progress_mode='1',stime=now(),ltime=now(),message='[STATUS] Encode Output Job Resubmitted' WHERE output_id=" . $job["output_id"] . " AND stage IN (-1,0,1,3);");
		    pg_free_result($updsql);
		    $updsql = pg_exec($link, "UPDATE split_output SET stage=2,c_cancel=false,err=0 WHERE output_id=" . $job["output_id"] . " AND stage>=2;");
		    pg_free_result($updsql);
		    $updsql = pg_exec($link, "UPDATE output SET c_cancel=false,err=0,stage=1,progress=1,progress_fps=0,progress_mode='1',stime=now(),ltime=now(),merge_stime=now(),merge_ltime=now(),mux_stime=now(),mux_ltime=now(),message='[STATUS] Encode Output Job Resubmitted' WHERE id=" . $job["output_id"] . ";");
		    pg_free_result($updsql);
		// Retry whole Job
		} else {
		    // Reset Work Trunk
		    $tquery = pg_exec($link, "SELECT id, trunk_number, split_srcpath, split_destpath FROM split_output WHERE output_id=" . $job["output_id"] . " LIMIT 1;");
		    $trunk = pg_fetch_array($tquery);
		    pg_free_result($tquery);
		    $jobFile = $PARA['volo_splitjob_work'] . "/SP_" . $job["job_id"];
		    $dstFile = $PARA['volo_splitjob_done'] . "/SP_" . $job["job_id"] . $retryfile;
		    exec("mv -f " . $jobFile . " " . $dstFile);
		    exec("rm -rf " . $trunk["split_srcpath"]);
		    exec("rm -rf " . $trunk["split_destpath"]);

		    // Reset Source Error Path to Source Path
		    $jobFile = $PARA['volo_job_path'] . "/" . $job["job_id"] . "." . $PARA["encode_err_pname"];
		    if (file_exists($jobFile)){
			$jobFile = file_get_contents($jobFile);
		    	exec("mv -f " . $jobFile);
		    }

		    // Reset Source BAK files
		    $updsql = pg_exec($link, "SELECT source FROM job WHERE id=" . $job["job_id"] . ";");
		    $srcjob = pg_fetch_array($updsql);
		    pg_free_result($updsql);
                    $jobInfo = pathinfo($srcjob["source"]);
                    $srcList = getDIRContents($jobInfo['dirname'], "");
                    foreach($srcList as $src){
                        $jobInfo = pathinfo($src);
                        if ( $jobInfo["extension"] == "bak" )
                           exec("mv -f " . $src . " " . $jobInfo["dirname"] . "/" . $jobInfo["filename"]);
                    }

		    // Delete SPLIT OUTPUT JOB
		    $updsql = pg_exec($link, "DELETE FROM split_output WHERE job_id=" . $job["job_id"] . ";");
		    pg_free_result($updsql);
		    // Reset OUTPUT JOB
		    $updsql = pg_exec($link, "UPDATE output SET c_cancel=false,stage=0,worker_id=0,err=0,progress=0,progress_fps=0,progress_mode='1',stime=now(),ltime=now(),merge_stime=now(),merge_ltime=now(),mux_stime=now(),mux_ltime=now(),message='[STATUS] Encode Output Job Resubmitted' WHERE job_id=" . $job["job_id"] . ";");
		    pg_free_result($updsql);
		}

		// Reset Retry Job File
		$jobFile = $PARA['volo_job_path'] . "/" . $job["job_id"] . ".job.done";
		if (file_exists($jobFile)){
			exec("mv " . $jobFile . " " . substr($jobFile, 0, -5));
		} else {
			$zipFile = $PARA['volo_job_path'] . "/Z_archive/" . $job["job_id"] . ".job.done.gz";
			if (file_exists($zipFile)){
				$zipFile = $PARA['volo_job_path'] . "/Z_archive/" . $job["job_id"] . ".*.gz";
				exec("mv " . $zipFile . " " . $PARA['volo_job_path']);
				$zipFile = $PARA['volo_job_path'] . "/" . $job["job_id"] . ".*.gz";
				exec("gunzip " . $zipFile);
				exec("mv " . $jobFile . " " . substr($jobFile, 0, -5));
			}
		}
	}

	// Mark Job Cancel
	if ($job["c_cancel"] == 't'){
	  $jobPath = $PARA['volo_splitjob_work'] . "/SP_" . $job["job_id"] . "/split";
	  if (is_dir($jobPath)){
		$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
		echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitUNIT Cancel Job:: JID[" . $job["job_id"] . "] OID[" . $job["output_id"] . "] CANCEL FILE[" .  $jobPath . "/job.cancel]" . "\n";;
		echo "VOLO.SplitUNIT Cancel Job:: JID[" . $job["job_id"] . "] OID[" . $job["output_id"] > $jobPath . "/job.cancel";
		file_put_contents($jobPath . "/job.cancel", $curTime);
		$updsql = pg_exec($link, "UPDATE split_output SET stage=3,c_cancel=true,ltime=now(),message='[STATUS] Trunk Encode Process Cancelled' WHERE job_id=" . $job["job_id"] . " AND stage=0;");
		pg_free_result($updsql);
	  	$updsql = pg_exec($link, "UPDATE output SET stage=3,c_cancel=true,ltime=now(),merge_stime=now(),merge_ltime=now(),mux_stime=now(),mux_ltime=now(),message='[STATUS] Encode Process Cancelled' WHERE job_id=" . $job["job_id"] . " AND stage IN (0,1);");
	  	pg_free_result($updsql);
	  } else {
		$updsql = pg_exec($link, "UPDATE output SET stage=3,c_cancel=true,ltime=now(),merge_stime=now(),merge_ltime=now(),mux_stime=now(),mux_ltime=now(),message='[STATUS] Encode Process Cancelled' WHERE job_id=" . $job["job_id"] . " AND stage=0;");
		pg_free_result($updsql);
	  }
	}

	// Generate Source Thumbnail
	if ($job["c_insert"] == 't'){
		// Log For No Job Publishing Flow Platform
		echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.Publisher Submit Job Success :: JOBID[" . $job["job_id"] . "] SOURCE[" . $job["source_file"] . "]\n";
		// End Log

		$thumb_path = $PARA["volo_thumb_path"] . "/" .  substr($job["ltime"],0,4) . substr($job["ltime"],5,2) . substr($job["ltime"],8,2);
		if (!file_exists($thumb_path . "/" . $job["job_id"] . ".jpg")){
                   if ($job["source_duration"] > 60){
                      $start_thumb = 60;
                   } else {
                      $start_thumb = round($job["source_duration"]/2, 0, PHP_ROUND_HALF_UP);
		   }
                   $cmd = $PARA["volo_thumb_cmd"] . " " . $job["source_file"] . " " . $job["job_id"] . " " . $start_thumb . " " . $thumb_path;
		   echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.SplitUNIT Generate Thumbnail :: JID[" . $job["job_id"] . "] COMMAND[" . $cmd . "]\n";
                   popen($cmd, "r");
		}
	}

	$updsql = pg_exec($link, "UPDATE job_handler SET mode='D', ltime=now() WHERE id=" . $job["id"] . ";");
	pg_free_result($updsql);
}
pg_free_result($result);
if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitUNIT Job Handler End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
}


// Manage Worker Unit
$curTime=$stTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
$result = pg_exec($link, "SELECT id, unit, worker_unit, mode FROM worker WHERE dead=false AND c_shutdown=false ORDER BY id ASC;");
while ($worker = pg_fetch_array($result)) {
   $workunit = 0;
   if ($worker["worker_unit"] < 1)
	$ctlunit = $PARA['volo_encode_unit'];
   else
	$ctlunit = $worker["worker_unit"];
   if ($worker["mode"] == "A"){
	$unitctl = pg_exec($link, "SELECT SUM(encode_unit) FROM split_output WHERE worker_id=" . $worker["id"] . " AND stage=1 AND c_cancel=false;");
	$jobunit = pg_fetch_array($unitctl);
	$workunit += $jobunit['0'];
	pg_free_result($unitctl);
	$unitctl = pg_exec($link, "SELECT SUM(encode_unit) FROM output WHERE worker_id=" . $worker["id"] . " AND stage=1 AND c_cancel=false;");
	$jobunit = pg_fetch_array($unitctl);
	$workunit += $jobunit['0'];
	pg_free_result($unitctl);
	$workunit = $ctlunit - $workunit;
	if ($workunit < 0) $workunit = 0;
   }

   if ($worker['unit'] != $workunit){
	if ($PARA["schd_debug_mode"]){
		$curTime=date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
		echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitCTRL Update Worker Unit :: WORKER ID[" . $worker["id"] . "] UNIT[" . $worker["unit"] . " -> " . $workunit. "]" . "\n";
	}
	$updsql = pg_exec($link, "UPDATE worker SET unit = '" . $workunit . "' WHERE id=" . $worker["id"] . ";");
	pg_free_result($updsql);
   }
}
pg_free_result($result);
if ($PARA["schd_debug_mode"]){
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitUNIT Reset Worker Unit End :: TIME[" . $stTime . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($stTime,0,19)) ) . ")\n";
}


// END
if ($PARA["schd_debug_mode"]){
        $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
        echo "[DEBUG][" . $curTime . "][" . gethostname() . "] VOLO.SplitUNIT Process Done :: TIME[" . $procStart . "-" . substr($curTime, 11, 8) . "](" . ( strtotime(substr($curTime,0, 19))-strtotime(substr($procStart,0,19)) ) . ")\n";
}
pg_close($link);
exit(0);
?>
