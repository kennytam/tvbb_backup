<?php
//
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
//
// Verify Merge Logs
//
if ($argc < 3){
        echo "USAGE: mergecheck.php [Merge Log Path] [Merge List]\n";
        exit(1);
}
$LOGPATH  = $argv[1];
$LOGLIST  = $argv[2];

//############################################################################
// Scan through the Merge Log Path
//############################################################################
function getTXTContents($dir)
{
global $curTime;
  $handle = opendir($dir);
  if ( !$handle ) return array();
  $contents = array();
  while ( $entry = readdir($handle) )
  {
    if ( $entry=='.' || $entry=='..' || substr($entry, 0, 1)=='.' || substr($entry, 0, 1)=='F' ) continue;

    $entry = $dir.DIRECTORY_SEPARATOR.$entry;

    if ( is_file($entry) )
    {
      if (pathinfo($entry, PATHINFO_EXTENSION) == "log")
	$contents[] = $entry;
    }
    else if ( is_dir($entry) )
    {
	$contents = array_merge($contents, getTXTContents($entry));
    }
  }
  closedir($handle);
  return $contents;
}

// 
// START HERE
// Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

if (!file_exists($LOGPATH)){
        echo "[ERROR][" . $curTime . "][" . gethostname() . "] VISA.MergeCheck Error :: Merge Path Not Found [" . $LOGPATH . "]\n";
        exit(1);
}

if (!file_exists($LOGLIST)){
        echo "[ERROR][" . $curTime . "][" . gethostname() . "] VISA.MergeCheck Error :: Merge List File Not Found [" . $LOGLIST . "]\n";
        exit(1);
}

// Process the Encode Jobs in List File
$WORKPATH="/tmp";
$fch = fopen($LOGLIST, "r");
while(!feof($fch)){
  $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
  $jobline = fgets($fch);
  $jobline = trim(preg_replace("/[\n]/", "", $jobline));
  if ($jobline == "") continue;

  if (substr($jobline, -3) == "tgz"){
	$jobpath = $LOGPATH . "/" . preg_replace("/[\n]/", "", $jobline);
	$cmd='cd ' . $WORKPATH . '; tar -zxvf ' . $jobpath;
        shell_exec($cmd);
	$jobpath = $WORKPATH . "/" . substr($jobline, 0, -4) . "/merge";
  } else {
	$jobpath = $LOGPATH . "/" . preg_replace("/[\n]/", "", $jobline) . "/merge";
  }

  // Process the Merge Log
  if (file_exists($jobpath)){
	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.MergeCheck Job Path :: PATH[" . $jobpath . "]\n";
	$errmerge = false;
	$dirList = getTXTContents($jobpath);
	foreach($dirList as $job) {
	  // echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.MergeCheck Merge Path :: PATH[" . $job . "]\n";
	  $cmd='grep "\[error\]" ' . $job . ' | wc -l | bc';
	  $errfile = shell_exec($cmd);
	  if ( trim($errfile) != "0" ){
		echo "[ERROR][" . $curTime . "][" . gethostname() . "] VISA.MergeCheck Merge Job Error :: PATH[" . $job . "]\n";
		$errmerge = true;
	  }
	} // End Parse Directory

	if (substr($jobline, -3) == "tgz"){
		$cmd='rm -rf ' . $WORKPATH . "/" . substr($jobline, 0, -4);
		shell_exec($cmd);
	}
  }
}
fclose($fch);
exit(0);
?>
