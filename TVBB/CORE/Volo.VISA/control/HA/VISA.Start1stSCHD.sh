#!/bin/sh
# Volo Primary Scheduler Failover Startup Script (* MUST RUN BY ROOT)
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
#
cd /opt/Volo.VISA/scheduler

# Force ALL DOWN
#su daolab -c 'cp ./worker.ctl.DOWN ./worker.ctl'
#su daolab -c 'cp ./scheduler.ctl.DOWN ./scheduler.ctl'

# Switch Over 1st Scheduler VISA Config
#cd /opt/Volo.VISA/scheduler/config
#su daolab -c 'cp ./scheduler.php.primary ./scheduler.php'
#su daolab -c 'cp ./database.php.primary ./database.php'
#cd /opt/Volo.VISA/client/config
#su daolab -c 'cp ./config.php.primary ./config.php'
#sleep 1

# Switch Over 1st Scheduler PGSQL DB
service nginx stop
service php-fpm stop
service postgresql-9.4 stop
sleep 1
su postgres -c 'cp /opt/Volo.VISA/control/HA/PrimaryDB/postgresql.conf /var/lib/pgsql/9.4/data/postgresql.conf'
service postgresql-9.4 start
#service php-fpm start
#service nginx start

# Force ALL UP
#cd /opt/Volo.VISA/scheduler
#su daolab -c 'cp ./worker.ctl.UP ./worker.ctl'
#su daolab -c 'cp ./scheduler.ctl.UP ./scheduler.ctl'

# VISA Periodic Scripts (Scheduler)
#su daolab -c '/opt/Volo.VISA/client/VISA.SCHD.Monitor.sh &'
