#!/bin/sh
# Volo Scheduler Stop Script (* MUST RUN BY ROOT)
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
#

echo "Stopping Scheduler Periodic ......"
jobStr=`ps -Alf | grep "periodic.php" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
if [ -z "${jobStr}" ]; then
           RUNNING=-1
else
           jobs=( ${jobStr} )
           if [ ${#jobs[@]} -gt 0 ]; then
		echo ${jobs[3]}
                kill -9 ${jobs[3]}
           fi
fi

echo "Stopping Scheduler Monitor ......"
jobStr=`ps -Alf | grep "VISA.SCHD.Monitor.sh" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
if [ -z "${jobStr}" ]; then
           RUNNING=-1
else
           jobs=( ${jobStr} )
           if [ ${#jobs[@]} -gt 0 ]; then
		echo ${jobs[3]}
                kill -9 ${jobs[3]}
           fi
fi

echo "Stopping Scheduler Publisher ......"
jobStr=`ps -Alf | grep "VISA.PUBS.Monitor.sh" | grep -v "grep " | grep -v "tail " | grep -v "vi " | head -1`
if [ -z "${jobStr}" ]; then
           RUNNING=-1
else
           jobs=( ${jobStr} )
           if [ ${#jobs[@]} -gt 0 ]; then
		echo ${jobs[3]}
                kill -9 ${jobs[3]}
           fi
fi

# Stop Scheduler Web Services
service nginx stop
service php-fpm stop
service postgresql-9.4 stop
