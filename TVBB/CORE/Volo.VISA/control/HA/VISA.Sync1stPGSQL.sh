#!/bin/sh
# Volo Primary Scheduler PGSQL Sync Script (** MUST RUN BY ROOT **)
# VISA v1.3.4 Build Sat Apr 25 23:51:12 HKT 2015 (DaoLab)
#
cd /var/lib/pgsql/9.4/data

echo Stopping PostgreSQL ......
service postgresql-9.4 stop
sleep 1
 
echo Cleaning up old cluster directory ......
su postgres -c 'rm -rf /var/lib/pgsql/9.4/data/*'
sleep 1
sync
 
echo Starting base backup as replicator ......
su postgres -c 'pg_basebackup -h 10.1.240.97 -D /var/lib/pgsql/9.4/data/ -U rep -v -P'
sleep 1
 
echo Setting Master DB in replication mode ......
su postgres -c 'cp /opt/Volo.VISA/control/HA/PrimaryDB/postgresql.conf /var/lib/pgsql/9.4/data/'
su postgres -c 'cp /opt/Volo.VISA/control/HA/PrimaryDB/pg_hba.conf /var/lib/pgsql/9.4/data/'
 
echo Starting PostgreSQL ......
service postgresql-9.4 start
