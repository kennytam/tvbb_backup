<?php
//
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
//
// Poll Media Source for Job Publishing
// eg. php pollsource.php /media/volo_source /media/volo_output TVBC_ID01
//
// [Video Source Structure]
// Source Path - /media/volo_source/cmd001/001/1
// Objects - 
// ------ come_home_love_epi_602.txt  (Trigger Publish Job for *.txt)
// ------ come_home_love_epi_602.png  (Video Thumbnail)
// ------ come_home_love_epi_602.mov  (Video Source)
// ****** come_home_love_epi_602.job  (Encode Job Write by this program)
// Destination Path - /media/volo_output
// Objects - 
// ------ come_home_love_epi_602.txt  (Trigger Encoded Finish Job to CMS)
// ------ come_home_love_epi_602.png  (Video Thumbnail)
// ------ come_home_love_epi_602.mp4  (Encoded Video)
//
if ($argc < 4){
        echo "USAGE: pollsource.php [Source Video Full Name Path] [Destination Full Path] [Encode Plan Name]\n";
        exit(1);
}
$SRCPATH  = $argv[1];
$DEST     = $argv[2];
$JOB_PLAN = $argv[3];

require_once("./client/visa.php");
$PARA = include('/opt/Volo.VISA/scheduler/config/scheduler.php');
define('PUBLISHER', "/opt/Volo.VISA/client/publish.php");
define('PLANPATH', "/opt/Volo.VISA/client/plan");
define('JOBPATH', "/opt/Volo.VISA/jobs");

//############################################################################
// VISA WATCH FOLDER FUNCTION
//############################################################################
function watch_folder($root = '.') {
    $directories = array();
    $last_letter = $root[strlen($root) - 1];
    $root = ($last_letter == '\\' || $last_letter == '/') ? $root : $root . DIRECTORY_SEPARATOR;
    $directories[] = $root;

    while (sizeof($directories)) {
        $dir = array_pop($directories);
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file == '.' || $file == '..') continue;
                $file = $dir . $file;
                if (is_dir($file)) {
                    $directory_path = $file . DIRECTORY_SEPARATOR;
                    array_push($directories, $directory_path);

                    //create a job file
                    if (basename($directory_path) == 'VOLO_JOB_SUBMIT') {
                        if ($handle_a = opendir($directory_path)) {
                            while (false !== ($entry = readdir($handle_a))) {
                                if ($entry == '.' || $entry == '..') continue;
                                $v_file = $directory_path.$entry;
                                if (is_file($v_file) && (time()-filemtime($v_file)) > 30){
                                    
                                    // use size to comfirm again
                                    $stat = stat($v_file);
                                    $size1 = $stat['size'];
                                    sleep(10);
                                    $stat = stat($v_file);
                                    $size2 = $stat['size'];
                                    if ($size1 == $size2) {                       
                                        $mv_loc = dirname($directory_path);
                                        #rename($v_file,$mv_loc.'/'.$entry);
					shell_exec('mv '.$v_file.' '.$mv_loc.'/'.$entry);
                                        touch($mv_loc.'/'.$entry.'.txt');
                                        $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
                                        echo '[INFO][' . $curTime . '][' . gethostname() . '] VISA.WATCH.FOLDER.JOB: '.$mv_loc.'/'.$entry.PHP_EOL;
                                    }
                                }
                            }
                            closedir($handle_a);
                        }
                    }
                    
                    //special case for XDCAM IMX SWITCH PATH 
                    if (basename($directory_path) == 'VOLO_JOB_SUBMIT_AUTO_SWITCH') {
                        if ($handle_a = opendir($directory_path)) {
                            while (false !== ($entry = readdir($handle_a))) {
                                if ($entry == '.' || $entry == '..') continue;
                                $v_file = $directory_path.$entry;
                                if (is_file($v_file) && (time()-filemtime($v_file)) > 30){
                                    
                                    // use size to comfirm again
                                    $stat = stat($v_file);
                                    $size1 = $stat['size'];
                                    sleep(10);
                                    $stat = stat($v_file);
                                    $size2 = $stat['size'];
                                    if ($size1 == $size2) {
                                        //entry = videofile
                                        //mv_loc = /home/Video.Source/nas/watchfolder
                                        $wf_loc = dirname($directory_path);
                                        $watchfolder = basename($wf_loc);
                                        $v_type = trim(str_replace(' ','',shell_exec('mediainfo --Output="General;%Format_Commercial%" '.$v_file)));
                                        
                                        $mv_loc = $wf_loc;
                                        $switch_path = file_exists('/home/Video.Profile/'.$watchfolder.'/SRCSWITCHPATH') ? trim(file_get_contents('/home/Video.Profile/'.$watchfolder.'/SRCSWITCHPATH')) : '';
                                        $switch_watchfolder = $switch_path != '' ? basename($switch_path) : '';
                                                                            
                                        if(file_exists('/home/Video.Profile/'.$switch_watchfolder.'/XDCAM'))
                                            if(preg_match("/XDCAM/", $v_type))
                                                $mv_loc = $switch_path;
                                            
                                        if(file_exists('/home/Video.Profile/'.$switch_watchfolder.'/IMX50'))
                                            if(preg_match("/IMX/", $v_type))
                                                $mv_loc = $switch_path;
                                        
					shell_exec('mv '.$v_file.' '.$mv_loc.'/'.$entry);
                                        touch($mv_loc.'/'.$entry.'.txt');
                                        $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
                                        echo '[INFO][' . $curTime . '][' . gethostname() . '] VISA.WATCH.FOLDER.JOB: '.$mv_loc.'/'.$entry.PHP_EOL;
                                    }
                                }
                            }
                            closedir($handle_a);
                        }
                    }
                    
                }
            }
            closedir($handle);
        }
    }
}

//############################################################################
// Call VISA XML JobSubmit, Return JOBID, Message for ERROR
//############################################################################
function VISAJobPublish($source, $dest, $encode_priority, $split_encode, $plan)
{
global $ERR;
// Get target basename without extension
$ext = pathinfo($source, PATHINFO_EXTENSION);
$file = basename($source, ".".$ext);
$planxml = PLANPATH . '/' . $plan . '.xml';

  if (file_exists($planxml)) {
    $outputs = array();

    $xml = simplexml_load_file($planxml);  
    foreach($xml->children() as $o){
		if ( strcasecmp($o->getName(), "output")!= 0 ) continue;

		array_push($outputs, array(
				'OutputFile'    => $file . $o->OutputSuffix . '.' . $o->OutputExtension,
				'EncodeMode' => $o->EncodeMode,
				'DeviceOutputProfile' => $o->DeviceOutputProfile,
				'Segmentor' => $o->Segmentor,
				'EncodeLength' => $o->EncodeLength
				));
    }
    $visa = new VisaClient();
    $xml = $visa->job_submit($source, $dest, $encode_priority, $split_encode, $outputs);

    if ( strcasecmp($xml->Response->Status, "OK") == 0 ) {
      return $xml->Response->JobID[0];
    } else {
      $ERR = $xml->Response->Reason[0];
      return 0;
    } 
    unset ($visa);
  } else {
    $ERR = 'VISA PLAN XML File Not Found';
    return 0;
  }
}

//############################################################################
// Scan through the Video Source Path
//############################################################################
function getTXTContents($dir)
{
global $PARA, $EXCPATH, $curTime;
  $handle = opendir($dir);
  if ( !$handle ) return array();
  $contents = array();
  while ( $entry = readdir($handle) )
  {
    if ( $entry=='.' || $entry=='..' || substr($entry, 0, 1)=='.' || $entry==$PARA['encode_control_file'] || $entry==$PARA['audio_map_file'] ) continue;

    // Check Exclude DIR List
    $skipProc=false;
    foreach($EXCPATH as $excList){
	if ( $entry==$excList ){
	  echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.Publisher :: Skip Source Exclude Path [" . $dir.DIRECTORY_SEPARATOR.$entry . "]\n";
		$skipProc = true;
		break;
	}
    }
    if ($skipProc) continue;

    $entry = $dir.DIRECTORY_SEPARATOR.$entry;

    if ( is_file($entry) )
    {
      if (pathinfo($entry, PATHINFO_EXTENSION) == "txt" && is_writable($entry))
	$contents[] = $entry;
    }
    else if ( is_dir($entry) )
    {
	$contents = array_merge($contents, getTXTContents($entry));
    }
  }
  closedir($handle);
  return $contents;
}

// 
// START HERE
// Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());
$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));

if (!file_exists($SRCPATH)){
        echo "[ERROR][" . $curTime . "][" . gethostname() . "] VISA.Publisher Error :: Job Monitor Path Not Found [" . $SRCPATH . "]\n";
        exit(1);
}

// Directory Global Encode Parameter - ENCODE
//$JOB_PRIORITY = $PARA["volo_split_priority"];
$JOB_PRIORITY = 20;
if ($PARA["volo_mode"] == "S")
	$JOB_SPENCODE = 'true';
else
	$JOB_SPENCODE = 'false';
$JOB_POST = "N"; // Job Post Processing Default No (ie. Move JPG, Rename TXT/MXF to bak)
$filelist = $SRCPATH . "/ENCODE";
if(file_exists($filelist)){
   $fch = fopen($filelist, "r");
   while(is_resource($fch) && !feof($fch)){
	$jobline = fgets($fch);
	$jobline = preg_replace("/[\n]/", "", $jobline);
	if ( (trim($jobline) != "") && (substr($jobline, 0, 1) != "#") ){
		$jobmode = explode("|", $jobline);
		if ($jobmode[0] == "N") $JOB_SPENCODE = 'false';
		if (trim($jobmode[1]) != "") $JOB_PLAN = $jobmode[1]; // Override input parameter
		if (trim($jobmode[2]) != "") $JOB_PRIORITY = $jobmode[2];
		if (isset($jobmode[3]) && trim($jobmode[3]) != "") $JOB_POST =  $jobmode[3];
	}
   }
   fclose($fch);
}

// Source Exclude Path
$EXCPATH = explode(",", $PARA["source_exclude_path"]);
// Supported Media Extension List
$SRCextList = explode(",", $PARA["source_list"]);
// Scan the Source Path for {SOURCE}.txt
watch_folder($SRCPATH);
$dirList = getTXTContents($SRCPATH);
foreach($dirList as $job) {
   $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
   $job = preg_replace("/[ ]/","\ ",$job);
   $jobInfo    = pathinfo($job);
   $fullPath   = $jobInfo['dirname'];
   $filePrefix = $jobInfo['filename'];
   $SUBPATH    = substr($fullPath, strspn($SRCPATH, $fullPath)+1);
   $SRCFILE    = "";

	// Get ALL Files with same Prefix
        // job -->        /media/volo_source_A/pssd/TEST150116.txt
        // or job -->     /media/volo_source_A/pssd/TEST150116.mxf.txt
        // media -->      /media/volo_source_A/pssd/TEST150116.mxf
        // $fullPath ->   /media/volo_source_A/pssd
        // $filePrefix -> TEST150116

	// Case for {media}.mxf.txt
	$SRCExtension = pathinfo($filePrefix, PATHINFO_EXTENSION);
	if ($SRCExtension != ""){
		foreach($SRCextList as $matchExt){
			if ( $matchExt == strtolower($SRCExtension) ){
				$SRCFILE = $fullPath . "/" . $filePrefix;
				$filePrefix = pathinfo($filePrefix, PATHINFO_FILENAME );
				break;
			}
		}
	}

	// Case for {media}.txt
	/*
	if ($SRCFILE == ""){
	    $SRCList = explode("\n", shell_exec("ls " . $fullPath . "/" .  $filePrefix . ".*"));
	    foreach($SRCList as $SRCContent){
		$SRCExtension = pathinfo($SRCContent, PATHINFO_EXTENSION);
		if ($SRCExtension != ""){
			foreach($SRCextList as $matchExt){
				if ( $matchExt == strtolower($SRCExtension) ){
					$SRCFILE = $fullPath . "/" . $filePrefix . "." . $SRCExtension;
					break;
				}
			}
		}
		if ($SRCFILE != "") break;
	    }
	}
	*/

	// if ($SRCFILE == ""){
	//   echo "[WARNS][" . $curTime . "][" . gethostname() . "] VISA.Publisher :: Skip Unmatched Source Prefix [" . $filePrefix . "]\n";
	// } else {
	if ($SRCFILE != "" && file_exists($SRCFILE)){
	   $SRCNAME = pathinfo($SRCFILE, PATHINFO_BASENAME);
	   $OUTPUT = $DEST . "/" . $SUBPATH;
	   $source_duration = shell_exec("mediainfo --Output=\"Video;%Duration%\" $SRCFILE");

	   if (trim($source_duration) == ""){
		echo "[WARNS][" . $curTime . "][" . gethostname() . "] VISA.Publisher :: Skip Unknown Source Video Type [" . $SRCFILE . "]\n";
                unlink($SRCFILE.'.txt');
	   } else if (!file_exists($SRCFILE)){
		echo "[ERROR][" . $curTime . "][" . gethostname() . "] VISA.Publisher Error :: Source Video Not Found [" . $SRCFILE . "]\n";
	   } else {
		// Set Encode Para from Global Setting
		$source_duration = round($source_duration/1000, 0, PHP_ROUND_HALF_UP);
		$SPEncode = $JOB_SPENCODE;
		$Priority = $JOB_PRIORITY;
		$Plan     = $JOB_PLAN;
		$JobPost  = $JOB_POST;

		// Loop Path Back if any ENCODE mode file
		$filelist = "";
		$curlist = $SRCPATH;
		$pathLoop = explode("/", substr($fullPath, strlen($SRCPATH)));
		for ($i=1; $i<sizeof($pathLoop); $i++){
			$curlist .= "/" . $pathLoop[$i];
			if(file_exists($curlist . "/ENCODE")) $filelist = $curlist . "/ENCODE";
		}
 
		// Check Encode Mode from ENCODE mode file
		if( $filelist != "" && file_exists($filelist) ){
		   $fch = fopen($filelist, "r");
		   while(is_resource($fch) && !feof($fch)){
			$jobline = fgets($fch);
			$jobline = preg_replace("/[\n]/", "", $jobline);
			if ( (trim($jobline) != "") && (substr($jobline, 0, 1) != "#") ){
				$jobmode = explode("|", $jobline);
				if ($jobmode[0] == "N") $SPEncode = 'false';
				if (trim($jobmode[1]) != "") $Plan = $jobmode[1];
				if (trim($jobmode[2]) != "") $Priority = $jobmode[2];
				if (isset($jobmode[3]) && trim($jobmode[3]) != "") $JobPost  = $jobmode[3];
			}
		   }
		   fclose($fch);
		}
                
                //kenny hack, redirect output location
                $profile_path = $PARA['volo_encode_profile_path'].'/'.basename($curlist);
                if(file_exists($profile_path.'/DESTINATION')){
                    $OUTPUT = file_get_contents($profile_path.'/DESTINATION');
                }else if(file_exists($PARA['volo_encode_profile_path'].'/ERROR/DESTINATION')){
                    $OUTPUT = file_get_contents($PARA['volo_encode_profile_path'].'/ERROR/DESTINATION');
                }
                //kenny hack end

		echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.Publisher Submit Job :: SOURCE[" . $SRCFILE . "] DEST[" . $OUTPUT . "] PLAN[" . $Plan . "]\n";
		echo "[INFOS][" . $curTime . "][" . gethostname() . "] File List :: " . "\n";
		$filelist = exec("ls -lt --full-time " . $fullPath . "/" . $filePrefix . ".*", $listout);
		while(list(,$filelist) = each($listout)){
			echo $filelist, "\n";
		}

		// Encode Job Publish
		$jobID = VISAJobPublish($SRCFILE, $OUTPUT, $Priority, $SPEncode, $Plan);

		if ($jobID > 0){
			// Generate the Thumbnail
			if ($source_duration > 60)
				$start_thumb = 60;
			else
				$start_thumb = round($source_duration/2, 0, PHP_ROUND_HALF_UP);
			$thumb_path = $PARA["volo_thumb_path"] . "/" . date("Ymd", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
			popen($PARA["volo_thumb_cmd"] . " $SRCFILE $jobID $start_thumb $thumb_path", "r");
 
			// Post Processing Mode (Default NULL|N, Y = Source Purge Mode, PROMO=Adhoc Promote API)
			if ($JobPost == "PROMO"){
				// OSCAR Adhoc API CASE
				$api_url="http://promoadmin.tvb.com/event/oscars2016/uploadStart.php?v=" . $filePrefix;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $api_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				$response = curl_exec($ch);
				curl_close($ch);
				echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.Publisher CMS API Return :: APICALL[" . $api_url . "] RETURN[" . $response . "]" . "\n";
			}

			// Housekeep Job File First
			popen("mv -f " . $job . " " . JOBPATH . "/" . $jobID . "." . $filePrefix . ".txt.encode", "r");
			echo "[INFOS][" . $curTime . "][" . gethostname() . "] VISA.Publisher Submit Job Success :: JOBID[" . $jobID . "] SOURCE[" . $SRCFILE . "] DEST[" . $OUTPUT . "] PLAN[" . $Plan . "] POSTMODE[" . $JobPost  . "]\n";
			// Job File --
			// 1977|/media/adhoc/oscar/source|op0101.txt|op0101.mxf|/media/adhoc/oscar/output/|PROMO|op0101|3565|{PUBLISH JOB DETAIL [/media/adhoc/oscar/source  /media/adhoc/oscar/source/op0101.mxf /media/adhoc/oscar/output/ OSCAR PROMO op0101 3565]}
			file_put_contents(JOBPATH . '/' . $jobID . '.job', $jobID . '|' . $fullPath . '|' . $filePrefix . '.txt|' . $SRCNAME . '|' . $OUTPUT . '|' . $JobPost  . '|' . $filePrefix . '|' . $source_duration . '|{PUBLISH DETAIL[' . $fullPath . ' ' . $SUBPATH . ' ' . $SRCFILE . ' ' . $OUTPUT . ' ' . $Plan . ' ' . $JobPost  . ' ' . $filePrefix . ' ' . $source_duration . ']}' . "\n");
		} else {
			echo "[ERROR][" . $curTime . "][" . gethostname() . "] VISA.Publisher Submit Job Error :: ERROR[" . $ERR . "]\n";
			file_put_contents(JOBPATH . '/' . $SRCNAME . '.job.error', $jobID . '|' . $fullPath . '|' . $filePrefix . '.txt|' . $SRCNAME . '|' . $OUTPUT . '|' . $JobPost . '|'. $filePrefix . '|'. $source_duration . '|{PUBLISH DETAIL[' . $fullPath . ' ' . $SUBPATH . ' ' . $SRCFILE . ' ' . $OUTPUT . ' ' . $Plan . ' ' . $JobPost  . ' ' . $filePrefix . ' ' . $source_duration . ']}' . "\n");
		}
	   } // End Correct Media Duration
	} // End Match Extension
} // End Parse Directory

exit(0);
?>

