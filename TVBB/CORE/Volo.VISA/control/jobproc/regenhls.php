<?php
//
// VISA (SE) v1.4.8 Build Tue Jan  5 16:03:43 HKT 2016 (DaoLab)
//
// Process Output Job Queue Status
// eg. php procqueue.php
// 
// [Video Source Structure]
// Source Path - /media/volo_source/cmd001/001/1
// Objects - 
// ------ come_home_love_epi_602.txt  (Trigger Publish Job for *.txt)
// ------ come_home_love_epi_602.png  (Video Thumbnail)
// ------ come_home_love_epi_602.mov  (Video Source)
// ****** come_home_love_epi_602.job  (Encode Job Write by this program)
// Destination Path - /media/volo_output
// Objects - 
// ------ come_home_love_epi_602.txt  (Trigger Encoded Finish Job to CMS)
// ------ come_home_love_epi_602.png  (Video Thumbnail)
// ------ come_home_love_epi_602.mp4  (Encoded Video)
//
$PARA = include('/opt/Volo.VISA/scheduler/config/scheduler.php');
//$JOBPATH = $PARA["volo_job_path"];
$JOBPATH = "/opt/Volo.VISA/jobregen";
$EHLSCMD = "/opt/Volo.VISA/client/helper/encodeETS.sh";

// Scan through the Video Source Path
function getDIRContents($dir, $ext)
{
global $PARA;
  $handle = opendir($dir);
  if ( !$handle ) return array();
  $contents = array();
  while ( $entry = readdir($handle) )
  {
    if ( $entry=='.' || $entry=='..' || substr($entry, 0, 1)=='.' || $entry==$PARA['encode_control_file'] || $entry==$PARA['audio_map_file'] ) continue;
 
    $entry = $dir.DIRECTORY_SEPARATOR.$entry;
    if ( pathinfo($entry, PATHINFO_EXTENSION)==$PARA['encode_err_pname'] ) continue;

    if ( is_file($entry) )
    {
      if (($ext == "" || pathinfo($entry, PATHINFO_EXTENSION) == $ext) && is_writable($entry))
        $contents[] = $entry;
    }
    // One Level Only
    // else if ( is_dir($entry) )
    // {
    //   $contents = array_merge($contents, getDIRContents($entry, $ext));
    // }
  }
  closedir($handle);
  return $contents;
}

// Send Job Notification Email
function sendEncodeMail($body, $success)
{
global $PARA;
   $curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
   $to = $PARA['support_email'];
   if ($success)
	$subject = "NOW.COM Normal Encode Job Done Notification - " . $curTime . "\r\n";
   else
	$subject = "NOW.COM Normal Encode Job Fail Notification - " . $curTime . "\r\n";
   $header = "From:\"Dao-lab Volo Support\" <support@dao-lab.com>\r\n";
   $retval = mail ($to,$subject,$body,$header);
   if( $retval == true )  
   {
      echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO Encode Job Notification Mail Sent\n"; 
   }
   else
   {
      echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO Encode Job Notification Mail Not Send\n"; 
   }
}

// 
// START HERE

// Connect Scheduler DB
$config = include('/opt/Volo.VISA/scheduler/config/database.php');
$connstr = "host=" . $config['db_server'] .
                             " user=". $config['db_user'] .
                             " password=". $config['db_pass'] .
                             " dbname=".$config['db_name'];
$link = pg_connect($connstr)
	or die('Could not connect: ' . pg_last_error());

// Current Date/Time
if(function_exists("date_default_timezone_set") and function_exists("date_default_timezone_get"))
   @date_default_timezone_set(@date_default_timezone_get());

$dirList = getDIRContents($JOBPATH, "done");
foreach($dirList as $job) {
  foreach(file($job) as $line) {
	// Housekeep Job File First
	$curTime = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y")));
	$list = explode("|", $line);
	if (!isset($list[0]) || $list[0] < 1){
        	echo "[ERROR][" . $curTime . "][" . gethostname() . "] VOLO.JobPROC Unknown Encode Job File :: FILE[" . $job . "]" . "\n";
		continue;
	// } else {
        // 	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.JobPROC Processing Encode Job File :: FILE[" . $job . "]" . "\n";
	}
	$result = pg_exec($link, "SELECT * FROM output WHERE job_id=" . $list[0] . " ORDER BY id;");
	$ttljob = pg_numrows($result);
	$runjob = 0;
	$compjob = 0;
	$failjob = 0;
	$canxjob = 0;
	$joblist = $faillist = array();
	for($rj = 0; $rj < $ttljob; $rj++) {
		$jrow = pg_fetch_array($result, $rj);
		if ($jrow["stage"] < 0){
			$failjob++;
			array_push($faillist, $jrow["job_id"] . "|" . $jrow["id"] . "|FAIL|" . $jrow["stime"] . "|" . $jrow["ltime"] . "|" . $jrow["message"]);
		} else if ($jrow["stage"] < 2) {
			$runjob++;
		} else if ($jrow["stage"] < 3) {
			$compjob++;
			array_push($joblist, $jrow["filename"]);
		} else {
			$canxjob++;
			array_push($faillist, $jrow["job_id"] . "|" . $jrow["id"] . "|CANCEL|" . $jrow["stime"] . "|" . $jrow["ltime"] . "|" . $jrow["message"]);
		}
	}
        // echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.JobPROC Encode Job Status :: JOBID[" . $list[0] . "] TOTAL[" . $ttljob . "] FAIL[" . $failjob . "] CANCEL[" . $canxjob . "] RUN[" . $runjob . "] DONE[" . $compjob . "]\n";
	// Job File --
        // 1977|/media/adhoc/oscar/source|op0101.txt|op0101.mxf|/media/adhoc/oscar/output/|PROMO|op0101|3565|{PUBLISH JOB DETAIL [/media/adhoc/oscar/source  /media/adhoc/oscar/source/op0101.mxf /media/adhoc/oscar/output/ OSCAR PROMO op0101 3565]}
	if ($compjob == $ttljob){
        	echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.JobPROC Encode Job Done :: JOBID[" . $list[0] . "] OUTPUT TOTAL[" . $ttljob . "]" . "\n";
		$procjob = true;
		// Reformat list[4]
		$genpath = explode("/", $list[4]);
		$srcpath = "/" . $genpath[1] . "/" . $genpath[2] . "/UD/" . $genpath[3];
		$hlsout  = "/" . $genpath[1] . "/" . $genpath[2] . "/regenhls/" . $genpath[3];
		// Housekeep M3U8
		if ($list[5] == "Y"){
			$cmd = "rm -rf " . $hlsout . "/*.m3u8";
			// exec("$cmd > /dev/null 2>&1");
		}
		foreach($joblist as $outfile) {
		   // NOWC HLS Post Processing
		   if ($list[5] == "Y"){
		      // EHLS Flow
                      echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.JobPROC Encode Job Done, Generate EHLS\n";
          	      if (file_exists($srcpath . "/" . $outfile)){
				$cmd = $EHLSCMD . " " . $srcpath . "/" . $outfile . " " . $hlsout;
				file_put_contents('/opt/Volo.VISA/jobregen/hlsregen.txt', $cmd . "\n", FILE_APPEND);
				// exec("$cmd > /dev/null 2>&1");
		      } else {
				echo "[WARNS][" . $curTime . "][" . gethostname() . "] VOLO.JobPROC Encode Output File Not Ready :: FILE[" . $srcpath . "/" . $outfile . "]" . "\n";
		      }
                      echo "[INFOS][" . $curTime . "][" . gethostname() . "] VOLO.JobPROC Generate EHLS Done\n";
		   }
		   // End Post Processing
		}
		if ($procjob){
		   // Housekeeping
		   popen("mv -f " . $job . " " . $JOBPATH . "/" . $list[0] . ".job.done.regen", "r");
		   $mbody  = "VOLO Encode Job Done - JOBID [" . $list[0] . "]\n";
		   $mbody .= "Video Source - " . $list[1]."/".$list[3] . "\n";
		   $mbody .= "Destination  - " . $list[4] . "\n";
		   // sendEncodeMail($mbody, true);
          	}
	} else if ($runjob == 0){
		popen("mv -f " . $job . " " . $JOBPATH . "/" . $list[0] . ".job.done.error", "r");
		$mbody  = "VOLO Encode Job Failed - JOBID [" . $list[0] . "]\n";
		$mbody .= "Video Source - " . $list[1]."/".$list[3] . "\n";
		$mbody .= "Destination  - " . $list[4] . "\n";
		// sendEncodeMail($mbody, false);

		// Log Error Job
		if (!empty($faillist)){
			foreach($faillist as $outjob) {
				$outList = explode("|", $outjob);
				echo "[ERROR][" . $curTime . "][" . gethostname() . "] VOLO.JobPROC Encode Job Failed :: JOBID[" . $outList[0] . "] OUTPUT ID[" . $outList[1] . "] STATUS[" . $outList[2] . "] MESSAGE[" . $outList[5] . "]\n";
			}
		}

		// Move to Error Folder for Failed Job
		/* Disabled
		if ( $failjob > 0 && file_exists($list[1]) ){
			$errPath = $list[1] . "-" . date("YmdHis", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"))) . "." . $PARA["encode_err_pname"];
			echo "[ERROR][" . $curTime . "][" . gethostname() . "] VOLO.JobPROC Backup Failed Sources :: ACTION[mv -f " . $list[1] . " " . $errPath . "]\n";
                	popen("mv -f " . $list[1] . " " . $errPath, "r");
			file_put_contents($JOBPATH . "/" . $list[0] . "." . $PARA["encode_err_pname"], $errPath . " " . $list[1]);
		}
		*/
	}
  } // End Line
} // End Job
pg_close($link);

// Force to SLEEP 5sec (ttl 10sec in Control Script) to decrease poll rate
// sleep(5);
exit(0);
?>
